%Test Channel Shift
% Itest = Iraw{1}(801:1000,1901:2250,:);
%Extra Channel Shifting
for i = 1:15
    for j = 1:15%:length(Iraw)
        Itest = Iraw{1}(801:1000,1901:2250,:);
        red  = Itest(1:2:end,1:2:end);
        g1   = Itest(1:2:end,2:2:end);
        g2   = Itest(2:2:end,1:2:end);
        blue = Itest(2:2:end,2:2:end);
        
        %positive value moves right/down
        redx = 3;
        redy = 0;
        redpad = padarray(red,[10 10],0,'both');
        red = redpad(10+redy+[1:size(red,1)],10-redx+[1:size(red,2)]);
        
        g1x = -1;
        g1y = 0;
        g1pad = padarray(g1,[10 10],0,'both');
        g1 = g1pad(10+g1y+[1:size(g1,1)],10-g1x+[1:size(g1,2)]);
        
        g2x = -2;
        g2y = 0;
        g2pad = padarray(g2,[10 10],0,'both');
        g2 = g2pad(10+g2y+[1:size(g2,1)],10-g2x+[1:size(g2,2)]);
        
        bluex = -1;
        bluey = 0;
        bluepad = padarray(blue,[10 10],0,'both');
        blue = bluepad(10+bluey+[1:size(blue,1)],10-bluex+[1:size(blue,2)]);
        
        Itest(1:2:end,1:2:end) = red;
        Itest(1:2:end,2:2:end) = g1;
        Itest(2:2:end,1:2:end) = g2;
        Itest(2:2:end,2:2:end) = blue;
        
        I = Itest;
        
%         Itwotmp = zeros(1080,1920,3);
        Itwotmp(:,:,1) = [I(1:2:end,3:2:end) I(1:2:end,end)];
        Itwotmp(:,:,2) = (I(1:2:end,2:2:end) + [I(2,1:2:end); I(2:2:(end-2),1:2:end)])/2;
        
        %Blue Channel - No Shift
        Itwotmp(:,:,3) = I(2:2:end,2:2:end);
        
        %Upsample
        Itwotmp2 = G2Scaler(Itwotmp);
        
        %Green Channel Shift - Down 1 Right 1?
        Green = [Itwotmp2(1,:,2); Itwotmp2(1:end-1,1,2) Itwotmp2(1:end-1,1:end-1,2)];
        
        Itwo{1} = cat(3,Itwotmp2(:,:,1),Green,Itwotmp2(:,:,3));
%         figure(2);subplot(15,15,j+(i-1)*15), imagesc(cat(3,Itwo{1}(:,:,1)/mean(mean(Itwo{1}(:,:,1))),Itwo{1}(:,:,2)/mean(mean(Itwo{1}(:,:,2))),Itwo{1}(:,:,3)/mean(mean(Itwo{1}(:,:,3))))/2)
%         title(sprintf('%d,%d',redx,redy))
        ItwoHSV = rgb2hsv(cat(3,Itwo{1}(:,:,1)/mean(mean(Itwo{1}(:,:,1))),Itwo{1}(:,:,2)/mean(mean(Itwo{1}(:,:,2))),Itwo{1}(:,:,3)/mean(mean(Itwo{1}(:,:,3))))/2);
        Color(i,j) = sum(sum(ItwoHSV(10:end-10,10:end-10,2)));
    end
end

figure;imagesc(Color)
%Create New rgb images
% 1. 1MOS Demosaic (5x5 kernals)
% 2. Current Process
% 3. Dan Wiley's "Matrixed Green Data Encoding"
% 4. ?



    I = Itest;
    
    %%
    % 1. 1MOS Demosaic (5x5 kernals)
%     
%     Iout = DeMosaic(I);
%     
%     Ione{i} = Iout;
    
%      Itwotmp = zeros(1080,1920,3);
% %     Itwotmp = zeros(1080,960,3);
%     %Red Channel - Shift Left 1
%     Itwotmp(:,:,1) = [I(1:2:end,3:2:end) I(1:2:end,end)];
%     
%     %Green Channel - Avg of G1 and G2delay
%     % G1
%     % Iout(:,:,2) = (I(2:2:end,1:2:end) + [I(1,2:2:end); I(1:2:(end-2),2:2:end)])/2;
%     Itwotmp(:,:,2) = (I(1:2:end,2:2:end) + [I(2,1:2:end); I(2:2:(end-2),1:2:end)])/2;
%     
%     %Blue Channel - No Shift
%     Itwotmp(:,:,3) = I(2:2:end,2:2:end);
%     
%     %Upsample
%     Itwotmp2 = G2Scaler(Itwotmp);
%       
%     %Green Channel Shift - Down 1 Right 1?
%     Green = [Itwotmp2(1,:,2); Itwotmp2(1:end-1,1,2) Itwotmp2(1:end-1,1:end-1,2)];
%     
%     Itwo{i} = cat(3,Itwotmp2(:,:,1),Green,Itwotmp2(:,:,3));
%     
