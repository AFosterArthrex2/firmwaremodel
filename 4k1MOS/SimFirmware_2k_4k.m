function [out, Isave] = SimFirmware_2k_4k(Iin,settings)

%% SimulateFirmware
% Function for Gen2 CCU Firmware Simulation.
% Standard 2k Input image (No Crops)
%
% Inputs:
%   1. Settings
%   2. Image (2160*3840).
%
% Outputs:
%   1. out (I1080, IDP4ks, ...)
%   2. Isave
%%

I = Iin;

%% 1080p Processing
% Pre-upscale/Offset Processing is performed at 1080p
% Isave keeps a backup of each step
Isave{1,1} = I;%fprintf('|%02d|.',testCase);

I = G2MedianFilter_3MOS(I,settings);
Isave{1,2} = I;

I = G2BlackOffset(I,settings);
Isave{1,3} = I;

I = G2AEC(I,settings);
Isave{1,4} = I;

I = G2PixelReplace(I,settings);
Isave{1,5} = I;

I = G2Demosaic(I,settings);
Isave{1,6} = I;

I = G2MedianFilter_1MOS(I,settings);
Isave{1,7} = I;

I = G2WhiteBalance(I,settings);
Isave{2,1} = I;

I = G2GreenFlare(I,settings);
Isave{2,2} = I;

I = G2Bilateral(I,settings);
Isave{2,3} = I;

I = G2BilateralSlope(I,settings);
Isave{2,4} = I;

I = G2FiberScopeFilter(I,settings);
Isave{2,5} = I;

%% 4k Processing
% Final Processing is performed at full 4k resolution
% Image is broken up into 4 vertical "stripes" 


IDP4k{1} = I((1:1080),(1:496)+0,:);
IDP4k{2} = I((1:1080),(1:496)+480,:);
IDP4k{3} = I((1:1080),(1:496)+960,:);
IDP4k{4} = [I((1:1080),(1:480)+1440,:) zeros(1080,16,3)];

Isave{4,1} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2Scaler(IDP4k{i},settings); end
Isave{4,2} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2GreenOffset(IDP4k{i},settings); end
Isave{4,3} = IDP4k;

for i = 1:4 IDP4k{i} = G2VerticalBinning(IDP4k{i},settings); end
Isave{4,4} = IDP4k;

for i = 1:4 IDP4k{i} = G2GreenEdgeEnhance(IDP4k{i},settings); end
Isave{4,5} = IDP4k;

for i = 1:4 IDP4k{i} = G2CSC(IDP4k{i},settings); end
Isave{4,6} = IDP4k;

for i = 1:4 IDP4k{i} = G2RGB2YCbCr(IDP4k{i},settings); end
Isave{4,7} = IDP4k;

for i = 1:4 IDP4k{i} = G2SharpAtt(IDP4k{i},settings); end
Isave{4,8} = IDP4k;

for i = 1:4 IDP4k{i} = G2YCbCr2RGB(IDP4k{i},settings); end
Isave{4,9} = IDP4k;

for i = 1:4 IDP4k{i} = G2Gamma(IDP4k{i},settings); end
Isave{4,10} = IDP4k;

%% 4k Output
% Assemble 4 stripes into full 4k output

%Crop from 992x2184 to 960x2160
DPoffsetx = 1;
DPoffsety = 1;

IDP4ks = cat(2,IDP4k{1}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
               IDP4k{2}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
               IDP4k{3}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
               IDP4k{4}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:));
     
IDP4ks = uint8(IDP4ks);

%% Downsampled 1080p Output
% Downsample without lowpass filtering to get default 1080p output

Isave{5,1} = Isave{4,10};
% fprintf(['|%02d|                       |>'],testCase);

%LowPass Filter
% for i = 1:4 I1080D{i} = G2LowPass(Isave{3,1}{i},settings.Downscale_Filter_Disable); end
I1080D = Isave{4,10};
Isave{5,2} = I1080D;

%Crop and Assemble
DSoffsetx = 1;
DSoffsety = 1;

I1080Ds = cat(2,I1080D{1}([13:end-12]+DSoffsety,[17:end-16]+DSoffsetx,:),...
                I1080D{2}([13:end-12]+DSoffsety,[17:end-16]+DSoffsetx,:),...
                I1080D{3}([13:end-12]+DSoffsety,[17:end-16]+DSoffsetx,:),...
                I1080D{4}([13:end-12]+DSoffsety,[17:end-16]+DSoffsetx,:));

Isave{5,3} = I1080Ds;

I1080Ds = G2DownScale(I1080Ds);
I1080Ds = uint8(I1080Ds);
Isave{5,4} = I1080Ds;

%Output

out.IDP4ks = IDP4ks;
out.I1080Ds = I1080Ds;

end


