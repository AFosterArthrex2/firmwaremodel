function [Ig1, Ig2, Ig3] = Capture4k1MOSSensor(vid,Port,S1)
%Captures the full sensor output of the 4k1MOS camera

%Enable Green Channel 1
SerialBitWriteCCU(Port,2,41,'01',[31:-1:30],S1);

    %Enable 12-bit mode left side
    SerialWriteCCU(Port,1,4*256+46,'00000001',S1);pause(0.25)

    Ic = getsnapshot(vid);
    Ileft = convert8to12(Ic);

    %Enable 12-bit mode right side
    SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);pause(0.25)

    Ic = getsnapshot(vid);
    Iright = convert8to12(Ic);

    Ig1 = [Ileft Iright];

%Enable Green Channel 2
SerialBitWriteCCU(Port,2,41,'10',[31:-1:30],S1);

    %Enable 12-bit mode left side
    SerialWriteCCU(Port,1,4*256+46,'00000001',S1);pause(0.25)

    Ic = getsnapshot(vid);
    Ileft = convert8to12(Ic);

    %Enable 12-bit mode right side
    SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);pause(0.25)

    Ic = getsnapshot(vid);
    Iright = convert8to12(Ic);

    Ig2 = [Ileft Iright];

%Enable Green Channel Average
SerialBitWriteCCU(Port,2,41,'00',[31:-1:30],S1);

    %Enable 12-bit mode left side
    SerialWriteCCU(Port,1,4*256+46,'00000001',S1);pause(0.25)

    Ic = getsnapshot(vid);
    Ileft = convert8to12(Ic);

    %Enable 12-bit mode right side
    SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);pause(0.25)

    Ic = getsnapshot(vid);
    Iright = convert8to12(Ic);

    Ig3 = [Ileft Iright];

%Disable 12-bit mode
SerialWriteCCU(Port,1,4*256+46,'02940000',S1);

end