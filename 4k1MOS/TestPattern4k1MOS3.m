%Turn on Sensor TP

SetupPath
Port = 'COM5';

if(~exist('S1','var') || ~isvalid(S1))
    S1 = serial(Port,'BaudRate',9600);
end

if(strcmp(S1.status,'closed'))
    fopen(S1);
else
    fclose(S1);
    pause(0.2)
    fopen(S1);
end

[BlackLevels, AECGain] = Setup4k1MOSSensor(Port,S1,'turnoff');

%     channelEnable1 = [0 0 0 0 0 0 0 0 0 0];
%     channelEnable1 = [1 0 1 0 1 0 1 0 1 0];
channelEnable1 = double(dec2bin(randi(2^10)-1,10))-48;
%     channelEnable2 = [0 0 0 0 0 0 0 0 0 0];
%     channelEnable2 = [0 1 0 1 0 1 0 1 0 1];
channelEnable2 = double(dec2bin(randi(2^10)-1,10))-48;

%Channel Enables
SerialBitWriteCCU(Port,2,41,fliplr(char(channelEnable1+48)),[9:-1:0],S1);
pause(1)
SerialBitWriteCCU(Port,2,41,fliplr(char(channelEnable2+48)),[25:-1:16],S1);

FWVersion = (SerialReadCCU(Port,2,0,S1));
buildNumber = hex2dec(SerialReadCCU(Port,2,3,S1));

currentTime = now;
datecode = datestr(now,'yymmddHHMMSS');
filename = [pwd '\4k1MOS\Results\' datecode '.txt'];
fid = fopen(filename,'w'); 

fprintf(fid,['Test Performed: ' datestr(currentTime) '\n']);
fprintf(fid,'Firmware Under Test:\n');
fprintf(fid,'850-00%02d-%02d Rev%s S%d\n',hex2dec(FWVersion(1:2)),hex2dec(FWVersion(3:4)),char(65+hex2dec(FWVersion(5:6))),buildNumber);
fprintf(fid,'\n');
fprintf(fid,'Channel Mask #1: 0x%d%d%d%d%d%d%d%d%d%d%d%d',channelEnable1);
fprintf(fid,'\n');
fprintf(fid,'Channel Mask #2: 0x%d%d%d%d%d%d%d%d%d%d%d%d',channelEnable2);
fprintf(fid,'\n');
fprintf(fid,'\n');
fprintf(fid,'Errors:\n');
fprintf(fid,'---------------------------\n');
fprintf(fid,'  Test Pattern| R | G | B |\n');
fprintf(fid,'--------------|---|---|---|\n');
TestPatterns = {' 1. All 000h  ',...
                ' 2. All FFFh  ',...
                ' 3. All 555h  ',...
                ' 4. All AAAh  ',...
                ' 5. 555h/AAAh ',...
                ' 6. AAAh/555h ',...
                ' 7. 000h/555h ',...
                ' 8. 555h/000h ',...
                ' 9. 000h/FFFh ',...
                '10. FFFh/000h ',...
                '11. Color Horz',...
                '12. Color Vert'};

%Enable Test Pattern
SerialWriteCCU(Port,2,6,'803C0000',S1);
for i = 1:12
    %Pick Test Pattern
    SerialWriteCCU(Port,2,6,['803D00' dec2hex(i-1,2)],S1);
    
    pause(2)
    [Ig1, Ig2, Ig3] = Capture4k1MOSSensor(vid,Port,S1);
    
    Ig6 = Model4k1MOSSensor(i,0,channelEnable1,channelEnable2);
    
    %Mask first and last columns and rows
    diff53 = double(Ig6)-double(Ig3);
    diff53([1 960],:,1:3) = 0;
    diff53(:,[1 960],1:3) = 0;
    
    %diff less then 10 ignore for now
    diff53(diff53<10) = 0;
    
    fprintf(fid,'%s| %d | %d | %d |\n',TestPatterns{i},round(squeeze(max(max(abs(diff53)))))');
    figure(1);LinkImage(double(Ig3)/2^10,Ig6/2^10,abs(double(Ig3)/2^10-Ig6/2^10))
    drawnow
end
fprintf(fid,'---------------------------\n');
fclose(fid);

%Read Back Saved Report
fid = fopen(filename,'r');

reporttext = char(fread(fid))';

fclose(fid);

fprintf(reporttext)