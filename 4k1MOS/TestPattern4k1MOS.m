%Turn on Sensor TP

SetupPath
Port = 'COM5';

if(~exist('S1','var') || ~isvalid(S1))
    S1 = serial(Port,'BaudRate',9600);
end

if(strcmp(S1.status,'closed'))
    fopen(S1);
else
    fclose(S1);
    pause(0.2)
    fopen(S1);
end

Setup4k1MOSSensor(Port,S1)
        
for i = 1
    %Pick Test Pattern
    SerialWriteCCU(Port,2,6,['803D00' dec2hex(i-1,2)],S1);
    
    channelEnable1 = [0 0 0 0 0 0 0 0 0 0];
    channelEnable2 = [1 0 0 1 0 0 0 0 0 1];
    
    %Channel Enables
    SerialBitWriteCCU(Port,2,41,fliplr(char(channelEnable1+48)),[9:-1:0],S1);
    SerialBitWriteCCU(Port,2,41,fliplr(char(channelEnable2+48)),[25:-1:16],S1);
    
    pause(1)
    [Ig1, Ig2, Ig3] = Capture4k1MOSSensor(vid,Port,S1);
%     fprintf('%d: %d %d %d - %d %d %d.\n',i,squeeze(max(max(abs(double(Ig1)-double(Ig2)))))',squeeze(max(max(abs(double(Ig1)-double(Ig3)))))')
    rgb1(i,:) = mat2cell(dec2hex(round(squeeze(mean(mean(Ig1)))),3),[1 1 1],3)';
    rgb2(i,:) = mat2cell(dec2hex(round(squeeze(mean(mean(Ig2)))),3),[1 1 1],3)';
    rgb3(i,:) = mat2cell(dec2hex(round(squeeze(mean(mean(Ig3)))),3),[1 1 1],3)';
    
    Ig4 = zeros(2160,1920,1);
    
    %Red
%     Ig4(1:2:end,1:2:end) = Ig1(:,:,1);
    Ig4(1:2:end,1:2:end) = [Ig1(:,2:end,1) Ig1(:,end,1)];
    
    %G1 - Shift G1 left 1
    Ig4(1:2:end,2:2:end) = Ig1(:,:,2);
%     Ig4(1:2:end,2:2:end) = [Ig1(:,2:end,2) Ig1(:,end,2)];
    
    %G2 - Shift G2 left 1 (no shift up, already done by selecting g2)
    Ig4(2:2:end,1:2:end) = Ig2(:,:,2);
%     Ig4(2:2:end,1:2:end) = [Ig2(:,2:end,2) Ig2(:,end,2)];
    
    %Blue - Shift blue left 1
    Ig4(2:2:end,2:2:end) = Ig1(:,:,3);
%     Ig4(2:2:end,2:2:end) = [Ig1(:,2:end,3) Ig1(:,end,3)];
    
    
    %Create Ig5 from Ig4, should match Ig3
    red = Ig4(1:2:end,1:2:end);
    
    %Shift g1 right one pixel
    g1 = Ig4(1:2:end,2:2:end);
    g1shift = [g1(:,1) g1(:,1:end-1)];
    
    %Shift g2 right one pixel and down one pixel
    g2 = Ig4(2:2:end,1:2:end);
%     g2shift = [g2(1,1) g2(1,1:end-1); g2(1:end-1,1) g2(1:end-1,1:end-1)];
%     g2shift = [g2(:,1) g2(:,1:end-1)];
    g2shift = [g2(1,:); g2(1:end-1,:)];
    
    %Shift blue right one pixel
    blue = Ig4(2:2:end,2:2:end);
    blueshift = [blue(:,1) blue(:,1:end-1)];
    
    Ig5 = cat(3,red,(g1+g2shift)/2,blue);
    
    Ig6 = Model4k1MOSSensor(i,0,channelEnable1,channelEnable2);
    
    %Mask first and last columns and rows
    diff53 = double(Ig6)-double(Ig3);
    diff53([1 960],:,1:3) = 0;
    diff53(:,[1 960],1:3) = 0;
    
    %diff less then 10 ignore for now
    diff53(diff53<10) = 0;
    
    fprintf('%d: %d %d %d.\n',i,round(squeeze(max(max(abs(diff53)))))')
end








function Setup4k1MOSSensor(Port,S1)

SerialWriteCCU(Port,1,9,'00000001',S1)
%Pick Test Pattern
SerialWriteCCU(Port,2,6,['803D00' dec2hex(10,2)],S1);
%Enable Test Pattern
SerialWriteCCU(Port,2,6,'803C0012',S1);

%ColorWidth
SerialWriteCCU(Port,2,6,'870E0000',S1);

SerialWriteCCU(Port,2,10,'00000000',S1);

%Sensor Black Level
SerialWriteCCU(Port,2,6,'80450000',S1);

%CCU Black Level
SerialWriteCCU(Port,1,40,'40000000',S1)

%Set Crops
SerialWriteCCU(Port,1,45+256*4,'41080806',S1);
%Manual Index
SerialBitWriteCCU(Port,1,4*256+10,'00',[4 3],S1);
%Set Index
SerialWriteCCU(Port,1,11+256*4,'0400035C',S1);
%View Buffer
SerialBitWriteCCU(Port,1,4*256+8,'1',[15],S1);
%Remeber Register 9
reg9 = SerialReadCCU(Port,1,9,S1);
%Turn off All Processing
SerialWriteCCU(Port,1,9,'FFFFFFFF',S1);
%1080 from frame buffer
SerialBitWriteCCU(Port,1,4*256+8,'1100',[11:-1:8],S1);
%4k Zoom Off
SerialWriteCCU(Port,1,1*256+88,'00000000',S1);

pause(0.15)
%Disable Processing (Gamma and Post Peaking/Sharpening)
SerialBitWriteCCU(Port,1,9,'11',[25:-1:24],S1);

%Enable 12-bit mode left side
SerialWriteCCU(Port,1,4*256+46,'00000001',S1);

end

function [Ig1, Ig2, Ig3] = Capture4k1MOSSensor(vid,Port,S1)
%Enable Green Channel 1
SerialBitWriteCCU(Port,2,41,'01',[31:-1:30],S1);

Ic = getsnapshot(vid);
Ig1 = convert8to12(Ic);

%Enable Green Channel 2
SerialBitWriteCCU(Port,2,41,'10',[31:-1:30],S1);

pause(0.25)
Ic = getsnapshot(vid);
Ig2 = convert8to12(Ic);


%Enable Green Channel Average
SerialBitWriteCCU(Port,2,41,'00',[31:-1:30],S1);

pause(0.25)
Ic = getsnapshot(vid);
Ig3 = convert8to12(Ic);
end

function [Iout] = Model4k1MOSSensor(TestPattern,GreenMux,ChannelEnable1,ChannelEnable2)
%Models the 4k1MOS sensor test pattern and fpga color control

switch TestPattern
    case 1
        %Black
        I = zeros(2160,1920);
    case 2
        %White
        I = ones(2160,1920)*hex2dec('FFF');
    case 3
        %Dark Grey
        I = ones(2160,1920)*hex2dec('555');
    case 4
        %Light Grey
        I = ones(2160,1920)*hex2dec('AAA');
    case 5
        %Stripes Dark/Light Grey
        I = ones(2160,1920)*hex2dec('555');
        I(:,2:2:end) = ones(2160,960)*hex2dec('AAA');
    case 6
        %Stripes Light/Dark Grey
        I = ones(2160,1920)*hex2dec('AAA');
        I(:,2:2:end) = ones(2160,960)*hex2dec('555');
    case 7
        %Stripes Black, Dark Grey
        I = ones(2160,1920)*hex2dec('000');
        I(:,2:2:end) = ones(2160,960)*hex2dec('555');
    case 8
        %Stripes Dark Grey, Black
        I = ones(2160,1920)*hex2dec('555');
        I(:,2:2:end) = ones(2160,960)*hex2dec('000');
    case 9
        %Stripes Black, White
        I = ones(2160,1920)*hex2dec('000');
        I(:,2:2:end) = ones(2160,960)*hex2dec('FFF');
    case 10
        %Stripes White, Black
        I = ones(2160,1920)*hex2dec('FFF');
        I(:,2:2:end) = ones(2160,960)*hex2dec('000');
    case 11
        %Color Bar (horiz)
        I = ones(2160,1920)*hex2dec('000');
        I(:,2:2:end) = ones(2160,960)*hex2dec('FFF');
    case 12
        %Color Bar (vert)
        I = ones(2160,1920)*hex2dec('FFF');
        I(:,2:2:end) = ones(2160,960)*hex2dec('000');
    otherwise
        I = zeros(2160,1920);
end

%Channel Mask
% 1 - Red and G1
Mask1 = zeros(2160,1920);
Mask2 = zeros(2160,1920);

Mask1(1:2:end,:) = repmat(ChannelEnable1,1080,192);
Mask2(2:2:end,:) = repmat(ChannelEnable2,1080,192);

I(Mask1 | Mask2) = hex2dec('3FF');

Iout = zeros(1080,960,3);
%Red Channel - Shift Left 1
Iout(:,:,1) = [I(1:2:end,3:2:end) I(1:2:end,end)];

%Green Channel - Avg of G1 and G2delay
                      % G1            
Iout(:,:,2) = (I(1:2:end,2:2:end) + [I(2,1:2:end); I(2:2:end-2,1:2:end)])/2;

%Blue Channel - No Shift
Iout(:,:,3) = I(2:2:end,2:2:end);
end