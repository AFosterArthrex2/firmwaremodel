%Simulate 4k1MOS to compare resolution of different encoding schemes.

%Read in images
directory{1} = 'G:\Matlab Images\4k1MOS\slantedgeimages\';
directory{2} = 'G:\Matlab Images\4k1MOS\reschartimages\';

for q = 1:length(directory)
    list{q} = dir([directory{q} '*.png']);
    for j = 1%:length(list{q})
        imageinfo(q,j) = regexp(list{q}(j).name,'(?<date>\d{12})-sensorimage.png','names');
    end
end

count = 1;
%Create "Raw" Images
for q = 1:length(directory)
    for j = 1%:length(list{q})
        Iraw{count} = double(imread([directory{q} list{q}(j).name]));
        count = count + 1;
    end
end
%Create New rgb images
% 1. 1MOS Demosaic (5x5 kernals)
% 2. Current Process
% 3. Dan Wiley's "Matrixed Green Data Encoding"
% 4. ?

for i = 1:length(Iraw)
    
    %%
    % 1. Current Process
    % Average green channels, offset green channel
    I = Iraw{i};
    %Shift from sensor image to create Captured "FPGA Image" - Shift Red Left 1, Shift G2 up 1
    I = BayerShiftChannel(I,[-1 0],[0 0],[0 1],[0 0]);
    
    Ionetmp = zeros(1080,1920,3);
 
    %Red Channel
    Ionetmp(:,:,1) = I(1:2:end,1:2:end);
        
    %Green Channel - Avg of G1 and G2delay
    Ionetmp(:,:,2) = floor((I(1:2:end,2:2:end) + I(2:2:end,1:2:end))/2);
    
    %Blue Channel - No Shift
    Ionetmp(:,:,3) = I(2:2:end,2:2:end);
    
    Ione{i} = cat(3,Ionetmp(:,:,1),Ionetmp(:,:,2),Ionetmp(:,:,3));  
    %Upsample
%     Ionetmp2 = G2Scaler(Ionetmp);
% 
%     Green = [Ionetmp2(2:end,2:end,2) Ionetmp2(2:end,end,2); Ionetmp2(end,2:end,2) Ionetmp2(end,end,2)];
%     
%     Ione{i} = cat(3,Ionetmp2(:,:,1),Green,Ionetmp2(:,:,3));   
%     
    
    % 2. Alternate Pixel Group
    % Average green channels, offset green channel
    % Keep Red and Blue Together, shift g1 left 1 g2 up 1.
    %
    I = Iraw{i};
    %Shift from sensor image to create Captured "FPGA Image" - Shift G1 Left 1, Shift G2 up 1
    I = BayerShiftChannel(I,[0 0],[1 0],[0 1],[0 0]);
    
    Itwotmp = zeros(1080,1920,3);
 
    %Red Channel
    Itwotmp(:,:,1) = I(1:2:end,1:2:end);
        
    %Green Channel - Avg of G1 and G2delay
    Itwotmp(:,:,2) = floor((I(1:2:end,2:2:end) + I(2:2:end,1:2:end))/2);
    
    %Blue Channel - No Shift
    Itwotmp(:,:,3) = I(2:2:end,2:2:end);
    
    Itwo{i} = cat(3,Itwotmp(:,:,1),Itwotmp(:,:,2),Itwotmp(:,:,3));
    %Upsample
%     Itwotmp2 = G2Scaler(Itwotmp);
% 
%     %Green Channel Shift - Down 1 Right 1
%     Green = [Itwotmp2(2:end,2:end,2) Itwotmp2(2:end,end,2); Itwotmp2(end,2:end,2) Itwotmp2(end,end,2)];
%  
%     Itwo{i} = cat(3,Itwotmp2(:,:,1),Green,Itwotmp2(:,:,3));
%     

    % 2. Alternate Pixel Group
    % Average green channels, offset green channel
    % Keep Red and Blue Together, shift g1 left 1 g2 up 1.
    %
    I = Iraw{i};
    %Shift from sensor image to create Captured "FPGA Image" - Shift G1 Left 0, Shift G2 up 0
    I = BayerShiftChannel(I,[0 0],[0 0],[0 0],[0 0]);
    
    Ithreetmp = zeros(1080,1920,3);
 
    %Red Channel
    Ithreetmp(:,:,1) = I(1:2:end,1:2:end);
        
    %Green Channel - Avg of G1 and G2
    Ithreetmp(:,:,2) = floor((I(1:2:end,2:2:end) + I(2:2:end,1:2:end))/2);
    
    %Blue Channel - No Shift
    Ithreetmp(:,:,3) = I(2:2:end,2:2:end);
    
    Ithree{i} = cat(3,Ithreetmp(:,:,1),Ithreetmp(:,:,2),Ithreetmp(:,:,3));
    %Upsample
%     Ithreetmp2 = G2Scaler(Ithreetmp);
% 
%     %Green Channel Shift - Down 0 Right 0
%     Green = Ithreetmp2(1:end,1:end,2);
%  
%     Ithree{i} = cat(3,Ithreetmp2(:,:,1),Green,Ithreetmp2(:,:,3));
    
    % 4. 1MOS Demosaic (5x5 kernals)
    I = Iraw{i};
    
    Iout = DeMosaic4k1MOS(I);
    
    Ifour{i} = Iout;
    
    % 5. Recover Gdiff at Green Offset
    % Dan Wiley's "Matrixed Green Data Encoding"
    % Data is 10 bits per channel
    %
    I = Iraw{i};
    Red =  I(1:2:end,1:2:end);
    
    G1 =   I(1:2:end,2:2:end);
    
    G2 =   I(2:2:end,1:2:end);
    
    Blue = I(2:2:end,2:2:end);
    
    Gavg = floor((G1+G2)/2);
    
    Gdiff = floor((G1-G2)/32);
    
    Gdiffsign = Gdiff >= 0;
    Gdiffabs = abs(Gdiff);
    
    %Split into two bit chunks
    Gdiffr = 2*Gdiffsign+floor(Gdiffabs/16);
    Gdiffg = floor(mod(Gdiffabs,16)/4);
    Gdiffb = floor(mod(Gdiffabs,4));
    
    Gdiffrec2 = (floor(Gdiffr/2)*2-1).*( 16*mod(Gdiffr,2)+4*Gdiffg+Gdiffb);
    
    %Add each 2 bit chunk to rgb channels.
    Redm = 4*Red + Gdiffr;
    Greenm = 4*Gavg + Gdiffg;
    Bluem = 4*Blue + Gdiffb;
    
    %Head Cable
    
    %Reconstruct G1 and G2
    
    Gdiffr = mod(Redm,4);
    Gdiffg = mod(Greenm,4);
    Gdiffb = mod(Bluem,4);
    
    Redrec = floor(Redm/4);
    Greenrec = floor(Greenm/4);
    Bluerec = floor(Bluem/4);
    
    Gdiffrec = (floor(Gdiffr/2)*2-1).*( 16*mod(Gdiffr,2)+4*Gdiffg+Gdiffb)*16;
    
    %no green offset
    Ifive{i} = cat(3,Redrec,Greenrec,Bluerec);
    
    IfiveGdiff{i} = Gdiffrec./Greenrec;
    
    Gaindiff = 1;
end

%%

% figure; imagesc(cat(3,Itwo{1}(800:1000,1900:2250,1)/mean(mean(Itwo{1}(:,:,1))),Itwo{1}(800:1000,1900:2250,2)/mean(mean(Itwo{1}(:,:,2))),Itwo{1}(800:1000,1900:2250,3)/mean(mean(Itwo{1}(:,:,3))))/2)
% figure; imagesc(cat(3,Ione{1}(800:1000,1900:2250,1)/mean(mean(Ione{1}(:,:,1))),Ione{1}(800:1000,1900:2250,2)/mean(mean(Ione{1}(:,:,2))),Ione{1}(800:1000,1900:2250,3)/mean(mean(Ione{1}(:,:,3))))/2)

inputSpreadSheet = [pwd '\Settings\' '4k1MosSettings.xlsx'];
testCase = 107;
settings = readSettingsFromExcel(inputSpreadSheet,testCase);
settings.Head_Type = 3;

for i = 1:length(Iraw)
    [IfourP{i}, ~] = SimFirmware_4k(Ifour{i},settings);
end

inputSpreadSheet = [pwd '\Settings\' '4k1MosSettings.xlsx'];
testCase = 106;
settings = readSettingsFromExcel(inputSpreadSheet,testCase);
settings.Head_Type = 3;
settings.Green_Offset_Disable = 0;
for i = 1:length(Iraw)
    [IoneP{i}, ~] = SimFirmware_2k_4k(Ione{i},settings);
    [ItwoP{i}, ~] = SimFirmware_2k_4k(Itwo{i},settings);
end

settings.Green_Offset_Disable = 1;
for i = 1:length(Iraw)
    [IthreeP{i}, ~] = SimFirmware_2k_4k(Ithree{i},settings);
end

settings.Green_Offset_Disable = 1;
for i = 1:length(Iraw)
    [IfiveP{i}, ~] = SimFirmware_2k_4k_diff(Ifive{i},IfiveGdiff{i},Gaindiff,settings);
end

%Horizontal Resolution
Hlim = [520 1300;1840 1910];
figure;LinkImage(1,5,IoneP{2}.IDP4ks(Hlim(1,1):Hlim(1,2),Hlim(2,1):Hlim(2,2),:),...
                     ItwoP{2}.IDP4ks(Hlim(1,1):Hlim(1,2),Hlim(2,1):Hlim(2,2),:),...
                   IthreeP{2}.IDP4ks(Hlim(1,1):Hlim(1,2),Hlim(2,1):Hlim(2,2),:),...
                    IfourP{2}.IDP4ks(Hlim(1,1):Hlim(1,2),Hlim(2,1):Hlim(2,2),:),...
                    IfiveP{2}.IDP4ks(Hlim(1,1):Hlim(1,2),Hlim(2,1):Hlim(2,2),:),...
    {'1. Current Method','2. Alternate Method','3. No Green Shift','4. De-mosaic Filter','5. Green Difference Recovery'})

%Vertical Resolution
Vlim = [1020 1090;720 1510];
figure;LinkImage(5,1,IoneP{2}.IDP4ks(Vlim(1,1):Vlim(1,2),Vlim(2,1):Vlim(2,2),:),...
                     ItwoP{2}.IDP4ks(Vlim(1,1):Vlim(1,2),Vlim(2,1):Vlim(2,2),:),...
                   IthreeP{2}.IDP4ks(Vlim(1,1):Vlim(1,2),Vlim(2,1):Vlim(2,2),:),...
                    IfourP{2}.IDP4ks(Vlim(1,1):Vlim(1,2),Vlim(2,1):Vlim(2,2),:),...
                    IfiveP{2}.IDP4ks(Vlim(1,1):Vlim(1,2),Vlim(2,1):Vlim(2,2),:),...
    {'1. Current Method','2. Alternate Method','3. No Green Shift','4. De-mosaic Filter','5. Green Difference Recovery'})


% inputSpreadSheet = [pwd '\Settings\' '4k1MosSettings.xlsx'];
% testCase = 108;
% settings = readSettingsFromExcel(inputSpreadSheet,testCase);
% settings.Head_Type = 3;
% 
% for i = 1:length(Iraw)
%     [IsixP{i}, ~] = SimFirmware_2k_4k_diff(Ifive{i},IfiveGdiff{i},Gaindiff,settings);
% end
% 
% 
% 
% 
save('4kUltraResModel_1','IoneP','ItwoP','IthreeP','IfourP','IfiveP')
