%% G2Scaler
% Scales the input image by 2 using bilinear method
%%
%  [Iout] = G2Scaler(Iin)
%   Inputs: 
%           Iin     - Input Image
%
%   Outputs: 
%           Iout    - Output Image
%

%   Gen2 Firmware Model - Scaler
%   Author: Alexander Foster
%   Date:   10/19/15

function [Iout] = G2Scaler_diff(Iin,Gdiff,Gaindiff,varargin)
%Truncated Bilinear 2x Upscaling
%Gdiff is used to calculate 1/2 the interpolated pixels in the green
%channel
%Assumes Bayer Grid:
% R  G1
% G2 B
%

[vert, horz, ~] = size(Iin);

I = double(Iin);

Iout = zeros(2*size(I,1),2*size(I,2),size(I,3));

%Red
    %Odd Row Odd Column
    %Copy corresponding value from Iin
    Iout(1:2:2*vert,1:2:2*horz,1) = I(1:vert,1:horz,1);

    %Even Row Odd Column
    %Average 2 Neighbors (Above and Below)
    Iout(2:2:2*vert,1:2:2*horz,1) = (double(I(1:vert,1:horz,1))...          %Above
        +padarray(double(I(2:vert,1:horz,1)),[1 0],'replicate','post'))/2;  %Below

    %Odd Row Even Column
    %Average 2 Neighbors (Left and Right)
    Iout(1:2:2*vert,2:2:2*horz,1) = (double(I(1:vert,1:horz,1))+...         %Left
        padarray(double(I(1:vert,2:horz,1)),[0 1],'replicate','post'))/2;   %Right

    %Even Row Even Column
    %Average 4 Neighbors 
    Iout(2:2:2*vert,2:2:2*horz,1) = (double(I(1:vert,1:horz,1))+...         %Top Left
        padarray(double(I(2:vert,1:horz,1)),[1 0],'replicate','post')+...   %Lower Left
        padarray(double(I(1:vert,2:horz,1)),[0 1],'replicate','post')+...   %Top Right
        padarray(double(I(2:vert,2:horz,1)),[1 1],'replicate','post'))/4;   %Lower Right

%Blue
    %Odd Row Odd Column
    %Average 4 Neighbors 
    Iout(1:2:2*vert,1:2:2*horz,3) = (double(I(1:vert,1:horz,3))+...         %Lower Right
        padarray(double(I(1:vert-1,1:horz,3)),[1 0],'replicate','pre')+...  %Top Right
        padarray(double(I(1:vert,1:horz-1,3)),[0 1],'replicate','pre')+...  %Lower Left
        padarray(double(I(1:vert-1,1:horz-1,3)),[1 1],'replicate','pre'))/4;%Top Left
    
    %Even Row Odd Column
    %Average 2 Neighbors (Left and Right)
    Iout(2:2:2*vert,1:2:2*horz,3) = (double(I(1:vert,1:horz,3))+...         %Right
        padarray(double(I(1:vert,1:horz-1,3)),[0 1],'replicate','pre'))/2;  %Left
    
    %Odd Row Even Column    
    %Average 2 Neighbors (Above and Below)
    Iout(1:2:2*vert,2:2:2*horz,3) = (double(I(1:vert,1:horz,3))...          %Below
        +padarray(double(I(1:vert-1,1:horz,3)),[1 0],'replicate','pre'))/2; %Above

    %Even Row Even Column
    %Copy corresponding value from Iin
    Iout(2:2:2*vert,2:2:2*horz,3) = I(1:vert,1:horz,3);

%Green
    %First Recover G1 and G2
    G1 = I(:,:,2).*(1+Gaindiff*Gdiff);
    G2 = I(:,:,2).*(1-Gaindiff*Gdiff);
    
    %Odd Row Odd Column
    %Average 4 Neighbors (Left-G1, Right-G1, Above-G2 and Below-G2)
    Iout(1:2:2*vert,1:2:2*horz,2) = (double(G1(1:vert,1:horz))+...        %Right
                                     double(G2(1:vert,1:horz))+...        %Below
        padarray(double(G2(1:vert-1,1:horz)),[1 0],'replicate','pre')+... %Above
        padarray(double(G1(1:vert,1:horz-1)),[0 1],'replicate','pre'))/4; %Left
                                 
    %Even Row Odd Column
    %Copy corresponding value from G2
    Iout(2:2:2*vert,1:2:2*horz,2) = G2(1:vert,1:horz);
    
    %Odd Row Even Column 
    %Copy corresponding value from G1
    Iout(1:2:2*vert,2:2:2*horz,2) = G1(1:vert,1:horz);
    
    %Even Row Even Column
    %Average 4 Neighbors (Left-G2, Right-G2, Above-G1 and Below-G1)
    Iout(2:2:2*vert,2:2:2*horz,2) = (double(G2(1:vert,1:horz))+...        %Left
                                     double(G1(1:vert,1:horz))+...        %Above
        padarray(double(G1(2:vert,1:horz)),[1 0],'replicate','post')+...  %Below
        padarray(double(G2(1:vert,2:horz)),[0 1],'replicate','post'))/4;  %Right
    
    
Iout = (min(max(floor(Iout),0),2^12-1));
end