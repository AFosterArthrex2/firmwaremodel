%Simulate 4k1MOS to compare resolution of different encoding schemes.

%Read in images
% directory{1} = 'G:\Matlab Images\4k1MOS\slantedgeimages\';
% directory{2} = 'G:\Matlab Images\4k1MOS\reschartimages\';
% 
% for q = 1:length(directory)
%     list{q} = dir([directory{q} '*.png']);
%     for j = 1:length(list{q})
%         imageinfo(q,j) = regexp(list{q}(j).name,'(?<date>\d{12})-(?<number>g\d).png','names');
%     end
% end
% 
% count = 1;
% 
% %Create "Raw" Images
% for q = 1%:length(directory)
%     for j = 1:length(list{q})
%         if(strcmpi(imageinfo(q,j).number,'g1'))
%             I1 = imread([directory{q} list{q}(j).name]);
%             I2 = imread([directory{q} imageinfo(q,j).date '-g2.png']);
%             
%             %Combine
%             I3 = zeros(2160,3840,1);
% %             I3 = zeros(2160,1920,1);
%             
%             %Red - Shift Left 1 Pixel
%             I3(1:2:end,1:2:end) = [I1(:,2:end,1) I1(:,end,1)];
%             
%             %G1
%             I3(1:2:end,2:2:end) = I1(:,:,2);
%             
%             %G2
%             I3(2:2:end,1:2:end) = I2(:,:,2);
%             
%             %Blue
%             I3(2:2:end,2:2:end) = I1(:,:,3);
%             
%             Iraw{count} = I3;
%             count = count + 1;
%         end
%     end
% end

I1 = Ig1;
I2 = Ig2;

%Combine
I3 = zeros(2160,3840,1);
%             I3 = zeros(2160,1920,1);

%Red - Shift Left 1 Pixel
% I3(1:2:end,1:2:end) = [I1(:,2:end,1) I1(:,end,1)];
I3(1:2:end,1:2:end) = I1(:,:,1);

%G1
I3(1:2:end,2:2:end) = I1(:,:,2);

%G2
I3(2:2:end,1:2:end) = I2(:,:,2);

%Blue
I3(2:2:end,2:2:end) = I1(:,:,3);



%Extra Channel Shifting
for i = 1%:length(Iraw)
    redS  = I3(1:2:end,1:2:end);
    g1S   = I3(1:2:end,2:2:end);
    g2S   = I3(2:2:end,1:2:end);
    blueS = I3(2:2:end,2:2:end);
    
    %positive value moves right/down
    redx = 0;
    redy = 0;
    redpad = padarray(redS,[10 10],0,'both');
    red = redpad(10 -redy +[1:size(redS,1)] ,10 -redx +[1:size(redS,2)]);
    
    g1x = 0;
    g1y = 0;
    g1pad = padarray(g1S,[10 10],0,'both');
    g1 =   g1pad(10 -g1y  +[1:size(g1S ,1)] ,10 -g1x  +[1:size(g1S ,2)]);
    
    g2x = 0;
    g2y = 0;
    g2pad = padarray(g2S,[10 10],0,'both');
    g2 =   g2pad(10 -g2y  +[1:size(g2S ,1)] ,10 -g2x  +[1:size(g2S ,2)]);
    
    bluex = 0;
    bluey = 0;
    bluepad = padarray(blueS,[10 10],0,'both');
    blue = bluepad(10-bluey+[1:size(blueS,1)],10-bluex+[1:size(blueS,2)]);
    
    Iraw2{i} = zeros(size(Iraw{i}));
    
    Iraw2{i}(1:2:end,1:2:end) = red;
    Iraw2{i}(1:2:end,2:2:end) = g1;
    Iraw2{i}(2:2:end,1:2:end) = g2;
    Iraw2{i}(2:2:end,2:2:end) = blue;
end
%Create New rgb images
% 1. 1MOS Demosaic (5x5 kernals)
% 2. Current Process
% 3. Dan Wiley's "Matrixed Green Data Encoding"
% 4. ?



for i = 1%:length(Iraw)
    
    I = Iraw2{i};
    
    %%
    % 1. 1MOS Demosaic (5x5 kernals)
    
    Iout = DeMosaic(I);
    
    Ione{i} = Iout;
    
    %%
    % 2. Current Process
    % Average green channels, offset green channel
    
    Itwotmp = zeros(1080,1920,3);
%     Itwotmp = zeros(1080,960,3);
    %Red Channel - Shift Left 1
%     Itwotmp(:,:,1) = [I(1:2:end,3:2:end) I(1:2:end,end)];
    Itwotmp(:,:,1) = I(1:2:end,1:2:end);
        
    %Green Channel - Avg of G1 and G2delay
    % G1
    % Iout(:,:,2) = (I(2:2:end,1:2:end) + [I(1,2:2:end); I(1:2:(end-2),2:2:end)])/2;
    Itwotmp(:,:,2) = (I(1:2:end,2:2:end) + [I(2,1:2:end); I(2:2:(end-2),1:2:end)])/2;
%     Itwotmp(:,:,2) = round((I(1:2:end,2:2:end) + I(2:2:end,1:2:end))/2);
    
    %Blue Channel - No Shift
    Itwotmp(:,:,3) = I(2:2:end,2:2:end);
    
    %Upsample
    Itwotmp2 = G2ScalerNN(Itwotmp);
      
    %Green Channel Shift - Down 1 Right 1?
%     Green = [Itwotmp2(1,:,2); Itwotmp2(1:end-1,1,2) Itwotmp2(1:end-1,1:end-1,2)];
    
%     Itwo{i} = cat(3,Itwotmp2(:,:,1),Green,Itwotmp2(:,:,3));
    Itwo{i} = cat(3,Itwotmp2(:,:,1),Itwotmp2(:,:,2),Itwotmp2(:,:,3));
    %%
    % 3. Dan Wiley's "Matrixed Green Data Encoding"
    % Data is 10 bits per channel
    
    Red =  I(1:2:end,1:2:end);
    
    G1 =   I(1:2:end,2:2:end);
    
    G2 =   I(2:2:end,1:2:end);
    
    Blue = I(2:2:end,2:2:end);
    
    Gsum = floor((G1+G2)/2);
    
    Gdiff = floor((G1-G2)/32);
    
    Gdiffsign = Gdiff >= 0;
    Gdiffabs = abs(Gdiff);
    
    %Split into two bit chunks
    Gdiffr = 2*Gdiffsign+floor(Gdiffabs/16);
    Gdiffg = floor(mod(Gdiffabs,16)/4);
    Gdiffb = floor(mod(Gdiffabs,4));
    
    Gdiffrec2 = (floor(Gdiffr/2)*2-1).*( 16*mod(Gdiffr,2)+4*Gdiffg+Gdiffb);
    
    %Add each 2 bit chunk to rgb channels.
    Redm = 4*Red + Gdiffr;
    Greenm = 4*Gsum + Gdiffg;
    Bluem = 4*Blue + Gdiffb;
    
    %Head Cable
    
    %Reconstruct G1 and G2
    
    Gdiffr = mod(Redm,4);
    Gdiffg = mod(Greenm,4);
    Gdiffb = mod(Bluem,4);
    
    Redrec = floor(Redm/4);
    Greenrec = floor(Greenm/4);
    Bluerec = floor(Bluem/4);
    
    Gdiffrec = (floor(Gdiffr/2)*2-1).*( 16*mod(Gdiffr,2)+4*Gdiffg+Gdiffb)*16;
%     Gdiffrec = (Gdiffr*16+Gdiffg*4+Gdiffb);
    
    G1rec = Greenrec+Gdiffrec;
    G2rec = Greenrec-Gdiffrec;
    
    Irec = zeros(2160,3840,1);
%     Irec = zeros(2160,1920,1);
    
    %Red - Shift Left 1 Pixel
    Irec(1:2:end,1:2:end) = Redrec;
    
    %G1
    Irec(1:2:end,2:2:end) = G1rec;

    %G2
    Irec(2:2:end,1:2:end) = G2rec;
    
    %Blue
    Irec(2:2:end,2:2:end) = Bluerec;
    
    Ithree{i} = DeMosaic(Irec);
    
    % 4. Alternate Pixel Group
    % Average green channels, offset green channel
    
    Ifourtmp = zeros(1080,1920,3);
%     Ifourtmp = zeros(1080,960,3);
    %Red Channel - No Shift
    Ifourtmp(:,:,1) = [I(1:2:end,1:2:end)];
    
    %Green Channel - Avg of G1 and G2delay
    % G1
    % Iout(:,:,2) = (I(2:2:end,1:2:end) + [I(1,2:2:end); I(1:2:(end-2),2:2:end)])/2;
    Ifourtmp(:,:,2) = (I(1:2:end,2:2:end) + [[I(2,3:2:end) I(2,end)]; [I(2:2:(end-2),3:2:end)  I(2:2:(end-2),end)]])/2;
    
    %Blue Channel - No Shift
    Ifourtmp(:,:,3) = I(2:2:end,2:2:end);
    
    %Upsample
    Ifourtmp2 = G2Scaler(Ifourtmp);
      
    %Green Channel Shift - Down 1 Right 1?
    Green = [Ifourtmp2(1,:,2); Ifourtmp2(1:end-1,1,2) Ifourtmp2(1:end-1,1:end-1,2)];
    
    Ifour{i} = cat(3,Ifourtmp2(:,:,1),Green,Ifourtmp2(:,:,3));
end

figure; imagesc(cat(3,Itwo{1}(800:1000,1900:2250,1)/mean(mean(Itwo{1}(:,:,1))),Itwo{1}(800:1000,1900:2250,2)/mean(mean(Itwo{1}(:,:,2))),Itwo{1}(800:1000,1900:2250,3)/mean(mean(Itwo{1}(:,:,3))))/2)
% figure; imagesc(cat(3,Ione{1}(800:1000,1900:2250,1)/mean(mean(Ione{1}(:,:,1))),Ione{1}(800:1000,1900:2250,2)/mean(mean(Ione{1}(:,:,2))),Ione{1}(800:1000,1900:2250,3)/mean(mean(Ione{1}(:,:,3))))/2)

inputSpreadSheet = [pwd '\Settings\' '4k1MosSettings.xlsx'];
testCase = 105;
settings = readSettingsFromExcel(inputSpreadSheet,testCase);
settings.Head_Type = 3;

% for i = 1%:length(Iraw)
%     [~, IoneP{i}] = SimFirmware_4k(Ione{i},settings,testCase);
%     [~, ItwoP{i}] = SimFirmware_4k(Itwo{i},settings,testCase);
%     [~, IthreeP{i}] = SimFirmware_4k(Ithree{i},settings,testCase);
%     [~, IfourP{i}] = SimFirmware_4k(Ifour{i},settings,testCase);
% end



function Iout = DeMosaic(I)

[x, y] = meshgrid(1:size(I,2),1:size(I,1));

%Pixel grid:   (1,1) (2,1)  -  G    R  -  One    Two
%              (1,2) (2,2)  -  B    G  -  Three  Four

One =   (mod(x,2) == 0) & (mod(y,2) == 1);
Two =   (mod(x,2) == 1) & (mod(y,2) == 1);
Three = (mod(x,2) == 0) & (mod(y,2) == 0);
Four =  (mod(x,2) == 1) & (mod(y,2) == 0);

% G @ R and G @ B
G1 = Two | Three;

% R @ G, B col R row
G2r = One;

% B @ G, R col B row
G2b = Four;

% R @ G, R col B row
G3r = Four;

% B @ G, B col R row
G3b = One;

% R @ B
G4r = Three;

% B @ R
G4b = Two;

% R @ R
G5 = Two;

% G @ G
G6 = ~G1;

% B @ B
G7 = Three;

Kg1 = [0 0 -2 0 0; 0 0 4 0 0; -2 4 8 4 -2; 0 0 4 0 0; 0 0 -2 0 0];

Kg2 = [0 0 1 0 0; 0 -2 0 -2 0; -2 8 10 8 -2; 0 -2 0 -2 0; 0 0 1 0 0];

Kg3 = Kg2';

Kg4 = [0 0 -3 0 0; 0 4 0 4 0; -3 0 12 0 -3; 0 4 0 4 0; 0 0 -3 0 0];

%Run all four kernals
Out1 = conv2(I,Kg1,'same');
Out2 = conv2(I,Kg2,'same');
Out3 = conv2(I,Kg3,'same');
Out4 = conv2(I,Kg4,'same');

%Build Channels by masking output of four kernals and summing.
Red = Out2.*G2r + Out3.*G3r + Out4.*G4r + 16*I.*G5;
Green = Out1.*G1 + 16*I.*G6;
Blue = Out2.*G2b + Out3.*G3b + Out4.*G4b + 16*I.*G7;

Iout = floor(cat(3,Red,Green,Blue)/16);
end

function [Iout] = Model4k1MOSSensor(Iin,GreenMux,ChannelEnable1,ChannelEnable2)
%Models the 4k1MOS sensor test pattern and fpga color control

switch TestPattern
    case 1
        %Black
        I = zeros(2160,1920);
    case 2
        %White
        I = ones(2160,1920)*hex2dec('FFF');
    case 3
        %Dark Grey
        I = ones(2160,1920)*hex2dec('555');
    case 4
        %Light Grey
        I = ones(2160,1920)*hex2dec('AAA');
    case 5
        %Stripes Dark/Light Grey
        I = ones(2160,1920)*hex2dec('555');
        I(:,2:2:end) = ones(2160,960)*hex2dec('AAA');
    case 6
        %Stripes Light/Dark Grey
        I = ones(2160,1920)*hex2dec('AAA');
        I(:,2:2:end) = ones(2160,960)*hex2dec('555');
    case 7
        %Stripes Black, Dark Grey
        I = ones(2160,1920)*hex2dec('000');
        I(:,2:2:end) = ones(2160,960)*hex2dec('555');
    case 8
        %Stripes Dark Grey, Black
        I = ones(2160,1920)*hex2dec('555');
        I(:,2:2:end) = ones(2160,960)*hex2dec('000');
    case 9
        %Stripes Black, White
        I = ones(2160,1920)*hex2dec('000');
        I(:,2:2:end) = ones(2160,960)*hex2dec('FFF');
    case 10
        %Stripes White, Black
        I = ones(2160,1920)*hex2dec('FFF');
        I(:,2:2:end) = ones(2160,960)*hex2dec('000');
    case 11
        %Color Bar (horiz)
        Irgb = ones(1080,960,3)*hex2dec('000');
        %Red - 120 width bars
        Irgb(:,:,1) = repmat([repmat(hex2dec('FFF'),1,120) repmat(hex2dec('000'),1,120)],1080,4);  
        %Green - 240 width bars        
        Irgb(:,:,2) = repmat([repmat(hex2dec('FFF'),1,240) repmat(hex2dec('000'),1,240)],1080,2);  
        %Blue - 60 width bars        
        Irgb(:,:,3) = repmat([repmat(hex2dec('FFF'),1,060) repmat(hex2dec('000'),1,060)],1080,8);   
        
        I = ones(2160,1920);
        I(1:2:end,1:2:end) = Irgb(:,:,1);
        I(2:2:end,1:2:end) = Irgb(:,:,2);
        I(1:2:end,2:2:end) = Irgb(:,:,2);
        I(2:2:end,2:2:end) = Irgb(:,:,3);
        
    case 12
        %Color Bar (vert)
        Irgb = ones(1080,960,3)*hex2dec('000');
        %Red - 120 width bars
        red = repmat([repmat(hex2dec('FFF'),120,1); repmat(hex2dec('000'),120,1)],5,960); 
        Irgb(:,:,1) = red(1:1080,1:960); 
        %Green - 240 width bars
        green = repmat([repmat(hex2dec('FFF'),240,1); repmat(hex2dec('000'),240,1)],3,960); 
        Irgb(:,:,2) = green(1:1080,1:960);   
        %Blue - 60 width bars
        blue = repmat([repmat(hex2dec('FFF'),60,1); repmat(hex2dec('000'),60,1)],9,960); 
        Irgb(:,:,3) = blue;  
        
        I = ones(2160,1920);
        I(1:2:end,1:2:end) = Irgb(:,:,1);
        I(2:2:end,1:2:end) = Irgb(:,:,2);
        I(1:2:end,2:2:end) = Irgb(:,:,2);
        I(2:2:end,2:2:end) = Irgb(:,:,3);
    otherwise
        I = zeros(2160,1920);
end

%Channel Mask
% 1 - Red and G1
Mask1 = zeros(2160,1920);
Mask2 = zeros(2160,1920);

Mask1(1:2:end,:) = repmat(ChannelEnable1,1080,192);
Mask2(2:2:end,:) = repmat(ChannelEnable2,1080,192);

I(Mask1 | Mask2) = hex2dec('3FF');

Iout = zeros(1080,960,3);
%Red Channel - Shift Left 1
Iout(:,:,1) = [I(1:2:end,3:2:end) I(1:2:end,end)];

%Green Channel - Avg of G1 and G2delay
                      % G1            
% Iout(:,:,2) = (I(2:2:end,1:2:end) + [I(1,2:2:end); I(1:2:(end-2),2:2:end)])/2;
Iout(:,:,2) = (I(1:2:end,2:2:end) + [I(2,1:2:end); I(2:2:(end-2),1:2:end)])/2;

%Blue Channel - No Shift
Iout(:,:,3) = I(2:2:end,2:2:end);
end