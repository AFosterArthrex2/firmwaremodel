%Turn on Sensor TP

SetupPath
Port = 'COM5';

if(~exist('S1','var') || ~isvalid(S1))
    S1 = serial(Port,'BaudRate',9600);
end

if(strcmp(S1.status,'closed'))
    fopen(S1);
else
    fclose(S1);
    pause(0.2)
    fopen(S1);
end

Setup4k1MOSSensor(Port,S1)

%     channelEnable1 = [0 0 0 0 0 0 0 0 0 0];
%     channelEnable1 = [1 0 1 0 1 0 1 0 1 0];
channelEnable1 = double(dec2bin(randi(2^10)-1,10))-48
%     channelEnable2 = [0 0 0 0 0 0 0 0 0 0];
%     channelEnable2 = [0 1 0 1 0 1 0 1 0 1];
channelEnable2 = double(dec2bin(randi(2^10)-1,10))-48

%Channel Enables
SerialBitWriteCCU(Port,2,41,fliplr(char(channelEnable1+48)),[9:-1:0],S1);
pause(1)
SerialBitWriteCCU(Port,2,41,fliplr(char(channelEnable2+48)),[25:-1:16],S1);

FWVersion = (SerialReadCCU(Port,2,0,S1));
buildNumber = hex2dec(SerialReadCCU(Port,2,3,S1));

currentTime = now;
datecode = datestr(now,'yymmddHHMMSS');
fid = fopen([pwd '\4k1MOS\Results\' datecode '.txt'],'w'); 

fprintf(fid,['Test Performed: ' datestr(currentTime) '\n']);
fprintf(fid,'Firmware Under Test:\n');
fprintf(fid,'850-00%02d-%02d Rev%s S%d\n',hex2dec(FWVersion(1:2)),hex2dec(FWVersion(3:4)),char(65+hex2dec(FWVersion(5:6))),buildNumber);
fprintf(fid,'\n');
fprintf(fid,'Channel Mask #1: 0x%d%d%d%d%d%d%d%d%d%d%d%d',channelEnable1);
fprintf(fid,'\n');
fprintf(fid,'Channel Mask #2: 0x%d%d%d%d%d%d%d%d%d%d%d%d',channelEnable2);
fprintf(fid,'\n');
fprintf(fid,'\n');
fprintf(fid,'Errors:\n');
fprintf(fid,'---------------------------\n');
fprintf(fid,'  Test Pattern| R | G | B |\n');
fprintf(fid,'--------------|---|---|---|\n');
TestPatterns = {' 1. All 000h  ',...
                ' 2. All FFFh  ',...
                ' 3. All 555h  ',...
                ' 4. All AAAh  ',...
                ' 5. 555h/AAAh ',...
                ' 6. AAAh/555h ',...
                ' 7. 000h/555h ',...
                ' 8. 555h/000h ',...
                ' 9. 000h/FFFh ',...
                '10. FFFh/000h ',...
                '11. Color Horz',...
                '12. Color Vert'};
    
for i = 1:12
    %Pick Test Pattern
    SerialWriteCCU(Port,2,6,['803D00' dec2hex(i-1,2)],S1);
    
    pause(2)
    [Ig1, Ig2, Ig3] = Capture4k1MOSSensor(vid,Port,S1);
    
    Ig6 = Model4k1MOSSensor(i,0,channelEnable1,channelEnable2);
    
    %Mask first and last columns and rows
    diff53 = double(Ig6)-double(Ig3);
    diff53([1 960],:,1:3) = 0;
    diff53(:,[1 960],1:3) = 0;
    
    %diff less then 10 ignore for now
    diff53(diff53<10) = 0;
    

    fprintf(fid,'%s| %d | %d | %d |\n',TestPatterns{i},round(squeeze(max(max(abs(diff53)))))');
    figure(1);LinkImage(double(Ig3)/2^10,Ig6/2^10,abs(double(Ig3)/2^10-Ig6/2^10))
    drawnow
end
fprintf(fid,'---------------------------\n');
fclose(fid);
    
function Setup4k1MOSSensor(Port,S1)

SerialWriteCCU(Port,1,9,'00000001',S1)
%Pick Test Pattern
SerialWriteCCU(Port,2,6,['803D00' dec2hex(10,2)],S1);
%Enable Test Pattern
SerialWriteCCU(Port,2,6,'803C0012',S1);

%ColorWidth
SerialWriteCCU(Port,2,6,'870E0000',S1);

SerialWriteCCU(Port,2,10,'00000000',S1);

%Sensor Black Level
SerialWriteCCU(Port,2,6,'80450000',S1);

%CCU Black Level
SerialWriteCCU(Port,1,40,'40000000',S1)

%Set Crops
SerialWriteCCU(Port,1,45+256*4,'38020806',S1);
%Manual Index
SerialBitWriteCCU(Port,1,4*256+10,'00',[4 3],S1);
%Set Index
SerialWriteCCU(Port,1,11+256*4,'0400035C',S1);
%View Buffer
SerialBitWriteCCU(Port,1,4*256+8,'1',[15],S1);
%Remeber Register 9
reg9 = SerialReadCCU(Port,1,9,S1);
%Turn off All Processing
SerialWriteCCU(Port,1,9,'FFFFFFFF',S1);
SerialWriteCCU(Port,1,9,dec2hex(bin2dec('00000000000000001111110111110110'),8),S1);
%1080 from frame buffer
SerialBitWriteCCU(Port,1,4*256+8,'1100',[11:-1:8],S1);
%4k Zoom Off
SerialWriteCCU(Port,1,1*256+88,'00000000',S1);

pause(0.15)
%Disable Processing (Gamma and Post Peaking/Sharpening)
SerialBitWriteCCU(Port,1,9,'11',[25:-1:24],S1);

%Enable 12-bit mode left side
SerialWriteCCU(Port,1,4*256+46,'00000001',S1);

end

function [Ig1, Ig2, Ig3] = Capture4k1MOSSensor(vid,Port,S1)
%Enable Green Channel 1
SerialBitWriteCCU(Port,2,41,'01',[31:-1:30],S1);
% dec2bin(hex2dec(SerialReadCCU(Port,2,41,S1)),32)
Ic = getsnapshot(vid);
Ig1 = convert8to12(Ic);

%Enable Green Channel 2
SerialBitWriteCCU(Port,2,41,'10',[31:-1:30],S1);
% dec2bin(hex2dec(SerialReadCCU(Port,2,41,S1)),32)
pause(0.25)
Ic = getsnapshot(vid);
Ig2 = convert8to12(Ic);


%Enable Green Channel Average
SerialBitWriteCCU(Port,2,41,'00',[31:-1:30],S1);
% dec2bin(hex2dec(SerialReadCCU(Port,2,41,S1)),32)
pause(0.25)
Ic = getsnapshot(vid);
Ig3 = convert8to12(Ic);
end

function [Iout] = Model4k1MOSSensor(TestPattern,GreenMux,ChannelEnable1,ChannelEnable2)
%Models the 4k1MOS sensor test pattern and fpga color control

switch TestPattern
    case 1
        %Black
        I = zeros(2160,1920);
    case 2
        %White
        I = ones(2160,1920)*hex2dec('FFF');
    case 3
        %Dark Grey
        I = ones(2160,1920)*hex2dec('555');
    case 4
        %Light Grey
        I = ones(2160,1920)*hex2dec('AAA');
    case 5
        %Stripes Dark/Light Grey
        I = ones(2160,1920)*hex2dec('555');
        I(:,2:2:end) = ones(2160,960)*hex2dec('AAA');
    case 6
        %Stripes Light/Dark Grey
        I = ones(2160,1920)*hex2dec('AAA');
        I(:,2:2:end) = ones(2160,960)*hex2dec('555');
    case 7
        %Stripes Black, Dark Grey
        I = ones(2160,1920)*hex2dec('000');
        I(:,2:2:end) = ones(2160,960)*hex2dec('555');
    case 8
        %Stripes Dark Grey, Black
        I = ones(2160,1920)*hex2dec('555');
        I(:,2:2:end) = ones(2160,960)*hex2dec('000');
    case 9
        %Stripes Black, White
        I = ones(2160,1920)*hex2dec('000');
        I(:,2:2:end) = ones(2160,960)*hex2dec('FFF');
    case 10
        %Stripes White, Black
        I = ones(2160,1920)*hex2dec('FFF');
        I(:,2:2:end) = ones(2160,960)*hex2dec('000');
    case 11
        %Color Bar (horiz)
        Irgb = ones(1080,960,3)*hex2dec('000');
        %Red - 120 width bars
        Irgb(:,:,1) = repmat([repmat(hex2dec('FFF'),1,120) repmat(hex2dec('000'),1,120)],1080,4);  
        %Green - 240 width bars        
        Irgb(:,:,2) = repmat([repmat(hex2dec('FFF'),1,240) repmat(hex2dec('000'),1,240)],1080,2);  
        %Blue - 60 width bars        
        Irgb(:,:,3) = repmat([repmat(hex2dec('FFF'),1,060) repmat(hex2dec('000'),1,060)],1080,8);   
        
        I = ones(2160,1920);
        I(1:2:end,1:2:end) = Irgb(:,:,1);
        I(2:2:end,1:2:end) = Irgb(:,:,2);
        I(1:2:end,2:2:end) = Irgb(:,:,2);
        I(2:2:end,2:2:end) = Irgb(:,:,3);
        
    case 12
        %Color Bar (vert)
        Irgb = ones(1080,960,3)*hex2dec('000');
        %Red - 120 width bars
        red = repmat([repmat(hex2dec('FFF'),120,1); repmat(hex2dec('000'),120,1)],5,960); 
        Irgb(:,:,1) = red(1:1080,1:960); 
        %Green - 240 width bars
        green = repmat([repmat(hex2dec('FFF'),240,1); repmat(hex2dec('000'),240,1)],3,960); 
        Irgb(:,:,2) = green(1:1080,1:960);   
        %Blue - 60 width bars
        blue = repmat([repmat(hex2dec('FFF'),60,1); repmat(hex2dec('000'),60,1)],9,960); 
        Irgb(:,:,3) = blue;  
        
        I = ones(2160,1920);
        I(1:2:end,1:2:end) = Irgb(:,:,1);
        I(2:2:end,1:2:end) = Irgb(:,:,2);
        I(1:2:end,2:2:end) = Irgb(:,:,2);
        I(2:2:end,2:2:end) = Irgb(:,:,3);
    otherwise
        I = zeros(2160,1920);
end

%Channel Mask
% 1 - Red and G1
Mask1 = zeros(2160,1920);
Mask2 = zeros(2160,1920);

Mask1(1:2:end,:) = repmat(ChannelEnable1,1080,192);
Mask2(2:2:end,:) = repmat(ChannelEnable2,1080,192);

I(Mask1 | Mask2) = hex2dec('3FF');

Iout = zeros(1080,960,3);
%Red Channel - Shift Left 1
Iout(:,:,1) = [I(1:2:end,3:2:end) I(1:2:end,end)];

%Green Channel - Avg of G1 and G2delay
                      % G1            
% Iout(:,:,2) = (I(2:2:end,1:2:end) + [I(1,2:2:end); I(1:2:(end-2),2:2:end)])/2;
Iout(:,:,2) = (I(1:2:end,2:2:end) + [I(2,1:2:end); I(2:2:(end-2),1:2:end)])/2;

%Blue Channel - No Shift
Iout(:,:,3) = I(2:2:end,2:2:end);
end