%Simulate the 4k1MOS camera head
%12-23-16

% Captures an image, processing disabled except black level

SetupPath
Port = 'COM5';

if(~exist('S1','var') || ~isvalid(S1))
    S1 = serial(Port,'BaudRate',9600);
end

if(strcmp(S1.status,'closed'))
    fopen(S1);
else
    fclose(S1);
    pause(0.2)
    fopen(S1);
end

%100 percent LED output
LEDShutterReg = SerialReadCCU(Port,1,31+1*256,S1);
LEDShutterBin(4) = '0';
SerialWriteCCU(Port,1,31+1*256,dec2hex(bin2dec(LEDShutterBin),8),S1);

imageDir = 'G:\Matlab Images\4k1MOS\reschartimages\';

list = 

for i = 1:
% [BlackLevels, AECGain] = Setup4k1MOSSensor(Port,S1,'turnoff');
% 
% settings.AEC_Gain = hex2dec(AECGain(5:8))/2^10;
% settings.Head_Type = 3;
% 
% [Ig1, Ig2, Ig3] = Capture4k1MOSSensor(vid,Port,S1);
% 
% Setup4k1MOSSensor(Port,S1,'turnon')

datecode = datestr(now,'yymmddHHMMSS')

% 
imwrite(Ig1,[imageDir datecode '-g1.png'])
imwrite(Ig2,[imageDir datecode '-g2.png'])
imwrite(Ig3,[imageDir datecode '-g3.png'])


%Combine
I3 = zeros(2160,3840,1);

%Red - Shift Left 1 Pixel
I3(1:2:end,1:2:end) = [Ig1(:,2:end,1) Ig1(:,end,1)];

%G1
I3(1:2:end,2:2:end) = Ig1(:,:,2);

%G2
I3(2:2:end,1:2:end) = Ig2(:,:,2);

%Blue
I3(2:2:end,2:2:end) = Ig1(:,:,3);

figure;imagesc(I3)

% I = DeMosaic4k1MOS(I3)*4088/4785;
I = DeMosaic4k1MOS(I3);
testCase = 1;

SimFirmware_4k;
