function [Iout] = Model4k1MOSSensor(TestPattern,GreenMux,ChannelEnable1,ChannelEnable2)
%Models the 4k1MOS sensor test pattern and fpga color control

switch TestPattern
    case 1
        %Black
        I = zeros(2160,3840);
    case 2
        %White
        I = ones(2160,3840)*hex2dec('FFF');
    case 3
        %Dark Grey
        I = ones(2160,3840)*hex2dec('555');
    case 4
        %Light Grey
        I = ones(2160,3840)*hex2dec('AAA');
    case 5
        %Stripes Dark/Light Grey
        I = ones(2160,3840)*hex2dec('555');
        I(:,2:2:end) = ones(2160,1920)*hex2dec('AAA');
    case 6
        %Stripes Light/Dark Grey
        I = ones(2160,3840)*hex2dec('AAA');
        I(:,2:2:end) = ones(2160,1920)*hex2dec('555');
    case 7
        %Stripes Black, Dark Grey
        I = ones(2160,3840)*hex2dec('000');
        I(:,2:2:end) = ones(2160,1920)*hex2dec('555');
    case 8
        %Stripes Dark Grey, Black
        I = ones(2160,3840)*hex2dec('555');
        I(:,2:2:end) = ones(2160,1920)*hex2dec('000');
    case 9
        %Stripes Black, White
        I = ones(2160,3840)*hex2dec('000');
        I(:,2:2:end) = ones(2160,1920)*hex2dec('FFF');
    case 10
        %Stripes White, Black
        I = ones(2160,3840)*hex2dec('FFF');
        I(:,2:2:end) = ones(2160,1920)*hex2dec('000');
    case 11
        %Color Bar (horiz)
        Irgb = ones(1080,1920,3)*hex2dec('000');
        %Red - 120 width bars
        Irgb(:,:,1) = repmat([repmat(hex2dec('FFF'),1,120) repmat(hex2dec('000'),1,120)],1080,8);  
        %Green - 240 width bars        
        Irgb(:,:,2) = repmat([repmat(hex2dec('FFF'),1,240) repmat(hex2dec('000'),1,240)],1080,4);  
        %Blue - 60 width bars        
        Irgb(:,:,3) = repmat([repmat(hex2dec('FFF'),1,060) repmat(hex2dec('000'),1,060)],1080,16);   
        
        I = ones(2160,3840);
        I(1:2:end,1:2:end) = Irgb(:,:,1);
        I(2:2:end,1:2:end) = Irgb(:,:,2);
        I(1:2:end,2:2:end) = Irgb(:,:,2);
        I(2:2:end,2:2:end) = Irgb(:,:,3);
        
    case 12
        %Color Bar (vert)
        Irgb = ones(1080,1920,3)*hex2dec('000');
        %Red - 120 width bars
        red = repmat([repmat(hex2dec('FFF'),120,1); repmat(hex2dec('000'),120,1)],5,1920); 
        Irgb(:,:,1) = red(1:1080,1:1920); 
        %Green - 240 width bars
        green = repmat([repmat(hex2dec('FFF'),240,1); repmat(hex2dec('000'),240,1)],3,1920); 
        Irgb(:,:,2) = green(1:1080,1:1920);   
        %Blue - 60 width bars
        blue = repmat([repmat(hex2dec('FFF'),60,1); repmat(hex2dec('000'),60,1)],9,1920); 
        Irgb(:,:,3) = blue;  
        
        I = ones(2160,3840);
        I(1:2:end,1:2:end) = Irgb(:,:,1);
        I(2:2:end,1:2:end) = Irgb(:,:,2);
        I(1:2:end,2:2:end) = Irgb(:,:,2);
        I(2:2:end,2:2:end) = Irgb(:,:,3);
    otherwise
        I = zeros(2160,3840);
end

%Channel Mask
% 1 - Red and G1
Mask1 = zeros(2160,3840);
Mask2 = zeros(2160,3840);

Mask1(1:2:end,:) = repmat(ChannelEnable1,1080,384);
Mask2(2:2:end,:) = repmat(ChannelEnable2,1080,384);

I(Mask1 | Mask2) = hex2dec('3FF');

%Shift to FPGA layout from true sensor layout
%Red Shifted Left 1, G2 Shifted Up 1.
Is = BayerShiftChannel(I,[-1 0],[0 0],[0 1],[0 0]);

Iout = zeros(1080,1920,3);
%Red Channel
Iout(:,:,1) = Is(1:2:end,1:2:end);

%Green Channel - Avg of G1 and G2
Iout(:,:,2) = (Is(1:2:end,2:2:end) + Is(2:2:end,1:2:end))/2;

%Blue Channel
Iout(:,:,3) = Is(2:2:end,2:2:end);
end