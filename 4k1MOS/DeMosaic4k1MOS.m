function Iout = DeMosaic4k1MOS(I)

[x, y] = meshgrid(1:size(I,2),1:size(I,1));

%Pixel grid:   (1,1) (2,1)  -  G    R  -  One    Two
%              (1,2) (2,2)  -  B    G  -  Three  Four

One =   (mod(x,2) == 0) & (mod(y,2) == 1);
Two =   (mod(x,2) == 1) & (mod(y,2) == 1);
Three = (mod(x,2) == 0) & (mod(y,2) == 0);
Four =  (mod(x,2) == 1) & (mod(y,2) == 0);

% G @ R and G @ B
G1 = Two | Three;

% R @ G, B col R row
G2r = One;

% B @ G, R col B row
G2b = Four;

% R @ G, R col B row
G3r = Four;

% B @ G, B col R row
G3b = One;

% R @ B
G4r = Three;

% B @ R
G4b = Two;

% R @ R
G5 = Two;

% G @ G
G6 = ~G1;

% B @ B
G7 = Three;

Kg1 = [0 0 -2 0 0; 0 0 4 0 0; -2 4 8 4 -2; 0 0 4 0 0; 0 0 -2 0 0];

Kg2 = [0 0 1 0 0; 0 -2 0 -2 0; -2 8 10 8 -2; 0 -2 0 -2 0; 0 0 1 0 0];

Kg3 = Kg2';

Kg4 = [0 0 -3 0 0; 0 4 0 4 0; -3 0 12 0 -3; 0 4 0 4 0; 0 0 -3 0 0];

%Run all four kernals
Out1 = conv2(I,Kg1,'same');
Out2 = conv2(I,Kg2,'same');
Out3 = conv2(I,Kg3,'same');
Out4 = conv2(I,Kg4,'same');

%Build Channels by masking output of four kernals and summing.
Red = Out2.*G2r + Out3.*G3r + Out4.*G4r + 16*I.*G5;
Green = Out1.*G1 + 16*I.*G6;
Blue = Out2.*G2b + Out3.*G3b + Out4.*G4b + 16*I.*G7;

Iout = floor(cat(3,Red,Green,Blue)/16);
end