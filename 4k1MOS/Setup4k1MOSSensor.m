
function [BlackLevels, AECGain] = Setup4k1MOSSensor(Port,S1,varargin)
AECGain = [];
BlackLevels = [];
if(isempty(varargin))
    mode = 'turnoff';
else
    mode = varargin{1};
end

if(strcmpi(mode,'turnoff'))
%     CCU Black Level
    SerialWriteCCU(Port,1,40,'40000000',S1)
    BlackLevels{1} = SerialReadCCU(Port,1,4*256+214,S1);
    BlackLevels{2} = SerialReadCCU(Port,1,4*256+215,S1);
    
    AECGain = SerialReadCCU(Port,1,4*256+19,S1);
    %Set Crops
    SerialWriteCCU(Port,1,45+256*4,'38020806',S1);
    %Manual Index
    SerialBitWriteCCU(Port,1,4*256+10,'00',[4 3],S1);
    %Set Index
    SerialWriteCCU(Port,1,11+256*4,'0400035C',S1);
    %View Buffer
    SerialBitWriteCCU(Port,1,4*256+8,'1',[15],S1);
    %Remeber Register 9
    reg9 = SerialReadCCU(Port,1,9,S1);
    
    SerialWriteCCU(Port,1,9,dec2hex(bin2dec('00000000000000001111111111110110'),8),S1);
    %1080 from frame buffer
    SerialBitWriteCCU(Port,1,4*256+8,'1100',[11:-1:8],S1);
    %4k Zoom Off
    SerialWriteCCU(Port,1,1*256+88,'00000000',S1);
    
    %100 percent LED output
    SerialWriteCCU(Port,1,42+1*256,'3A50AF5D',S1)
    LEDShutterReg = SerialReadCCU(Port,1,31+1*256,S1);
    LEDShutterBin = dec2bin(hex2dec(LEDShutterReg));
    LEDShutterBin(4) = '0';
    SerialWriteCCU(Port,1,31+1*256,dec2hex(bin2dec(LEDShutterBin),8),S1);
    
    pause(0.15)
    %Disable Processing (Gamma and Post Peaking/Sharpening)
    SerialBitWriteCCU(Port,1,9,'11',[25:-1:24],S1);
else
    %CCU Black Level
    SerialWriteCCU(Port,1,40,'2050101B',S1)
    
    %Set Crops
    SerialWriteCCU(Port,1,45+256*4,'40080806',S1);
    %Manual Index
    SerialWriteCCU(Port,1,4*256+10,'00001428',S1);
    %Set Index
    SerialWriteCCU(Port,1,11+256*4,'0400021C',S1);
    %View Buffer
    SerialWriteCCU(Port,1,4*256+8,'00850000',S1);
    
    SerialWriteCCU(Port,1,9,'00008000',S1);
    
    %100 percent LED output
    SerialWriteCCU(Port,1,43+1*256,'53FAC90A',S1)
    LEDShutterReg = SerialReadCCU(Port,1,31+1*256,S1);
    LEDShutterBin = dec2bin(hex2dec(LEDShutterReg));
    LEDShutterBin(4) = '1';
    SerialWriteCCU(Port,1,31+1*256,dec2hex(bin2dec(LEDShutterBin),8),S1);
    
    %4k Zoom On
    SerialWriteCCU(Port,1,1*256+88,'00000009',S1);   
end

end