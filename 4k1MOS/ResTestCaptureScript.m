
imageDir = 'G:\Matlab Images\4k1MOS\test\';

Port = 'COM5';

if(~exist('S1','var') || ~isvalid(S1))
    S1 = serial(Port,'BaudRate',9600);
end

if(strcmp(S1.status,'closed'))
    fopen(S1);
else
    fclose(S1);
    pause(0.2)
    fopen(S1);
end

%Enable Test Pattern
% SerialWriteCCU(Port,2,6,'803C0012',S1);
%Pick Test Pattern
%     SerialWriteCCU(Port,2,6,['803D00' dec2hex(9,2)],S1);

Setup4k1MOSSensor(Port,S1,'turnoff');

[Ig1, Ig2, Ig3] = Capture4k1MOSSensor(vid,Port,S1);

Setup4k1MOSSensor(Port,S1,'turnon')
drawnow

datecode = datestr(now,'yymmddHHMMSS');

%Process Image, save as 4k 1-channel
I1 = Ig1;
I2 = Ig2;

%Combine
I3 = zeros(2160,3840,1);

%Red
I3(1:2:end,1:2:end) = I1(:,:,1);

%G1
I3(1:2:end,2:2:end) = I1(:,:,2);

%G2
I3(2:2:end,1:2:end) = I2(:,:,2);

%Blue
I3(2:2:end,2:2:end) = I1(:,:,3);

%Shift from two stage capture to create Captured "FPGA Image" - Shift G2 up 1
Ifpga2 = BayerShiftChannel(I3,[0 0],[0 0],[0 1],[0 0]);

%Shift from two stage capture to create true "Sensor Image" - Red Shift Right 1, no green shift.
Isensor = BayerShiftChannel(I3,[1 0],[0 0],[0 0],[0 0]);

imwrite(Isensor,[imageDir datecode '-sensorimage.png']);

%Shift from sensor image to create Captured "FPGA Image" - Shift Red Left 1, Shift G2 up 1
Ifpga = BayerShiftChannel(Isensor,[-1 0],[0 0],[0 1],[0 0]);

I5 = zeros(1080,1920,3);

I4 = Ifpga;
%Red Channel
I5(:,:,1) = I4(1:2:end,1:2:end);

%Green Channel - Avg of G1 and G2
I5(:,:,2) = round((I4(1:2:end,2:2:end) + I4(2:2:end,1:2:end))/2);

%Blue Channel -
I5(:,:,3) = I4(2:2:end,2:2:end);

%Upsample
I6 = G2Scaler(I5);

%Green Channel Shift - Down 1 Right 1
Green = [I6(1,:,2); I6(1:end-1,1,2) I6(1:end-1,1:end-1,2)];

I7 = cat(3,I6(:,:,1),Green,I6(:,:,3));

I8 = G2DownScale(I7);

I9 = uint8(I8/16);

figure;imagesc(I9)