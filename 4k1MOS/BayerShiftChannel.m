function Iout = BayerShiftChannel(Iin,R,G1,G2,B)
% Iin - R G1
%       G2 B
% R = [x y];
% G1= [x y];
% G2= [x y];
% B = [x y];

%Extra Channel Shifting
redS  = Iin(1:2:end,1:2:end);
g1S   = Iin(1:2:end,2:2:end);
g2S   = Iin(2:2:end,1:2:end);
blueS = Iin(2:2:end,2:2:end);

%positive value moves right/up
redx = R(1);
redy = R(2);
redpad = padarray(redS,[10 10],0,'both');
red = redpad(10 -redy +[1:size(redS,1)] ,10 -redx +[1:size(redS,2)]);

g1x = G1(1);
g1y = G1(2);
g1pad = padarray(g1S,[10 10],0,'both');
g1 =   g1pad(10 -g1y  +[1:size(g1S ,1)] ,10 -g1x  +[1:size(g1S ,2)]);

g2x = G2(1);
g2y = G2(2);
g2pad = padarray(g2S,[10 10],0,'both');
g2 =   g2pad(10 -g2y  +[1:size(g2S ,1)] ,10 -g2x  +[1:size(g2S ,2)]);

bluex = B(1);
bluey = B(2);
bluepad = padarray(blueS,[10 10],0,'both');
blue = bluepad(10-bluey+[1:size(blueS,1)],10-bluex+[1:size(blueS,2)]);

Iout = zeros(size(Iin));

Iout(1:2:end,1:2:end) = red;
Iout(1:2:end,2:2:end) = g1;
Iout(2:2:end,1:2:end) = g2;
Iout(2:2:end,2:2:end) = blue;
end