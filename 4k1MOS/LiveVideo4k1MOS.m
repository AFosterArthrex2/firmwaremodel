%Live Video Test 4k1MOS

%Turn on Sensor TP

SetupPath
Port = 'COM5';

if(~exist('S1','var') || ~isvalid(S1))
    S1 = serial(Port,'BaudRate',9600);
end

if(strcmp(S1.status,'closed'))
    fopen(S1);
else
    fclose(S1);
    pause(0.2)
    fopen(S1);
end

Setup4k1MOSSensor(Port,S1)
        
% for i = 1:32
    
    [Ig1, Ig2, Ig3] = Capture4k1MOSSensor(vid,Port,S1);
    
    Ig4 = zeros(2160,1920,1);
    
    %Red
    Ig4(1:2:end,1:2:end) = Ig1(:,:,1);
    
    %G1
    Ig4(1:2:end,2:2:end) = Ig1(:,:,2);
    
    %G2
    Ig4(2:2:end,1:2:end) = Ig2(:,:,2);
    
    %Blue
    Ig4(2:2:end,2:2:end) = Ig1(:,:,3);

    
    
    
    %Create Ig5 from Ig4, should match Ig3
    red = Ig4(1:2:end,1:2:end);
    
    %Shift g1 right one pixel
    g1 = Ig4(1:2:end,2:2:end);
%     g1shift = [g1(:,1) g1(:,1:end-1)];
    
    %Shift g2 right one pixel and down one pixel
    g2 = Ig4(2:2:end,1:2:end);
%     g2shift = [g2(1,1) g2(1,1:end-1); g2(1:end-1,1) g2(1:end-1,1:end-1)];
%     g2shift = [g2(:,1) g2(:,1:end-1)];
    g2shift = [g2(1,:); g2(1:end-1,:)];
    
    %Shift blue right one pixel
    blue = Ig4(2:2:end,2:2:end);
    blueshift = [blue(:,1) blue(:,1:end-1)];
    
    Ig5 = cat(3,red,(g1+g2shift)/2,blue);
    
    %Mask first and last columns and rows
    diff53 = double(Ig5)-double(Ig3);
    diff53([1 end],:) = 0;
    diff53(:,[1 end]) = 0;
    
    fprintf('%d: %d %d %d.\n',i,round(squeeze(max(max(abs(diff53)))))')
    
    
    %Use RegTest
    S = 050;R = 50;
%     cropx = [550:900];
    cropx = [1:960];
    cropy = [1:1080];
    
    [outputs1] = RegTest(Ig1(cropy,cropx,:),S,R);
    [outputs2] = RegTest(Ig2(cropy,cropx,:),S,R);
    [outputs3] = RegTest(Ig3(cropy,cropx,:),S,R);
    
    [median(reshape(outputs1{1}(1,1,:,:),1,[])) median(reshape(outputs1{1}(1,2,:,:),1,[]));...
     median(reshape(outputs1{1}(2,1,:,:),1,[])) median(reshape(outputs1{1}(2,2,:,:),1,[]));...
     median(reshape(outputs1{1}(3,1,:,:),1,[])) median(reshape(outputs1{1}(3,2,:,:),1,[]))]
 
     [median(reshape(outputs2{1}(1,1,:,:),1,[])) median(reshape(outputs2{1}(1,2,:,:),1,[]));...
     median(reshape(outputs2{1}(2,1,:,:),1,[])) median(reshape(outputs2{1}(2,2,:,:),1,[]));...
     median(reshape(outputs2{1}(3,1,:,:),1,[])) median(reshape(outputs2{1}(3,2,:,:),1,[]))]
 
     [median(reshape(outputs3{1}(1,1,:,:),1,[])) median(reshape(outputs3{1}(1,2,:,:),1,[]));...
     median(reshape(outputs3{1}(2,1,:,:),1,[])) median(reshape(outputs3{1}(2,2,:,:),1,[]));...
     median(reshape(outputs3{1}(3,1,:,:),1,[])) median(reshape(outputs3{1}(3,2,:,:),1,[]))]
% end








function Setup4k1MOSSensor(Port,S1)

SerialWriteCCU(Port,1,9,'00000001',S1)
% %Pick Test Pattern
% SerialWriteCCU(Port,2,6,['803D00' dec2hex(10,2)],S1);
% %Enable Test Pattern
% SerialWriteCCU(Port,2,6,'803C0012',S1);

%ColorWidth
SerialWriteCCU(Port,2,6,'870E0000',S1);

SerialWriteCCU(Port,2,10,'00000000',S1);

%Sensor Black Level
SerialWriteCCU(Port,2,6,'80450000',S1);

%CCU Black Level
SerialWriteCCU(Port,1,40,'40000000',S1)

%Manual Index
SerialBitWriteCCU(Port,1,4*256+10,'00',[4 3],S1);
%Set Index
SerialWriteCCU(Port,1,11+256*4,'0400035C',S1);
%View Buffer
SerialBitWriteCCU(Port,1,4*256+8,'1',[15],S1);
%Remeber Register 9
reg9 = SerialReadCCU(Port,1,9,S1);
%Turn off All Processing
SerialWriteCCU(Port,1,9,'FFFFFFFF',S1);
%1080 from frame buffer
SerialBitWriteCCU(Port,1,4*256+8,'1100',[11:-1:8],S1);
%4k Zoom Off
SerialWriteCCU(Port,1,1*256+88,'00000000',S1);

pause(0.15)
%Disable Processing (Gamma and Post Peaking/Sharpening)
SerialBitWriteCCU(Port,1,9,'11',[25:-1:24],S1);

%Enable 12-bit mode left side
SerialWriteCCU(Port,1,4*256+46,'00000001',S1);

end

function [Ig1, Ig2, Ig3] = Capture4k1MOSSensor(vid,Port,S1)
%Enable Green Channel 1
SerialBitWriteCCU(Port,2,41,'01',[31:-1:30],S1);

Ic = getsnapshot(vid);
Ig1 = convert8to12(Ic);

%Enable Green Channel 2
SerialBitWriteCCU(Port,2,41,'10',[31:-1:30],S1);

pause(0.25)
Ic = getsnapshot(vid);
Ig2 = convert8to12(Ic);


%Enable Green Channel Average
SerialBitWriteCCU(Port,2,41,'00',[31:-1:30],S1);

pause(0.25)
Ic = getsnapshot(vid);
Ig3 = convert8to12(Ic);
end