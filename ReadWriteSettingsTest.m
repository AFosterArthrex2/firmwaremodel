%Quick Test
%Read Settings From Excel
%Write Them to CCU
%Read Settings from CCU
%Compare to what was written
%Compare to what was read from Excel

SettingsDefinition = SettingsDef();

ExcelSettings = readSettingsFromExcel('Settings.xlsx',1);

values = WriteSettingsDef(Port,ExcelSettings);

[CcuSettings, output] = ReadSettingsDef(Port);

if(isequal(values',output(:,1)))
    fprintf('Settings Read From CCU Match what was written\n');
else
    fprintf('ERROR: Settings Read From CCU DO NOT Match what was written\n');
end


if(isequal(output(:,2),struct2cell(CcuSettings)))
    fprintf('Settings Read From CCU Match Excel Settings\n');
else
    fprintf('ERROR: Settings Read From CCU DO NOT Match Excel Settings\n');
end


eqVector = cell(1,size(output,1));
for i = 1:size(output,1)
    eqVector{i} = values{i} == output{i,1};
end

[SettingsDefinition(:,1) values' output(:,1) mat2cell(cellfun(@min,eqVector)',ones(1,size(output,1)))]