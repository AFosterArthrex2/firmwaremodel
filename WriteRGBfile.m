function WriteRGBfile(Iin,filename)

%Write an image in raw RGB format

I2 = permute(Iin,[3 2 1]);

I3 = reshape(I2,[],1,1);

fid = fopen(filename,'w');

fwrite(fid,I3);

