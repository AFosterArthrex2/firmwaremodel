function [settings, output] = ReadSettingsDef(Port,varargin)

if(~isempty(varargin))
    S1 = varargin{1};
end

if(~exist('S1','var') || ~isvalid(S1))
    S1 = serial(Port,'BaudRate',9600);
end

if(strcmp(S1.status,'closed'))
    fopen(S1);
end

% fopen(S1);
%Read Settings Using SettingsDefinition to structure
SettingsDefinition = SettingsDef();
output = cell(size(SettingsDefinition,1),5);
for i = 1:size(SettingsDefinition,1)
    out = SerialReadCCU(Port,1,SettingsDefinition{i,2}*256+SettingsDefinition{i,3},S1);
    outBinary = dec2bin(hex2dec(out),32);
    output{i,1} = outBinary(32-SettingsDefinition{i,4});
    switch SettingsDefinition{i,5}(1:3)
        case 'boo'
            
            output{i,2} = logical(str2double(output{i,1}));
            
        case 'int'
            
            if(length(SettingsDefinition{i,5}) > 4)
                divideby = str2double(SettingsDefinition{i,5}(5:end));
            else divideby = 0; 
            end
            output{i,2} = bin2dec(output{i,1})/(2^divideby);
            
        case 'sin'
            
            if(length(SettingsDefinition{i,5}) > 5)
                divideby = str2double(SettingsDefinition{i,5}(6:end));
            else divideby = 0; 
            end
            
            if(output{i,1}(1))
                %Negative
                sign = -1;
            else
                %Positive
                sign = 1;
            end
            
            output{i,2} = sign*bin2dec(output{i,1}(2:end))/(2^divideby);
        otherwise
            output{i,2} = 'error';
    end
    output{i,3} = bin2dec(output{i,1});
    output{i,4} = dec2hex(output{i,3});
    output{i,5} = output{i,3}/2^8;
    output{i,6} = output{i,3}/2^10;
end
% fclose(S1)
settings = cell2struct(output(:,2),SettingsDefinition(:,1),1);