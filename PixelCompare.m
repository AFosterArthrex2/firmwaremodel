%Compares errors between two images

a = find(diffDP4k);

[row col chan] = ind2sub(size(diffDP4k),a);

Start = round(length(a)/3*2);
Limit = 100;

for i = 1:Limit
    index = Start+i;
    table(i,:) = [double(IDP4ks(row(index),col(index),chan(index))) double(IDP4k_cap(row(index),col(index),chan(index))) diffDP4k(row(index),col(index),chan(index)) chan(index)] ;
end