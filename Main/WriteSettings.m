
%G2 Register Writing Script
SerialWriteCCU(Port,1,9,'00000000',S1);

if(~exist('S1','var'))
    S1 = serial(Port,'BaudRate',9600);
end
closeWhenDone = 0;
if(strcmpi('closed',S1.status))
    fopen(S1);
    closeWhenDone = 1;
end
%TPG_Mode
SerialWriteCCU(Port,1,256*4+128,[dec2hex(settings.TPG_Mode,1) '0000000'],S1);
if(settings.TPG_Mode == 4)
    SerialBitWriteCCU(Port,1,256*4+8,'0',16,S1);
else
    SerialBitWriteCCU(Port,1,256*4+8,'1',16,S1);
end
%Median_Filter
SerialBitWriteCCU(Port,1,9,dec2bin(~settings.Median_Filter_Enable),1,S1);
%Black_Offset
SerialBitWriteCCU(Port,1,9,dec2bin(~settings.Black_Offset_Enable),2,S1);
SerialBitWriteCCU(Port,1,40,dec2bin(settings.Black_Offset_Fixed),30,S1);
SerialBitWriteCCU(Port,1,40,dec2bin(settings.Black_Offset_RedLevel,12),[11:-1:0],S1);
SerialBitWriteCCU(Port,1,41,dec2bin(settings.Black_Offset_GreenLevel,12),[11:-1:0],S1);
SerialBitWriteCCU(Port,1,41,dec2bin(settings.Black_Offset_BlueLevel,12),[27:-1:16],S1);
%Column_Gains
SerialBitWriteCCU(Port,1,9,dec2bin(~settings.Column_Gains),3,S1);
%AEC_Gain
switch settings.AEC_Enable
    case 0
        SerialBitWriteCCU(Port,1,9,'1',4,S1);
    case 1
        SerialBitWriteCCU(Port,1,9,'0',4,S1);
        SerialBitWriteCCU(Port,1,256*4+10,'10',[4:-1:3],S1); %Gain Mode
    case 2
        SerialBitWriteCCU(Port,1,9,'0',4,S1);
        SerialBitWriteCCU(Port,1,256*4+10,'00',[4:-1:3],S1); %Index Mode
    case 3
        SerialBitWriteCCU(Port,1,9,'0',4,S1);
        SerialBitWriteCCU(Port,1,256*4+9,'01',[4:-1:3],S1); %Auto Mode
    otherwise
end
SerialBitWriteCCU(Port,1,256*4+11,dec2bin(settings.AEC_Gain,15),[30:-1:16],S1);
SerialBitWriteCCU(Port,1,256*4+11,dec2bin(settings.AEC_Index,10),[9:-1:0],S1);

%WB_Gain_Red
SerialBitWriteCCU(Port,1,15,dec2bin(settings.WB_Gain_Red,13),[12:-1:0],S1);
SerialBitWriteCCU(Port,1,32,dec2bin(settings.WB_Gain_Red,13),[12:-1:0],S1);

%WB_Gain_Green
SerialBitWriteCCU(Port,1,16,dec2bin(settings.WB_Gain_Green,13),[12:-1:0],S1);
SerialBitWriteCCU(Port,1,33,dec2bin(settings.WB_Gain_Green,13),[12:-1:0],S1);

%WB_Gain_Blue
SerialBitWriteCCU(Port,1,17,dec2bin(settings.WB_Gain_Blue,13),[12:-1:0],S1);
SerialBitWriteCCU(Port,1,34,dec2bin(settings.WB_Gain_Blue,13),[12:-1:0],S1);
SerialBitWriteCCU(Port,1,94,dec2bin(settings.WB_Gain_Blue,13),[12:-1:0],S1);
SerialBitWriteCCU(Port,1,95,dec2bin(settings.WB_Gain_Blue,13),[12:-1:0],S1);
SerialBitWriteCCU(Port,1,96,dec2bin(settings.WB_Gain_Blue,13),[12:-1:0],S1);
SerialBitWriteCCU(Port,1,97,dec2bin(settings.WB_Gain_Blue,13),[12:-1:0],S1);

%Pixel_Replace 
SerialBitWriteCCU(Port,1,9,dec2bin(~settings.Pixel_Replace_Enable),6,S1);
%Bilateral Seperate Control
SerialBitWriteCCU(Port,1,44,'1',12,S1);
%Bilateral_Filter_Enable
SerialBitWriteCCU(Port,1,9,dec2bin(~settings.Bilateral_Filter_Enable),7,S1);
%Bilateral_Bit_Select
SerialBitWriteCCU(Port,1,44,dec2bin(settings.Bilateral_Bit_Select,2),[27 26],S1);
%Bilateral_Slope
SerialBitWriteCCU(Port,1,9,dec2bin(~settings.Bilateral_Slope_Enable),8,S1);
%Bilateral_Slope_Thresh
SerialBitWriteCCU(Port,1,44,dec2bin(settings.Bilateral_Slope_Thresh,8),[7:-1:0],S1);
%Bilateral_Slope_Bit_Select
SerialBitWriteCCU(Port,1,44,dec2bin(settings.Bilateral_Slope_Bit_Select,2),[11 10],S1);
%Green_Offset
SerialBitWriteCCU(Port,1,9,dec2bin(~settings.Green_Offset_Enable),9,S1);
%Vertical_Binning
SerialBitWriteCCU(Port,1,9,dec2bin(~settings.Vertical_Binning),10,S1);
%Green_Edge_Enhance_Enable
SerialBitWriteCCU(Port,1,9,dec2bin(~settings.Green_Edge_Enhance_Enable),11,S1);
SerialBitWriteCCU(Port,1,50,dec2bin(~settings.Green_Edge_Enhance_Enable),0,S1);
%Green_Edge_Enhance_Gain	
SerialBitWriteCCU(Port,1,50,dec2bin(settings.Green_Edge_Enhance_Gain,13),[28:-1:16],S1);
%Green_Edge_Enhance_Thresh_Enable	
SerialBitWriteCCU(Port,1,51,dec2bin(settings.Green_Edge_Enhance_Thresh_Enable),31,S1);
%Green_Edge_Enhance_Threshold
SerialBitWriteCCU(Port,1,51,dec2bin(settings.Green_Edge_Enhance_Threshold,10),[9:-1:0],S1);
%Disable Attenuation Based on AEC
SerialBitWriteCCU(Port,1,50,'1',31,S1);

%[  CSC_1_1	CSC_1_2 CSC_1_3;
%   CSC_2_1	CSC_2_2 CSC_2_3;
%   CSC_3_1	CSC_3_2 CSC_3_3];

SerialBitWriteCCU(Port,1,9,'0',[12],S1);
%CSC_1_1	
%CSC_1_2	
SerialWriteCCU(Port,1,58,[settings.CSC_1_2 settings.CSC_1_1],S1);
%CSC_1_3	
%CSC_2_1	
SerialWriteCCU(Port,1,59,[settings.CSC_2_1 settings.CSC_1_3],S1);
%CSC_2_2	
%CSC_2_3	
SerialWriteCCU(Port,1,60,[settings.CSC_2_3 settings.CSC_2_2],S1);
%CSC_3_1	
%CSC_3_2	
SerialWriteCCU(Port,1,61,[settings.CSC_3_2 settings.CSC_3_1],S1);
%CSC_3_3	
SerialWriteCCU(Port,1,62,['0000' settings.CSC_3_3],S1);

%CSC_1_1	
%CSC_1_2	
SerialWriteCCU(Port,1,63,[settings.CSC_1_2 settings.CSC_1_1],S1);
%CSC_1_3	
%CSC_2_1	
SerialWriteCCU(Port,1,64,[settings.CSC_2_1 settings.CSC_1_3],S1);
%CSC_2_2	
%CSC_2_3	
SerialWriteCCU(Port,1,65,[settings.CSC_2_3 settings.CSC_2_2],S1);
%CSC_3_1	
%CSC_3_2	
SerialWriteCCU(Port,1,66,[settings.CSC_3_2 settings.CSC_3_1],S1);
%CSC_3_3	
SerialWriteCCU(Port,1,67,['0000' settings.CSC_3_3],S1);

%Gamma_1080
if(settings.Gamma >= 1)
    SerialBitWriteCCU(Port,1,9,dec2bin(0),13,S1);
    SerialBitWriteCCU(Port,1,80,dec2bin(settings.Gamma-1,3),[30:-1:28],S1);
else
    SerialBitWriteCCU(Port,1,9,dec2bin(1),24,S1);
end
%Peaking_Gain_1080
SerialBitWriteCCU(Port,1,9,'0',[25],S1);
SerialBitWriteCCU(Port,1,80,'1',[12],S1);
SerialBitWriteCCU(Port,1,80,dec2bin(settings.Peaking_Gain,10),[9:-1:0],S1);
%Sharpening_Gain_1080
SerialBitWriteCCU(Port,1,81,dec2bin(settings.Sharpening_Gain,10),[9:-1:0],S1);
%Gamma_4K
if(settings.Gamma >= 1)
    SerialBitWriteCCU(Port,1,9,dec2bin(0),13,S1);
    SerialBitWriteCCU(Port,1,68,dec2bin(settings.Gamma-1,3),[30:-1:28],S1);
else
    SerialBitWriteCCU(Port,1,9,dec2bin(1),13,S1);
end
%Peaking_Gain_4K
SerialBitWriteCCU(Port,1,68,dec2bin(settings.Peaking_Gain,10),[9:-1:0],S1);
%Sharpening_Gain_4K
SerialBitWriteCCU(Port,1,69,dec2bin(settings.Sharpening_Gain,10),[9:-1:0],S1);
SerialBitWriteCCU(Port,1,74,dec2bin(settings.Sharpening_Gain,10),[9:-1:0],S1);
%TC_White_Clip
SerialBitWriteCCU(Port,1,9,dec2bin(~settings.TC_White_Clip),15,S1);

%Crops
SerialBitWriteCCU(Port,1,256*4+45,dec2bin(settings.Crop_Proc_Pixel,7),[30:-1:24],S1);
SerialBitWriteCCU(Port,1,256*4+45,dec2bin(settings.Crop_Proc_Line,7),[22:-1:16],S1);
SerialBitWriteCCU(Port,1,256*4+45,dec2bin(settings.Crop_HDMI_Pixel,7),[14:-1:8],S1);
SerialBitWriteCCU(Port,1,256*4+45,dec2bin(settings.Crop_HDMI_Line,7),[6:-1:0],S1);

%Head_Type
SerialBitWriteCCU(Port,1,256*4+9,'11',[2 1],S1);
SerialBitWriteCCU(Port,1,256*4+9,settings.Head_Type,0,S1);

%Demosaic_Force_Disable
SerialBitWriteCCU(Port,1,256*4+49,settings.Demosaic_Force_Disable,0,S1);

%Force_TP_Error
% SerialBitWriteCCU(Port,1,256*4+131,settings.Force_TP_Error,26,S1);
% SerialBitWriteCCU(Port,1,256*4+131,'11',[1:-1:0],S1);

%Downscale_GEE_Enable
% SerialBitWriteCCU(Port,1,256*4+181,settings.Downscale_GEE_Enable,1,S1);

%Downscale_Filter_Enable
% SerialBitWriteCCU(Port,1,256*4+181,settings.Downscale_Filter_Disable,0,S1);

%Enable BioOptico
SerialBitWriteCCU(Port,1,256*1+50,or(settings.BioOptico_Spectral,settings.BioOptico_Structure),0,S1);

%Enable BioOptico Write/Read
SerialBitWriteCCU(Port,1,256*4+50,'00',[9:-1:8],S1);

%Choose Spectral or Structure
SerialBitWriteCCU(Port,1,256*1+50,settings.BioOptico_Spectral,1,S1);
SerialBitWriteCCU(Port,1,256*1+50,[settings.StructureScalerBilinear settings.SpectralScalerBilinear],[9 8],S1);

%BioOptico Spectral Coefficents
SerialWriteCCU(Port,1,256*1+51,[settings.B1 settings.B0],S1);
SerialWriteCCU(Port,1,256*1+52,[settings.B3 settings.B2],S1);

% %BioOptico Spectral Limits
% SerialWriteCCU(Port,1,256*1+51,[settings.D1 settings.D0],S1);
% SerialWriteCCU(Port,1,256*1+52,[settings.D3 settings.D2],S1);

%BioOptico Structure Thresholds
SerialWriteCCU(Port,1,256*1+56,['0' dec2hex(settings.LuminanceThreshold,3) '0000'],S1);
SerialWriteCCU(Port,1,256*1+62,['00000' dec2hex(settings.Vthreshold,3)],S1);

%Fiber Scope
SerialWriteCCU(Port,1,256*0+46,['0000000' dec2hex(settings.FiberScopeEnable,1)],S1);
SerialWriteCCU(Port,1,256*0+70,[settings.FiberScope2 settings.FiberScope1],S1);
SerialWriteCCU(Port,1,256*0+71,[settings.FiberScope4 settings.FiberScope3],S1);
SerialWriteCCU(Port,1,256*0+72,[settings.FiberScope6 settings.FiberScope5],S1);

%Disable WatchDog
SerialBitWriteCCU(Port,1,256*2+34,'1',7,S1);

%Force Head Detect
SerialBitWriteCCU(Port,1,256*4+9,'1',2,S1);

%Electronic Zoom
SerialWriteCCU(Port,1,256*0+88,'00000000',S1);

%Frame_Integration
SerialBitWriteCCU(Port,1,256*4+185,settings.Frame_Integration,12,S1);

%Sharp_Att
SerialBitWriteCCU(Port,1,256*4+69,~settings.Sharp_Att,20,S1);

%1CMOS median filter disable
SerialBitWriteCCU(Port,1,0*4+9,'1',16,S1);

%1CMOS dark pixel replace disable
SerialBitWriteCCU(Port,1,0*4+41,'0',30,S1);


%CMOS Sensors
% if(isfield(settings,'Analog_Gain_Red'))
%     CMOSregWrite(Port,[settings.Analog_Gain_Red;...
%                    settings.Analog_Gain_Green;...
%                    settings.Analog_Gain_Blue],...
%                   [settings.Column_Gain_Red;...
%                    settings.Column_Gain_Green;...
%                    settings.Column_Gain_Blue],S1)
% end
if(closeWhenDone)
    fclose(S1);
    closeWhenDone = 0;
end

% SerialWriteCCU(Port,1,9,'0000FFC6',S1);