%% ModelG2
% Main Script for G2 Firmware Model
%%

%Alex Foster
%11-04-13
% prev_cap = I1080_cap;
%%%%%%%%%%%%%%%% MODEL %%%%%%%%%%%%%%%%%%%%%%
SetupPath
Port = 'COM5';
InternalCap = true;

if(~exist('S1','var') || ~isvalid(S1))
    S1 = serial(Port,'BaudRate',9600);
end

if(strcmp(S1.status,'closed'))
    fopen(S1);
end

%100 percent LED output
LEDShutterReg = SerialReadCCU(Port,1,31+1*256,S1);
LEDShutterBin(4) = '0';
SerialWriteCCU(Port,1,31+1*256,dec2hex(bin2dec(LEDShutterBin),8),S1);

%Turn Off Overlay
for i = 192:239
    overlayRegs(i,:) = SerialReadCCU([],1,i+2*256,S1);
    SerialWriteCCU([],1,i+2*256,'00000000',S1);
end

inputSpreadSheet = 'SettingsBioOptio.xlsx';

saveImDir = 'C:\Users\AFoster\Pictures\FWModel Images\';

[a b c] = xlsread(inputSpreadSheet);
if(~exist('d','var'))
    d = c;
end
ResultsCol1080 = size(c,2)+1;
d{1,ResultsCol1080} = 'Results 1080p';
d{2,ResultsCol1080} = 'int';

ResultsCol12bit = size(c,2)+2;
d{1,ResultsCol12bit} = 'Results 12 bit';
d{2,ResultsCol12bit} = 'int';

ResultsColDP4k = size(c,2)+3;
d{1,ResultsColDP4k} = 'Results DisplayPort 4k';
d{2,ResultsColDP4k} = 'int';

ResultsCol1080D = size(c,2)+4;
d{1,ResultsCol1080D} = 'Results Downsampled 1080p';
d{2,ResultsCol1080D} = 'int';

ResultsCol4kint = size(c,2)+5;
d{1,ResultsCol4kint} = 'Results Internal 4k';
d{2,ResultsCol4kint} = 'int';

ResultsCol1080int = size(c,2)+6;
d{1,ResultsCol1080int} = 'Results Internal 1080';
d{2,ResultsCol1080int} = 'int';

ResultsColPip = size(c,2)+7;
d{1,ResultsColPip} = 'Results Pip';
d{2,ResultsColPip} = 'int';

ResultsColThumb = size(c,2)+8;
d{1,ResultsColThumb} = 'Results Thumbnail';
d{2,ResultsColThumb} = 'int';

ResultsColThumbPip = size(c,2)+9;
d{1,ResultsColThumbPip} = 'Results Thumbnail Pip';
d{2,ResultsColThumbPip} = 'int';


ResultsRetry = size(c,2)+10;
d{1,ResultsRetry} = 'Attempts';
d{2,ResultsRetry} = 'int';


fprintf('UHD4 Firmware Model.\nUsing Test Case Spreadsheet: %s\n',inputSpreadSheet);
errors = [];

maxRetry = 5;
complete = zeros(1,183);
% for testCase = 4
% for testCase = sort([1+4*[0:24] 2+4*[0:24] 3+4*[0:24]]) %No Live Images
for testCase = 1:96   % All Test Cases
    retry = 0;
    while((~complete(testCase)&&retry<=maxRetry))
        if(retry > 0)
            fprintf('|%02d| Retry #%d.\n',testCase,retry);
        end
        %Get Settings from excel
        readSettingsFromExcel
        
        if(mod(length(settings.Comments),2) == 0)
            settings.Comments = sprintf('Test Case #%02d, %s-',testCase,settings.Comments);
        else
            settings.Comments = sprintf('Test Case #%02d, %s',testCase,settings.Comments);
        end
        
        padamount = floor(25-length(settings.Comments)/2);
        
        settings.Comments = char(padarray(double(settings.Comments),[0 padamount],double('-')));
        fprintf('|%02d|%s|%02d|\n',testCase,settings.Comments,testCase);
        
        %%%%%%%%%%% Firmware Simulation %%%%%%%%%%%%%%%555
        fprintf('|%02d|Running Firmware Model Simulation\n',testCase)
        SimulateFirmware
                
        %%%%%%%%%%%%%%%% Setting CCU REGS %%%%%%%%%%%%%%%
        fprintf('|%02d|Setting CCU Registers\n',testCase)
        WriteSettings
        
        %%%%%%%%%%%%%%% CAPTURE %%%%%%%%%%%%%%
        if(InternalCap)
            fprintf('|%02d|Internal Capture\n',testCase)
            try
                InternalCapture2
%                 fwrite(S2,sprintf('sudo rm /var/lib/log/*.rgb\r\n'));
%                 pause(1)
%                 lastread = char(fread(S2,S2.BytesAvailable)');
                
            catch err
                err
            end
        end
        fprintf('|%02d|Grabing Images\n',testCase)
        [I1080_cap] = G2Capture(17,vid,Port,S1);
%         I1080_cap = padarray(I1080_cap(:,2:end,:),[0 1],0,'post');
        [I12bit_cap] = G2Capture(18,vid,Port,S1);
%         I12bit_cap = padarray(I12bit_cap(:,2:end,:),[0 1],0,'post');
        [IDP4k_cap] = G2Capture(9,vid,Port,S1);
%         IDP4k_cap = padarray(IDP4k_cap(:,2:end,:),[0 1],0,'post');
        [I1080D_cap] = G2Capture(14,vid,Port,S1);
%         I1080D_cap = padarray(I1080D_cap(:,2:end,:),[0 1],0,'post');
        if(settings.BioOptico_Spectral || settings.BioOptico_Structure)
            BioOpticoCapture
        end
        %unFreeze Video
        SerialBitWriteCCU(Port,1,4*256+161,'0',[18],S1);
        
        %View Live Video
        SerialBitWriteCCU(Port,1,4*256+8,'0',[15],S1);
        %%%%%%%%% Calculate Differences %%%%%%%
        diff1080 = double(I1080_cap)- double(I1080);        
        diff12bit = double(I12bit_cap)- double(I12bit);
        diffDP4k = double(IDP4k_cap) - double(IDP4ks);
        diff1080D = double(I1080D_cap) - double(I1080Ds);
        
        if(InternalCap)
            diff4kint = double(I4kint) - double(IDP4ks);
            diff1080int = double(I1080int) - double(I1080Ds);
            diffPip = double(I1080pip) - double(I1080int);
            diffThumb = double(Ithumb) - double(I1080int(1:4:end,1:4:end,:));
            diffThumbPip = double(Ithumbpip) - double(I1080pip(1:4:end,1:4:end,:));
        end
        
        %Mask Out Borders
        diff1080([1:6 end-5:end],:,:) = 0;
        diff12bit([1:6 end-5:end],:,:) = 0;
        diffDP4k([1:12 end-12:end],:,:) = 0;
        diff1080D([1:6 end-5:end],:,:) = 0;
        
        if(InternalCap)
            diff4kint([1:12 end-12:end],:,:) = 0;
            diff1080int([1:6 end-5:end],:,:) = 0;
        end
        
        imwrite(I1080Ds,sprintf(   '%sCase#%03d_Simulation_Downscale.png',saveImDir,testCase));
        imwrite(I1080D_cap,sprintf('%sCase#%03d_Captured_Downscale.png',saveImDir,testCase));
        imwrite(diff1080D,sprintf( '%sCase#%03d_Difference_Downscale.png',saveImDir,testCase));
        if(InternalCap)
            imwrite(I1080int,sprintf('%sCase#%03d_Internal_Downscale.png',saveImDir,testCase));
            imwrite(I1080pip,sprintf('%sCase#%03d_Internal_PIP.png',saveImDir,testCase));
            imwrite(Ithumb,sprintf('%sCase#%03d_Internal_ThumbNail.png',saveImDir,testCase));
            imwrite(Ithumbpip,sprintf('%sCase#%03d_Internal_ThumbNail_PIP.png',saveImDir,testCase));
        end
        imwrite(IDP4ks,sprintf(   '%sCase#%03d_Simulation_DP4k.png',saveImDir,testCase));
        imwrite(IDP4k_cap,sprintf('%sCase#%03d_Captured_DP4k.png',saveImDir,testCase));
        imwrite(diffDP4k,sprintf( '%sCase#%03d_Difference_DP4k.png',saveImDir,testCase));
        if(InternalCap)
            imwrite(I4kint,sprintf('%sCase#%03d_Internal_DP4k.png',saveImDir,testCase));
        end
        errors(1,testCase) = length(find(diff1080));
        errors(2,testCase) = length(find(diff12bit));
        errors(3,testCase) = length(find(diffDP4k));
        errors(4,testCase) = length(find(diff1080D));
        if(InternalCap)
            errors(5,testCase) = length(find(diff4kint));
            errors(6,testCase) = length(find(diff1080int));
            errors(7,testCase) = length(find(diffPip));
            errors(8,testCase) = length(find(diffThumb));
            errors(9,testCase) = length(find(diffThumbPip));
        end
        %%%%%%%%%%%Print Results%%%%%%%%%%%%%%
        if(~InternalCap)
            fprintf('|%02d|Errors - 1080p: %09d\n|%02d|Errors - 12bit: %09d\n|%02d|Errors -  DP4k: %09d\n|%02d|Errors - 1080D: %09d\n',...
                testCase,errors(1,testCase),testCase,errors(2,testCase),testCase,errors(3,testCase),testCase,errors(4,testCase))
        else
            fprintf(['|%02d|Errors - 1080p: %09d\n|%02d|Errors - 12bit: %09d' ...
                   '\n|%02d|Errors -  DP4k: %09d\n|%02d|Errors - 1080D: %09d' ...
                   '\n|%02d|Errors - 4kint: %09d\n|%02d|Errors - 1080i: %09d' ...
                   '\n|%02d|Errors - Thumb: %09d\n|%02d|Errors - PipTh: %09d\n'],...
                testCase,errors(1,testCase),testCase,errors(2,testCase),...
                testCase,errors(3,testCase),testCase,errors(4,testCase),...
                testCase,errors(5,testCase),testCase,errors(6,testCase),...
                testCase,errors(7,testCase),testCase,errors(8,testCase))
        end
        fprintf('|%02d|--------------------------------------------------|%02d|\n',testCase,testCase);
        if(errors(:,testCase) == 0)
            complete(testCase) = 1;
        end
        retry = retry +1;
    end
    d{testCase+2,ResultsCol1080} = errors(1,testCase);
    d{testCase+2,ResultsCol12bit} = errors(2,testCase);
    d{testCase+2,ResultsColDP4k} = errors(3,testCase);
    d{testCase+2,ResultsCol1080D} = errors(4,testCase);
    if(InternalCap)
        d{testCase+2,ResultsCol4kint} = errors(5,testCase);
        d{testCase+2,ResultsCol1080int} = errors(6,testCase);
        d{testCase+2,ResultsColPip} = errors(7,testCase);
        d{testCase+2,ResultsColThumb} = errors(8,testCase);
        d{testCase+2,ResultsColThumbPip} = errors(9,testCase);
    end
    d{testCase+2,ResultsRetry} = retry;
    d{testCase+2,ResultsRetry+1} = datestr(now,'yy-mm-dd HH.MM.SS.FFF');
end

%Turn On Overlay
for i = 192:239
    SerialWriteCCU([],1,i+2*256,overlayRegs(i,:),S1);
end
fclose(S1)
% xlswrite('Results.xlsx',d);
% 
% winopen('Results.xlsx')