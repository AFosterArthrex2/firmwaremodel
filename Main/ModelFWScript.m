%% ModelFWScript
% Main Script for G2 Firmware Model
%%

%Alex Foster
%1-30-17

%%%%%%%%%%%%%%%% MODEL %%%%%%%%%%%%%%%%%%%%%%
SetupPath
Port = 'COM4';
InternalCap = true;

if(~exist('S1','var') || ~isvalid(S1))
    S1 = serial(Port,'BaudRate',9600);
end

if(strcmp(S1.status,'closed'))
    fopen(S1);
else
    fclose(S1);
    pause(0.2)
    fopen(S1);
end

%% Initial Setup
%100 percent LED output
SerialWriteCCU(Port,1,42+1*256,'3A50AF5D',S1)
LEDShutterReg = SerialReadCCU(Port,1,31+1*256,S1);
LEDShutterBin = dec2bin(hex2dec(LEDShutterReg));
LEDShutterBin(4) = '0';
SerialWriteCCU(Port,1,31+1*256,dec2hex(bin2dec(LEDShutterBin),8),S1);

%Turn Off Overlay
for i = 192:239
    overlayRegs(i,:) = SerialReadCCU([],1,i+2*256,S1);
    SerialWriteCCU([],1,i+2*256,'00000000',S1);
end


inputSpreadSheet = [pwd '\Settings\' '4k1MosSettings.xlsx'];

%CCU Version
FWVersion = (SerialReadCCU(Port,1,0,S1));
buildNumber = hex2dec(SerialReadCCU(Port,1,4*256+0,S1));
CCUFW = sprintf('850-00%02d-%02d%s S%d',hex2dec(FWVersion(1:2)),hex2dec(FWVersion(3:4)),char(65+hex2dec(FWVersion(5:6))),buildNumber);

datecode = datestr(now,'yymmddHHMMSS');

%Head Version
FWVersion2 = (SerialReadCCU(Port,2,0,S1));
buildNumber2 = hex2dec(SerialReadCCU(Port,2,3,S1));
HeadFW = sprintf('850-00%02d-%02d%s S%d',hex2dec(FWVersion2(1:2)),hex2dec(FWVersion2(3:4)),char(65+hex2dec(FWVersion2(5:6))),buildNumber2);

status = fliplr(dec2bin(hex2dec(SerialReadCCU(Port,1,0*256+11,S1)),32));
if(strcmpi('1',status(7)))
    %4k1MOS Head
    HeadType = '4k1MOS';
elseif(strcmpi('0',status(1)))
    %1MOS Head
    HeadType = '1MOS';
else
    if(strcmpi('0',status(4)))
        %3MOS Head
        HeadType = '3MOS';
    else
        %3MOS HighSens
        HeadType = 'HiSens';
    end
end

%% Get Test Cases From Settings.xlsx
switch HeadType
    case '1MOS'
        inputSpreadSheet = [pwd '\Settings\' '1MosSettings.xlsx'];
        saveImDir = ['G:\Matlab Images\FWModel Images\1MOS_' datecode '\'];
    case '3MOS'
        inputSpreadSheet = [pwd '\Settings\' '3MosSettings.xlsx'];
        saveImDir = ['G:\Matlab Images\FWModel Images\3MOS_' datecode '\'];
    case 'HiSens'
        inputSpreadSheet = [pwd '\Settings\' 'HighSensSettings.xlsx'];
        saveImDir = ['G:\Matlab Images\FWModel Images\HiSens_' datecode '\'];
    case '4k1MOS'
        inputSpreadSheet = [pwd '\Settings\' '4k1MosSettings.xlsx'];
        saveImDir = ['G:\Matlab Images\FWModel Images\4k1MOS_' datecode '\'];
end


% if(~exist(saveImDir,'dir'))
%     mkdir(saveImDir)
% end

[a b c] = xlsread(inputSpreadSheet);
if(~exist('d','var'))
    d = c;
end

ResultsCol1080 = size(c,2)+1;
d{1,ResultsCol1080} = 'Results 1080p';
d{2,ResultsCol1080} = 'int';

ResultsCol12bit = size(c,2)+2;
d{1,ResultsCol12bit} = 'Results 12 bit';
d{2,ResultsCol12bit} = 'int';

ResultsColDP4k = size(c,2)+3;
d{1,ResultsColDP4k} = 'Results DisplayPort 4k';
d{2,ResultsColDP4k} = 'int';

ResultsCol1080D = size(c,2)+4;
d{1,ResultsCol1080D} = 'Results Downsampled 1080p';
d{2,ResultsCol1080D} = 'int';

ResultsCol4kint = size(c,2)+5;
d{1,ResultsCol4kint} = 'Results Internal 4k';
d{2,ResultsCol4kint} = 'int';

ResultsCol1080int = size(c,2)+6;
d{1,ResultsCol1080int} = 'Results Internal 1080';
d{2,ResultsCol1080int} = 'int';

ResultsColPip = size(c,2)+7;
d{1,ResultsColPip} = 'Results Pip';
d{2,ResultsColPip} = 'int';

ResultsColThumb = size(c,2)+8;
d{1,ResultsColThumb} = 'Results Thumbnail';
d{2,ResultsColThumb} = 'int';

ResultsColThumbPip = size(c,2)+9;
d{1,ResultsColThumbPip} = 'Results Thumbnail Pip';
d{2,ResultsColThumbPip} = 'int';

ResultsRetry = size(c,2)+10;
d{1,ResultsRetry} = 'Attempts';
d{2,ResultsRetry} = 'int';

fprintf('UHD4 Firmware Model.\nUsing Test Case Spreadsheet: %s\n',inputSpreadSheet);
errors = [];

maxRetry = 0;
complete = zeros(1,size(c,1));
% for testCase = [ 57:64 77:80]
% for testCase = sort([1+4*[0:24] 2+4*[0:24] 3+4*[0:24]]) %No Live Images
for testCase = [1 ]
% for testCase = [sort([4:4:108])]
% for testCase = [1:(size(c,1)-2)]
    retry = 0;
%     figure;
    while((~complete(testCase)&&retry<=maxRetry))
    subplot(3,3,1);imagesc([]),s{2} = subplot(3,3,2);imagesc([]),s{3} = subplot(3,3,3);imagesc([]),drawnow
        
        if(retry > 0)
            fprintf('|%02d| Retry #%d.\n',testCase,retry);
        end
        %Get Settings from excel
        settings = readSettingsFromExcel(inputSpreadSheet,testCase);
        
        if(mod(length(settings.Comments),2) == 0)
            settings.Comments = sprintf('Test Case #%02d, %s-',testCase,settings.Comments);
        else
            settings.Comments = sprintf('Test Case #%02d, %s',testCase,settings.Comments);
        end
        
        padamount = floor(25-length(settings.Comments)/2);
        
        settings.Comments = char(padarray(double(settings.Comments),[0 padamount],double('-')));
        fprintf('|%02d|%s|%02d|\n',testCase,settings.Comments,testCase);
        
        
        %%%%%%%%%%% Firmware Simulation %%%%%%%%%%%%%%%%%%%
        fprintf('|%02d|Running Firmware Model Simulation\n',testCase)
       
        
        SimulateFirmware
        
        %Crops for Isave{1,1} [Yoffset Xoffset Ysize Xsize]
        IsaveCrop = [15 73 1080 1920];
        yRange = IsaveCrop(1):(IsaveCrop(1)+IsaveCrop(3));
        xRange = IsaveCrop(2):(IsaveCrop(2)+IsaveCrop(4));
        
%         figure(1)
        if(InternalCap)
            s{1} = subplot(3,3,1);imagesc(uint8(IDP4ks)),title('Simulation 4k'),drawnow
            s{4} = subplot(3,3,4);imagesc(imresize(double(Isave{1,1}(yRange,xRange,:))/(2^12-1),2,'nearest')),title('Simulation Start'),drawnow
        else
            s{1} = subplot(3,1,1);imagesc(uint8(IDP4ks)),title('Simulation 4k'),drawnow
        end
                
        %%%%%%%%%%%%%%%% Setting CCU REGS %%%%%%%%%%%%%%%
        fprintf('|%02d|Setting CCU Registers\n',testCase)
        WriteSettingsDef(Port,settings,S1);
        
        %%%%%%%%%%%%%%% CAPTURE %%%%%%%%%%%%%%
        if(InternalCap)
            fprintf('|%02d|Internal Capture\n',testCase)
            try
                [I,Modified,Errors] = CaptureInternal();
                I4kint = I{1};
                I1080int = I{2};
                I1080pip = I{3};
                Ithumb = I{4};
                Ithumbpip = I{5};                
            catch err
                err
            end
        end
        
        fprintf('|%02d|Grabing Images\n',testCase)
        [I1080_cap] = G2Capture(17,vid,Port,S1);
        [I12bit_cap] = G2Capture(18,vid,Port,S1);
        [IDP4k_cap] = G2Capture(9,vid,Port,S1);
        [I1080D_cap] = G2Capture(14,vid,Port,S1);
        
        if(settings.BioOptico_Enable)
            BioOpticoCapture
        end
        
        if(InternalCap)
            s{2} = subplot(3,3,2);imagesc(IDP4k_cap),title('Captured 4k'),drawnow
            s{5} = subplot(3,3,5);imagesc(abs(I4kint)),title('Internal 4k'),drawnow
            s{8} = subplot(3,3,8);imagesc(imresize(abs(I1080pip),2,'nearest')),title('Captured Pip'),drawnow
            s{7} = subplot(3,3,7);imagesc(imresize(I1080_cap,2,'nearest')),title('PreScalar Capture'),drawnow
        else
            s{2} = subplot(3,1,2);imagesc(IDP4k_cap),title('Captured 4k'),drawnow
        end
        %unFreeze Video
        SerialBitWriteCCU(Port,1,4*256+161,'0',[18],S1);
        
        %View Live Video
        SerialBitWriteCCU(Port,1,4*256+8,'0',[15],S1);
        %%%%%%%%% Calculate Differences %%%%%%%
        diff1080 = double(I1080_cap)- double(I1080);        
        diff12bit = double(I12bit_cap)- double(I12bit);
        diffDP4k = double(IDP4k_cap) - double(IDP4ks);
        diff1080D = double(I1080D_cap) - double(I1080Ds);
        
        if(InternalCap)
            diff4kint = double(I4kint) - double(IDP4ks);
            diff1080int = double(I1080int) - double(I1080Ds);
            diffPip = double(I1080pip) - double(I1080int);
            diffThumb = double(Ithumb) - double(I1080int(1:4:end,1:4:end,:));
            diffThumbPip = double(Ithumbpip) - double(I1080pip(1:4:end,1:4:end,:));
        end
        
        %Mask Out Borders
        bordersize = 24;
        
        diff1080([1:bordersize end-bordersize:end],:,:) = 0;
        diff1080(:,[1:bordersize end-bordersize:end],:) = 0;
        
        diff12bit([1:bordersize end-bordersize:end],:,:) = 0;
        diff12bit(:,[1:bordersize end-bordersize:end],:) = 0;
        
        diffDP4k([1:2*bordersize end-2*bordersize:end],:,:) = 0;
        diffDP4k(:,[1:2*bordersize end-2*bordersize:end],:) = 0;
        
        diff1080D([1:bordersize end-bordersize:end],:,:) = 0;
        diff1080D(:,[1:bordersize end-bordersize:end],:) = 0;
        
        if(InternalCap)
            diff4kint([1:2*bordersize end-2*bordersize:end],:,:) = 0;
            diff4kint(:,[1:2*bordersize end-2*bordersize:end],:) = 0;
            
            diff1080int([1:bordersize end-bordersize:end],:,:) = 0;
            diff1080int(:,[1:bordersize end-bordersize:end],:) = 0;
        end
        
        if(InternalCap)
            s{3} = subplot(3,3,3);imagesc(abs(diffDP4k)),title('Difference Captured'),drawnow
            s{6} = subplot(3,3,6);imagesc(abs(diff4kint)),title('Difference Internal'),drawnow
            s{9} = subplot(3,3,9);imagesc(imresize(abs(diffPip),2,'nearest')),title('Difference Pip'),drawnow
        else
            s{3} = subplot(3,1,3);imagesc(abs(diffDP4k)),title('Difference Captured'),drawnow            
        end
        linkaxes([s{:}])
        
        imwrite(I1080Ds,sprintf(   '%sCase#%03d_Simulation_Downscale.png',saveImDir,testCase));
        imwrite(I1080D_cap,sprintf('%sCase#%03d_Captured_Downscale.png',saveImDir,testCase));
        imwrite(diff1080D,sprintf( '%sCase#%03d_Difference_Downscale.png',saveImDir,testCase));
        if(InternalCap)
            imwrite(I1080int,sprintf('%sCase#%03d_Internal_Downscale.png',saveImDir,testCase));
            imwrite(I1080pip,sprintf('%sCase#%03d_Internal_PIP.png',saveImDir,testCase));
            imwrite(Ithumb,sprintf('%sCase#%03d_Internal_ThumbNail.png',saveImDir,testCase));
            imwrite(Ithumbpip,sprintf('%sCase#%03d_Internal_ThumbNail_PIP.png',saveImDir,testCase));
        end
        imwrite(IDP4ks,sprintf(   '%sCase#%03d_Simulation_DP4k.png',saveImDir,testCase));
        imwrite(IDP4k_cap,sprintf('%sCase#%03d_Captured_DP4k.png',saveImDir,testCase));
        imwrite(diffDP4k,sprintf( '%sCase#%03d_Difference_DP4k.png',saveImDir,testCase));
        if(InternalCap)
            imwrite(I4kint,sprintf('%sCase#%03d_Internal_DP4k.png',saveImDir,testCase));
        end
        errors(1,testCase) = length(find(diff1080));
        errors(2,testCase) = length(find(diff12bit));
        errors(3,testCase) = length(find(diffDP4k));
        errors(4,testCase) = length(find(diff1080D));
        if(InternalCap)
            errors(5,testCase) = length(find(diff4kint));
            errors(6,testCase) = length(find(diff1080int));
            errors(7,testCase) = length(find(diffPip));
            errors(8,testCase) = length(find(diffThumb));
            errors(9,testCase) = length(find(diffThumbPip));
        end
        %%%%%%%%%%%Print Results%%%%%%%%%%%%%%
        if(~InternalCap)
            fprintf('|%02d|Errors - 1080p: %09d\n|%02d|Errors - 12bit: %09d\n|%02d|Errors -  DP4k: %09d\n|%02d|Errors - 1080D: %09d\n',...
                testCase,errors(1,testCase),testCase,errors(2,testCase),testCase,errors(3,testCase),testCase,errors(4,testCase))
        else
            fprintf(['|%02d|Errors - 1080p: %09d\n|%02d|Errors - 12bit: %09d' ...
                   '\n|%02d|Errors -  DP4k: %09d\n|%02d|Errors - 1080D: %09d' ...
                   '\n|%02d|Errors - 4kint: %09d\n|%02d|Errors - 1080i: %09d' ...
                   '\n|%02d|Errors -   PIP: %09d\n|%02d|Errors - Thumb: %09d' ...
                   '\n|%02d|Errors - PipTh: %09d\n'],...
                testCase,errors(1,testCase),testCase,errors(2,testCase),...
                testCase,errors(3,testCase),testCase,errors(4,testCase),...
                testCase,errors(5,testCase),testCase,errors(6,testCase),...
                testCase,errors(7,testCase),testCase,errors(8,testCase),...
                testCase,errors(9,testCase))
        end
        fprintf('|%02d|--------------------------------------------------|%02d|\n',testCase,testCase);
        
        if(InternalCap)
            if(errors([1:9],testCase) == 0)
                complete(testCase) = 1;
            end
        else
            if(errors([1:4],testCase) == 0)
                complete(testCase) = 1;
            end
        end
        retry = retry +1;
    end
    d{testCase+2,ResultsCol1080} = errors(1,testCase);
    d{testCase+2,ResultsCol12bit} = errors(2,testCase);
    d{testCase+2,ResultsColDP4k} = errors(3,testCase);
    d{testCase+2,ResultsCol1080D} = errors(4,testCase);
    if(InternalCap)
        d{testCase+2,ResultsCol4kint} = errors(5,testCase);
        d{testCase+2,ResultsCol1080int} = errors(6,testCase);
        d{testCase+2,ResultsColPip} = errors(7,testCase);
        d{testCase+2,ResultsColThumb} = errors(8,testCase);
        d{testCase+2,ResultsColThumbPip} = errors(9,testCase);
    end
    d{testCase+2,ResultsRetry} = retry;
    d{testCase+2,ResultsRetry+1} = datestr(now,'yy-mm-dd HH.MM.SS.FFF');
end

SerialWriteCCU(Port,1,43+1*256,'53FAC90A',S1)
%Turn On Overlay
for i = 192:239
    SerialWriteCCU([],1,i+2*256,overlayRegs(i,:),S1);
end
fclose(S1)

switch HeadType
    case '1MOS'
        xlswrite([pwd '\Results\' 'CCU_' CCUFW '_Head_1MOS_' HeadFW '_' datecode '_Results.xlsx'],d);
        xlswrite([saveImDir 'CCU_' CCUFW '_Head_1MOS_' HeadFW '_' datecode '_Results.xlsx'],d);
    case '3MOS'
        xlswrite([pwd '\Results\' 'CCU_' CCUFW '_Head_3MOS_' HeadFW '_' datecode '_Results.xlsx'],d);
        xlswrite([saveImDir 'CCU_' CCUFW '_Head_3MOS_' HeadFW '_' datecode '_Results.xlsx'],d);
    case 'HiSens'
        xlswrite([pwd '\Results\' 'CCU_' CCUFW '_Head_HiSens_' HeadFW '_' datecode '_Results.xlsx'],d);
        xlswrite([saveImDir 'CCU_' CCUFW '_Head_HiSens_' HeadFW '_' datecode '_Results.xlsx'],d);
    case '4k1MOS'
        xlswrite([pwd '\Results\' 'CCU_' CCUFW '_Head_4kUltra_' HeadFW '_' datecode '_Results.xlsx'],d);
        xlswrite([saveImDir 'CCU_' CCUFW '_Head_4kUltra_' HeadFW '_' datecode '_Results.xlsx'],d);
end
% 