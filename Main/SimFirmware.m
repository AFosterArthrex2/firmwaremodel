function [out, Isave] = SimFirmware(Iin,settings)

%% SimulateFirmware
% Function for Gen2 CCU Firmware Simulation.
%
% Inputs:
%   1. Settings
%   2. Image (1108x2016).
%
% Outputs:
%   1. out (I1080, IDP4ks, ...)
%   2. Isave
%%

I = Iin;

%% 1080p Processing
% Pre-upscale/Offset Processing is performed at 1080p
% Isave keeps a backup of each step
Isave{1,1} = I;%fprintf('|%02d|.',testCase);

I = G2MedianFilter_3MOS(I,settings);
Isave{1,2} = I;fprintf('.');

I = G2BlackOffset(I,settings);
Isave{1,3} = I;fprintf('.');

I = G2AEC(I,settings);
Isave{1,4} = I;fprintf('.');

I = G2PixelReplace(I,settings);
Isave{1,5} = I;fprintf('.');

I = G2Demosaic(I,settings);
Isave{1,6} = I;fprintf('.');

I = G2MedianFilter_1MOS(I,settings);
Isave{1,7} = I;fprintf('.');

I = G2WhiteBalance(I,settings);
Isave{2,1} = I;fprintf('.');

I = G2GreenFlare(I,settings);
Isave{2,2} = I;fprintf('.');

I = G2Bilateral(I,settings);
Isave{2,3} = I;fprintf('.');

I = G2BilateralSlope(I,settings);
Isave{2,4} = I;fprintf('.');

I = G2FiberScopeFilter(I,settings);
Isave{2,5} = I;fprintf('.');

%Crop Before DDR Memory

xOffset = double(settings.Crop_Proc_Pixel);

if(sensorHighSens)
    if(settings.TPG_Mode ~= 0)
        yOffset = double(settings.Crop_Proc_Line_HighSens);
    else
        yOffset = 0;
    end
else
    yOffset = double(settings.Crop_Proc_Line);
end

if(settings.Head_Type == 0)
    I = I((1:1092)+yOffset-1,(1:1936)+xOffset,:);
else
    I = I((1:1092)+yOffset,(1:1936)+xOffset,:);
end
Isave{2,6} = I;fprintf('|');


%% 1080p HDMI output
% No longer used, replaced by downscaled 4k output

%Crop to 1080x1920
xOffset2 = double(settings.Crop_HDMI_Pixel);
yOffset2 = double(settings.Crop_HDMI_Line);
I1080 = I((1:1080)+yOffset2,(1:1920)+xOffset2,:);
Isave{3,1} = I1080;fprintf('.');
I12bit = I1080;
I1080 = G2Gamma1080(I1080,settings);
Isave{3,2} = I1080;fprintf('.');
I1080 = G2RGB2YCbCr1080(I1080,settings);
Isave{3,3} = I1080;fprintf('.');
I1080 = G2SharpAtt(I1080,[]);
Isave{3,4} = I1080;fprintf('.');
I1080 = G2YCbCr2RGB1080(I1080,settings);
Isave{3,5} = I1080;fprintf('|1080p & 12bit Done.\n');
I1080 = uint8(I1080);

%% 4k Processing
% Final Processing is performed at full 4k resolution
% Image is broken up into 4 vertical "stripes" 
     
%Crop 4x 1092x494
% fprintf(['|%02d|            |>'],testCase);
IDP4k{1} = I((1:1092),(1:496)+0,:);
IDP4k{2} = I((1:1092),(1:496)+480,:);
IDP4k{3} = I((1:1092),(1:496)+960,:);
IDP4k{4} = I((1:1092),(1:496)+1440,:);

Isave{4,1} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2Scaler(IDP4k{i},settings); end
Isave{4,2} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2GreenOffset(IDP4k{i},settings); end
Isave{4,3} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2VerticalBinning(IDP4k{i},settings); end
Isave{4,4} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2GreenEdgeEnhance(IDP4k{i},settings); end
Isave{4,5} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2CSC(IDP4k{i},settings); end
Isave{4,6} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2RGB2YCbCr(IDP4k{i},settings); end
Isave{4,7} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2SharpAtt(IDP4k{i},settings); end
Isave{4,8} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2YCbCr2RGB(IDP4k{i},settings); end
Isave{4,9} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2Gamma(IDP4k{i},settings); end
Isave{4,10} = IDP4k;fprintf('|DisplayPort Done.\n');

%% 4k Output
% Assemble 4 stripes into full 4k output

%Crop from 992x2184 to 960x2160
DPoffsetx = 1;
DPoffsety = 1;

IDP4ks = cat(2,IDP4k{1}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
               IDP4k{2}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
               IDP4k{3}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
               IDP4k{4}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:));
     
IDP4ks = uint8(IDP4ks);

%% Downsampled 1080p Output
% Downsample without lowpass filtering to get default 1080p output

Isave{5,1} = Isave{4,10};
% fprintf(['|%02d|                       |>'],testCase);

%LowPass Filter
% for i = 1:4 I1080D{i} = G2LowPass(Isave{3,1}{i},settings.Downscale_Filter_Disable); end
I1080D = Isave{4,10};
Isave{5,2} = I1080D;fprintf('.');

%Crop and Assemble
DSoffsetx = 1;
DSoffsety = 1;

I1080Ds = cat(2,I1080D{1}([13:end-12]+DSoffsety,[17:end-16]+DSoffsetx,:),...
                I1080D{2}([13:end-12]+DSoffsety,[17:end-16]+DSoffsetx,:),...
                I1080D{3}([13:end-12]+DSoffsety,[17:end-16]+DSoffsetx,:),...
                I1080D{4}([13:end-12]+DSoffsety,[17:end-16]+DSoffsetx,:));

Isave{5,3} = I1080Ds;fprintf('.');

I1080Ds = G2DownScale(I1080Ds);
I1080Ds = uint8(I1080Ds);
Isave{5,4} = I1080Ds;
fprintf('|1080 Downscaled Done.\n');

%Output

out.I1080 = I1080;
out.I12bit = I12bit;
out.IDP4ks = IDP4ks;
out.I1080Ds = out.I1080Ds;

end

