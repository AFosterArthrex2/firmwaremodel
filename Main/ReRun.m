%Resim
%Re run current settings

Port = 'COM3';
if(~exist('S1','var') || ~isvalid(S1))
    S1 = serial(Port,'BaudRate',9600);
end

if(strcmp(S1.status,'closed'))
    fopen(S1);
end

%Get Settings from excel
readSettingsFromExcel
if(mod(length(settings.Comments),2) == 0)
    settings.Comments = sprintf('Test Case #%02d, %s-',testCase,settings.Comments);
else
    settings.Comments = sprintf('Test Case #%02d, %s',testCase,settings.Comments);
end

padamount = floor(25-length(settings.Comments)/2);

settings.Comments = char(padarray(double(settings.Comments),[0 padamount],double('-')));
fprintf('|%02d|%s|%02d|\n',testCase,settings.Comments,testCase);

%%%%%%%%%%% Firmware Simulation %%%%%%%%%%%%%%%555
fprintf('|%02d|Running Firmware Model Simulation\n',testCase)
IDP4k = Isave{4,7};
for i = 1:4 IDP4k{i} = G2SharpAtt(IDP4k{i},settings.Sharpening_Gain,settings.Sharp_Att); end
Isave{4,8} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2YCbCr2RGB(IDP4k{i}); end
Isave{4,9} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2Gamma(IDP4k{i},settings.Gamma); end
Isave{4,10} = IDP4k;fprintf('|DisplayPort Done.\n');

%Crop from 992x2184 to 960x2160
DPoffsetx = 1;
DPoffsety = 1;

IDP4ks = cat(2,IDP4k{1}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
               IDP4k{2}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
               IDP4k{3}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
               IDP4k{4}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:));
     

%%%%%%%%%%%%%%%%%%%Downsampled 1080%%%%%%%%%%%%%%%%%%%%%%%%%
Isave{5,1} = Isave{4,10};
fprintf(['|%02d|               |>'],testCase);

%LowPass Filter
% for i = 1:4 I1080D{i} = G2LowPass(Isave{5,1}{i},settings.Downscale_Filter_Disable); end
I1080D = Isave{4,10};
Isave{5,2} = I1080D;fprintf('.');

%Crop and Assemble
DSoffsetx = 1;
DSoffsety = 1;

I1080Ds = cat(2,I1080D{1}([13:end-12]+DSoffsety,[17:end-16]+DSoffsetx,:),...
               I1080D{2}([13:end-12]+DSoffsety,[17:end-16]+DSoffsetx,:),...
               I1080D{3}([13:end-12]+DSoffsety,[17:end-16]+DSoffsetx,:),...
               I1080D{4}([13:end-12]+DSoffsety,[17:end-16]+DSoffsetx,:));

Isave{5,3} = I1080Ds;fprintf('.');

I1080Ds = G2DownScale(I1080Ds);

% I1080D12bit = I1080Ds;
Isave{5,4} = I1080Ds;fprintf('.');

% I1080Ds = G2Gamma(I1080Ds,settings.Gamma_1080);
Isave{5,5} = I1080Ds;fprintf('.');

% I1080Ds = G2RGB2YCbCr(I1080Ds);
Isave{5,6} = I1080Ds;fprintf('.');

% I1080Ds = G2PostPeaking(I1080Ds,settings.Peaking_Gain_4K,settings.Sharpening_Gain_4K);
% I1080Ds = G2PostPeaking(I1080Ds,0,0);
Isave{5,7} = I1080Ds;fprintf('.');

% I1080Ds = G2YCbCr2RGB(I1080Ds);
Isave{5,8} = I1080Ds;fprintf('.');

fprintf('|1080 Downscaled Done.\n');


%Add BioOptico Overlays
if(~or(settings.BioOptico_Spectral,settings.BioOptico_Structure))
%     I1080D_over = I1080Ds;
%     IDP4k_over = IDP4ks;
else
    fprintf(['|%02d|   |>'],testCase);
    %Output of FiberScopeFilter
    IBioOptico = Isave{1,11};
    Isave{6,1} = IBioOptico;fprintf('.');
    
    %Decimate By 2
    IBioOptico = IBioOptico(1:2:end,1:2:end,:);
    Isave{6,2} = IBioOptico;fprintf('.');
    
    %Initial Crop
    xOffset = double(16);
    yOffset = double(4);
    IBioOptico = IBioOptico((1:546)+yOffset,(1:992)+xOffset,:);
    Isave{6,3} = IBioOptico;fprintf('.');
    
    %11x11 Low Pass
    IBioOptico = G2BioOpticoLowPass(IBioOptico);
    Isave{6,4} = IBioOptico;fprintf('.');
    
    %BioOptico Modules
    %             1  2  3  4  5  6  7  8  9 10 11 12
    extrabits = [12  8  8  4  8  4  8  8  4  4  8  8];
    %Extrabits defines the precision to truncate to at the numbered locations.
%     [IBioStructure, IBioStructureAlpha, varianceCombined, meanCombined, Int, ...
%     varianceMid, meanMid] = G2BioOpticoStructure(IBioOptico,...
%         settings.LuminanceThreshold,settings.Vthreshold,extrabits);
%     
    [IBioStructure, IBioStructureAlpha, varianceCombined, meanCombined, Int, ...
    varianceMid, meanMid] = G2BioOpticoStructure(IBioOptico,...
        settings.LuminanceThreshold,settings.Vthreshold,extrabits);
    
    
    Isave{6,5} = IBioStructure;
    
    IBioStructure2(:,:,1) = uint16(IBioStructureAlpha).*uint16(Isave{6,5}*16) + Isave{6,3}(:,:,1).*(1-uint16(IBioStructureAlpha));
    IBioStructure2(:,:,2) = uint16(IBioStructureAlpha).*uint16(Isave{6,5}*16) + Isave{6,3}(:,:,2).*(1-uint16(IBioStructureAlpha));
    IBioStructure2(:,:,3) = uint16(IBioStructureAlpha).*uint16(Isave{6,5}*16) + Isave{6,3}(:,:,3).*(1-uint16(IBioStructureAlpha));
    
    [IBioSpectral] = G2BioOpticoSpectral(IBioOptico,[1 0 0 0 0 0 0],...
        [hex2dec(settings.B0) hex2dec(settings.B1) hex2dec(settings.B2) hex2dec(settings.B3)],...
        [ 64   192   320   384],3685,128);

    Isave{6,6} = IBioSpectral;
    IBioSpectralAlpha = ones(size(Isave{6,6},1),size(Isave{6,6},2));
    IBioSpectralAlpha(sum(Isave{6,6},3) == 0) = 0;
    IBioSpectralAlpha(1:10,:) = 0;
    IBioSpectralAlpha = cat(3,IBioSpectralAlpha,IBioSpectralAlpha,IBioSpectralAlpha);
    IBioSpectral2 = uint16(IBioSpectralAlpha).*uint16(Isave{6,6}*16) + Isave{6,3}.*(1-uint16(IBioSpectralAlpha));
    
    
    
    
    %2x Scaler
    if(settings.StructureScalerBilinear)
        IBioStructure = G2Scaler(IBioStructure);
    else
        IBioStructure = G2ScalerNN(IBioStructure);
    end
    if(settings.SpectralScalerBilinear)
        IBioSpectral = G2Scaler(IBioSpectral);
    else    
        IBioSpectral = G2ScalerNN(IBioSpectral);
    end
    Isave{6,7} = IBioStructure;
    Isave{6,8} = IBioSpectral;
    
    %Final Crop Before Memory
    xOffset = double(32);
    yOffset = double(0);
    IBioStructure = IBioStructure((1:1092)+yOffset,(1:1936)+xOffset,:);
    Isave{6,9} = IBioStructure;fprintf('.');
    IBioSpectral = IBioSpectral((1:1092)+yOffset,(1:1936)+xOffset,:);
    Isave{6,10} = IBioSpectral;fprintf('.');
    
    %Create 4k stripes
    IBioStructure4k{1} = IBioStructure((1:1092),(1:496)+0,:);
    IBioStructure4k{2} = IBioStructure((1:1092),(1:496)+480,:);
    IBioStructure4k{3} = IBioStructure((1:1092),(1:496)+960,:);
    IBioStructure4k{4} = IBioStructure((1:1092),(1:496)+1440,:);
    Isave{6,11} = IBioStructure4k;fprintf('.');
    IBioSpectral4k{1} = IBioSpectral((1:1092),(1:496)+0,:);
    IBioSpectral4k{2} = IBioSpectral((1:1092),(1:496)+480,:);
    IBioSpectral4k{3} = IBioSpectral((1:1092),(1:496)+960,:);
    IBioSpectral4k{4} = IBioSpectral((1:1092),(1:496)+1440,:);
    Isave{6,12} = IBioSpectral4k;fprintf('.');
    %2x Scale NN
    for i = 1:4
        IBioStructure4k{i} = G2ScalerNN(IBioStructure4k{i});
        IBioSpectral4k{i} = G2ScalerNN(IBioSpectral4k{i});
    end
    Isave{6,13} = IBioStructure4k;fprintf('.');
    Isave{6,14} = IBioSpectral4k;fprintf('.');
    %Crop
    DPoffsetx = 1;
    DPoffsety = 0;
    
    IBioStructure4ks = cat(2,...
        IBioStructure4k{1}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioStructure4k{2}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioStructure4k{3}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioStructure4k{4}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:));
     
    IBioSpectral4ks = cat(2,...
        IBioSpectral4k{1}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioSpectral4k{2}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioSpectral4k{3}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioSpectral4k{4}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:));
    Isave{6,15} = IBioStructure4ks;fprintf('.');
    Isave{6,16} = IBioSpectral4ks;fprintf('.');
    
    IBioStructureAlpha4ks = zeros(size(IBioStructure4ks),'uint8');
    IBioStructureAlpha4ks(IBioStructure4ks>0) = 1;
    IBioStructure4ks = cat(3,IBioStructure4ks,IBioStructure4ks,IBioStructure4ks);
    IBioStructureAlpha4ks = cat(3,IBioStructureAlpha4ks,IBioStructureAlpha4ks,IBioStructureAlpha4ks);
    
    IBioSpectralAlpha4ks = ones(size(IBioSpectral4ks,1),size(IBioSpectral4ks,2),'uint8');
    IBioSpectralAlpha4ks(sum(IBioSpectral4ks,3) == 0) = 0;
    IBioSpectralAlpha4ks([1:12 2149:end],:) = 0;
%     IBioSpectralAlpha4ks(:,3840) = 0;
    IBioSpectralAlpha4ks = cat(3,IBioSpectralAlpha4ks,IBioSpectralAlpha4ks,IBioSpectralAlpha4ks);
    
    if(settings.BioOptico_Structure)
        IDP4ks = IBioStructureAlpha4ks.*uint8(IBioStructure4ks) + IDP4ks.*(1-IBioStructureAlpha4ks);
    else
        IDP4ks = IBioSpectralAlpha4ks.*uint8(IBioSpectral4ks) + IDP4ks.*(1-IBioSpectralAlpha4ks);
    end
    
    %Downscale overlays
%     for i = 1:4
%         IBioStructureD{i} = G2LowPass(Isave{6,13}{i},settings.Downscale_Filter_Disable);
%         IBioSpectralD{i} = G2LowPass(Isave{6,14}{i},settings.Downscale_Filter_Disable);
%     end
    IBioStructureD = Isave{6,13};
    IBioSpectralD = Isave{6,14};
    
    DPoffsetx = 1;
    DPoffsety = 0;
    
    IBioStructureDs = cat(2,...
        IBioStructureD{1}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioStructureD{2}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioStructureD{3}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioStructureD{4}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:));
     
    IBioSpectralDs = cat(2,...
        IBioSpectralD{1}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioSpectralD{2}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioSpectralD{3}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioSpectralD{4}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:));
    
    IBioStructureDs = G2DownScale(IBioStructureDs);
    IBioStructureDsAlpha = zeros(size(IBioStructureDs),'uint8');
    IBioStructureDsAlpha(IBioStructureDs>0) = 1;
    IBioStructureDs = cat(3,IBioStructureDs,IBioStructureDs,IBioStructureDs);
    IBioStructureDsAlpha = cat(3,IBioStructureDsAlpha,IBioStructureDsAlpha,IBioStructureDsAlpha);
    
    IBioSpectralDs = G2DownScale(IBioSpectralDs);
    IBioSpectralDsAlpha = ones(size(IBioSpectralDs,1),size(IBioSpectralDs,2),'uint8');
    IBioSpectralDsAlpha(sum(IBioSpectralDs,3) == 0) = 0;
    IBioSpectralDsAlpha([1:6 1075:1080],:) = 0;
    IBioSpectralDsAlpha = cat(3,IBioSpectralDsAlpha,IBioSpectralDsAlpha,IBioSpectralDsAlpha);
    
    if(settings.BioOptico_Structure)
        I1080Ds = IBioStructureDsAlpha.*uint8(IBioStructureDs) + Isave{5,8}.*(1-IBioStructureDsAlpha);
    else
        I1080Ds = IBioSpectralDsAlpha.*uint8(IBioSpectralDs) + Isave{5,8}.*(1-IBioSpectralDsAlpha);
    end
    
    fprintf('|\n');
end

%%%%%%%%% Calculate Differences %%%%%%%
diff1080 = double(I1080_cap)- double(I1080);
diff12bit = double(I12bit_cap)- double(I12bit);
diffDP4k = double(IDP4k_cap) - double(IDP4ks);
diff1080D = double(I1080D_cap) - double(I1080Ds);
% diff4kint = double(I4kint) - double(IDP4ks);
% diff1080int = double(I1080int) - double(I1080Ds);

diff1080([1:6 end-5:end],:,:) = 0;
diff12bit([1:6 end-5:end],:,:) = 0;
diffDP4k([1:12 end-12:end],:,:) = 0;
diff1080D([1:6 end-5:end],:,:) = 0;
% diff4kint([1:12 end-12:end],:,:) = 0;
% diff1080int([1:6 end-5:end],:,:) = 0;

% imwrite(I1080Ds,sprintf(   '%sCase#%03d_Simulation_Downscale.png',saveImDir,testCase));
% imwrite(I1080D_cap,sprintf('%sCase#%03d_Captured_Downscale.png',saveImDir,testCase));
% imwrite(diff1080D,sprintf( '%sCase#%03d_Difference_Downscale.png',saveImDir,testCase));
% imwrite(I1080int,sprintf('%sCase#%03d_Internal_Downscale.png',saveImDir,testCase));
% 
% imwrite(IDP4ks,sprintf(   '%sCase#%03d_Simulation_DP4k.png',saveImDir,testCase));
% imwrite(IDP4k_cap,sprintf('%sCase#%03d_Captured_DP4k.png',saveImDir,testCase));
% imwrite(diffDP4k,sprintf( '%sCase#%03d_Difference_DP4k.png',saveImDir,testCase));
% imwrite(I4kint,sprintf('%sCase#%03d_Internal_DP4k.png',saveImDir,testCase));

errors(1,testCase) = length(find(diff1080));
errors(2,testCase) = length(find(diff12bit));
errors(3,testCase) = length(find(diffDP4k));
errors(4,testCase) = length(find(diff1080D));
% errors(5,testCase) = length(find(diff4kint));
% errors(6,testCase) = length(find(diff1080int));
%%%%%%%%%%%%Print Results%%%%%%%%%%%%%%
fprintf('|%02d|Errors - 1080p: %09d\n|%02d|Errors - 12bit: %09d\n|%02d|Errors -  DP4k: %09d\n|%02d|Errors - 1080D: %09d\n',...
            testCase,errors(1,testCase),testCase,errors(2,testCase),testCase,errors(3,testCase),testCase,errors(4,testCase))
%         fprintf('|%02d|Errors - 1080p: %09d\n|%02d|Errors - 12bit: %09d\n|%02d|Errors -  DP4k: %09d\n|%02d|Errors - 1080D: %09d\n|%02d|Errors - 4kint: %09d\n|%02d|Errors - 1080i: %09d\n',...
%             testCase,errors(1,testCase),testCase,errors(2,testCase),testCase,errors(3,testCase),testCase,errors(4,testCase),testCase,errors(5,testCase),testCase,errors(6,testCase))
fprintf('|%02d|--------------------------------------------------|%02d|\n',testCase,testCase);

fclose(S1)

