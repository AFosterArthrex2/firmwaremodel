Q = imaqhwinfo('winvideo');

rows = ceil(sqrt(length(Q.DeviceIDs)));

columns = ceil(length(Q.DeviceIDs)/rows);

figure

cam = videoinput('winvideo',2,'YUY2_1920x1080','ReturnedColorSpace','rgb');

vidRes = cam.VideoResolution;
hImage = image( zeros(vidRes(2), vidRes(1), 3) );
preview(cam,hImage)
