%Linked Images
function LinkImage(varargin)

if(isempty(varargin))
    error('Not Enough Arugments')
    return
end

% figure;

if(iscell(varargin{end}))
    titles = varargin{end};
    varargin = varargin(1:end-1);
else
    titles = cell(1,length(varargin));
end

%Check for (rows,cols) in first two args.
if(min(size(varargin{1}) == 1))
    x = varargin{2};
    y = varargin{1};
    
    varargin = varargin(3:end);
    nImage = length(varargin);
else
    nImage = length(varargin);
    %number of rows
    x = max(1,floor(sqrt(nImage)));
    y = ceil(nImage/x);
end



for i = 1:nImage
    s{i} = subplot(y,x,i);
    imagesc(varargin{i})
%     colorbar
    title(titles{i})
end

linkaxes([s{:}])