function values = WriteSettingsDef(Port,settings,varargin)

if(~isempty(varargin))
    S1 = varargin{1};
end

if(~exist('S1','var') || ~isvalid(S1))
    S1 = serial(Port,'BaudRate',9600);
end

if(strcmp(S1.status,'closed'))
    fopen(S1);
end

%Use Settings Definition to write to registers

SettingsDefinition = SettingsDef();

for i = 1:size(SettingsDefinition,1)
    if(isfield(settings,SettingsDefinition{i,1}))
        switch SettingsDefinition{i,5}(1:3)
            case 'boo'
                
                value = num2str(settings.(SettingsDefinition{i,1}));

            case 'int'
                
                if(length(SettingsDefinition{i,5}) > 4)
                    divideby = str2double(SettingsDefinition{i,5}(5:end));
                else divideby = 0;
                end
                
                value = dec2bin(floor(2^divideby*settings.(SettingsDefinition{i,1})),length(SettingsDefinition{i,4}));
    
            case 'sin'
                
                if(length(SettingsDefinition{i,5}) > 5)
                    divideby = str2double(SettingsDefinition{i,5}(6:end));
                else divideby = 0;
                end
                
                if(settings.(SettingsDefinition{i,1}) < 0)
                    %Negative
                    signbit = '1';
                else
                    %Positive
                    signbit = '0';
                end
                
                value = [signbit dec2bin(floor(2^divideby*abs(settings.(SettingsDefinition{i,1}))),length(SettingsDefinition{i,4})-1)];

            case '2co'
                %2s compliment
                if(length(SettingsDefinition{i,5}) > 5)
                    divideby = str2double(SettingsDefinition{i,5}(7:end));
                else divideby = 0;
                end
                
                N = length(SettingsDefinition{i,4});
                
                tmp = mod(2^N+2^divideby*settings.(SettingsDefinition{i,1}),2^N);
                
                value = dec2bin(tmp,N);
                
            otherwise
                output{i,2} = 'error';
        end
        SerialBitWriteCCU(Port,1,SettingsDefinition{i,2}*256+...
            SettingsDefinition{i,3},value,SettingsDefinition{i,4},S1);
        values{i} = value;
    end
end
% fclose(S1)