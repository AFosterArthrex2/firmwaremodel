Ivramp = G2TestPatternGenerator8bit([400 400],struct('TPG_Mode',2));

Ivramp2 = imresize(Ivramp,2,'bilinear');
Ivramp4 = imresize(Ivramp,2,'nearest');

Ivramp3 = zeros(800,800,3);
% Ivramp3(1:2:end,1:2:end,:) = Ivramp;

% Ivramp3(1:2:end,2:2:end,:) = (Ivramp3(1:2:end,1:2:end,:) + Ivramp3(1:2:end,[3:2:799 799],:))/2;




for i = 1:2:799     %Rows
    for j = 2:2:800
        Ivramp3(i,j,1) = 1/2 * (Ivramp3(i,j-1,1) + Ivramp3(i,min(j+1,799),1));
        %         Ivramp3(i,j,1) = mod(((i+1)/2)*(2^14),8196)/(8196*2) * (Ivramp3(i,j-1,1) + Ivramp3(i,min(j+1,799),1));
        Ivramp3(i,j,2) = Ivramp3(i,j,1);
        Ivramp3(i,j,3) = Ivramp3(i,j,1);
    end
end


for i = 2:2:800     %Rows
    for j = 1:2:799
        Ivramp3(i,j,1) = 1/2 * (Ivramp3(i-1,j,1) + Ivramp3(min(i+1,799),j,1));
        %         Ivramp3(i,j,1) = mod(((i+1)/2)*(2^14),8196)/(8196*2) * (Ivramp3(i,j-1,1) + Ivramp3(i,min(j+1,799),1));
        Ivramp3(i,j,2) = Ivramp3(i,j,1);
        Ivramp3(i,j,3) = Ivramp3(i,j,1);
    end
end



for i = 2:2:800     %Rows
    for j = 2:2:800
        Ivramp3(i,j,1) = 1/4 * (Ivramp3(i-1,j-1,1) + Ivramp3(i-1,min(j+1,799),1) + Ivramp3(min(i+1,799),j-1,1) + Ivramp3(min(i+1,799),min(j+1,799),1));
        %         Ivramp3(i,j,1) = mod(((i+1)/2)*(2^14),8196)/(8196*2) * (Ivramp3(i,j-1,1) + Ivramp3(i,min(j+1,799),1));
        Ivramp3(i,j,2) = Ivramp3(i,j,1);
        Ivramp3(i,j,3) = Ivramp3(i,j,1);
    end
end