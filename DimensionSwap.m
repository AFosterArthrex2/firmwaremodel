function Iout = DimensionSwap(Iin,varargin)
%Swaps image dimensions
%Default is swap 1st and 3rd dimensions
%Otherwise follow the input vector in varargin{1}

if(length(varargin) == 1)
    vector = varargin{1};
else
    vector = [3 2 1];
end

Iout = permute(Iin,vector);