%Show All Isave

[rows cols] = size(Isave);

figure(2);

for i = 1:rows
    for j = 1:cols
        if(iscell(Isave{i,j}))
            Itemp = double(cat(2,Isave{i,j}{1}([13:end-12]+1,[17:end-16]+1,:),...
                          Isave{i,j}{2}([13:end-12]+1,[17:end-16]+1,:),...
                          Isave{i,j}{3}([13:end-12]+1,[17:end-16]+1,:),...
                          Isave{i,j}{4}([13:end-12]+1,[17:end-16]+1,:)));
        elseif(isempty(Isave{i,j}))
            Itemp = zeros(270,480,3,'double');
        else
            Itemp = double(Isave{i,j});
        end
        if(max(max(max(Itemp))) > 2^8)
            Itemp = double(Itemp/2^12);
        else
            Itemp = double(Itemp/2^8);
        end
%         Icell{i,j} = double(imresize(Itemp,[270 480]));
        Icell{i,j} = double(Itemp(1:270, 1:480,:));
    end
end

Iall = cell2mat(Icell);

imagesc(Iall)