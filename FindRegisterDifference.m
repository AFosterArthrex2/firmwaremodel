ValuesDiff = BadValues == GoodValues;

differences = find(sum(~ValuesDiff,2));

for q = 1:length(differences)
    testCase = differences(q);
    test = 1;
    SerialWriteCCU(Port,1,differences(q)-1,GoodValues(differences(q),:),S1)
    ReCapture
%     pause
    SerialWriteCCU(Port,1,differences(q)-1,BadValues(differences(q),:),S1)
    ReCapture
end