SetupPath
Port = 'COM4';
InternalCap = true;

if(~exist('S1','var') || ~isvalid(S1))
    S1 = serial(Port,'BaudRate',9600);
end
if(strcmp(S1.status,'closed'))
    fopen(S1);
else
    fclose(S1);
    pause(0.2)
    fopen(S1);
end

inputSpreadSheet = 'SensTest.xlsx';
for testCase = [1:8 ]
    settings = readSettingsFromExcel(inputSpreadSheet,testCase);
    WriteSettingsDef(Port,settings,S1);
    cam = videoinput('winvideo',2,'YUY2_1920x1080','ReturnedColorSpace','rgb');
    a{testCase}=getsnapshot(cam);
    figure
    imshow(a{testCase})
end

