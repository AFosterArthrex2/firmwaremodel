function [data_out] = twos_comp(data, bits) 
% twos_comp converts input variable 
% "data" to it's twos complement 
% value. Input variable "data" is 
% expected to be hexadecimal.

data_out = hex2dec(data) ;

if data_out >= 2^(bits - 1) 
data_out = data_out - 2^bits ; 
end