%Reruns simulation then compares to already captured images

figure(1);s{1} = subplot(3,1,1);imagesc([]),s{2} = subplot(3,1,2);imagesc([]),s{3} = subplot(3,1,3);imagesc([]),drawnow


fprintf('|%02d|Running Firmware Model Simulation\n',testCase)
        SimulateFirmware2
        s{1} = subplot(3,1,1);imagesc(uint8(IDP4ks)),title('Simulation'),drawnow
        s{2} = subplot(3,1,2);imagesc(IDP4k_cap),title('Captured'),drawnow
        
        
         %%%%%%%%% Calculate Differences %%%%%%%
        diff1080 = double(I1080_cap)- double(I1080);        
        diff12bit = double(I12bit_cap)- double(I12bit);
        diffDP4k = double(IDP4k_cap) - double(IDP4ks);
        diff1080D = double(I1080D_cap) - double(I1080Ds);
        
        if(InternalCap)
            diff4kint = double(I4kint) - double(IDP4ks);
            diff1080int = double(I1080int) - double(I1080Ds);
            diffPip = double(I1080pip) - double(I1080int);
            diffThumb = double(Ithumb) - double(I1080int(1:4:end,1:4:end,:));
            diffThumbPip = double(Ithumbpip) - double(I1080pip(1:4:end,1:4:end,:));
        end
        
        %Mask Out Borders
        bordersize = 10;
        
        diff1080([1:bordersize end-bordersize:end],:,:) = 0;
        diff12bit([1:bordersize end-bordersize:end],:,:) = 0;
        diffDP4k([1:2*bordersize end-2*bordersize:end],:,:) = 0;
        diff1080D([1:bordersize end-bordersize:end],:,:) = 0;
        
        if(InternalCap)
            diff4kint([1:2*bordersize end-2*bordersize:end],:,:) = 0;
            diff1080int([1:bordersize end-bordersize:end],:,:) = 0;
        end
        
        s{3} = subplot(3,1,3);imagesc(abs(diffDP4k)),title('Difference'),drawnow
        
        errors(1,testCase) = length(find(diff1080));
        errors(2,testCase) = length(find(diff12bit));
        errors(3,testCase) = length(find(diffDP4k));
        errors(4,testCase) = length(find(diff1080D));
        if(InternalCap)
            errors(5,testCase) = length(find(diff4kint));
            errors(6,testCase) = length(find(diff1080int));
            errors(7,testCase) = length(find(diffPip));
            errors(8,testCase) = length(find(diffThumb));
            errors(9,testCase) = length(find(diffThumbPip));
        end
        
        
        %%%%%%%%%%%Print Results%%%%%%%%%%%%%%
        if(~InternalCap)
            fprintf('|%02d|Errors - 1080p: %09d\n|%02d|Errors - 12bit: %09d\n|%02d|Errors -  DP4k: %09d\n|%02d|Errors - 1080D: %09d\n',...
                testCase,errors(1,testCase),testCase,errors(2,testCase),testCase,errors(3,testCase),testCase,errors(4,testCase))
        else
            fprintf(['|%02d|Errors - 1080p: %09d\n|%02d|Errors - 12bit: %09d' ...
                   '\n|%02d|Errors -  DP4k: %09d\n|%02d|Errors - 1080D: %09d' ...
                   '\n|%02d|Errors - 4kint: %09d\n|%02d|Errors - 1080i: %09d' ...
                   '\n|%02d|Errors -   PIP: %09d\n|%02d|Errors - Thumb: %09d' ...
                   '\n|%02d|Errors - PipTh: %09d\n'],...
                testCase,errors(1,testCase),testCase,errors(2,testCase),...
                testCase,errors(3,testCase),testCase,errors(4,testCase),...
                testCase,errors(5,testCase),testCase,errors(6,testCase),...
                testCase,errors(7,testCase),testCase,errors(8,testCase),...
                testCase,errors(9,testCase))
        end
        fprintf('|%02d|--------------------------------------------------|%02d|\n',testCase,testCase);
        