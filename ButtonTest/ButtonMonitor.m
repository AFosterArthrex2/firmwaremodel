%Monitors the rest values in the head and motherboard

TestLength = 5; %in Minutes

HeadRestVals = SerialReadCCU(Port,2,69,S1);

%Disable Beeps
SerialBitWriteCCU(Port,1,24,'11',[21 20],S1)

start = now;

for i = 1:TestLength*60
    pause(i-((now - start)*24*60*60))
    
    output(i).SampleTime = now - start;
    
    HeadRestVals = SerialReadCCU(Port,2,69,S1);
    output(i).HeadRestVal1 = hex2dec(HeadRestVals(5:8));
    output(i).HeadRestVal2 = hex2dec(HeadRestVals(1:4));
    
    output(i).CCURestVal1 = mod(hex2dec(SerialReadCCU(Port,1,22,S1)),2^10);
    output(i).CCURestVal2 = mod(hex2dec(SerialReadCCU(Port,1,23,S1)),2^10);
    
    output(i).DiffVal1 = output(i).HeadRestVal1 - output(i).CCURestVal1;
    output(i).DiffVal2 = output(i).HeadRestVal2 - output(i).CCURestVal2;
end

fprintf('Button 1:\nMax Difference: %d\n Min Difference: %d\nTime Elapsed: %.1f seconds\n'...
    ,max([output(:).DiffVal1]),min([output(:).DiffVal1]),output(i).SampleTime*24*60*60);

fprintf('Button 2:\nMax Difference: %d\n Min Difference: %d\nTime Elapsed: %.1f seconds\n'...
    ,max([output(:).DiffVal2]),min([output(:).DiffVal2]),output(i).SampleTime*24*60*60);


SerialBitWriteCCU(Port,1,24,'00',[21 20],S1)