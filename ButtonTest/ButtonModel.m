%Models the new button algorithm
Port = 'COM5';

if(~exist('S1','var') || ~isvalid(S1))
    S1 = serial(Port,'BaudRate',9600);
end

if(strcmp(S1.status,'closed'))
    fopen(S1);
else
    fclose(S1);
    pause(0.2)
    fopen(S1);
end

FWVersion = (SerialReadCCU(Port,2,0,S1));
buildNumber = hex2dec(SerialReadCCU(Port,2,3,S1));

% currentTime = now;
% datecode = datestr(now,'yymmddHHMMSS');
% fid = fopen([pwd '\ButtonTest\Results\' datecode '.txt'],'w'); 
% 
% fprintf(fid,['Test Performed: ' datestr(currentTime) '\n']);
% fprintf(fid,'Firmware Under Test:\n');
% fprintf(fid,'850-00%02d-%02d Rev%s S%d\n',hex2dec(FWVersion(1:2)),hex2dec(FWVersion(3:4)),char(65+hex2dec(FWVersion(5:6))),buildNumber);
% fprintf(fid,'\n');

%Right or Left
left = 1;

%Set Parameters
b = 515;
D = 256;
m = 'B0';  %-0.3125 in 2's compliment

SerialWriteCCU(Port,2,64,[dec2hex(b,4) dec2hex(D,2) m],S1)

%Enable Test Mode
SerialWriteCCU(Port,2,65,'31120100',S1);

%Disable Beeper
SerialWriteCCU(Port,1,24,'00286424',S1);

% ADC = 0:4095;
% Rest = 0:1023;

ADC = round(linspace(-1023,1023,2^3));
Rest = round(linspace(0,1023,2^3));

output = cell(length(ADC),length(Rest));
pass = zeros(length(ADC),length(Rest));

w = waitbar(0,'Start');

for i = 1:length(ADC) %Full ADC range
    for j = 1:length(Rest)  %Full Rest Value Range
% for i = 4
%     for j = 16
        %Write New Values
        newADC = twos_conv(ADC(i),12);
        newRest = Rest(j);
        
        output{i,j}.Rest = newRest;
        output{i,j}.ADC = newADC;
        output{i,j}.ADC_dec = ADC(i);
        
        reg64b = ['00' dec2bin(b,10) '000' dec2bin(D,9) dec2bin(hex2dec(m),8)];
        reg64h = dec2hex(bin2dec(reg64b),8);
        SerialWriteCCU(Port,2,64,reg64h,S1);
        
        reg65b = [num2str(left) '011' dec2bin(hex2dec(newADC),12) '00000' dec2bin(newRest,11)];
        reg65h = dec2hex(bin2dec(reg65b),8);
        SerialWriteCCU(Port,2,65,reg65h,S1);
        
        output{i,j}.reg68 = SerialReadCCU(Port,2,68,S1);
        output{i,j}.reg69 = SerialReadCCU(Port,2,69,S1);
        output{i,j}.reg70 = SerialReadCCU(Port,2,70,S1);
        output{i,j}.reg71 = SerialReadCCU(Port,2,71,S1);
        output{i,j}.reg72 = SerialReadCCU(Port,2,72,S1);
        output{i,j}.reg73 = SerialReadCCU(Port,2,73,S1);
        
        if(left)
            ADC_FPGA = twos_conv(output{i,j}.reg73(2:4),12);
        else
            ADC_FPGA = twos_conv(output{i,j}.reg73(6:8),12);
        end
        
       %Modl
        
        %Clamp to >0
        RESTmADC = newRest - ADC(i);
        output{i,j}.RmA_Modl = RESTmADC;
        output{i,j}.RmA_FPGA = twos_conv(output{i,j}.reg72(5:8),12);
        
        RESTtM = newRest * twos_conv(m,8)/2^8;
        output{i,j}.RtM_Modl = RESTtM;
        output{i,j}.RtM_FPGA = twos_conv(output{i,j}.reg70(4:8),20)/256;
        
        RESTtMpB = floor(RESTtM + b);
        output{i,j}.RtMpB_Modl = RESTtMpB;
        output{i,j}.RtMpB_FPGA = hex2dec(output{i,j}.reg70(1:3));
        
        Quotient = floor(D*2^12/(RESTtMpB)/2);
        output{i,j}.Quo_Modl = Quotient;
        output{i,j}.Quo_FPGA = hex2dec(output{i,j}.reg71(6:8));
        
        if(RESTmADC < 0)
            RESTmADC = 0;
        end
        KtRESTmADC = floor(Quotient*RESTmADC/2^11);
 
        output{i,j}.KtRmA_Modl = KtRESTmADC;
        output{i,j}.KtRmA_FPGA = hex2dec(output{i,j}.reg72(1:4));
        
        ADCprime = newRest - KtRESTmADC;
        
        output{i,j}.ADC_Modl = round(ADCprime);
        output{i,j}.ADC_FPGA = ADC_FPGA;
        
        if(output{i,j}.ADC_Modl == ADC_FPGA)
            pass(i,j) = 1;
        else
            pass(i,j) = 0;
        end
        
        output{i,j}.pass = pass(i,j);
    end
    waitbar(i/length(ADC),w,sprintf('%2.0f%% Complete',i/length(ADC)*100))
end

delete(w)