%AEC Test Script

%Alex Foster
%5-18-16

%%%%%%%%%%%%%%%% MODEL %%%%%%%%%%%%%%%%%%%%%%
SetupPath
Port = 'COM4';
InternalCap = true;

if(~exist('S1','var') || ~isvalid(S1))
    S1 = serial(Port,'BaudRate',9600);
end

if(strcmp(S1.status,'closed'))
    fopen(S1);
else
    fclose(S1);
    pause(0.2)
    fopen(S1);
end

%% Initial Setup
%100 percent LED output
LEDShutterReg = SerialReadCCU(Port,1,31+1*256,S1);
LEDShutterBin = dec2bin(hex2dec(LEDShutterReg));
LEDShutterBin(4) = '0';
SerialWriteCCU(Port,1,31+1*256,dec2hex(bin2dec(LEDShutterBin),8),S1);

%Turn Off Overlay
for i = 192:239
    overlayRegs(i,:) = SerialReadCCU([],1,i+2*256,S1);
    SerialWriteCCU([],1,i+2*256,'00000000',S1);
end

%% Get Test Cases From Settings.xlsx
inputSpreadSheet = 'SettingsForAECTest.xlsx';

saveImDir = 'C:\Matlab Images\FWModel Images';

[a b c] = xlsread(inputSpreadSheet);
if(~exist('d','var'))
    d = c;
end

%Windows
WinZero = zeros(1108,2016,1);
% WinZero = zeros(1080,1920,1);
%000 - Square
Window{1} = WinZero;
% Line = 206-9; Pixel = 646+13; Lines = 696; Pixels = 780;
Line = 197; Pixel = 659; Lines = 696; Pixels = 780;
Window{1}(Line:(Line+Lines-1),Pixel:(Pixel+Pixels-1),1) = 1;

%001 - Cross
Window{2} = WinZero;
Line = 66; Pixel = 138; Lines = 976; Pixels = 1700;
Window{2}(Line:(Line+Lines-1),Pixel:(Pixel+Pixels-1),1) = 1;

%010 - ?
Window{3} = WinZero;
Line = 66; Pixel = 138; Lines = 976; Pixels = 1700;
Window{3}(Line:(Line+Lines-1),Pixel:(Pixel+Pixels-1),1) = 1;

%011 - Large Rectangle
Window{4} = WinZero;
% Line = 66-9; Pixel = 138+3; Lines = 976; Pixels = 1796;
Line = 57; Pixel = 141; Lines = 976; Pixels = 1796;
Window{4}(Line:(Line+Lines-1),Pixel:(Pixel+Pixels-1),1) = 1;

%100 - ?
Window{5} = WinZero;
Line = 66; Pixel = 138; Lines = 976; Pixels = 1700;
Window{5}(Line:(Line+Lines-1),Pixel:(Pixel+Pixels-1),1) = 1;

%101 - Large Square
Window{6} = WinZero;
% Line = 35-9; Pixel = 504+3; Lines = 1038; Pixels = 1064;
Line = 26; Pixel = 507; Lines = 1038; Pixels = 1064;
Window{6}(Line:(Line+Lines-1),Pixel:(Pixel+Pixels-1),1) = 1;

%110 - 256x256
Window{7} = WinZero;
% Line = 426-9; Pixel = 905+6; Lines = 256; Pixels = 256;
Line = 417; Pixel = 911; Lines = 256; Pixels = 256;
Window{7}(Line:(Line+Lines-1),Pixel:(Pixel+Pixels-1),1) = 1;

%111 - Weighted Cross
Window{8} = WinZero;
Line = 426-9; Pixel = 905+6; Lines = 256; Pixels = 256;
% Line = 400; Pixel = 839; Lines = 256; Pixels = 256;
Window{8}(Line:(Line+Lines-1),Pixel:(Pixel+Pixels-1),1) = 1;



% for testCase = [3+[0:4:16]]
for testCase = [3:4:32]
    %Get Settings from excel
    settings = readSettingsFromExcel(inputSpreadSheet,testCase);
    
    if(mod(length(settings.Comments),2) == 0)
        settings.Comments = sprintf('Test Case #%02d, %s-',testCase,settings.Comments);
    else
        settings.Comments = sprintf('Test Case #%02d, %s',testCase,settings.Comments);
    end
    
    padamount = floor(25-length(settings.Comments)/2);
    
    settings.Comments = char(padarray(double(settings.Comments),[0 padamount],double('-')));
    fprintf('|%02d|%s|%02d|\n',testCase,settings.Comments,testCase);
    
    fprintf('|%02d|Setting CCU Registers\n',testCase)
    WriteSettingsDef(Port,settings,S1);
    SerialBitWriteCCU(Port,1,9,'0',0,S1)
    %Disable Overlays
    SerialBitWriteCCU(Port,1,256*1+50,'0',[0],S1);
    SerialBitWriteCCU(Port,1,256*4+50,'0',[28],S1);
    
    %Disable DeMosaic
    SerialBitWriteCCU(Port,1,256*4+49,'1',0,S1);
    
    %Remeber Register 9
%     reg9 = SerialReadCCU(Port,1,9,S1);
%     SerialWriteCCU(Port,1,256*0+9,'FFFFFFFF',S1);
    
    if(settings.TPG_Mode == 0)
    %Start Buffer
    SerialBitWriteCCU(Port,1,4*256+8,'1',[15],S1);
    pause(1.25)
    
    %Set Crop to 0
%     SerialBitWriteCCU(Port,1,4*256+45,dec2bin(0,7),[22:-1:16],S1)
     end   
    %Freeze Video
    SerialBitWriteCCU(Port,1,4*256+161,'1',[18],S1);
    pause(1.25)
    
    redAvgOfSqr = hex2dec(SerialReadCCU(Port,1,116,S1));
    greenAvgOfSqr = hex2dec(SerialReadCCU(Port,1,117,S1));
    blueAvgOfSqr = hex2dec(SerialReadCCU(Port,1,118,S1));
    
    [I12bit_cap] = G2Capture(22,vid,Port,S1);

    %Set Crop back to settings.Crop_Proc_Line
%     SerialBitWriteCCU(Port,1,4*256+45,dec2bin(settings.Crop_Proc_Line,7),[22:-1:16],S1);
    
    %unFreeze Video
    SerialBitWriteCCU(Port,1,4*256+161,'0',[18],S1);
    
    %View Live Video
    SerialBitWriteCCU(Port,1,4*256+8,'0',[15],S1);
%     SerialWriteCCU(Port,1,9,reg9,S1);
      
    WindowedImage = cat(3,Window{settings.AEC_Window+1},Window{settings.AEC_Window+1},Window{settings.AEC_Window+1}).*double(I12bit_cap);
    AvgSumOfSquares = (squeeze(sum(sum(WindowedImage.^2,1),2))*(settings.AEC_Window_Factor/2^32))';
    
    SumOfSquaresDiff(testCase,:) = (abs(floor(AvgSumOfSquares) - [redAvgOfSqr greenAvgOfSqr blueAvgOfSqr]));
    
    fprintf('|%02d|Errors -   Red: %09.0f\n|%02d|Errors - Green: %09.0f\n|%02d|Errors -  Blue: %09.0f\n',...
        testCase,SumOfSquaresDiff(testCase,1),testCase,SumOfSquaresDiff(testCase,2),testCase,SumOfSquaresDiff(testCase,3))

    fprintf('|%02d|--------------------------------------------------|%02d|\n',testCase,testCase);
end