function CMOSregWrite2(Port,Reg,Data,Colors,S1)
%General CMOS Write

%Red
if(Colors(1))
    SerialWriteCCU(Port,2,7,'00000100',S1)
    SerialWriteCCU(Port,2,6,[Reg Data],S1)
    SerialWriteCCU(Port,2,31,'80000000',S1)
end
%Green
if(Colors(2))
    SerialWriteCCU(Port,2,7,'00000200',S1)
    SerialWriteCCU(Port,2,6,[Reg Data],S1)
    SerialWriteCCU(Port,2,31,'80000000',S1)
end
%Blue
if(Colors(3))
    SerialWriteCCU(Port,2,7,'00000300',S1)
    SerialWriteCCU(Port,2,6,[Reg Data],S1)
    SerialWriteCCU(Port,2,31,'80000000',S1)
end
%Reset SPI setting
SerialWriteCCU(Port,2,7,'00000000',S1)

