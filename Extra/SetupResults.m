ResultsCol(1) = size(c,2)+1;
d{1,ResultsCol(1)} = 'Results SNR(dB) High';
d{2,ResultsCol(1)} = 'res';

ResultsCol(2) = size(c,2)+2;
d{1,ResultsCol(2)} = 'Results SNR(ratio) High';
d{2,ResultsCol(2)} = 'res';

ResultsCol(3) = size(c,2)+3;
d{1,ResultsCol(3)} = 'Results SNR(diff) High';
d{2,ResultsCol(3)} = 'res';

ResultsCol(4) = size(c,2)+4;
d{1,ResultsCol(4)} = 'Results SNR(dB) Low';
d{2,ResultsCol(4)} = 'res';

ResultsCol(5) = size(c,2)+5;
d{1,ResultsCol(5)} = 'Results SNR(ratio) Low';
d{2,ResultsCol(5)} = 'res';

ResultsCol(6) = size(c,2)+6;
d{1,ResultsCol(6)} = 'Results SNR(diff) Low';
d{2,ResultsCol(6)} = 'res';