%Bio Optico Debuging

IDecimateSim = Isave{6,2}(8:end-7,21:end-12,:);

%Compare Low Pass
ILowPassSim = Isave{6,4}(4:end-3,21:end-12,:);

ImeanSimV = meanCombined(4:end-3,21:end-12,:);
ImeanSimH = meanMid(4:end-3,21:end-12,:);

IVarianceSimV = varianceCombined(4:end-3,21:end-12,:);
IVarianceSimH = varianceMid(4:end-3,21:end-12,:);

IThick2 = G2ScalerNN(IThick(4:end-3,21:end-12,:));

LinkImage(diff1080D(:,:,1),I1080Ds,I1080D_cap,BioCodesRawSpec,IThick2(:,:,1),{'Difference','Simulation','Captured','Raw Codes','Thickness'})

% LinkImage(I1080Ds,I1080D_cap,BioCodesRawSpec,diff1080D(:,:,1),{'Simulation','Capture','Raw Codes','Errors'})

% LinkImage(ILowPassSim(:,:,1),LowPassRed,double(LowPassRed)-double(ILowPassSim(:,:,1)),{'LowPass - Simulation','LowPass - Capture','LowPass - Difference'})

% LinkImage(ImeanSimH,HorizMean,double(HorizMean)-ImeanSimH,{'Mean - Simulation','Mean - Capture','Mean - Difference'})
% LinkImage(ImeanSimV,VertMean,double(VertMean)-double(ImeanSimV),{'Mean - Simulation','Mean - Capture','Mean - Difference'})

% LinkImage(IVarianceSimV,VertVar,double(VertVar)-IVarianceSimV,{'Variance - Simulation','Variance - Capture','Variance - Difference'})

% DecRed;

% LinkImage(IDecimateSim(:,:,1),DecRed,double(DecRed)-double(IDecimateSim(:,:,1)),{'Decimated - Simulation','Decimated - Capture','Decimated - Difference'})

% LinkImage(diffDP4k(:,:,1),IDP4ks,IDP4k_cap)