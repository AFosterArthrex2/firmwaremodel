directory = 'C:\Users\AFoster\Pictures\Matlab Images\ColumnCorrection2\';

Rawlist = dir([directory 'Raw\*.png']);

Corlist = dir([directory 'Corrected\*.png']);

Settings4Video;

for l = 2
    if(l == 1)
        subdir = 'Raw\';
        Ilist = Rawlist;
        Odir = 'Processed UnCorrected\';
    else
        subdir = 'Corrected\';
        Ilist = Corlist;
        Odir = 'Processed Corrected\';
    end
    
    if(~exist([directory Odir],'dir'))
        mkdir([directory Odir]);
    end
    
    for i = 1:length(Ilist)
        I = imread([directory subdir Ilist(i).name]);
        SimulateFirmware4Video
        imwrite(I1080,[directory Odir num2str(i,'%04d') '.png'])
    end
end
    