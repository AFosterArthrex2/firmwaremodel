%MakeInterfaceDef
% make an interface for the SettingsDefinition

SettingsDef

fid = fopen('InterfaceStructure.txt','w');

fprintf(fid,'public struct SettingsStruct\r\n');
fprintf(fid,'{\r\n');

%declare variables
for i = 1:size(SettingsDefinition,1)
%     switch SettingsDefinition{i,5}(1:3)
%         case 'boo'
%             type = 'Boolean';
%         otherwise
%             type = 'double';
%     end
    type = 'double';
    fprintf(fid,'\tpublic %s %s;\r\n',type,SettingsDefinition{i,1});

end


% 
% %noarg constructor
% fprintf(fid,'\r\n\r\n')
% fprintf(fid,'\tpublic SettingsStruct()\r\n')
% fprintf(fid,'\t{\r\n')
% 
% %initialize variables
% for i = 1:size(SettingsDefinition,1)
%     fprintf(fid,'\t\t%s = %d;\r\n',SettingsDefinition{i,1},SettingsDefinition{i,6})
% end
% fprintf(fid,'\t}\r\n');

fprintf(fid,'}\r\n');
