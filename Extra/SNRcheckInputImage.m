function [valid, msg] = SNRcheckInputImage(Iin,r,show,varargin)
%Checks input image to SNR algoritm for:
%1. Black and Signal Sections do not vary more than limit1
%   Single outliers are removed
%2. Difference between Signal and Black is greater than limit2
%3. No Saturation
%   Max Saturated Pixels = limit3  

if(~isempty(varargin))
    limit1 = varargin{1};
    limit2 = varargin{2};
    limit3 = varargin{3};
else
    limit1 = 250;
    limit2 = 500;
    limit3 = 5;
end

msg = cell(0,1);

valid = 1;

Yrange = r(1,:);
XrangeSignal = r(2,:);
XrangeBlack = r(3,:);
Signal = cat(3,Iin(Yrange,XrangeSignal,:),rgb2gray(Iin(Yrange,XrangeSignal,:)));
Black = cat(3,Iin(Yrange,XrangeBlack,:),rgb2gray(Iin(Yrange,XrangeBlack,:)));
    
Iover = DrawBox2(zeros(size(Iin,1),size(Iin,2)),...
    [min(XrangeBlack) max(XrangeBlack) min(Yrange) max(Yrange);...
    min(XrangeSignal) max(XrangeSignal) min(Yrange) max(Yrange)]);

for i = 1:3
    if(strcmpi(class(Iin),'uint16'))
        Ishow(:,:,i) = min(1,double(Iin(:,:,i))/2^12+Iover);
    else
        Ishow(:,:,i) = min(1,double(Iin(:,:,i))/2^8+Iover);
    end
end

if(show > 0)
    figure(show);subplot(3,2,1:4),imagesc(Ishow)
    subplot(3,2,5),imagesc(Signal(:,:,4))
    colorbar
    subplot(3,2,6),imagesc(Black(:,:,4))
    colorbar
end

%Check Signal
if(strcmpi(class(Iin),'uint16'))
    [counts,x] = imhist(Signal(:,:,4),2^16);
else
    [counts,x] = imhist(Signal(:,:,4),2^8);
end
counts(counts == 1) = 0;
counts(counts > 1) = 1;
counts(1) = 0;
counts(end) = 0;

if(find(counts.*x,1,'last')-find(counts.*x,1,'first')>limit1)
    valid = 0;
    msg = [msg; ['Limit 1, High Signal. Limit: ' num2str(limit1)...
    ' Measured: ' num2str(find(counts.*x,1,'last')-find(counts.*x,1,'first'))]];
    % uiwait(msgbox('High Signal Limit 1'))
end

%Check Black
if(strcmpi(class(Iin),'uint16'))
    [counts,x] = imhist(Black(:,:,4),2^16);
else
    [counts,x] = imhist(Black(:,:,4),2^8);
end
counts(counts == 1) = 0;
counts(counts > 1) = 1;
counts(1) = 0;
counts(end) = 0;

if(find(counts.*x,1,'last')-find(counts.*x,1,'first')>limit1)
    valid = 0;
    msg = [msg; ['Limit 1, Low Signal. Limit: ' num2str(limit1)...
    ' Measured: ' num2str(find(counts.*x,1,'last')-find(counts.*x,1,'first'))]];
    % uiwait(msgbox('Low Signal Limit 1'))
end

%Check Difference
if(mean(mean(Signal)) - mean(mean(Black)) < limit2)
    valid = 0;
    msg = [msg; ['Limit 2. Limit: ' num2str(limit2)...
    ' Measured: ' num2str(mean(mean(Signal)) - mean(mean(Black)))]];
    
    % uiwait(msgbox('Limit 2'))
end

%Check Saturation
if(strcmpi(class(Iin),'uint16'))
    satLimit = 3844;
else
    satLimit = 255;
end
if(length(find(Signal >= satLimit)) + length(find(Signal == 0)) > limit3)
    valid = 0;
    msg = [msg; ['Limit 3, High Signal. Limit: ' num2str(limit3)...
    ' Measured: ' num2str(length(find(Signal >= satLimit)) + length(find(Signal == 0)))]];
    % uiwait(msgbox('High Signal Limit 3'))
end

if(length(find(Black >= satLimit)) + length(find(Black == 0)) > limit3)
    valid = 0;
    msg = [msg; ['Limit 3, Low Signal. Limit: ' num2str(limit3)...
    ' Measured: ' num2str(length(find(Signal >= satLimit)) + length(find(Signal == 0)))]];
    % uiwait(msgbox('Low Signal Limit 3'))
end

