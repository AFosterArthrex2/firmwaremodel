%Psuedo Random
bSeed = dec2bin(hex2dec('A7819F'),24);

Seed = [];

for i = 1:length(bSeed)
    Seed(i) = logical(str2num(bSeed(i)));
end

I = zeros(2160,960,3);

Q = fliplr(Seed);

for i = 1:2160
    for j = 1:960
        seeds(i,j) = {char(double(squeeze(Q))+48)};
        temp = xor(xor(xor(Q(24),Q(23)),Q(22)),Q(17));
        r = sum(double(Q(17:24)).*(2.^(0:7)));
        g = sum(double(Q(9:16)).*(2.^(0:7)));
        b = sum(double(Q(1:8)).*(2.^(0:7)));
        I(i,j,:) = [r g b];
        Q = [temp Q(1:end-1)];
    end
end

Iout = uint8(I);
% imwrite(Iout,['PrandomImages\' num2str(imgSize(1)) num2str(imgSize(2)) '\' cSeed '.png']);
