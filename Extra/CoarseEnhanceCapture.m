%Coarse Enhance Capture

%Switch to 12bit mode Center of screen
Port = 'COM11';

if(~exist('S1','var'))
    S1 = serial(Port,'BaudRate',9600);
end
closeWhenDone = 0;
if(strcmpi('closed',S1.status))
    fopen(S1);
    closeWhenDone = 1;
end

SerialWriteCCU(Port,1,4*256+46,'00000071',S1);

SerialBitWriteCCU(Port,1,4*256+8,'1100',[11:-1:8],S1);

set(vid,'FramesPerTrigger',300)

pause(1)

start(vid)


pause(10)

SerialWriteCCU(Port,1,4*256+46,'00000000',S1);

SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);

I = getdata(vid);