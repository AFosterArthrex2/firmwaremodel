function Iout = CoarseEdgeEnhance(Iin,gain)

k = [-1 -1 -1 -1 -1 ;...
     -1  2  2  2 -1 ;...
     -1  2  0  2 -1 ;...
     -1  2  2  2 -1 ;...
     -1 -1 -1 -1 -1];

Imax = double(max(Iin,[],3));

Iedge = conv2(Imax,k,'same');

I2 = Imax + Iedge*gain;

Iratio = I2./Imax;

Iout(:,:,1) = uint16(double(Iin(:,:,1)).*Iratio);
Iout(:,:,2) = uint16(double(Iin(:,:,2)).*Iratio);
Iout(:,:,3) = uint16(double(Iin(:,:,3)).*Iratio);




