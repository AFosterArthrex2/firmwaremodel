function out = ColPixRep2(in)
    P = [7 7 7];

    c = in(5,:);
    ct = floor(c/2^2);
    
    in(5,:) = 0;
    

    [m i] = max(in,[],1);
    if(size(in,2) > 1)
        in(sub2ind([9 6808],i, 1:6808)) = 0;
    else
        in(i) = 0;
    end
    [m2 i] = max(in,[],1);
    a = sum(in)*(1/64+1/8);
    
    at = floor(a/2^2)*2^2;
    
    G = floor(103 + double(m2)) - ct;
    gain = floor(G/128*2^P(1))/2^P(1) + floor(G/512*2^P(2))/2^P(2);
    
    gain(ct <= m2) = 1;
    gain(ct >= 103 + m2) = 0;
    
    out = max(0,min(1,floor(gain*2^P(3))/2^P(3)));
%     out = floor((1-gainT).*double(a))+floor((gainT).*double(c));
end