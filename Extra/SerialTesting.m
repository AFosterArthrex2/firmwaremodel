addpath('SSH&Misc\')

Port = 'COM4';

% S1 = serial(Port,'BaudRate',9600);
fopen(S1)

for i = 1:100
    [r(i,1)] = hex2dec(SerialReadCCU(Port,2,23,S1));
    [r(i,2)] = hex2dec(SerialReadCCU(Port,2,22,S1));
    r(i,3) = now;
end

fclose(S1)

x1 = floor(r(:,1)/2^16);
x2 = mod(r(:,1),2^16);
x3 = floor(r(:,2)/2^16);
x4 = mod(r(:,2),2^16);
t = r(:,3);