set(vid,'FramesPerTrigger',200)
set(vid,'LoggingMode','disk')

Writer = VideoWriter('test.avi','Uncompressed AVI');

set(vid,'DiskLogger',Writer)

imaqmem(4000000000)

Port = 'COM4';
if(~exist('S1','var') || ~isvalid(S1))
    S1 = serial(Port,'BaudRate',9600);
end

if(strcmp(S1.status,'closed'))
    fopen(S1);
end
SerialWriteCCU(Port,1,11+4*256,'00000013',S1);
start(vid)
pause(1)
SerialWriteCCU(Port,1,11+4*256,'00000014',S1);
