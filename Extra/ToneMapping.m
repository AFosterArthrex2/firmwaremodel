%Tone Mapping
%Mix of Histogram and Linear Mapping
Port = 'COM4';
S1 = serial(Port,'BaudRate',9600);
fopen(S1)
bitsIn = 14;
bitsOut = 14;

Ic = G2Capture(1,vid,'COM4',S1);
I = G2Capture(2,vid,'COM4',S1);
fclose(S1)
I2 = myRGB2YCbCr(I);
% I2 = rgb2ycbcr((I));

values = [0.01 0.1 0.5 1.0 5 10];

figure;subplot(2,4,1)
imagesc(Ic(:,480:1439,:))
title('8 bit Capture')
subplot(2,4,5)
imagesc(I*16)
title('12 bit Capture')
imgOrder = [2 3 4 6 7 8];

for j = 1:6
[counts C] = hist(reshape(I2(:,:,1),1,[]),1:2^14);
countsC = cumsum(counts);

map = (2^14*counts/sum(counts)+values(j))/(2^14*(1+values(j)));
mapC = round(cumsum(map)*960*1080);

for i = 1:2^14
    out(i) = find(mapC >= countsC(i),1,'first');
end
% for i = 1:2^14
%     for j = 1:2^14
%         if(i == 1526)
%             abc = 1;
%         end
%         
%         if(countsC(i) <= mapC(j))
%             out(i) = j;
%             break
%         end
%     end
% end

I3(:,:,1) = out(I2(:,:,1));

Iout{j} = myYCbCr2RGB(cat(3,I3(:,:,1),I2(:,:,2),I2(:,:,3)));

subplot(2,4,imgOrder(j))
Iout{j} = max(0,Iout{j});
imagesc(Iout{j}/max(max(max(Iout{j}))))
title(['lambda = ' num2str(values(j))])
end
% Iout = ycbcr2rgb(cat(3,I3(:,:,1),I2(:,:,2),I2(:,:,3)));



