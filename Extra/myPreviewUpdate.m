function myPreviewUpdate(obj,event,hImage)
global L
I = uint16(event.Data);
    
Id = ((I(:,1:960,:))*2^4 + (I(:,961:1920,:)));

I = AHistMap(Id,L);

event.Data = I;

set(hImage, 'CData', 16*[Id event.Data])

figure(5);
subplot(2,1,1)
imhist(Id(:,:,2),2^12)
axis([0 2^12 0 10000])
title('Original')
subplot(2,1,2)
imhist(I(:,:,2),2^12)
axis([0 2^12 0 10000])
title('Processed')
end