%Apply Gain Correction

% Itest = double(I(:,:,:,1));
bits = 16;
I3 = I;
for i = 1:size(I2,4)
I3(:,:,:,i) = ApplyGainCorrection(I2(:,:,:,i),ColGainCorrection,bits);
end
I4 = I2-I3;


% figure;imagesc(squeeze(mean(double(I2(:,:,2,:)),4)),[2560 2790])
figure;imagesc(squeeze(mean(double(I3(:,:,2,:)),4)))
NoiseDelta(:,1) = (qSNR(I3(:,:,:,1)) - qSNR(I2(:,:,:,1)))';
