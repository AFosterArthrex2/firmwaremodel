for i = IndexRange(testCase,1):IndexRange(testCase,2)
    SerialWriteCCU(Port,1,11,['0000' dec2hex(i,4)],S1)
    if(i == 453)
        abc = 1;
    end
    %Check captured image
    
    Id = getsnapshot(vid);
    Ic = convert8to12(Id);
    %     imagesc(Ic*16)
    Signal = cat(3,Ic(Yrange,XrangeSignal,:),rgb2gray(Ic(Yrange,XrangeSignal,:)));
    SignalF = zeros(size(Signal));
    Black = cat(3,Ic(Yrange,XrangeBlack,:),rgb2gray(Ic(Yrange,XrangeBlack,:)));
    BlackF = zeros(size(Black));
    
    [valid(testCase,i) msg{testCase,i}] = SNRcheckInputImage(Ic,[Yrange; XrangeSignal; XrangeBlack],0,300,50,20);
    
    [x y] = meshgrid(1:100,1:100);
    indepvar = [reshape(x,[],1) reshape(y,[],1)];
    for k = 1:4
        depvar = double(reshape(Signal(:,:,k),[],1));
        Model = polyfitn(indepvar,depvar,2);
        Model.Coefficients(end) = 0;
        Trend = polyvaln(Model,indepvar);
        SignalF(:,:,k) = reshape(depvar-Trend+mean(mean(Trend)),100,100);
    end
        
    for k = 1:4
        depvar = double(reshape(Black(:,:,k),[],1));
        Model = polyfitn(indepvar,depvar,2);
        Model.Coefficients(end) = 0;
        Trend = polyvaln(Model,indepvar);
        BlackF(:,:,k) = reshape(depvar-Trend+mean(mean(Trend)),100,100);
        
    end
       
    Avg(testCase,:,i) = squeeze(mean(mean(SignalF,1),2)-mean(mean(BlackF,1),2))';
    
    Noise(testCase,:,i) = [std2(SignalF(:,:,1)) std2(SignalF(:,:,2)) std2(SignalF(:,:,3)) std2(SignalF(:,:,4))];
    Noise2(testCase,:,i) = [std2(BlackF(:,:,1)) std2(BlackF(:,:,2)) std2(BlackF(:,:,3)) std2(BlackF(:,:,4))];
    
    SNR(testCase,:,i) = 20*log10(Avg(testCase,:,i)./Noise(testCase,:,i));
    SNR2(testCase,:,i) = 20*log10(Avg(testCase,:,i)./Noise2(testCase,:,i));
    fprintf('%d',i)
    if(mod(i,10) == 0)
        fprintf('\n')
    end
end

figure(testCase);plot(IndexRange(testCase,1):IndexRange(testCase,2),squeeze(SNR2(testCase,1,IndexRange(testCase,1):IndexRange(testCase,2)))','r',...
    IndexRange(testCase,1):IndexRange(testCase,2),squeeze(SNR2(testCase,2,IndexRange(testCase,1):IndexRange(testCase,2)))','g',...
    IndexRange(testCase,1):IndexRange(testCase,2),squeeze(SNR2(testCase,3,IndexRange(testCase,1):IndexRange(testCase,2)))','b',...
    IndexRange(testCase,1):IndexRange(testCase,2),squeeze(SNR2(testCase,4,IndexRange(testCase,1):IndexRange(testCase,2)))','k'),axis([200 600 35 65]),grid on
legend('Red','Green','Blue','Y')

[b a] = find(squeeze(SNR2(testCase,:,:)) == max(valid(testCase,IndexRange(testCase,1):IndexRange(testCase,2)).*squeeze(SNR2(testCase,2,IndexRange(testCase,1):IndexRange(testCase,2)))'))
AvgF(testCase,:) = Avg(testCase,:,a);
NoiseF(testCase,:) = Noise(testCase,:,a);
SNRF(testCase,:) = 20*log10(AvgF(testCase,:)./NoiseF(testCase,:));
Noise2F(testCase,:) = Noise2(testCase,:,a);
SNR2F(testCase,:) = 20*log10(AvgF(testCase,:)./Noise2F(testCase,:));

PeakIndex = a;
