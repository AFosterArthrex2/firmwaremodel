% Noise Measurement Script 8bit
% Alexander Foster
% 12/4/13
% UHD4 project


addpath('PolyfitnTools\')
Port = 'COM4';
% inputSpreadSheet = 'RampSNRSettings.xlsx';
Yrange = 200:299;
XrangeSignal = 200:299;
XrangeBlack = 700:799;
% 
% Repeats = 1;
% 
% [a b c] = xlsread(inputSpreadSheet);
% % if(~exist('d','var'))
%     d = c;
% % end
% ResultsCol(1) = size(c,2)+1;
% d{1,ResultsCol(1)} = 'Results SNR(dB) High';
% d{2,ResultsCol(1)} = 'res';
% 
% ResultsCol(2) = size(c,2)+2;
% d{1,ResultsCol(2)} = 'Results SNR(ratio) High';
% d{2,ResultsCol(2)} = 'res';
% 
% ResultsCol(3) = size(c,2)+3;
% d{1,ResultsCol(3)} = 'Results SNR(diff) High';
% d{2,ResultsCol(3)} = 'res';
% 
% ResultsCol(4) = size(c,2)+4;
% d{1,ResultsCol(4)} = 'Results SNR(dB) Low';
% d{2,ResultsCol(4)} = 'res';
% 
% ResultsCol(5) = size(c,2)+5;
% d{1,ResultsCol(5)} = 'Results SNR(ratio) Low';
% d{2,ResultsCol(5)} = 'res';
% 
% ResultsCol(6) = size(c,2)+6;
% d{1,ResultsCol(6)} = 'Results SNR(diff) Low';
% d{2,ResultsCol(6)} = 'res';
% 
% fprintf('UHD4 Noise Measurement.\nUsing Test Case Spreadsheet: %s\n',inputSpreadSheet);
%         
% [a b c] = xlsread(inputSpreadSheet);

S1 = serial(Port,'BaudRate',9600);


figure(1)
for testCase = 1
%     fprintf('Test Case: %02d.\n',testCase)

    
% readSettingsFromExcel

% WriteSettings
        fopen(S1)
    for i = 300:800
        SerialWriteCCU(Port,1,11,['0000' dec2hex(i,4)],S1)
    valid = 0;
    retry = 0;
    %Check captured image
    
    Id = G2Capture(1,vid,Port);
    Ic = convert8to12(Id);
    imagesc(Ic*16)
    Signal = cat(3,Ic(Yrange,XrangeSignal,:),rgb2gray(Ic(Yrange,XrangeSignal,:)));
    SignalF = zeros(size(Signal));
    Black = cat(3,Ic(Yrange,XrangeBlack,:),rgb2gray(Ic(Yrange,XrangeBlack,:)));
    BlackF = zeros(size(Black));
    
    valid(i) = SNRcheckInputImage(Ic,[Yrange; XrangeSignal; XrangeBlack],0,35,5,2);
    
    [x y] = meshgrid(1:100,1:100);
    indepvar = [reshape(x,[],1) reshape(y,[],1)];
    for k = 1:4
        depvar = double(reshape(Signal(:,:,k),[],1));
        Model = polyfitn(indepvar,depvar,2);
        Model.Coefficients(end) = 0;
        Trend = polyvaln(Model,indepvar);
        SignalF(:,:,k) = reshape(depvar-Trend+mean(mean(Trend)),100,100);
    end
    
    
    
    for k = 1:4
        depvar = double(reshape(Black(:,:,k),[],1));
        Model = polyfitn(indepvar,depvar,2);
        Model.Coefficients(end) = 0;
        Trend = polyvaln(Model,indepvar);
        BlackF(:,:,k) = reshape(depvar-Trend+mean(mean(Trend)),100,100);
        
    end
    if(i == 452)
        abc = 1;
    end
    
    
    Avg(testCase,:,i) = squeeze(mean(mean(SignalF,1),2)-mean(mean(BlackF,1),2))';
    
    Noise(testCase,:,i) = [std2(SignalF(:,:,1)) std2(SignalF(:,:,2)) std2(SignalF(:,:,3)) std2(SignalF(:,:,4))];
    Noise2(testCase,:,i) = [std2(BlackF(:,:,1)) std2(BlackF(:,:,2)) std2(BlackF(:,:,3)) std2(BlackF(:,:,4))];
    
    SNR(testCase,:,i) = 20*log10(Avg(testCase,:,i)./Noise(testCase,:,i));
    SNR2(testCase,:,i) = 20*log10(Avg(testCase,:,i)./Noise2(testCase,:,i));
    fprintf('%d',i)
    if(mod(i,10) == 0)
        fprintf('\n')
    end
    end
        fclose(S1)
        [b a] = find(squeeze(SNR2) == max(SNR2(1,2,300:800)),1,'first')
        
    AvgF(testCase,:) = Avg(testCase,:,a);
    NoiseF(testCase,:) = Noise(testCase,:,a);
    SNRF(testCase,:) = 20*log10(AvgF(testCase,:)./NoiseF(testCase,:));
    Noise2F(testCase,:) = Noise2(testCase,:,a);
    SNR2F(testCase,:) = 20*log10(AvgF(testCase,:)./Noise2F(testCase,:));
    
    fprintf('Test Case: %03d - %f\n',testCase,SNRF(testCase,4))
    d{testCase+2,ResultsCol(1)} = SNRF(testCase,4);
    d{testCase+2,ResultsCol(2)} = AvgF(testCase,4)/NoiseF(testCase,4);
    d{testCase+2,ResultsCol(3)} = AvgF(testCase,4)/NoiseF(testCase,4) - AvgF(1,4)/NoiseF(1,4);
    d{testCase+2,ResultsCol(4)} = SNR2F(testCase,4);
    d{testCase+2,ResultsCol(5)} = AvgF(testCase,4)/Noise2F(testCase,4);
    d{testCase+2,ResultsCol(6)} = AvgF(testCase,4)/Noise2F(testCase,4) - AvgF(1,4)/Noise2F(1,4);  
  
end

xlswrite('ResultsRamp.xlsx',d);

winopen('ResultsRamp.xlsx')