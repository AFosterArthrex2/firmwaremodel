%Run Coarse Filter Simulation

w = waitbar(0,'Loading');

load('5seconds_002.mat')
load('5seconds_002_settings.mat')
settings.Sharpening_Gain = 64;

VidWrite1 = VideoWriter('C:\Users\AFoster\Pictures\Matlab Images\CoarseEnhance\2k_Standard Enhance.avi','Uncompressed AVI');
VidWrite2 = VideoWriter('C:\Users\AFoster\Pictures\Matlab Images\CoarseEnhance\2k_Coarse0.1 Enhance.avi','Uncompressed AVI');
VidWrite3 = VideoWriter('C:\Users\AFoster\Pictures\Matlab Images\CoarseEnhance\2k_Coarse0.25 Enhance.avi','Uncompressed AVI');

VidWrite4 = VideoWriter('C:\Users\AFoster\Pictures\Matlab Images\CoarseEnhance\4k_Standard Enhance.avi','Uncompressed AVI');
VidWrite5 = VideoWriter('C:\Users\AFoster\Pictures\Matlab Images\CoarseEnhance\4k_Coarse0.1 Enhance.avi','Uncompressed AVI');
VidWrite6 = VideoWriter('C:\Users\AFoster\Pictures\Matlab Images\CoarseEnhance\4k_Coarse0.25 Enhance.avi','Uncompressed AVI');

open(VidWrite1);
open(VidWrite2);
open(VidWrite3);

for i = 1:300
    Ic = convert8to12(I(:,:,:,i));
    CoarseGain = 0;
    settings.Sharpening_Gain = 64;
    SimulateFirmwareTesting
    settings.Sharpening_Gain = 0;
    I4k_1 = I4k;
    I1080Ds_1 = I1080Ds;
    
    imwrite(I4k,['C:\Users\AFoster\Pictures\Matlab Images\CoarseEnhance\4k_' num2str(CoarseGain) '_' num2str(i,'%03d') '.png'])
    imwrite(I1080Ds,['C:\Users\AFoster\Pictures\Matlab Images\CoarseEnhance\1080_' num2str(CoarseGain) '_' num2str(i,'%03d') '.png'])
    
    writeVideo(VidWrite1,I1080Ds);
    
    Ic = convert8to12(I(:,:,:,i));
    CoarseGain = 0.1;
    SimulateFirmwareTesting
    
    I4k_2 = I4k;
    I1080Ds_2 = I1080Ds;
    
    imwrite(I4k,['C:\Users\AFoster\Pictures\Matlab Images\CoarseEnhance\4k_' num2str(CoarseGain) '_' num2str(i,'%03d') '.png'])
    imwrite(I1080Ds,['C:\Users\AFoster\Pictures\Matlab Images\CoarseEnhance\1080_' num2str(CoarseGain) '_' num2str(i,'%03d') '.png'])
    
    writeVideo(VidWrite2,I1080Ds);
    
    Ic = convert8to12(I(:,:,:,i));
    CoarseGain = 0.25;
    SimulateFirmwareTesting
    
    I4k_3 = I4k;
    I1080Ds_3 = I1080Ds;
    
    imwrite(I4k,['C:\Users\AFoster\Pictures\Matlab Images\CoarseEnhance\4k_' num2str(CoarseGain) '_' num2str(i,'%03d') '.png'])
    imwrite(I1080Ds,['C:\Users\AFoster\Pictures\Matlab Images\CoarseEnhance\1080_' num2str(CoarseGain) '_' num2str(i,'%03d') '.png'])
    
    writeVideo(VidWrite3,I1080Ds);
    
    waitbar(i/300,w,sprintf('Frame: %03d',i));
end
delete(w)
close(VidWrite1);
close(VidWrite2);
close(VidWrite3);
