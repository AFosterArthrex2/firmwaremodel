addpath('polyfitnTools\')
addpath('SSH&Misc\')
% ColumnStats

Id = G2Capture(3,vid,Port,S1);

%Measure SNR

I100 = Id(540+[-49:50],960+[-49:50],:);
I100(:,:,4) = rgb2gray(I100);
% [valid(testCase,i) msg{testCase,i}] = SNRcheckInputImage(Ic,[Yrange; XrangeSignal; XrangeBlack],0,300,50,20);

[x y] = meshgrid(1:100,1:100);
indepvar = [reshape(x,[],1) reshape(y,[],1)];
for k = 1:4
    depvar = double(reshape(I100(:,:,k),[],1));
    Model = polyfitn(indepvar,depvar,2);
    Model.Coefficients(end) = 0;
    Trend = polyvaln(Model,indepvar);
    I100F(:,:,k) = reshape(depvar-Trend+mean(mean(Trend)),100,100);
end

Noise = [std2(I100F(:,:,1)) std2(I100F(:,:,2)) std2(I100F(:,:,3)) std2(I100F(:,:,4))];

SNR = 20*log10(2^12./Noise)