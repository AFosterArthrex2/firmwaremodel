directory = 'C:\Users\AFoster\Pictures\Matlab Images\ColumnCorrection2\';

Rawlist = dir([directory 'Raw\*.png']);

mkdir([directory 'Corrected'])

load('ColGains_3_5_14')

for i = 1:length(Rawlist)
    I = imread([directory 'Raw\' Rawlist(i).name]);
    I2 = ApplyGainCorrection(I,ColGainCorrection,16);
    imwrite(I2,[directory 'Corrected\' num2str(i,'%04d') '.png'])
end