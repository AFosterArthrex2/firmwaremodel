% Noise Measurement Script
% Alexander Foster
% 12/4/13
% UHD4 project


addpath('PolyfitnTools\')
Port = 'COM4';
inputSpreadSheet = 'SNRSettings.xlsx';
Yrange = 400:499;
XrangeSignal = 200:299;
XrangeBlack = 700:799;

Repeats = 10;

[a b c] = xlsread(inputSpreadSheet);
% if(~exist('d','var'))
    d = c;
% end
ResultsCol1 = size(c,2)+1;
d{1,ResultsCol1} = 'Results SNR(dB)';
d{2,ResultsCol1} = 'int';

ResultsCol2 = size(c,2)+2;
d{1,ResultsCol2} = 'Results SNR(diff db)';
d{2,ResultsCol2} = 'int';

ResultsCol3 = size(c,2)+3;
d{1,ResultsCol3} = 'Results SNR(diff)';
d{2,ResultsCol3} = 'int';

fprintf('UHD4 Noise Measurement.\nUsing Test Case Spreadsheet: %s\n',inputSpreadSheet);
        
[a b c] = xlsread(inputSpreadSheet);

for testCase = 4
    
    readSettingsFromExcel
    
    WriteSettings
    
    for i = 1:Repeats
    valid = 0;
    retry = 0;
    %Check captured image
    while(~valid) 
        Id = G2Capture(2,vid);
     
        Signal = cat(3,Id(Yrange,XrangeSignal,:),rgb2gray(Id(Yrange,XrangeSignal,:)));
        SignalF = zeros(size(Signal));
        Black = cat(3,Id(Yrange,XrangeBlack,:),rgb2gray(Id(Yrange,XrangeBlack,:)));
        BlackF = zeros(size(Black));
        
        valid = SNRcheckInputImage(Id,[Yrange; XrangeSignal; XrangeBlack],testCase);
        drawnow
        retry = retry + 1;
        if(~valid)
            if(retry < 3)
                continue;
            end
            if(strcmpi('Cancel',questdlg('Invalid Image. Adjust Head and press OK to try again.','Invalid Image','OK','Cancel','OK')))
                return
            end
        end
        
    end
    
    [x y] = meshgrid(1:100,1:100);
    indepvar = [reshape(x,[],1) reshape(y,[],1)];
    for k = 1:4
        depvar = double(reshape(Signal(:,:,k),[],1));
        Model = polyfitn(indepvar,depvar,2);
        Model.Coefficients(end) = 0;
        Trend = polyvaln(Model,indepvar);
        SignalF(:,:,k) = reshape(depvar-Trend+mean(mean(Trend)),100,100);
    end
    
    
    
    for k = 1:4
        depvar = double(reshape(Black(:,:,k),[],1));
        Model = polyfitn(indepvar,depvar,2);
        Model.Coefficients(end) = 0;
        Trend = polyvaln(Model,indepvar);
        BlackF(:,:,k) = reshape(depvar-Trend+mean(mean(Trend)),100,100);
        
    end
    
    
    Avg(testCase,:,i) = squeeze(mean(mean(SignalF,1),2)-mean(mean(BlackF,1),2))';
    
    Noise(testCase,:,i) = [std2(SignalF(:,:,1)) std2(SignalF(:,:,2)) std2(SignalF(:,:,3)) std2(SignalF(:,:,4))];
    
    SNR(testCase,:,i) = 20*log10(Avg(testCase,:,i)./Noise(testCase,:,i));

    end
    AvgF(testCase,:) = mean(Avg(testCase,:,i),3);
    NoiseF(testCase,:) = mean(Noise(testCase,:,i),3);
    SNRF(testCase,:) = 20*log10(AvgF(testCase,:)./NoiseF(testCase,:));
    
    d{testCase+2,ResultsCol1} = SNRF(testCase,4);
    d{testCase+2,ResultsCol2} = AvgF(testCase,4)/NoiseF(testCase,4);
    d{testCase+2,ResultsCol3} = AvgF(testCase,4)/NoiseF(testCase,4) - AvgF(1,4)/NoiseF(1,4);
end

xlswrite('Results.xlsx',d);

winopen('Results.xlsx')