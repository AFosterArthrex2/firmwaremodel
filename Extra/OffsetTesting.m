%Column & Line Offsets

RowNoise = floor(randn(1,25))*0.5;
ColNoise = floor(randn(1,50))*0.5;

I = 20+zeros(25,50);

for i = 1:25
    for j = 1:50
        I(i,j) = RowNoise(i)+ColNoise(j);
    end
end

figure;imagesc(I,[10 40])

% func = @(x) sum(sum(abs(I-(x(1:25)'*ones(1,50) + ones(25,1)*x(26:end))).^2));
% options = optimset('MaxFunEvals',10000000,'MaxIter',200000);
% out = fminsearch(func,[15*ones(1,25) 8*ones(1,50)],options);

x = mean(I,1)-mean(mean(I,1));
y = mean(I,2)-mean(mean(I,2));

for i = 1:25
    for j = 1:50
        I2(i,j) = I(i,j)-x(j)-y(i);
    end
end

figure;imagesc(I2)