addpath('SSH&Misc\')
Port = 'COM4';
if(~exist('S1','var'))
    S1 = serial(Port,'BaudRate',9600);
end
if(~strcmpi('open',S1.status) || ~isvalid(S1))
    fopen(S1);
end

ColFiltSize = 8;
frameBuff = 8;
numFrames = 360+frameBuff;
imaqmem(1920*1080*3*numFrames)
set(vid,'FramesPerTrigger',numFrames)

SerialBitWriteCCU(Port,1,9,'1',0,S1);
SerialBitWriteCCU(Port,1,10,'0',3,S1);
SerialWriteCCU(Port,1,11,'00000300',S1);
SerialWriteCCU(Port,1,45,'00000000',S1);
SerialWriteCCU(Port,1,46,'00000001',S1);

% I = zeros(1080,960,3,numFrames,'uint16');
test1 = zeros(8,960,3,numFrames,'uint16');

SNR1 = zeros(numFrames-frameBuff,4);
SNR2 = zeros(numFrames-frameBuff,4);

dir1 = 'C:\Users\AFoster\Videos\';


% Writer1 = VideoWriter([dir1 'test' num2str(12) 'UnCorrected1.avi'],'Uncompressed AVI');
% Writer2 = VideoWriter([dir1 'test' num2str(12) 'Corrected1.avi'],'Uncompressed AVI');
Writer3 = VideoWriter([dir1 'test' num2str(13) 'UnCorrected-Corrected.avi'],'Uncompressed AVI');
set(Writer3,'FrameRate',60);
settings.Median_Filter_Enable = 0;

settings.Black_Offset_Enable = 0;
settings.Black_Offset_Level = 260;

% settings.AEC_Gain = hex2dec('0400');
settings.AEC_Gain = hex2dec('7400');

settings.WB_Gain_Red = hex2dec('0835');
settings.WB_Gain_Green = 1024;
settings.WB_Gain_Blue = hex2dec('0B6F');

settings.Pixel_Replace_Enable = 1;
settings.Head_Type = 1;

settings.Demosaic_Force_Disable = 0;

settings.Bilateral_Filter_Enable = 1;
settings.Bilateral_Bit_Select = 2;

settings.Bilateral_Slope_Enable = 1;
settings.Bilateral_Slope_Thresh = 100;
settings.Bilateral_Slope_Bit_Select = 2;

settings.Gamma_1080 = 4;

settings.Peaking_Gain_1080 = 0;
settings.Sharpening_Gain_1080 = 0;
% % 
% % open(Writer1)
% % open(Writer2)
open(Writer3)
start(vid)
while(isrunning(vid))
pause(1)
end
for i = 1:numFrames
    Iraw = convert8to12(getdata(vid,1));
    I = Iraw-260;
    test1(:,:,:,i) = Iraw(1:8,:,:);
    if(i >= frameBuff)
        %Top Left no adaptive correction
        SNR1(i-frameBuff+1,:) = qSNR(Iraw);
        SimulateFirmware4Video
%         writeVideo(Writer1,I1080);
        ITopLeft = I1080;
        
        %Top Right Only Time Averaging
        
%         lineOffR = conv(sum(sum(test1(:,:,1,i-frameBuff+1:i),4),1),1/4*ones(1,4),'same');
%         lineOffG = conv(sum(sum(test1(:,:,2,i-frameBuff+1:i),4),1),1/4*ones(1,4),'same');
%         lineOffB = conv(sum(sum(test1(:,:,3,i-frameBuff+1:i),4),1),1/4*ones(1,4),'same');
        lineOffR = sum(sum(test1(:,:,1,i-frameBuff+1:i),4),1);
        lineOffG = sum(sum(test1(:,:,2,i-frameBuff+1:i),4),1);
        lineOffB = sum(sum(test1(:,:,3,i-frameBuff+1:i),4),1);
        offsetR = ones(1080,1)*lineOffR;
        offsetG = ones(1080,1)*lineOffG;
        offsetB = ones(1080,1)*lineOffB;
        offsetSave(i-frameBuff+1,:,:) = cat(3,offsetR(1,:),offsetG(1,:),offsetB(1,:));
        offset = 1*cat(3,offsetR,offsetG,offsetB);
        test2 = mean(mean(mean(offset)));
        I2 = uint16(double(Iraw(:,:,:))-(offset/8/frameBuff)-(260-test2/8/frameBuff));
%         I2 = uint16(double(Iraw(:,:,:))-(offset/8/frameBuff)-5);
        I3 = uint16(double(Iraw(:,:,:))-(offset/8/frameBuff)-(0-test2/8/frameBuff));
        SNR2(i-frameBuff+1,:) = qSNR(I3);
        
        I = I2;
        SimulateFirmware4Video
        ITopRight = I1080;
%         writeVideo(Writer2,I1080);
        
        %Lower Left Column Filtering no Time Averaging
                
        lineOffR = conv(sum(sum(test1(:,:,1,i-1+1:i),4),1),1/ColFiltSize*ones(1,ColFiltSize),'same');
        lineOffG = conv(sum(sum(test1(:,:,2,i-1+1:i),4),1),1/ColFiltSize*ones(1,ColFiltSize),'same');
        lineOffB = conv(sum(sum(test1(:,:,3,i-1+1:i),4),1),1/ColFiltSize*ones(1,ColFiltSize),'same');
        offsetR = ones(1080,1)*lineOffR;
        offsetG = ones(1080,1)*lineOffG;
        offsetB = ones(1080,1)*lineOffB;
%         offsetSave(i-frameBuff+1,:,:) = cat(3,offsetR(1,:),offsetG(1,:),offsetB(1,:));
        offset = 1*cat(3,offsetR,offsetG,offsetB);
        test2 = mean(mean(mean(offset)));
        I2 = uint16(double(Iraw(:,:,:))-(offset/8/frameBuff)-(260-test2/8/frameBuff));
%         I2 = uint16(double(Iraw(:,:,:))-(offset/8/frameBuff)-5);
        I3 = uint16(double(Iraw(:,:,:))-(offset/8/frameBuff)-(0-test2/8/frameBuff));
        SNR3(i-frameBuff+1,:) = qSNR(I3);
        
        I = I2;
        SimulateFirmware4Video
        ILowerLeft = I1080;
        
        %Lower Right Column Filtering & Time Averaging
                
        lineOffR = conv(sum(sum(test1(:,:,1,i-frameBuff+1:i),4),1),1/ColFiltSize*ones(1,ColFiltSize),'same');
        lineOffG = conv(sum(sum(test1(:,:,2,i-frameBuff+1:i),4),1),1/ColFiltSize*ones(1,ColFiltSize),'same');
        lineOffB = conv(sum(sum(test1(:,:,3,i-frameBuff+1:i),4),1),1/ColFiltSize*ones(1,ColFiltSize),'same');
        offsetR = ones(1080,1)*lineOffR;
        offsetG = ones(1080,1)*lineOffG;
        offsetB = ones(1080,1)*lineOffB;
%         offsetSave(i-frameBuff+1,:,:) = cat(3,offsetR(1,:),offsetG(1,:),offsetB(1,:));
        offset = 1*cat(3,offsetR,offsetG,offsetB);
        test2 = mean(mean(mean(offset)));
        I2 = uint16(double(Iraw(:,:,:))-(offset/8/frameBuff)-(260-test2/8/frameBuff));
%         I2 = uint16(double(Iraw(:,:,:))-(offset/8/frameBuff)-5);
        I3 = uint16(double(Iraw(:,:,:))-(offset/8/frameBuff)-(0-test2/8/frameBuff));
        SNR4(i-frameBuff+1,:) = qSNR(I3);
        
        I = I2;
        SimulateFirmware4Video
        ILowerRight = I1080;
        
        writeVideo(Writer3,cat(1,cat(2,ITopLeft(1:540,:,:),ITopRight(1:540,:,:)),cat(2,ILowerLeft(541:1080,:,:),ILowerRight(541:1080,:,:))));
    end
    if(mod(i,5) == 0)
        fprintf('%d\n',i)
    end
    
    
end
% close(Writer1)
% close(Writer2)   
close(Writer3)
