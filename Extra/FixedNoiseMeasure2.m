I1 = sum(sum(Iout2,4),1);

I2 = mean(mean(Iout2,4),1);

for i = 1:3
    ColNoise(i,:) = mean(mean(Iout2(:,:,i,:),4),1) - conv( mean(mean(Iout2(:,:,i,:),4),1),0.1*ones(1,10),'same');
end

I3 = zeros(size(I2));


I4 = mean(mean(Iout2,4),2);

for i = 1:3
    RowNoise(i,:) = mean(mean(Iout2(:,:,i,:),4),2) - conv( mean(mean(Iout2(:,:,i,:),4),2),0.1*ones(1,10),'same');
end
I5 = zeros(size(I4));


I6 = mean(Iout2,4);


FixedOutput = {I2, I3, I4, I5, I6, RowNoise, ColNoise};