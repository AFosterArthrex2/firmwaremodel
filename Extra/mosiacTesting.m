 [x, y] = meshgrid(1:size(I,2),1:size(I,1));

% R @ R
    G5 = (mod(x,2) == 0) & (mod(y,2) == 1);
    
    % G @ G
    G6 = ((mod(x,2) == 1) & (mod(y,2) == 1)) | ...
         ((mod(x,2) == 0) & (mod(y,2) == 0));
    
    % B @ B
    G7 = (mod(x,2) == 1) & (mod(y,2) == 0);
    
    Red = double(I(:,:,1));
    Green = double(I(:,:,2));
    Blue = double(I(:,:,3));
    
    Im = uint16(Red.*G5 + Green.*G6 + Blue.*G7);