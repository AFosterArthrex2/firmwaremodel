%AEC find Pixel
% Finds the correct Pixel Offset for AEC Window

Offsets = -600:600;

clear SumOfSquaresDiff2
SumOfSquaresDiff2 = zeros(length(Offsets),3);

% w = waitbar(0,'Running');
I12bit_cap2 = double(I12bit_cap).^2;
for i = 1:length(Offsets)
%     waitbar(((i-1)*length(Offsets)+j)/length(Offsets)^2,w,sprintf('%d out of %d',((i-1)*length(Offsets)+j),length(Offsets)^2))
%     fprintf('i = %d\n',i)    
%     for j = 1:length(Offsets)
%         for k = 1:length(Offsets)
%             Line = 35; Pixel = 504+Offsets(i); Lines = 1038; Pixels = 1064;
%                     Line = 400; Pixel = 839+Offsets(i); Lines = 256+Offsets(j); Pixels = 256+Offsets(k);
%                     Line = 206; Pixel = 646+Offsets(i); Lines = 696; Pixels = 780;
            %         Line = 206+Offsets(j); Pixel = 646-13; Lines = 696+Offsets(i); Pixels = 780-36;
%                     Line = 35; Pixel = 504+Offsets(i); Lines = 1038; Pixels = 1064;
%             Line = 206+Offsets(i); Pixel = 646+13; Lines = 696; Pixels = 780;
            Line = 66-9; Pixel = 138+Offsets(i); Lines = 976; Pixels = 1796;
%                     Line = 426+Offsets(i); Pixel = 905+6; Lines = 256; Pixels = 256;
            %         WindowedImage = cat(3,Window{settings.AEC_Window+1},Window{settings.AEC_Window+1},Window{settings.AEC_Window+1}).*double(I12bit_cap);
            %         WindowedImage = I12bit_cap(logical(cat(3,Window{settings.AEC_Window+1},Window{settings.AEC_Window+1},Window{settings.AEC_Window+1})));
%             WindowedImage2 = (I12bit_cap2(Line:(Line+Lines-1),Pixel:(Pixel+Pixels-1),:));
            AvgSumOfSquares2 = (squeeze(sum(sum(I12bit_cap2(max(min(Line:(Line+Lines-1),1108),1),max(min(Pixel:(Pixel+Pixels-1),2016),1),:),1),2))*(settings.AEC_Window_Factor/2^32))';
            %         figure(8);imagesc(WindowedImage)
            %         drawnow
            SumOfSquaresDiff2(i,:) = (abs(floor(AvgSumOfSquares2) - [redAvgOfSqr greenAvgOfSqr blueAvgOfSqr]));
%         end
%     end
end

% delete(w)

[a] = find(sum(SumOfSquaresDiff2,2) == min(sum(SumOfSquaresDiff2,2)));

[row,col,vol] = ind2sub(size(sum(SumOfSquaresDiff2,2)),a);