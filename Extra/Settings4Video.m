set(vid,'FramesPerTrigger',600)
set(vid,'LoggingMode','disk')

Writer = VideoWriter('test.avi','Uncompressed AVI');

set(vid,'DiskLogger',Writer)

imaqmem(6000000000)

settings.Median_Filter_Enable = 0;

settings.Black_Offset_Enable = 1;
settings.Black_Offset_Level = 260;

settings.AEC_Gain = 32767;

settings.WB_Gain_Red = 2048;
settings.WB_Gain_Green = 1024;
settings.WB_Gain_Blue = 2560;

settings.Pixel_Replace_Enable = 1;
settings.Head_Type = 1;

settings.Demosaic_Force_Disable = 0;

settings.Bilateral_Filter_Enable = 1;
settings.Bilateral_Bit_Select = 2;

settings.Bilateral_Slope_Enable = 1;
settings.Bilateral_Slope_Thresh = 100;
settings.Bilateral_Slope_Bit_Select = 2;

settings.Gamma_1080 = 4;

settings.Peaking_Gain_1080 = 0;
settings.Sharpening_Gain_1080 = 0;