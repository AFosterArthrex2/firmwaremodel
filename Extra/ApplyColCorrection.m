function out = ApplyColCorrection(I,C,gain)
%Applies the columns correction factors in C to I
%
% I: 12-bit image
% C: 8-bit correction, size(C,1) = size(I,2), size(C,2) = size(I,3)
% Gain: the sensor gain used to capture I (2, 4 or 8)
%
% Apply Correction on 14-bit image, drop extra bits

I14 = 4*I;

Correction(:,:,1) = uint16(gain/8*double(C(:,1))*ones(1,1080))';
Correction(:,:,2) = uint16(gain/8*double(C(:,2))*ones(1,1080))';
Correction(:,:,3) = uint16(gain/8*double(C(:,3))*ones(1,1080))';

out = I14-Correction;