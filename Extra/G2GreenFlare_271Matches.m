%% G2GreenFlare
% Removes green from images, per pixel
%%
%  [Iout] = G2GreenFlare(Iin,settings)
%   Inputs: 
%           Iin         - Input Image
%           settings
%               GreenFlare_Enable        - Enable/Disable Filter
%               GreenFlare_Theta1       - Parameter
%               GreenFlare_Theta2       - Parameter
%               GreenFlare_Theta3       - Parameter
%               GreenFlare_Theta4       - Parameter
%
%   Outputs: 
%           Iout        - Output Image
%

%   Gen2 Firmware Model - GreenFlare Filter
%   Author: Alexander Foster
%   Date:   06/14/16

function [Iout, Alpha, H] = G2GreenFlare(Iin,varargin)

%% Settings
reqsettings = {'GreenFlare_Enable',   0;...
               'GreenFlare_Threshold',3750;...
               'GreenFlare_Theta1', -0.5;...
               'GreenFlare_Theta2', -0.25;...
               'GreenFlare_Theta3',  0.25;...
               'GreenFlare_Theta4',  0.5};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

% settings.GreenFlare_Theta1 = -0.5;
% settings.GreenFlare_Theta2 = -0.25;
% settings.GreenFlare_Theta3 = 0.25;
% settings.GreenFlare_Theta4 = 0.5;
% settings.GreenFlare_Theta1 = -1;
% settings.GreenFlare_Theta2 = -0.5;
% settings.GreenFlare_Theta3 = 0.5;
% settings.GreenFlare_Theta4 = 1;

if(~settings.GreenFlare_Enable)
    Iout = Iin;
    Alpha = zeros(size(Iin));
    return;
end

%% Initialize
Iin = double(Iin);

HexConvMat = {'367' 'b72' '128'; '1d5' '62b' '800'; '800' '744' '0bc'};
ConvMat = reshape(hex2dec(HexConvMat),3,3);
ConvMat4 = ConvMat/2^12.*[1 1 1; -1 -1 1; 1 -1 -1];

Yr = floor(Iin(:,:,1)*ConvMat(1,1)/2^12);
Yg = floor(Iin(:,:,2)*ConvMat(1,2)/2^12);
Yb = floor(Iin(:,:,3)*ConvMat(1,3)/2^12);

Y = floor(Yr + Yg + Yb);

Pbr = floor(Iin(:,:,1)*ConvMat(2,1)/2^12);
Pbg = floor(Iin(:,:,2)*ConvMat(2,2)/2^12);
Pbb = floor(Iin(:,:,3)/2);

PbRmB = floor(Pbb - Pbr);

Pb = floor(PbRmB-Pbg);


Prr = floor(Iin(:,:,1)/2);
Prg = floor(Iin(:,:,2)*ConvMat(3,2)/2^12);
Prb = floor(Iin(:,:,3)*ConvMat(3,3)/2^12);

PrRmB = floor(Prr - Prb);

Pr = floor(PrRmB - Prg);

I_YPbPr = cat(3,Y,Pb,Pr);

I_YPbPr(:,:,1) = max(min(floor(I_YPbPr(:,:,1)),2^12-1),0);
I_YPbPr(:,:,2) = max(min(floor(I_YPbPr(:,:,2)),2^11-1),-2^11);
I_YPbPr(:,:,3) = max(min(floor(I_YPbPr(:,:,3)),2^11-1),-2^11);
% 

%Keep Extra Bits
I_YPbPr2 = cat(3,Iin(:,:,1)*ConvMat4(1,1)+Iin(:,:,2)*ConvMat4(1,2)+Iin(:,:,3)*ConvMat4(1,3),...
                Iin(:,:,1)*ConvMat4(2,1)+Iin(:,:,2)*ConvMat4(2,2)+Iin(:,:,3)*ConvMat4(2,3),...
                Iin(:,:,1)*ConvMat4(3,1)+Iin(:,:,2)*ConvMat4(3,2)+Iin(:,:,3)*ConvMat4(3,3));
            
            
[Imax, indexMax] = max(Iin,[],3);
% Imin only looks at red,blue channels
[Imin, indexMin] = min(Iin(:,:,[1 3]),[],3);

H = (Iin(:,:,3) - Iin(:,:,1))./(Iin(:,:,2)-Imin);

%H is 11 bits (signed)
Hsign = sign(H);
Habs = abs(H);
Habs2 = zeros(size(Habs));

%WHY? - The source looks like 10 bits, but this matches the capture
Habs2(Hsign == -1) = ceil(Habs(Hsign == -1)*2^9)/2^9;
Habs2(Hsign == 1) = floor(Habs(Hsign == 1)*2^9)/2^9;

H = Habs2.*Hsign;

%calculate alpha, correction factor
Alpha = ones(size(Iin,1),size(Iin,2));
Ramp1 = (settings.GreenFlare_Theta2-H)./(settings.GreenFlare_Theta2 - settings.GreenFlare_Theta1);
Ramp2 = (H-settings.GreenFlare_Theta3)./(settings.GreenFlare_Theta4 - settings.GreenFlare_Theta3);

Alpha(H <=  settings.GreenFlare_Theta1) = 1;

% Alpha(H <  settings.GreenFlare_Theta2 & H >= settings.GreenFlare_Theta1) = ...
% Ramp1(H <  settings.GreenFlare_Theta2 & H >= settings.GreenFlare_Theta1);
Alpha(H <=  settings.GreenFlare_Theta2 & H > settings.GreenFlare_Theta1) = 0;

Alpha(H <= settings.GreenFlare_Theta3 & H > settings.GreenFlare_Theta2) = 0;

Alpha(H >  settings.GreenFlare_Theta3 & H <= settings.GreenFlare_Theta4) = ...
Ramp2(H >  settings.GreenFlare_Theta3 & H <= settings.GreenFlare_Theta4);

Alpha(H >  settings.GreenFlare_Theta4) = 1;

%Only change if green is max
Alpha(indexMax ~= 2) = 1;

%Only change if above threshold
% Alpha(Iin(:,:,2) >= settings.GreenFlare_Threshold) = 1;
% Alpha(Iin(:,:,2) < settings.GreenFlare_Threshold) = 1;

%Alpha is a 10-bit value, 1.9 
Alpha2 = floor(Alpha*2^9)/2^9;

if(~settings.GreenFlare_Enable)
    I_YPbPr_out = floor(cat(3,I_YPbPr(:,:,1),I_YPbPr(:,:,2),I_YPbPr(:,:,3)));
else
    I_YPbPr_out = floor(cat(3,I_YPbPr(:,:,1),I_YPbPr(:,:,2).*Alpha2,I_YPbPr(:,:,3).*Alpha2));
end

HexConvMat2 = {'400' '000' '64e'; '400' '0c0' '1e0'; '400' '76e' '000'};
ConvMat3 = reshape(hex2dec(HexConvMat2),3,3);
ConvMat2 = reshape(hex2dec(HexConvMat2)/2^10,3,3).*[1 1 1; 1 -1 -1; 1 1 1];

Y = I_YPbPr_out(:,:,1);
gPb = floor(I_YPbPr_out(:,:,2)*ConvMat3(2,2)/2^10);
gPr = floor(I_YPbPr_out(:,:,3)*ConvMat3(2,3)/2^10);

YmgPb = floor(Y - gPb);

G = floor(YmgPb - gPr);

rPr = floor(I_YPbPr_out(:,:,3)*ConvMat3(1,3)/2^10);

%Overflow
% R = floor(mod(Y + rPr,4096));
R = floor(Y + rPr);

bPb = floor(I_YPbPr_out(:,:,2)*ConvMat3(3,2)/2^10);

%Overflow
% B = floor(mod(Y + bPb,4096));
B = floor(Y + bPb);

I_YPbPr_out = I_YPbPr2;
Iout2 = floor(cat(3,I_YPbPr_out(:,:,1)*ConvMat2(1,1)+I_YPbPr_out(:,:,2)*ConvMat2(1,2)+I_YPbPr_out(:,:,3)*ConvMat2(1,3),...
             I_YPbPr_out(:,:,1)*ConvMat2(2,1)+I_YPbPr_out(:,:,2)*ConvMat2(2,2)+I_YPbPr_out(:,:,3)*ConvMat2(2,3),...
             I_YPbPr_out(:,:,1)*ConvMat2(3,1)+I_YPbPr_out(:,:,2)*ConvMat2(3,2)+I_YPbPr_out(:,:,3)*ConvMat2(3,3)));

      
IoutTemp = cat(3,R,G,B);

Iout = uint16(max(min(floor(IoutTemp),2^12-1),0));

abc = 1;
