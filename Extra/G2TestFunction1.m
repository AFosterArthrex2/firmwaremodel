%% G2TestFunction1
% Displays input arguments and number of output args
%%
function varargout =  G2TestFunction1(varargin)
%  [Iout] = G2BioOpticoLowPass(Iin)
%   Inputs: 
%           Iin         - Input Image
%   Outputs: 
%           Iout        - Output Image
%

%   Gen2 Firmware Model - BioOptico Low Pass Filter
%   Author: Alexander Foster
%   Date:   10/12/15
varargout = {[]};
uiwait(msgbox(sprintf('Number of Input Arguments: %d\nNumber of Output Arguments: %d',nargin,nargout)))