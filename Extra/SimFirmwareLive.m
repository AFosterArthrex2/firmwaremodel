I = G2Capture(3,vid,'COM6',S1);
I = padarray(I,[(1108-1080)/2 (2016-1920)/2],'replicate','both');
Isave{1,1} = I;fprintf('|%02d|.',testCase);

I = G2MedianFilter(I,settings.Median_Filter_Enable);
Isave{1,2} = I;fprintf('.');

I = G2BlackOffset(I,settings.Black_Offset_Enable,settings.Black_Offset_Level);
Isave{1,3} = I;fprintf('.');

I = G2ColumnGains(I);
Isave{1,4} = I;fprintf('.');

I = G2AEC(I,settings.AEC_Gain);
Isave{1,5} = I;fprintf('.');

I = G2WhiteBalance(I,settings.WB_Gain_Red,settings.WB_Gain_Green,settings.WB_Gain_Blue);
Isave{1,6} = I;fprintf('.');

I = G2PixelReplace(I,settings.Pixel_Replace_Enable,settings.Head_Type);
% I = procHotPixel(I,settings.Pixel_Replace_Enable);
Isave{1,7} = I;fprintf('.');

I = G2Demosaic(I,settings.Demosaic_Force_Disable,settings.Head_Type);
Isave{1,8} = I;fprintf('.');

I = G2Bilateral(I,settings.Bilateral_Filter_Enable,settings.Bilateral_Bit_Select);
Isave{1,9} = I;fprintf('.');

I = G2BilateralSlope(I,settings.Bilateral_Slope_Enable,settings.Bilateral_Slope_Thresh,settings.Bilateral_Slope_Bit_Select);
Isave{1,10} = I;fprintf('.');

%Crop Before DDR Memory
xOffset = double(settings.Crop_Proc_Pixel);
yOffset = double(settings.Crop_Proc_Line);
I = I((1:1092)+yOffset,(1:1936)+xOffset,:);
Isave{1,11} = I;fprintf('|');


%1080p HDMI output

%Crop to 1080x1920
xOffset2 = double(settings.Crop_HDMI_Pixel);
yOffset2 = double(settings.Crop_HDMI_Line);
I1080 = I((1:1080)+yOffset2,(1:1920)+xOffset2,:);
Isave{2,1} = I1080;fprintf('.');
I12bit = I1080;
I1080 = G2Gamma(I1080,settings.Gamma_1080);
Isave{2,2} = I1080;fprintf('.');
I1080 = G2RGB2YCbCr(I1080);
Isave{2,3} = I1080;fprintf('.');
I1080 = G2PostPeaking(I1080,settings.Peaking_Gain_1080,settings.Sharpening_Gain_1080);
Isave{2,4} = I1080;fprintf('.');
I1080 = G2YCbCr2RGB(I1080);
Isave{2,5} = I1080;fprintf('|1080p & 12bit Done.\n');

%4K Quad 3G output

%Crop 4x 976x552
fprintf(['|%02d|          |>'],testCase);
IQ3G{1} = I((1:552)+0,(1:976)+0,:);
IQ3G{2} = I((1:552)+0,(1:976)+960,:);
IQ3G{3} = I((1:552)+540,(1:976)+0,:);
IQ3G{4} = I((1:552)+540,(1:976)+960,:);

Isave{3,1} = IQ3G;fprintf('.');

for i = 1:4 IQ3G{i} = G2Scaler(IQ3G{i}); end
Isave{3,2} = IQ3G;fprintf('.');

for i = 1:4 IQ3G{i} = G2GreenOffset(IQ3G{i},settings.Green_Offset_Enable); end
Isave{3,3} = IQ3G;fprintf('.');

if(settings.Green_Edge_Enhance_Enable)
    for i = 1:4
        IQ3G{i} = G2GreenEdgeEnhance(IQ3G{i},...
            settings.Green_Edge_Enhance_Gain,...
            settings.Green_Edge_Enhance_Thresh_Enable,...
            settings.Green_Edge_Enhance_Threshold);
    end
end
Isave{3,4} = IQ3G;fprintf('.');


for i = 1:4 IQ3G{i} = G2CSC(IQ3G{i},...
        [settings.CSC_1_1; settings.CSC_1_2; settings.CSC_1_3;...
         settings.CSC_2_1; settings.CSC_2_2; settings.CSC_2_3;...
         settings.CSC_3_1; settings.CSC_3_2; settings.CSC_3_3]);
end
Isave{3,5} = IQ3G;fprintf('.');

for i = 1:4 IQ3G{i} = G2Gamma(IQ3G{i},settings.Gamma_4K); end
Isave{3,6} = IQ3G;fprintf('.');

for i = 1:4 IQ3G{i} = G2RGB2YCbCr(IQ3G{i}); end
Isave{3,7} = IQ3G;fprintf('.');

for i = 1:4 IQ3G{i} = G2PostPeaking(IQ3G{i},settings.Peaking_Gain_4K,settings.Sharpening_Gain_4K); end
Isave{3,8} = IQ3G;fprintf('.');

for i = 1:4 IQ3G{i} = G2YCbCr2RGB(IQ3G{i}); end
Isave{3,9} = IQ3G;fprintf('|Quad 3G Done.\n');


IQ3Gs = [IQ3G{1}(13:end-12,17:end-16,:) IQ3G{2}(13:end-12,17:end-16,:);...
         IQ3G{3}(13:end-12,17:end-16,:) IQ3G{4}(13:end-12,17:end-16,:)];
     
%DisplayPort 4k
     
%Crop 4x 1092x494
fprintf(['|%02d|          |>'],testCase);
IDP4k{1} = I((1:1092),(1:496)+0,:);
IDP4k{2} = I((1:1092),(1:496)+480,:);
IDP4k{3} = I((1:1092),(1:496)+960,:);
IDP4k{4} = I((1:1092),(1:496)+1440,:);

Isave{4,1} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2Scaler(IDP4k{i}); end
Isave{4,2} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2GreenOffset(IDP4k{i},settings.Green_Offset_Enable); end
Isave{4,3} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2VerticalBinning(IDP4k{i},settings.Vertical_Binning); end
Isave{4,4} = IDP4k;fprintf('.');

if(settings.Green_Edge_Enhance_Enable)
    for i = 1:4
        IDP4k{i} = G2GreenEdgeEnhance(IDP4k{i},...
            settings.Green_Edge_Enhance_Gain,...
            settings.Green_Edge_Enhance_Thresh_Enable,...
            settings.Green_Edge_Enhance_Threshold);
    end
end
Isave{4,5} = IDP4k;fprintf('.');


for i = 1:4 IDP4k{i} = G2CSC(IDP4k{i},...
        [settings.CSC_1_1; settings.CSC_1_2; settings.CSC_1_3;...
         settings.CSC_2_1; settings.CSC_2_2; settings.CSC_2_3;...
         settings.CSC_3_1; settings.CSC_3_2; settings.CSC_3_3]);
end
Isave{4,6} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2Gamma(IDP4k{i},settings.Gamma_4K); end
Isave{4,7} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2RGB2YCbCr(IDP4k{i}); end
Isave{4,8} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2PostPeaking(IDP4k{i},settings.Peaking_Gain_4K,settings.Sharpening_Gain_4K); end
Isave{4,9} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2YCbCr2RGB(IDP4k{i}); end
Isave{4,10} = IDP4k;fprintf('|DisplayPort Done.\n');

%Crop from 992x2184 to 960x2160
% IDP4ks = cat(2,IDP4k{1}(13:end-12,17:end-16,:),IDP4k{2}(13:end-12,17:end-16,:),...
%          IDP4k{3}(13:end-12,17:end-16,:),IDP4k{4}(13:end-12,17:end-16,:));
if(settings.Vertical_Binning)
    IDP4ks = cat(2,IDP4k{1}(13:end-12,18:end-15,:),IDP4k{2}(13:end-12,18:end-15,:),...
         IDP4k{3}(13:end-12,18:end-15,:),IDP4k{4}(13:end-12,18:end-15,:));  
else
    IDP4ks = cat(2,IDP4k{1}(14:end-11,18:end-15,:),IDP4k{2}(14:end-11,18:end-15,:),...
         IDP4k{3}(14:end-11,18:end-15,:),IDP4k{4}(14:end-11,18:end-15,:));  
end


%Downsampled 1080
if(settings.Downscale_GEE_Enable)
    Isave{5,1} = Isave{4,4};
else
    Isave{5,1} = Isave{4,3};
end
fprintf(['|%02d|               |>'],testCase);

%LowPass Filter
for i = 1:4 I1080D{i} = G2LowPass(Isave{5,1}{i},settings.Downscale_Filter_Disable); end
Isave{5,2} = I1080D;fprintf('.');

%Crop and Assemble
% I1080Ds = cat(2,I1080D{1}(14:end-11,17:end-16,:),I1080D{2}(14:end-11,17:end-16,:),...
%                 I1080D{3}(14:end-11,17:end-16,:),I1080D{4}(14:end-11,17:end-16,:)); 
if(settings.Vertical_Binning)
    I1080Ds = cat(2,I1080D{1}(17:end-8,17:end-16,:),I1080D{2}(17:end-8,17:end-16,:),...
                I1080D{3}(17:end-8,17:end-16,:),I1080D{4}(17:end-8,17:end-16,:));
else
    I1080Ds = cat(2,I1080D{1}(18:end-7,17:end-16,:),I1080D{2}(18:end-7,17:end-16,:),...
                I1080D{3}(18:end-7,17:end-16,:),I1080D{4}(18:end-7,17:end-16,:));
end

Isave{5,3} = I1080Ds;fprintf('.');

I1080Ds = G2DownScale(I1080Ds);

I1080D12bit = I1080Ds;
Isave{5,4} = I1080Ds;fprintf('.');

I1080Ds = G2Gamma(I1080Ds,settings.Gamma_1080);
Isave{5,5} = I1080Ds;fprintf('.');

I1080Ds = G2RGB2YCbCr(I1080Ds);
Isave{5,6} = I1080Ds;fprintf('.');

I1080Ds = G2PostPeaking(I1080Ds,settings.Peaking_Gain_1080,settings.Sharpening_Gain_1080);
Isave{5,7} = I1080Ds;fprintf('.');

I1080Ds = G2YCbCr2RGB(I1080Ds);
Isave{5,8} = I1080Ds;fprintf('.');

fprintf('|1080 Downscaled Done.\n');

