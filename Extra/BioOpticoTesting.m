SerialWriteCCU(Port,1,50+256*1,'00040301');
SerialBitWriteCCU(Port,1,128+4*256,'0010',[19:-1:16])

%Simulate BioOptico Signal Path

IBioTP = G2TestPatternGenerator(2,[546 992],'9d36caf15');
Isave{10,1} = IBioTP;
IBioTP = G2ScalerNN(IBioTP);
 Isave{10,2} = IBioTP;
 
 %Final Crop Before Memory
    xOffset = double(48);
    yOffset = double(0);
    IBioTP = IBioTP((1:1092)+yOffset,(1:1936)+xOffset,:);
    Isave{10,3} = IBioTP;
    
    
    %Create 4k stripes
    IBioTP4k{1} = IBioTP((1:1092),(1:496)+0,:);
    IBioTP4k{2} = IBioTP((1:1092),(1:496)+480,:);
    IBioTP4k{3} = IBioTP((1:1092),(1:496)+960,:);
    IBioTP4k{4} = IBioTP((1:1092),(1:496)+1440,:);
    Isave{10,4} = IBioTP4k;
    
        %2x Scale NN
    for i = 1:4
        IBioTP4k{i} = G2ScalerNN(IBioTP4k{i});
    end
    Isave{10,4} = IBioTP4k;
    
    %Crop
    IBioTP4ks = cat(2,...
        IBioTP4k{1}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioTP4k{2}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioTP4k{3}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioTP4k{4}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:));
    
  IBioTPDs = G2DownScale( IBioTP4ks);
  
  Ic= getsnapshot(vid);
  
  [IDP4k_cap] = G2Capture(9,vid,Port);
  
  figure;subplot(2,1,1),imagesc(Ic(:,:,1)),subplot(2,1,2),imagesc(IBioTPDs(:,:,1)/16)
figure;subplot(2,1,1),imagesc(IDP4k_cap(:,:,1)),subplot(2,1,2),imagesc(IBioTP4ks(:,:,1)/16)