%AEC find Pixel
% Finds the correct Line Offset for AEC Window
% uses [row col vol] from horizontal ramp


Offsets = [-100:100];
clear SumOfSquaresDiff2
w = waitbar(0,'Running');
I12bit_cap2 = double(I12bit_cap).^2;
for i = 1:size(Set1,1)
%     waitbar(((i-1)*length(Offsets)+j)/length(Offsets)^2,w,sprintf('%d out of %d',((i-1)*length(Offsets)+j),length(Offsets)^2))
        waitbar(i/size(Set1,1),w,num2str(i))
    for j = 1:length(Offsets)
            %         Line = 426+Offsets(j); Pixel = 905-66; Lines = 256+Offsets(i); Pixels = 256;
            %         Line = 206+Offsets(j); Pixel = 646-13; Lines = 696+Offsets(i); Pixels = 780-36;
            %         Line = 35; Pixel = 1+Offsets(i); Lines = 1038; Pixels = 1+Offsets(j);
            Line = 206+Offsets(j); Pixel = 646+Set1(i,1); Lines = 696+Set1(i,2); Pixels = 780+Set1(i,3);
            %         Line = 426+Offsets(j); Pixel = 905+Offsets(i); Lines = 256; Pixels = 256;
            %         WindowedImage = cat(3,Window{settings.AEC_Window+1},Window{settings.AEC_Window+1},Window{settings.AEC_Window+1}).*double(I12bit_cap);
            %         WindowedImage = I12bit_cap(logical(cat(3,Window{settings.AEC_Window+1},Window{settings.AEC_Window+1},Window{settings.AEC_Window+1})));
            WindowedImage2 = (I12bit_cap2(max(min(Line:(Line+Lines-1),1080),1),max(min(Pixel:(Pixel+Pixels-1),1920),1),:));
            AvgSumOfSquares2 = (squeeze(sum(sum(WindowedImage2,1),2))*(settings.AEC_Window_Factor/2^32))';
            %         figure(8);imagesc(WindowedImage)
            %         drawnow
            SumOfSquaresDiff2(i,j,:) = (abs(floor(AvgSumOfSquares2) - [redAvgOfSqr greenAvgOfSqr blueAvgOfSqr]));
    end
end

delete(w)


[a] = find(sum(SumOfSquaresDiff2,3) == (min(min(sum(SumOfSquaresDiff2,3)))));

[SetIndex,LineOffset] = ind2sub(size(sum(SumOfSquaresDiff2,3)),a);