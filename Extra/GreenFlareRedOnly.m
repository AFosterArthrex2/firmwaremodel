%Red Channel
load('12bitCase1.mat')
IsaveCrop = [18 73 1080 1920];
I12bit_cap2 = zeros(size(Iin));
yRange = IsaveCrop(1):(IsaveCrop(1)+IsaveCrop(3)-1);
xRange = IsaveCrop(2):(IsaveCrop(2)+IsaveCrop(4)-1);
I12bit_cap2(yRange,xRange,:) = I12bit_cap;

Yr = floor(Iin(:,:,1)*ConvMat(1,1)/2^12);
Yg = floor(Iin(:,:,2)*ConvMat(1,2)/2^12);
Yb = floor(Iin(:,:,3)*ConvMat(1,3)/2^12);

Y = floor(Yr + Yg + Yb);

Pbr = floor(Iin(:,:,1)*ConvMat(2,1)/2^12);
Pbg = floor(Iin(:,:,2)*ConvMat(2,2)/2^12);
Pbb = floor(Iin(:,:,3)/2);

PbRmB = floor(Pbb - Pbr);

Pb = floor(PbRmB-Pbg);


Prr = floor(Iin(:,:,1)/2);
Prg = floor(Iin(:,:,2)*ConvMat(3,2)/2^12);
Prb = floor(Iin(:,:,3)*ConvMat(3,3)/2^12);

PrRmB = floor(Prr - Prb);

Pr = floor(PrRmB - Prg);

I_YPbPr = cat(3,Y,Pb,Pr);
I_YPbPr(:,:,1) = max(min(floor(I_YPbPr(:,:,1)),2^12-1) ,0);
I_YPbPr(:,:,2) = max(min(floor(I_YPbPr(:,:,2)),2^11-1),-2^11);
I_YPbPr(:,:,3) = max(min(floor(I_YPbPr(:,:,3)),2^11-1),-2^11);

      
I_YPbPr2 = cat(3,Iin(:,:,1)*ConvMat4(1,1)+Iin(:,:,2)*ConvMat4(1,2)+Iin(:,:,3)*ConvMat4(1,3),...
                Iin(:,:,1)*ConvMat4(2,1)+Iin(:,:,2)*ConvMat4(2,2)+Iin(:,:,3)*ConvMat4(2,3),...
                Iin(:,:,1)*ConvMat4(3,1)+Iin(:,:,2)*ConvMat4(3,2)+Iin(:,:,3)*ConvMat4(3,3));
            
            
            

rPr = floor(I_YPbPr(:,:,3)*ConvMat3(1,3)/2^11);

R = floor(I_YPbPr(:,:,1) - rPr);

R2 = floor(I_YPbPr(:,:,1)*ConvMat2(1,1)+I_YPbPr(:,:,2)*ConvMat2(1,2)+I_YPbPr(:,:,3)*ConvMat2(1,3));

R3 = I_YPbPr2(:,:,1)*ConvMat2(1,1)+I_YPbPr2(:,:,2)*ConvMat2(1,2)+I_YPbPr2(:,:,3)*ConvMat2(1,3);

figure;plot(1:2016,I12bit_cap2(500,:,1),1:2016,R2(500,:,1))
            