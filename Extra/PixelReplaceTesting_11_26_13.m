Isafe = Isave{1,1};
% 
% I2 = G2PixelReplace(Isafe,1);
% I3 = procHotPixel(Isafe,1);
% I4 = Isave{1,7};
% 
% diff2 = double(Isafe)-double(I2);
% 
% diff3 = double(Isafe)-double(I3);
% 
% diff4 = double(Isafe)-double(I4);
% 
% diff23 = double(I2)-double(I3);
% 
% diff34 = double(I4)-double(I3);


myfunc = @(x) max2nd(x);
myfunc2 = @(x) avgWOmax(x);
Imax(:,:,1) = nlfilter(Isafe(:,:,1),[3 3],myfunc);
Iavg(:,:,1) = nlfilter(Isafe(:,:,1),[3 3],myfunc2);


% Imax(:,:,2) = nlfilter(Isafe(:,:,2),[3 3],myfunc);
% Imax(:,:,3) = nlfilter(Isafe(:,:,3),[3 3],myfunc);


xOffset2 = double(settings.Crop_HDMI_Pixel);
yOffset2 = double(settings.Crop_HDMI_Line);
xOffset = 64;yOffset = 8;
I2c = I2((1:1080)+yOffset,(1:1920)+xOffset,:);
Imaxc = Imax((1:1080)+yOffset,(1:1920)+xOffset,:);
Iavgc = Iavg((1:1080)+yOffset,(1:1920)+xOffset,:);
Isafec = Isafe((1:1080)+yOffset,(1:1920)+xOffset,:);

diffcap = double(I12bit_cap)-double(I12bit);

Igain_cap(:,:,1) = (double(I12bit_cap(:,:,1)) - double(Iavgc(:,:,1)))./(double(Isafec(:,:,1)) - double(Iavgc(:,:,1)));
Igain(:,:,1) = (double(I12bit(:,:,1)) - double(Iavgc(:,:,1)))./(double(Isafec(:,:,1)) - double(Iavgc(:,:,1)));