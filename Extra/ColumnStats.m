%Column Stats
clear Iout Iout2

startTime = tic;
Port = 'COM4';
if(~exist('S1','var'))
    S1 = serial(Port,'BaudRate',9600);
end
if(~strcmpi('open',S1.status) || ~isvalid(S1))
    fopen(S1);
end
set(vid,'FramesPerTrigger',16)

aGain = 2;
cGain = 3;

CMOSregWrite(Port,[aGain aGain aGain],[cGain cGain cGain],S1)

%12bit capture. Left Side

%Remeber Register 9
reg9 = SerialReadCCU(Port,1,9,S1);

%Disable Processing (Gamma and Post Peaking/Sharpening)
% SerialBitWriteCCU(Port,1,9,'11',[25:-1:24],S1);
SerialBitWriteCCU(Port,1,9,'1',[0],S1);

%Fix Exposure Index
SerialBitWriteCCU(Port,1,10,'00',[3 4],S1);

%Enable 12-bit mode left side
SerialWriteCCU(Port,1,46,'00000001',S1);

start(vid)
while(isrunning(vid))
    pause(0.1)
end

Iraw = getdata(vid);
Ileft = convert8to12(Iraw);
clear Iraw
%12bit capture. Right Side

%Enable 12-bit mode left side
SerialWriteCCU(Port,1,46,'000000F1',S1);

start(vid)
while(isrunning(vid))
    pause(0.1)
end

Iraw = getdata(vid);
Iright = convert8to12(Iraw);
clear Iraw
Iout = cat(2,Ileft,Iright);
clear Ileft Iright
% Ib = Iright(400:527,400:527,:,:);

% RedB = squeeze(Ib(:,:,1,:));

%Enable Processing
SerialWriteCCU(Port,1,9,reg9,S1);

%Enable AutoExposure
SerialBitWriteCCU(Port,1,10,'10',[3 4],S1);

%Disable 12-bit mode
SerialWriteCCU(Port,1,46,'02940000',S1);


%Sum over time and columns
I1 = sum(sum(Iout,4),1);

I2 = mean(mean(Iout,4),1);

I3 = zeros(size(I2));
% for i = 1:size(I2,2)
%     for j = 1:3
%         I3(:,i,j) = std2(squeeze(Iout(:,i,j,:)));
%     end
% end

%Filter out bad pixels
Ip = mean(double(Iout),4);
badPix = find(Ip>mean(mean(mean(Ip)))+50);
[y x z] = ind2sub(size(Ip),badPix);
Iout2 = Iout;
for i = 1:16
    for j = 1:length(y)
        if(x(j)>1 && x(j)<1920 && y(j)>1 && y(j)<1080)
            Iout2(y,x,z,i) = median(median(Iout(y(j)+[-1:1],x(j)+[-1:1],z(j),i),1),2);
        end
    end
end
%Sum over time and columns
I4 = sum(sum(Iout2,4),1);

I5 = mean(mean(Iout2,4),1);

I6 = zeros(size(I2));
% for i = 1:size(I2,2)
%     for j = 1:3
%         I6(:,i,j) = std2(squeeze(Iout2(:,i,j,:)));
%     end
% end
% fclose(S1)
ColNoise = int8(squeeze(round((I5-mean(mean(I5)))*4*8/aGain/cGain ))); % *4 to store 2 fractional bits
figure;plot(ColNoise)