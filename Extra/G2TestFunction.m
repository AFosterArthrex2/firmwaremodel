%% G2TestFunction
% 
%%
function Iout = G2TestFunction(Iin,varargin)
%  [Iout] = G2TestFunction(Iin)
%   Inputs: 
%           Iin         - Input Image
%   Outputs: 
%           Iout        - Output Image
%

%   Gen2 Firmware Model - 
%   Author: Alexander Foster
%   Date:   10/12/15

if(~isempty(varargin))
    settings = varargin{1};
else
    %Use Default Settings
    settings = struct;
end

settings

Iout = Iin;
