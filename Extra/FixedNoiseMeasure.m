set(vid,'FramesPerTrigger',128)

%12bit capture. Left Side

%Remeber Register 9
reg9 = SerialReadCCU(Port,1,9,S1);

%Disable Processing (Gamma and Post Peaking/Sharpening)
SerialBitWriteCCU(Port,1,9,'11',[25:-1:24],S1);

%Enable 12-bit mode left side
SerialWriteCCU(Port,1,46,'00000001',S1);

start(vid)
while(isrunning(vid))
    pause(0.1)
end

Iraw = getdata(vid);
for i = 1:128
    Ileft(:,:,:,i) = convert8to12(Iraw(:,:,:,i));
end
clear Iraw
%12bit capture. Right Side

%Enable 12-bit mode right side
SerialWriteCCU(Port,1,46,'000000F1',S1);

start(vid)
while(isrunning(vid))
    pause(0.1)
end

Iraw = getdata(vid);
for i = 1:128
    Iright(:,:,:,i) = convert8to12(Iraw(:,:,:,i));
end
clear Iraw

%Enable Processing
SerialWriteCCU(Port,1,9,reg9,S1);

%Disable 12-bit mode
SerialWriteCCU(Port,1,46,'02940000',S1);

Iout = cat(2,Ileft,Iright);
clear Ileft Iright
%Sum over time and columns
%%
I1 = sum(sum(Iout,4),1);

I2 = mean(mean(Iout,4),1);

% ColNoise = mean(mean(Iout,4),1) - permute(squeeze(mean(mean(mean(Iout,1),2),4))*(ones(1,1920,1)),[3 2 1]);
for i = 1:3
    ColNoise(i,:) = mean(mean(Iout(:,:,i,:),4),1) - conv( mean(mean(Iout(:,:,i,:),4),1),0.1*ones(1,10),'same');
end

I3 = zeros(size(I2));
% for i = 1:size(I2,2)
%     for j = 1:3
%         I3(:,i,j) = std2(squeeze(Iout(:,i,j,:)));
%     end
% end

I4 = mean(mean(Iout,4),2);

% RowNoise = mean(mean(Iout,4),2) - permute(squeeze(mean(mean(mean(Iout,1),2),4))*(ones(1,1080)),[2 3 1]);
for i = 1:3
    RowNoise(i,:) = mean(mean(Iout(:,:,i,:),4),2) - conv( mean(mean(Iout(:,:,i,:),4),2),0.1*ones(1,10),'same');
end
I5 = zeros(size(I4));
for i = 1:size(I4,1)
    for j = 1:3
        I5(i,1,j) = std2(squeeze(Iout(i,:,j,:)));
    end
end

I6 = mean(Iout,4);
% I7 = zeros(size(Iout,1),size(Iout,2),size(Iout,3));
% for i = 1:size(Iout,1)
%     for j = 1:size(Iout,2)
%         for k = 1:size(Iout,3)
%             I7(i,j,k) = std(squeeze(single(Iout(i,j,k,:))));
%         end
%     end
% end
% I7 = std(double(Iout),0,4);

% figure;imagesc(I7(:,:,2))
% figure;imagesc(I6(:,:,2),[mean(mean(I6(:,:,2)))-2 mean(mean(I6(:,:,2)))+2])
% figure;plot(1:1920,squeeze(I2(:,:,2)),1:1920,squeeze(I3(:,:,2)))
% figure;plot(1:1080,squeeze(I4(:,:,2)),1:1080,squeeze(I5(:,:,2)))

% 
% load BlackNoise


FixedOutput = {I2, I3, I4, I5, I6, RowNoise, ColNoise};