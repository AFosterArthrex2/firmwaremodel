%GreenFlareAlphaTest
% load('safe.mat')
IsaveCrop = [15 73 1080 1920];
I12bit_cap2 = zeros(1108,2016,3);

yRange = IsaveCrop(1):(IsaveCrop(1)+IsaveCrop(3)-1);
xRange = IsaveCrop(2):(IsaveCrop(2)+IsaveCrop(4)-1);
I12bit_cap2(yRange,xRange,:) = I12bit_cap;

diff12bit2 = double(I12bit_cap2) - double(I);

R = I12bit_cap2(:,:,1);
G = I12bit_cap2(:,:,2);
B = I12bit_cap2(:,:,3);

Y = I_YPbPr(:,:,1);
Pb = I_YPbPr(:,:,2);
Pr = I_YPbPr(:,:,3);

HexConvMat2 = {'400' '000' '64e'; '400' '0c0' '1e0'; '400' '76e' '000'};
ConvMat3 = reshape(hex2dec(HexConvMat2),3,3);
ConvMat2 = reshape(hex2dec(HexConvMat2)/2^10,3,3).*[1 1 1; 1 -1 -1; 1 1 1];

alpha1 = min(max((R - ConvMat2(1,1)*Y)./(ConvMat2(1,2)*Pb + ConvMat2(1,3)*Pr),0),1);

alpha2 = min(max((G - ConvMat2(2,1)*Y)./(ConvMat2(2,2)*Pb + ConvMat2(2,3)*Pr),0),1);

alpha3 = min(max((B - ConvMat2(3,1)*Y)./(ConvMat2(3,2)*Pb + ConvMat2(3,3)*Pr),0),1);

figure;LinkImage(min(2,max(-2,H)),Alpha,abs(diff12bit2),alpha1,alpha2,alpha3,double(Alpha)-double(alpha1),double(Alpha)-double(alpha2),double(Alpha)-double(alpha3),{'','Alpha Sim','diff12bit','Red','Green','Blue','diffRed','diffGreen','diffBlue'})
