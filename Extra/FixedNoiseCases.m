% Noise Measurement Script 8bit
% Alexander Foster
% 12/4/13
% UHD4 project


addpath('PolyfitnTools\')
Port = 'COM6';
inputSpreadSheet = 'SettingsForNoise.xlsx';
% Yrange = 500:599;

[a b c] = xlsread(inputSpreadSheet);
% if(~exist('d','var'))
    d = c;
% end

fprintf('UHD4 Noise Measurement.\nUsing Test Case Spreadsheet: %s\n',inputSpreadSheet);
        
[a b c] = xlsread(inputSpreadSheet);

if(~exist('S1','var'))
    S1 = serial(Port,'BaudRate',9600);
end
if(strcmpi('closed',S1.status))
    fopen(S1)
end
%Remeber Register 9
reg9 = SerialReadCCU(Port,1,9,S1);
%Disable Processing (Gamma and Post Peaking/Sharpening)
SerialBitWriteCCU(Port,1,9,'11',[25:-1:24],S1);

%Enable 12-bit mode
SerialWriteCCU(Port,1,46,'00000071',S1);

% figure(1)
for testCase = 4

readSettingsFromExcel

WriteSettings


    FixedNoiseMeasure
    Fixed{testCase} = FixedOutput;
    
testCase
end

%Enable Processing
SerialWriteCCU(Port,1,9,reg9,S1);

%Disable 12-bit mode
SerialWriteCCU(Port,1,46,'00000000',S1);

% fclose(S1)

% xlswrite('ResultsFixed.xlsx',d);

% winopen('ResultsFixed.xlsx')