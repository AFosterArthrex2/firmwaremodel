function [Iout] = procYCbCr2RGB(Iin)

I = floor(double(Iin));

R = 4*I(:,:,3) + I(:,:,1);
G = I(:,:,1) - 2*I(:,:,2) - 2*I(:,:,3);
B = 4*I(:,:,2) + I(:,:,1);

Iout(:,:,1) = min(max(fix(R),0),2^12-1);
Iout(:,:,2) = min(max(fix(G),0),2^12-1);
Iout(:,:,3) = min(max(fix(B),0),2^12-1);

%truncate and cap as a 12 bit integer
Iout = uint16(max(floor(Iout),0));

end