% Noise Measurement Script 8bit
% Alexander Foster
% 12/4/13
% UHD4 project


addpath('PolyfitnTools\')
Port = 'COM4';
inputSpreadSheet = 'SNRSettings.xlsx';
Yrange = 800:899;
XrangeSignal = 1000:1099;
XrangeBlack = 3400:3499;

Repeats = 1;

[a b c] = xlsread(inputSpreadSheet);
% if(~exist('d','var'))
    d = c;
% end
ResultsCol(1) = size(c,2)+1;
d{1,ResultsCol(1)} = 'Results SNR(dB) High';
d{2,ResultsCol(1)} = 'res';

ResultsCol(2) = size(c,2)+2;
d{1,ResultsCol(2)} = 'Results SNR(ratio) High';
d{2,ResultsCol(2)} = 'res';

ResultsCol(3) = size(c,2)+3;
d{1,ResultsCol(3)} = 'Results SNR(diff) High';
d{2,ResultsCol(3)} = 'res';

ResultsCol(4) = size(c,2)+4;
d{1,ResultsCol(4)} = 'Results SNR(dB) Low';
d{2,ResultsCol(4)} = 'res';

ResultsCol(5) = size(c,2)+5;
d{1,ResultsCol(5)} = 'Results SNR(ratio) Low';
d{2,ResultsCol(5)} = 'res';

ResultsCol(6) = size(c,2)+6;
d{1,ResultsCol(6)} = 'Results SNR(diff) Low';
d{2,ResultsCol(6)} = 'res';

ResultsCol(7) = size(c,2)+7;
d{1,ResultsCol(7)} = 'Results Periodic Low Period';
d{2,ResultsCol(7)} = 'res';

ResultsCol(8) = size(c,2)+8;
d{1,ResultsCol(8)} = 'Results Periodic Low Value';
d{2,ResultsCol(8)} = 'res';

ResultsCol(9) = size(c,2)+9;
d{1,ResultsCol(9)} = 'Results Periodic Low Diff';
d{2,ResultsCol(9)} = 'res';

ResultsCol(10) = size(c,2)+10;
d{1,ResultsCol(10)} = 'Results Periodic Low Period';
d{2,ResultsCol(10)} = 'res';

ResultsCol(11) = size(c,2)+11;
d{1,ResultsCol(11)} = 'Results Periodic Low Value';
d{2,ResultsCol(11)} = 'res';

ResultsCol(12) = size(c,2)+12;
d{1,ResultsCol(12)} = 'Results Periodic Low Diff';
d{2,ResultsCol(12)} = 'res';

fprintf('UHD4 Noise Measurement.\nUsing Test Case Spreadsheet: %s\n',inputSpreadSheet);
        
[a b c] = xlsread(inputSpreadSheet);

for testCase = 21:25
    fprintf('Test Case: %02d.\n',testCase)
    readSettingsFromExcel
    
    WriteSettings
    
    for i = 1:Repeats
    valid = 0;
    retry = 0;
    %Check captured image
    while(~valid) 
        Ic = G2Capture(4,vid);
     
        Signal = cat(3,Ic(Yrange,XrangeSignal,:),rgb2gray(Ic(Yrange,XrangeSignal,:)));
        SignalF = zeros(size(Signal));
        Black = cat(3,Ic(Yrange,XrangeBlack,:),rgb2gray(Ic(Yrange,XrangeBlack,:)));
        BlackF = zeros(size(Black));
        
        valid = SNRcheckInputImage(Ic,[Yrange; XrangeSignal; XrangeBlack],testCase,100,50,100);
        drawnow
        retry = retry + 1;
        if(~valid)
            if(retry < 3)
                continue;
            end
            if(strcmpi('Cancel',questdlg('Invalid Image. Adjust Head and press OK to try again.','Invalid Image','OK','Cancel','OK')))
                return
            end
        end
        
    end
    
    
    %SNR
    
    [x y] = meshgrid(1:100,1:100);
    indepvar = [reshape(x,[],1) reshape(y,[],1)];
    for k = 1:4
        depvar = double(reshape(Signal(:,:,k),[],1));
        Model = polyfitn(indepvar,depvar,2);
        Model.Coefficients(end) = 0;
        Trend = polyvaln(Model,indepvar);
        SignalF(:,:,k) = reshape(depvar-Trend+mean(mean(Trend)),100,100);
    end
    
    
    
    for k = 1:4
        depvar = double(reshape(Black(:,:,k),[],1));
        Model = polyfitn(indepvar,depvar,2);
        Model.Coefficients(end) = 0;
        Trend = polyvaln(Model,indepvar);
        BlackF(:,:,k) = reshape(depvar-Trend+mean(mean(Trend)),100,100);
        
    end
    
    
    Avg(testCase,:,i) = squeeze(mean(mean(SignalF,1),2)-mean(mean(BlackF,1),2))';
    
    Noise(testCase,:,i) = [std2(SignalF(:,:,1)) std2(SignalF(:,:,2)) std2(SignalF(:,:,3)) std2(SignalF(:,:,4))];
    Noise2(testCase,:,i) = [std2(BlackF(:,:,1)) std2(BlackF(:,:,2)) std2(BlackF(:,:,3)) std2(BlackF(:,:,4))];
    
    SNR(testCase,:,i) = 20*log10(Avg(testCase,:,i)./Noise(testCase,:,i));
    SNR2(testCase,:,i) = 20*log10(Avg(testCase,:,i)./Noise2(testCase,:,i));
    
    %Periodic
    
    %High Intensity
    tmp = log10(abs(fft(sum(SignalF(:,:,1)))));
    tmp(1) = 0;
    [pks,locs] = findpeaks(tmp(1:50),'SORTSTR','descend');
    PhighV = pks(1);
    PhighT = ((locs(1)-1)/100)^-1;
    PhighD = max(pks)-mean(pks(2:10));
    
    %Low Intensity
    tmp = log10(abs(fft(sum(BlackF(:,:,1)))));
    tmp(1) = 0;
    [pks,locs] = findpeaks(tmp(1:50),'SORTSTR','descend');
    PlowV = pks(1);
    PlowT = ((locs(1)-1)/100)^-1;
    PlowD = max(pks)-mean(pks(2:10));
    
    Pnoise(testCase,:,i) = [PhighT PhighV PhighD PlowT PlowV PlowD];
    
    end
    AvgF(testCase,:) = mean(Avg(testCase,:,i),3);
    NoiseF(testCase,:) = mean(Noise(testCase,:,i),3);
    SNRF(testCase,:) = 20*log10(AvgF(testCase,:)./NoiseF(testCase,:));
    Noise2F(testCase,:) = mean(Noise2(testCase,:,i),3);
    SNR2F(testCase,:) = 20*log10(AvgF(testCase,:)./Noise2F(testCase,:));
    
    PnoiseF(testCase,:) = [mode(Pnoise(testCase,1,:)) mean(Pnoise(testCase,2,:)) mean(Pnoise(testCase,3,:)) mode(Pnoise(testCase,4,:))  mean(Pnoise(testCase,5,:)) mean(Pnoise(testCase,6,:)) ];
    
    d{testCase+2,ResultsCol(1)} = SNRF(testCase,4);
    d{testCase+2,ResultsCol(2)} = AvgF(testCase,4)/NoiseF(testCase,4);
    d{testCase+2,ResultsCol(3)} = AvgF(testCase,4)/NoiseF(testCase,4) - AvgF(1,4)/NoiseF(1,4);
    d{testCase+2,ResultsCol(4)} = SNR2F(testCase,4);
    d{testCase+2,ResultsCol(5)} = AvgF(testCase,4)/Noise2F(testCase,4);
    d{testCase+2,ResultsCol(6)} = AvgF(testCase,4)/Noise2F(testCase,4) - AvgF(1,4)/Noise2F(1,4);  
    d{testCase+2,ResultsCol(7)} = PnoiseF(testCase,1);
    d{testCase+2,ResultsCol(8)} = PnoiseF(testCase,2);
    d{testCase+2,ResultsCol(9)} = PnoiseF(testCase,3);
    d{testCase+2,ResultsCol(10)} = PnoiseF(testCase,4);
    d{testCase+2,ResultsCol(11)} = PnoiseF(testCase,5);
    d{testCase+2,ResultsCol(12)} = PnoiseF(testCase,6);
end

% xlswrite('Results.xlsx',d);
% 
% winopen('Results.xlsx')