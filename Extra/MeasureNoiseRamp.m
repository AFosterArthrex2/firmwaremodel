% Noise Measurement Script 8bit
% Alexander Foster
% 12/4/13
% UHD4 project


addpath('PolyfitnTools\')
Port = 'COM4';
inputSpreadSheet = 'RampSNRSettings.xlsx';
Yrange = 500:599;
XrangeSignal = 200:299;
XrangeBlack = 700:799;
IndexRange = [1 800;1 800;1 800;1 800;1 800;1 800;1 800;1 800;1 800;1 800];
% IndexRange = [400 500;...   %1
%               400 500;...   %2
%               400 500;...   %3
%               400 500;...   %4  
%               400 500;...   %5
%               300 450;...   %6
%               300 450;...   %7
%               400 500;...   %8
%               400 500;...   %9
%               400 500];     %10

Repeats = 1;

[a b c] = xlsread(inputSpreadSheet);
% if(~exist('d','var'))
    d = c;
% end
SetupResults

fprintf('UHD4 Noise Measurement.\nUsing Test Case Spreadsheet: %s\n',inputSpreadSheet);
        
[a b c] = xlsread(inputSpreadSheet);

if(~exist('S1','var'))
    S1 = serial(Port,'BaudRate',9600);
end
if(strcmpi('closed',S1.status))
    fopen(S1)
end
%Remeber Register 9
reg9 = SerialReadCCU(Port,1,9,S1);
%Disable Processing (Gamma and Post Peaking/Sharpening)
SerialBitWriteCCU(Port,1,9,'11',[25:-1:24],S1);

%Enable 12-bit mode
SerialWriteCCU(Port,1,46,'00000071',S1);

figure(1)
for testCase = 1
  
readSettingsFromExcel

WriteSettings



%Signal To Noise
    SignalToNoiseMeasure
    fprintf('Test Case: %03d - %f\n',testCase,SNRF(testCase,4))
    d{testCase+2,ResultsCol(1)} = SNRF(testCase,4);
    d{testCase+2,ResultsCol(2)} = AvgF(testCase,4)/NoiseF(testCase,4);
    d{testCase+2,ResultsCol(3)} = AvgF(testCase,4)/NoiseF(testCase,4) - AvgF(1,4)/NoiseF(1,4);
    d{testCase+2,ResultsCol(4)} = SNR2F(testCase,4);
    d{testCase+2,ResultsCol(5)} = AvgF(testCase,4)/Noise2F(testCase,4);
    d{testCase+2,ResultsCol(6)} = AvgF(testCase,4)/Noise2F(testCase,4) - AvgF(1,4)/Noise2F(1,4);

%Periodic&Fixed Pattern Noise
    %Run each once at peak index
    SerialWriteCCU(Port,1,11,['0000' dec2hex(PeakIndex,4)],S1)
    PeriodicNoiseMeasure
    Periodic{1,testCase} = ChannelMsg;
    clear I12 SignalF2 indepvar depvar Trend
    
%     FixedNoiseMeasure
%     Fixed{1,testCase} = FixedOutput;
%     clear Iout Ileft Iright Iraw
    
    %Run each once at 0 index
    SerialWriteCCU(Port,1,11,['0000' dec2hex(0,4)],S1)
    PeriodicNoiseMeasure
    Periodic{2,testCase} = ChannelMsg;
    clear I12 SignalF2 indepvar depvar Trend
    
    FixedNoiseMeasure
    Fixed{testCase} = FixedOutput;
    clear Iout Ileft Iright Iraw
end

%Enable Processing
SerialWriteCCU(Port,1,9,reg9,S1);

%Disable 12-bit mode
SerialWriteCCU(Port,1,46,'00000000',S1);

fclose(S1)

xlswrite('ResultsRamp.xlsx',d);

winopen('ResultsRamp.xlsx')