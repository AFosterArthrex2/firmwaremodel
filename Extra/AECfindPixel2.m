%AEC find Pixel
% Finds the correct Pixel Offset for AEC Window

Offsets = [-200:200];
clear SumOfSquaresDiff2
w = waitbar(0,'Running');
I12bit_cap2 = double(I12bit_cap).^2;
for i = 1:1920
%     waitbar(((i-1)*length(Offsets)+j)/length(Offsets)^2,w,sprintf('%d out of %d',((i-1)*length(Offsets)+j),length(Offsets)^2))
        
    for j = 1:1920
% %         Line = 426+Offsets(j); Pixel = 905; Lines = 256+Offsets(i); Pixels = 256;
        Line = 206; Pixel = i; Lines = 696; Pixels = min(j,1920-i);
%         Line = 35; Pixel = 1+Offsets(i); Lines = 1038; Pixels = 1+Offsets(j);
%         Line = 206; Pixel = 646+Offsets(i); Lines = 696; Pixels = 780+Offsets(j);
%         Line = 426; Pixel = 905+Offsets(i); Lines = 256; Pixels = 256+Offsets(j);
%         WindowedImage = cat(3,Window{settings.AEC_Window+1},Window{settings.AEC_Window+1},Window{settings.AEC_Window+1}).*double(I12bit_cap);
%         WindowedImage = I12bit_cap(logical(cat(3,Window{settings.AEC_Window+1},Window{settings.AEC_Window+1},Window{settings.AEC_Window+1})));
        WindowedImage2 = (I12bit_cap2(Line:(Line),Pixel:(Pixel+Pixels-1),:));
        AvgSumOfSquares2 = (squeeze(sum(sum(WindowedImage2,1),2))/(size(WindowedImage2,1)*size(WindowedImage2,2)))';
%         figure(8);imagesc(WindowedImage)
%         drawnow
        SumOfSquaresDiff2(i,j,:) = (abs(AvgSumOfSquares2 - [redAvgOfSqr greenAvgOfSqr blueAvgOfSqr]));
    end
end

delete(w)