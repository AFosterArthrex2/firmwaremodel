function Iout = AHistMap2(Iin,lambda)

I2 = myRGB2YCbCr(Iin);
[counts C] = hist(reshape(I2(:,:,1),1,[]),1:2^14);
countsC = cumsum(counts);

inputMax = find(counts,1,'last');

map = (inputMax*counts/sum(counts)+lambda)/(inputMax*(1+lambda));
mapC = round(cumsum(map)*size(Iin,2)*size(Iin,1));

for i = 1:2^14
    if(~isempty(find(mapC >= countsC(i),1,'first')))
        out(i) = find(mapC >= countsC(i),1,'first');
    else
        out(i) = 2^14;
    end
end

I3(:,:,1) = out(uint16(round(min(2^14,max(1,I2(:,:,1))))));

Iout = myYCbCr2RGB(cat(3,I3(:,:,1),I2(:,:,2),I2(:,:,3)));