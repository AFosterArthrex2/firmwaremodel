Port = 'COM4';
global L
L = 0;
%Disable Processing (Gamma and Post Peaking/Sharpening)
%         SerialBitWriteCCU(Port,1,9,'11',[25:-1:24]);
        
        %Enable 12-bit mode
%         SerialWriteCCU(Port,1,46,'00000071');     

F = figure('Name', 'My Custom Preview Window');
      uicontrol('String', 'Close', 'Callback', 'close(gcf)');
%
%       % Create an image object for previewing.
      vidRes = get(vid, 'VideoResolution');
      nBands = get(vid, 'NumberOfBands');
      hImage = image( zeros(vidRes(2), vidRes(1), nBands) );
      
      setappdata(hImage,'UpdatePreviewWindowFcn',@myPreviewUpdate2)
      preview(vid, hImage);
%    
i = 1;
while(ishghandle(hImage))
    pause(5)
    if(i < 6)
        i = i + 1;
    else
        i = 1;
    end
    L = values(i)
    set(F,'Name',num2str(L))
end
