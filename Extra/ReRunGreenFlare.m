%Rerun G2GreenFlare

I = Isave{1,8};

I = G2GreenFlare(I,settings);

xOffset = double(settings.Crop_Proc_Pixel);
yOffset = double(settings.Crop_Proc_Line);
if(settings.Head_Type == 0)
    I = I((1:1092)+yOffset-1,(1:1936)+xOffset,:);
else
    I = I((1:1092)+yOffset,(1:1936)+xOffset,:);
end

%Crop to 1080x1920
xOffset2 = double(settings.Crop_HDMI_Pixel);
yOffset2 = double(settings.Crop_HDMI_Line);
I1080 = I((1:1080)+yOffset2,(1:1920)+xOffset2,:);
Isave{2,1} = I1080;fprintf('.');
I12bit = I1080;

diff12bit = double(I12bit_cap)- double(I12bit);
%Mask Out Borders
bordersize = 12;

diff12bit([1:bordersize end-bordersize:end],:,:) = 0;
diff12bit(:,[1:bordersize end-bordersize:end],:) = 0;

fprintf('12bit Errors: %d.\n',length(find(diff12bit)))

s{1} = subplot(3,3,1);imagesc(double(I12bit(:,:,1))),title('Simulation 12bit Red'),drawnow
s{2} = subplot(3,3,2);imagesc(double(I12bit(:,:,2))),title('Simulation 12bit Green'),drawnow
s{3} = subplot(3,3,3);imagesc(double(I12bit(:,:,3))),title('Simulation 12bit Blue'),drawnow

s{4} = subplot(3,3,4);imagesc(double(I12bit_cap(:,:,1))),title('Captured 12bit Red'),drawnow
s{5} = subplot(3,3,5);imagesc(double(I12bit_cap(:,:,2))),title('Captured 12bit Green'),drawnow
s{6} = subplot(3,3,6);imagesc(double(I12bit_cap(:,:,3))),title('Captured 12bit Blue'),drawnow

s{7} = subplot(3,3,7);imagesc(double(diff12bit(:,:,1))),title('Difference 12bit Red'),drawnow
s{8} = subplot(3,3,8);imagesc(double(diff12bit(:,:,2))),title('Difference 12bit Green'),drawnow
s{9} = subplot(3,3,9);imagesc(double(diff12bit(:,:,3))),title('Difference 12bit Blue'),drawnow
linkaxes([s{:}])