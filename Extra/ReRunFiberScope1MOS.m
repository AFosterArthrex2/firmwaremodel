%Rerun 
% save('safe.mat','I12bit_cap')
% 
IsaveCrop = [15 73 1080 1920];
I12bit_cap2 = zeros(1108,2016,1);
% 
yRange = IsaveCrop(1):(IsaveCrop(1)+IsaveCrop(3)-1);
xRange = IsaveCrop(2):(IsaveCrop(2)+IsaveCrop(4)-1);
I12bit_cap2(yRange,xRange,1) = I12bit_cap(:,:,3);
% 
I = Isave{1,10};
I12bitNoFilter = I(:,:,3);

save('safe.mat','I12bit_cap2','I12bitNoFilter')

% settings.Pixel_Replace_Disable = 1;
I = G2FiberScopeFilter(I,settings);

I12bit = I(:,:,3);
diff12bit = double(I12bit_cap2)- double(I12bit);
% Mask Out Borders
bordersize = 120;

diff12bit([1:bordersize end-bordersize:end],:,:) = 0;
diff12bit(:,[1:bordersize end-bordersize:end],:) = 0;
diff12bit2 = zeros(size(I12bit_cap2));
diff12bit2(yRange,xRange,:) = diff12bit(yRange,xRange,:);

fprintf('12bit Errors: %d.\n',length(find(diff12bit)))

s{1} = subplot(4,4,1);imagesc(double(I12bit(1:2:end,1:2:end,1))),title('Simulation 12bit Red'),drawnow
s{2} = subplot(4,4,2);imagesc(double(I12bit(1:2:end,2:2:end,1))),title('Simulation 12bit Green'),drawnow
s{3} = subplot(4,4,3);imagesc(double(I12bit(2:2:end,1:2:end,1))),title('Simulation 12bit Green2'),drawnow
s{4} = subplot(4,4,4);imagesc(double(I12bit(2:2:end,2:2:end,1))),title('Simulation 12bit Blue'),drawnow

s{5} = subplot(4,4,5);imagesc(double(I12bit_cap2(1:2:end,1:2:end,1))),title('Captured 12bit Red'),drawnow
s{6} = subplot(4,4,6);imagesc(double(I12bit_cap2(1:2:end,2:2:end,1))),title('Captured 12bit Green'),drawnow
s{7} = subplot(4,4,7);imagesc(double(I12bit_cap2(2:2:end,1:2:end,1))),title('Captured 12bit Green2'),drawnow
s{8} = subplot(4,4,8);imagesc(double(I12bit_cap2(2:2:end,2:2:end,1))),title('Captured 12bit Blue'),drawnow

s{9} = subplot(4,4,9);imagesc(double(diff12bit2(1:2:end,1:2:end,1))),title('Difference 12bit Red'),drawnow
s{10} = subplot(4,4,10);imagesc(double(diff12bit2(1:2:end,2:2:end,1))),title('Difference 12bit Green'),drawnow
s{11} = subplot(4,4,11);imagesc(double(diff12bit2(2:2:end,1:2:end,1))),title('Difference 12bit Green2'),drawnow
s{12} = subplot(4,4,12);imagesc(double(diff12bit2(2:2:end,2:2:end,1))),title('Difference 12bit Blue'),drawnow

s{13} = subplot(4,4,13);imagesc(double(I12bitNoFilter(1:2:end,1:2:end,1))),title('PreFilter 12bit Red'),drawnow
s{14} = subplot(4,4,14);imagesc(double(I12bitNoFilter(1:2:end,2:2:end,1))),title('PreFilter 12bit Green'),drawnow
s{15} = subplot(4,4,15);imagesc(double(I12bitNoFilter(2:2:end,1:2:end,1))),title('PreFilter 12bit Green2'),drawnow
s{16} = subplot(4,4,16);imagesc(double(I12bitNoFilter(2:2:end,2:2:end,1))),title('PreFilter 12bit Blue'),drawnow

% s{8} = subplot(4,3,8);imagesc(double(diff12bit(:,:,2))),title('Difference 12bit Green'),drawnow
% s{9} = subplot(4,3,9);imagesc(double(diff12bit(:,:,3))),title('Difference 12bit Blue'),drawnow

% s{10} = subplot(4,3,10);imagesc(double(Isave{1,1}(:,:,1))),title('Difference 12bit Red'),drawnow
% s{11} = subplot(4,3,11);imagesc(double(Isave{1,1}(:,:,2))),title('Difference 12bit Green'),drawnow
% s{12} = subplot(4,3,12);imagesc(double(Isave{1,1}(:,:,3))),title('Difference 12bit Blue'),drawnow
% 
% s{11} = subplot(4,3,11);imagesc(double(Alpha)),title('Alpha'),drawnow
% s{10} = subplot(4,3,10);imagesc(double(H),[-1 1]),title('H'),drawnow
% s{12} = subplot(4,3,12);imagesc(Isave{1,8}),title('PreFilter'),drawnow


linkaxes([s{:}])