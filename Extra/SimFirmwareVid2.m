reader = VideoReader('test.avi');
Writer = VideoWriter('test2.avi','Uncompressed AVI');
Writer2 = VideoWriter('test3.avi','Uncompressed AVI');
open(Writer)
open(Writer2)
load('ColGains_3_5_14')
for i = 1:reader.NumberOfFrames
    Id = read(reader,i);
    Iraw = convert8to12(Id);
    I = Iraw;
    SimulateFirmware4Video
    IunCorrected = I1080;
    
    
    I = ApplyGainCorrection(Iraw,ColGainCorrection,16);
    SimulateFirmware4Video
    ICorrected = I1080;
    
    writeVideo(Writer,IunCorrected);
    writeVideo(Writer2,ICorrected);
    if(mod(i,25) == 0)
        fprintf('%d\n',i)
    end
    
end
close(Writer)
close(Writer2)