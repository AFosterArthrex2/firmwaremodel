I = double(imread('03-9.30.13.png'));

for i = 1:3
    I(:,:,i) = double(I(:,:,i))/mean(mean(double(I(:,:,i))));
end

I = I/max(max(max(I)));

Istart{1} = I(1:540,1:960,:);
Istart{2} = I(1:540,961:1920,:);
Istart{3} = I(541:1080,1:960,:);
Istart{4} = I(541:1080,961:1920,:);

for i = 1:4
    Iscaled{i} = imresize(Istart{i},2,'method','bilinear');
end

figure;imagesc([Iscaled{1} Iscaled{2}; Iscaled{3} Iscaled{4}])

zoomF = 1.5;

offset = round([1080 1920]*(1-1/zoomF));


Izoom{1} = imresize(Iscaled{1}(offset(1)+1:1080,offset(2)+1:1920,:),zoomF,'method','bilinear');

Izoom{2} = imresize(Iscaled{2}(offset(1)+1:1080,1:1920-offset(2),:),zoomF,'method','bilinear');

Izoom{3} = imresize(Iscaled{3}(1:1080-offset(1),offset(2)+1:1920,:),zoomF,'method','bilinear');

Izoom{4} = imresize(Iscaled{4}(1:1080-offset(1),1:1920-offset(2),:),zoomF,'method','bilinear');

figure;imagesc([Izoom{1} Izoom{2}; Izoom{3} Izoom{4}])
