%Gain Correction Measure
%Measure Illumination at several exposure levels
startTime = tic;
Port = 'COM6';
if(~exist('S1','var'))
    S1 = serial(Port,'BaudRate',9600);
end
if(~strcmpi('open',S1.status) || ~isvalid(S1))
    fopen(S1);
end

%Setup Frame Grabber
set(vid,'FramesPerTrigger',8)

%Disable Processing 
SerialBitWriteCCU(Port,1,9,'1',0,S1);

%Manual Exposure Index
SerialBitWriteCCU(Port,1,9,'0',4,S1);
SerialBitWriteCCU(Port,1,10,'00',[4:-1:3],S1);

Exposure = [0:10:700];

for i = 1:length(Exposure)
i,toc(startTime)
    %Set Exposure Index
    SerialBitWriteCCU(Port,1,11,dec2bin(Exposure(i),10),[9:-1:0],S1);
    
    %Enable 12-bit mode left side
    SerialWriteCCU(Port,1,46,'00000001',S1);
    start(vid)
    while(isrunning(vid))
        pause(0.1)
    end
    
    Iraw = getdata(vid);
    Ileft = convert8to12(Iraw);
    
    %12bit capture. Right Side
    
    %Enable 12-bit mode left side
    SerialWriteCCU(Port,1,46,'000000F1',S1);
    
    start(vid)
    while(isrunning(vid))
        pause(0.1)
    end
    
    Iraw = getdata(vid);
    Iright = convert8to12(Iraw);
    
    clear Iraw
    Iout = cat(2,Ileft,Iright);
    clear Ileft Iright
    
    I1(:,:,i) = squeeze(mean(mean(Iout,4),2));
    I2(:,:,i) = squeeze(mean(mean(Iout,4),1));
    I3(:,i) = squeeze(mean(mean(mean(Iout,4),1),2));
    clear Iout
end
for i = 1:size(I2,1)
    I4(i,:,:) = squeeze(I2(i,:,:))./I3;
end
    
