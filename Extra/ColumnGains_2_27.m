Llist = dir('C:\Users\AFoster\Videos\Left*.avi');
Rlist = dir('C:\Users\AFoster\Videos\Right*.avi');
Llist2 = dir('C:\Users\AFoster\Videos\No Diff Left*.avi');
Rlist2 = dir('C:\Users\AFoster\Videos\No Diff Right*.avi');
addpath('polyfitnTools\')
addpath('SSH&Misc\')
list = Llist;

Columns = zeros(960,3,length(list));
for i = 1:length(list)
    vidRead = VideoReader(['C:\Users\AFoster\Videos\' list(i).name]);
    I = zeros(1080,960,3,50,'uint16');
    for j = 1:get(vidRead,'NumberOfFrames')
        tmp = read(vidRead,j);
        I(:,:,:,j) = convert8to12(tmp);
    end
    
    %Filter out bad pixels
    Ip = mean(double(I),4);
    I2 = I;
    
    for chan = 1:3
        [counts,x] = imhist(Ip(:,:,chan)/2^12,2^12);
        C = cumsum(counts);
        thresh = find(C>(C(end)-50),1,'first');
        badPix = find(Ip(:,:,chan)>thresh);
        
        [y x] = ind2sub(size(Ip(:,:,chan)),badPix);
        for k = 1:size(I,4)
            for j = 1:length(y)
                if(x(j)>1 && x(j)<960 && y(j)>1 && y(j)<1080)
                    I2(y(j),x(j),chan,k) = median(median(I(y(j)+[-1:1],x(j)+[-1:1],chan,k),1),2);
                end
            end
        end
    end
end
for i = 1:length(list)
    %Avg Of Each Column
    Columns(:,:,i) = squeeze(mean(mean(I2(:,:,:,:),4),1));
end

for win = [30]
    ColumnsP = padarray(Columns,[2*win 0 0],'symmetric');
    
    ColumnGains = zeros(960,3,6);
    for i = 1:3
        for j = 1:length(list)
            tmp = ColumnsP(:,i,j) - conv(ColumnsP(:,i,j),1/2/win*ones(1,2*win),'same');
            ColumnGains(:,i,j) = 1-(tmp(2*win+1:end-2*win)/(mean(mean(Columns(:,i,:)))));
        end
    end
    ColGainCorrection = squeeze(mean(ColumnGains(:,:,:),3));
    TestApplyGains
end
