%ReRun Internal Capture

[I,Modified,Errors] = CaptureInternal();
I4kint = I{1};
I1080int = I{2};
I1080pip = I{3};
Ithumb = I{4};
Ithumbpip = I{5};



if(InternalCap)
    diff4kint = double(I4kint) - double(IDP4ks);
    diff1080int = double(I1080int) - double(I1080Ds);
    diffPip = double(I1080pip) - double(I1080int);
    diffThumb = double(Ithumb) - double(I1080int(1:4:end,1:4:end,:));
    diffThumbPip = double(Ithumbpip) - double(I1080pip(1:4:end,1:4:end,:));
end

%Mask Out Borders
bordersize = 12;

if(InternalCap)
    diff4kint([1:2*bordersize end-2*bordersize:end],:,:) = 0;
    diff4kint(:,[1:2*bordersize end-2*bordersize:end],:) = 0;
    
    diff1080int([1:bordersize end-bordersize:end],:,:) = 0;
    diff1080int(:,[1:bordersize end-bordersize:end],:) = 0;
end

if(InternalCap)
    errors(5,testCase) = length(find(diff4kint));
    errors(6,testCase) = length(find(diff1080int));
    errors(7,testCase) = length(find(diffPip));
    errors(8,testCase) = length(find(diffThumb));
    errors(9,testCase) = length(find(diffThumbPip));
end
%%%%%%%%%%%Print Results%%%%%%%%%%%%%%

fprintf(['|%02d|Errors - 4kint: %09d\n|%02d|Errors - 1080i: %09d' ...
    '\n|%02d|Errors -   PIP: %09d\n|%02d|Errors - Thumb: %09d' ...
    '\n|%02d|Errors - PipTh: %09d\n'],...
    testCase,errors(5,testCase),testCase,errors(6,testCase),...
    testCase,errors(7,testCase),testCase,errors(8,testCase),...
    testCase,errors(9,testCase))

fprintf('|%02d|--------------------------------------------------|%02d|\n',testCase,testCase);
