function [Iover] = BioOpticoSpectral(I,C,B,Dlim,Imin,BioOrange)

Iout = zeros(size(I),'uint16');
Ifilt = zeros(size(I),'uint16');
Iover = zeros(size(I),'uint16');

Iin = I;

%Filter input image with 11x11 averaging filter
for k = 1:3
   for i = 1:size(I,1)
        Ifilt(i,:,k) = floor(conv(double(I(i,:,k)),ones(1,11)*5957/2^16,'same'));
    end
    for j = 1:size(I,2)
        Ifilt(:,j,k) = floor(conv(double(Ifilt(:,j,k)),ones(11,1)*5957/2^16,'same'));
    end
end

%Outer 5 pixels are passed unfiltered
Imask = ones(size(Ifilt));
Imask(5:end-4,14:end-13,:) = 0;
Ifilt(logical(Imask)) = Iin(logical(Imask));

%Intensity
Int = sum(double(Ifilt),3);

%Calculate Thickness D and clamp to 0<=D<=Dlim(4)
D = B(1) - floor((B(2)*double(Ifilt(:,:,1)) + B(3)*double(Ifilt(:,:,3)))./Int) + floor((1/4096)*B(4)*double(Int));
D(D>Dlim(4)) = Dlim(4);
D(D<0) = 0;


for i = 1:size(I,1)
    for j = 1:size(I,2)
        if(i == 13)
            if(j == 57)
                abc = 1;
            end
        end
        if(C(1) == 0)                       %Check On/Off Control Bit

        elseif(Int(i,j) < Imin)             %Intensity Error
            if(C(3))
                Iout(i,j,:) = [0 4095 0];   %If C[2] set display green
                Iover(i,j,:) = [0 4095 0];   %If C[2] set display green
            end
        elseif(sum(I(i,j,:)>=4095) && C(7) == 0)  %Saturation
            if(C(6))
                Iout(i,j,:) = [2048 2048 2048];   %If C[5] set display gray
                Iover(i,j,:) = [2048 2048 2048];   %If C[5] set display gray
            end
        elseif(D(i,j) == 0)
            if(C(4))
                Iout(i,j,:) = [0 0 4095];   %If C[3] set display Blue.
                Iover(i,j,:) = [0 0 4095];   %If C[3] set display Blue
            end
        elseif(D(i,j) < Dlim(1))
            Iout(i,j,:) = [4095 0 0];       %Red if less than D[0]
            Iover(i,j,:) = [4095 0 0];       %Red if less than D[0]
        elseif(D(i,j) < Dlim(2))
            Iout(i,j,:) = [4095 BioOrange 0];%Orange if less than D[1]
            Iover(i,j,:) = [4095 BioOrange 0];%Orange if less than D[1]
        elseif(D(i,j) < Dlim(3))
            Iout(i,j,:) = [4095 4095 0];    %Yellow if less than D[2]
            Iover(i,j,:) = [4095 4095 0];    %Yellow if less than D[2]
        elseif(C(5))
            Iout(i,j,:) = [4095 0 4095];    %Magenta if C[4] = 1
            Iover(i,j,:) = [4095 0 4095];    %Magenta if C[4] = 1
        end
    end
    if(mod(i,floor(size(I,1)/10)) == 0)
        fprintf('.')
    end
end
