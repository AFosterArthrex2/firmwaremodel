function [I,Status,Errors] = CaptureInternal(varargin)
%mode: 'all' - captures all images (10 of them), I is a cell array of images
%mode: vector - 10 elements, only returns image if mode(i) = 1.
% 1. 4k rgb
% 2. 1080 rgb
% 3. 1080 pip rgb
% 4. thumb rgb
% 5. thumb pip rgb
% 6. 4k jpg
% 7. 1080 jpg
% 8. 1080 pip jpg
% 9. thumb jpg
% 10. thumb pip jpg

%% Settings
reqsettings = {'IP',            '192.168.1.2';... %IP address of CCU
               'mode',          'all';...         %10 elements, only returns image if mode(i) = 1
               'saveraw',       false;...         %Saves the raw .rgb file
               'ID',            '';...            %Adds ID to raw .rgb file
               'MaxRetry',      3};               %Retrys x times if old image found

settings = struct();
for i = 1:(length(varargin)/2)
    settings.(varargin{i*2-1}) = varargin{i*2};
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

IP = settings.IP;
[status, r] = SSHFirstConnection(IP);

%Read Current Time to compare to modified time on image
[status, Date_Message] = system(sprintf('plink -pw Arthrex1 arthrex@%s date',IP));
OldModified = datenum(Date_Message(5:end));

Status = 0;
Errors = [];
I = [];
Mod = [];
Modified = [];


if(~status)
    %Failed to Connect
    Errors = [Errors;{sprintf('PCIe Capture Error: Cannot Connect to CCU @ %s',IP),r,50}];
end

directory = 'C:\Matlab Images\Internal Captures\';
if(~exist(directory,'dir'))
    mkdir(directory)
end

retry = 0;done = 0;
while(done == 0 && retry <= settings.MaxRetry)
    retry = retry + 1;
    pause(2)
    [status, Capture_Message] = system(sprintf('plink -pw Arthrex1 arthrex@%s capture',IP));
    if(regexp(Capture_Message,'FATAL ERROR'))
        Errors = [Errors;{'PCIe Capture Error: Capture Command Error',Capture_Message,52}];
        continue;
    end

    pause(5)
    
    [status, LS_Message] = system(sprintf('plink -pw Arthrex1 arthrex@%s ls /live/data/tmp/*.rgb',IP));
    if(regexp(LS_Message,'FATAL ERROR'))
        Errors = [Errors;{'PCIe Capture Error: Image Transfer Error',LS_Message,53}];
        continue;
    end
    
    [status, LS_Message_Full] = system(sprintf('plink -pw Arthrex1 arthrex@%s ls -l --full-time /live/data/tmp/*.rgb',IP));
    if(regexp(LS_Message_Full,'FATAL ERROR'))
        Errors = [Errors;{'PCIe Capture Error: Image Transfer Error',LS_Message_Full,53}];
        continue;
    end
    
    files = textscan(LS_Message,'%s');
    clear imNumberS imNumber
    imNumberS = regexp([files{1}{:}],'\d\d\d\d(?!p)','match');
    
    Mod = regexp(LS_Message_Full,'\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d.\d\d\d\d\d\d\d\d\d','match');

    ModifiedTime = datenum(Mod,'yyyy-mm-dd HH:MM:SS');
    ModifiedDate = datestr(ModifiedTime,'yy-mm-dd HH.MM.SS');
    
    [~, new] = max(ModifiedTime);
    
    if((ModifiedTime(new)-OldModified)*60*24 > 0)
        done = 1;
        Status = 1;
    end
end
if(~done)
    Errors = [Errors;{'PCIe Capture Error: New Image Not Found',Capture_Message,51}];
    I{1} = ones(2160,3840,3);
    I{2} = ones(1080,1920,3);
    I{3} = ones(1080,1920,3);
    I{4} = ones( 270, 480,3);
    I{5} = ones( 270, 480,3);
else
    
    %             Image         Horz    Vert
    imageTable = {'4k',         3840,   2160;...
                  '1080p',      1920,   1080;...
                  '1080p_pip',  1920,   1080;...
                  'thumb',      0480,   0270;...
                  'thumb_pip',  0480,   0270};
    
    for i = 1:length(imageTable)
        %filename of image on CCU
        filename1 = sprintf('/live/data/tmp/im_%s_%s.rgb',imNumberS{new},imageTable{i,1});
        %filename of image on PC
        if(~isempty(settings.ID))
            filename2 = sprintf('%sim_%s_%s_%s_%s.rgb',directory,settings.ID,imNumberS{new},imageTable{i,1},ModifiedDate(new,:));
        else
            filename2 = sprintf('%sim_%s_%s_%s.rgb',directory,imNumberS{new},imageTable{i,1},ModifiedDate(new,:));
        end
        %Transfer Image
        str = sprintf('pscp -scp -C -pw Arthrex1 arthrex@%s:%s "%s"',IP,filename1,filename2);
        try
            [status, Transfer_Message{i}] = system(str);
        catch err
            getReport(err)
        end
        if(status)
            Errors = [Errors;{['PCIe Capture Error: Cound Not Transfer ' imageTable{i,1} ' Image'],Transfer_Message{i},53}];
            Status = 0;
        end
        fid = fopen(filename2);
        data = fread(fid);fclose(fid);
        I{i} = uint8(permute(reshape(data,3,imageTable{i,2},imageTable{i,3}),[3 2 1]));

        if(~settings.saveraw)
            delete(filename2)
        end
    end
end