Values = char(zeros(5*256,8));

for i = 1:5 %Banks
    for j = 1:256   %Registers
        Values((i-1)*256+j,:) = SerialReadCCU(Port,1,256*(i-1)+j-1,S1);
    end
end
