function [Iout] = myRGB2YCbCr(Iin)

I = double(Iin);

Y  =  1*I(:,:,1) + 2*I(:,:,2) + 1*I(:,:,3);
Cb = -1*I(:,:,1) - 2*I(:,:,2) + 3*I(:,:,3);
Cr =  3*I(:,:,1) - 2*I(:,:,2) - 1*I(:,:,3);

Iout = cat(3,Y,Cb,Cr);

end