%Runs Input Image I (half image) through the proccessing chain.

% Ic = G2MedianFilter(Ic,settings.Median_Filter_Enable);
% 
% Ic = G2BlackOffset(Ic,settings.Black_Offset_Enable,[settings.Black_Offset_RedLevel;...
%                                                   settings.Black_Offset_GreenLevel;...
%                                                   settings.Black_Offset_BlueLevel]);
% 
% Ic = G2ColumnGains(Ic);
% 
% if(settings.AEC_Enable)
%     Ic = G2AEC(Ic,settings.AEC_Gain);
% end
% 
% Ic = G2PixelReplace(Ic,settings.Pixel_Replace_Enable,1);
% 
% Ic = G2Demosaic(Ic,settings.Demosaic_Force_Disable,1);
% 
% Ic = G2WhiteBalance(Ic,settings.WB_Gain_Red,settings.WB_Gain_Green,settings.WB_Gain_Blue);
% 
% Ic = G2Bilateral(Ic,settings.Bilateral_Filter_Enable,settings.Bilateral_Bit_Select);
% 
% Ic = G2BilateralSlope(Ic,settings.Bilateral_Slope_Enable,settings.Bilateral_Slope_Thresh,settings.Bilateral_Slope_Bit_Select);
% 
% Ic = G2FiberScopeFilter(Ic,settings.FiberScopeEnable,...
%                          [hex2dec(settings.FiberScope1)...
%                           hex2dec(settings.FiberScope2)...
%                           hex2dec(settings.FiberScope3)...
%                           hex2dec(settings.FiberScope4)...
%                           hex2dec(settings.FiberScope5)...
%                           hex2dec(settings.FiberScope6)]/4096);

%%%%%%%%%%%%%%%%%%%%%%%%%%%DisplayPort 4k%%%%%%%%%%%%%%%%%%%%%%%%
Ic = CoarseEdgeEnhance(Ic,CoarseGain);

Ic = G2Scaler(Ic);

Ic = G2GreenOffsetRev(Ic,settings.Green_Offset_Enable);
Ic = G2VerticalBinning(Ic,settings.Vertical_Binning);

if(settings.Green_Edge_Enhance_Enable)
    Ic = G2GreenEdgeEnhance(Ic,...
        settings.Green_Edge_Enhance_Gain,...
        settings.Green_Edge_Enhance_Thresh_Enable,...
        settings.Green_Edge_Enhance_Threshold);
end

Ic = G2CSC(Ic,...
    [settings.CSC_1_1; settings.CSC_1_2; settings.CSC_1_3;...
    settings.CSC_2_1; settings.CSC_2_2; settings.CSC_2_3;...
    settings.CSC_3_1; settings.CSC_3_2; settings.CSC_3_3]);

Ic = G2Gamma(Ic,settings.Gamma);

Ic = G2RGB2YCbCr(Ic);

Ic = G2PostPeaking(Ic,settings.Peaking_Gain,settings.Sharpening_Gain);

I4k = G2YCbCr2RGB(Ic);

%%%%%%%%%%%%%%%%%%%Downsampled 1080%%%%%%%%%%%%%%%%%%%%%%%%%
I1080Ds = G2DownScale(I4k);


