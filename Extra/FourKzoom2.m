I = double(imread('03-9.30.13.png'));

for i = 1:3
    I(:,:,i) = double(I(:,:,i))/mean(mean(double(I(:,:,i))));
end

I = I/max(max(max(I)));

ItruScale = imresize(I,1.5,'method','bilinear');
%1 pixel overlap

I1 = I(1:1080,1:965,:);
I1s = imresize(I1,1.5,'method','bilinear');
I2 = I(1:1080,955:end,:);
I2s = imresize(I2,1.5,'method','bilinear');

Ifinal = [I1s(:,1:1437,:) I2s(:,7:end,:)];
