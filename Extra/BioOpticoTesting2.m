%TPG_Mode
SerialWriteCCU(Port,1,256*4+128,[dec2hex(settings.TPG_Mode,1) '0000000'],S1);

SerialWriteCCU(Port,1,256*0+9,'FFFFFFFF',S1);

%Disable WatchDog
SerialBitWriteCCU(Port,1,256*2+34,'1',7,S1);

%Force Head Detect
SerialBitWriteCCU(Port,1,256*4+9,'1',2,S1);

%Enable BioOptico
SerialBitWriteCCU(Port,1,256*1+50,or(settings.BioOptico_Spectral,settings.BioOptico_Structure),0,S1);
%Choose Spectral or Structure
SerialBitWriteCCU(Port,1,256*1+50,settings.BioOptico_Spectral,1,S1);