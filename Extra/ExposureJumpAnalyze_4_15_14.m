directory = 'C:\Users\AFoster\Videos\';

file{1} = 'test2(with delay).avi';
file{2} = 'test2(without delay).avi';

Reader1= VideoReader([directory file{1}]);
Reader2= VideoReader([directory file{2}]);

for i = 1:get(Reader1,'NumberOfFrames')
    avg(1,i) = mean(mean(mean(read(Reader1,i))));
    avg(2,i) = mean(mean(mean(read(Reader2,i))));
end