
% I = G2MedianFilter(I,settings.Median_Filter_Enable);

I = G2AEC(I,settings.AEC_Gain);

I = G2WhiteBalance(I,settings.WB_Gain_Red,settings.WB_Gain_Green,settings.WB_Gain_Blue);

I = G2PixelReplace(I,settings.Pixel_Replace_Enable,settings.Head_Type);

I = G2Demosaic(I,settings.Demosaic_Force_Disable,settings.Head_Type);

I = G2Bilateral(I,settings.Bilateral_Filter_Enable,settings.Bilateral_Bit_Select);

I = G2BilateralSlope(I,settings.Bilateral_Slope_Enable,settings.Bilateral_Slope_Thresh,settings.Bilateral_Slope_Bit_Select);

%1080p HDMI output

I1080 = I;
I1080 = G2Gamma(I1080,settings.Gamma_1080);
I1080 = G2RGB2YCbCr(I1080);
I1080 = G2PostPeaking(I1080,settings.Peaking_Gain_1080,settings.Sharpening_Gain_1080);
I1080 = G2YCbCr2RGB(I1080);
