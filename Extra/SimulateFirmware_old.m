%% SimulateFirmware
% Script for Gen2 CCU Firmware Simulation.
%%

%% TestPattern
% Start with a test pattern or live image captured from the CCU Frame
% Buffer
if(settings.TPG_Mode > 0)
    I = G2TestPatternGenerator(settings.TPG_Mode,[1108 2016],settings.Seed_SIM);
else
    if(settings.TPG_Head > 0)
        SerialWriteCCU(Port,2,10,['0000000' num2str(settings.TPG_Head)],S1);
    end
    %Disable Overlays
    SerialBitWriteCCU(Port,1,256*1+50,'0',[0],S1);
    SerialBitWriteCCU(Port,1,256*4+50,'0',[28],S1);
    
    %Disable DeMosaic
    SerialBitWriteCCU(Port,1,256*4+49,'1',0,S1);
    
    %Remeber Register 9
    reg9 = SerialReadCCU(Port,1,9,S1);
    SerialWriteCCU(Port,1,256*0+9,'FFFFFFFF',S1);
    
    %Start Buffer
    SerialBitWriteCCU(Port,1,4*256+8,'1',[15],S1);
    pause(1.25)
    
    %Freeze Video
    SerialBitWriteCCU(Port,1,4*256+161,'1',[18],S1);
    pause(1.25)

    %Capture Image
    Iraw = G2Capture(22,vid,Port,S1);
    I = Iraw;

    %Restore Register 9
    SerialWriteCCU(Port,1,256*0+9,reg9,S1);
end

%% 1080p Processing
% Pre-upscale/Offset Processing is performed at 1080p
% Isave keeps a backup of each step
Isave{1,1} = I;fprintf('|%02d|.',testCase);

I = G2MedianFilter(I,settings);
Isave{1,2} = I;fprintf('.');

I = G2BlackOffset(I,settings);
Isave{1,3} = I;fprintf('.');

I = G2AEC(I,settings);
Isave{1,5} = I;fprintf('.');

I = G2PixelReplace(I,settings);
Isave{1,6} = I;fprintf('.');

I = G2Demosaic(I,settings);
Isave{1,7} = I;fprintf('.');

I = G2WhiteBalance(I,settings);
Isave{1,8} = I;fprintf('.');

I = G2Bilateral(I,settings.Bilateral_Filter_Enable,settings.Bilateral_Bit_Select);
Isave{1,9} = I;fprintf('.');

I = G2BilateralSlope(I,settings.Bilateral_Slope_Enable,settings.Bilateral_Slope_Thresh,settings.Bilateral_Slope_Bit_Select);
Isave{1,10} = I;fprintf('.');

I = G2FiberScopeFilter(I,settings.FiberScopeEnable,...
                         [hex2dec(settings.FiberScope1)...
                          hex2dec(settings.FiberScope2)...
                          hex2dec(settings.FiberScope3)...
                          hex2dec(settings.FiberScope4)...
                          hex2dec(settings.FiberScope5)...
                          hex2dec(settings.FiberScope6)]/4096);
Isave{1,11} = I;fprintf('.');

%Crop Before DDR Memory

xOffset = double(settings.Crop_Proc_Pixel);
yOffset = double(settings.Crop_Proc_Line);
if(settings.Head_Type == 0)
    I = I((1:1092)+yOffset-1,(1:1936)+xOffset,:);
else
    I = I((1:1092)+yOffset,(1:1936)+xOffset,:);
end
Isave{1,12} = I;fprintf('|');


%% 1080p HDMI output
% No longer used, replaced by downscaled 4k output

%Crop to 1080x1920
xOffset2 = double(settings.Crop_HDMI_Pixel);
yOffset2 = double(settings.Crop_HDMI_Line);
I1080 = I((1:1080)+yOffset2,(1:1920)+xOffset2,:);
Isave{2,1} = I1080;fprintf('.');
I12bit = I1080;
I1080 = G2Gamma1080(I1080,0);
Isave{2,2} = I1080;fprintf('.');
I1080 = G2RGB2YCbCr1080(I1080);
Isave{2,3} = I1080;fprintf('.');
I1080 = G2PostPeaking1080(I1080,0,0);
Isave{2,4} = I1080;fprintf('.');
I1080 = G2YCbCr2RGB1080(I1080);
Isave{2,5} = I1080;fprintf('|1080p & 12bit Done.\n');

%% 4k Processing
% Final Processing is performed at full 4k resolution
% Image is broken up into 4 vertical "stripes" 
     
%Crop 4x 1092x494
fprintf(['|%02d|          |>'],testCase);
IDP4k{1} = I((1:1092),(1:496)+0,:);
IDP4k{2} = I((1:1092),(1:496)+480,:);
IDP4k{3} = I((1:1092),(1:496)+960,:);
IDP4k{4} = I((1:1092),(1:496)+1440,:);

Isave{4,1} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2Scaler(IDP4k{i}); end
% Isave{4,2} = IDP4k;fprintf('.');
% for i = 1:4 IDP4k{i} = G2ScalerNN(IDP4k{i}); end
Isave{4,2} = IDP4k;fprintf('.');

if(settings.Head_Type)
    for i = 1:4 IDP4k{i} = G2GreenOffsetRev(IDP4k{i},settings.Green_Offset_Enable); end
else
    for i = 1:4 IDP4k{i} = G2GreenOffset(IDP4k{i},settings.Green_Offset_Enable); end
end
Isave{4,3} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2VerticalBinning(IDP4k{i},settings.Vertical_Binning); end
Isave{4,4} = IDP4k;fprintf('.');

if(settings.Green_Edge_Enhance_Enable)
    for i = 1:4
        IDP4k{i} = G2GreenEdgeEnhance(IDP4k{i},...
            settings.Green_Edge_Enhance_Gain,...
            settings.Green_Edge_Enhance_Thresh_Enable,...
            settings.Green_Edge_Enhance_Threshold);
    end
end
Isave{4,5} = IDP4k;fprintf('.');


for i = 1:4 IDP4k{i} = G2CSC(IDP4k{i},...
        [settings.CSC_1_1; settings.CSC_1_2; settings.CSC_1_3;...
         settings.CSC_2_1; settings.CSC_2_2; settings.CSC_2_3;...
         settings.CSC_3_1; settings.CSC_3_2; settings.CSC_3_3]);
end
Isave{4,6} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2RGB2YCbCr(floor(double(IDP4k{i}))); end
Isave{4,7} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2SharpAtt(IDP4k{i},settings.Sharpening_Gain,settings.Sharp_Att); end
Isave{4,8} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2YCbCr2RGB(IDP4k{i}); end
Isave{4,9} = IDP4k;fprintf('.');

for i = 1:4 IDP4k{i} = G2Gamma(IDP4k{i},settings.Gamma); end
% for i = 1:4 IDP4k{i} = G2Gamma(IDP4k{i},settings.Gamma); end
Isave{4,10} = IDP4k;fprintf('|DisplayPort Done.\n');

%% 4k Output
% Assemble 4 stripes into full 4k output

%Crop from 992x2184 to 960x2160
DPoffsetx = 1;
DPoffsety = 1;

IDP4ks = cat(2,IDP4k{1}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
               IDP4k{2}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
               IDP4k{3}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
               IDP4k{4}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:));
     

%% Downsampled 1080p Output
% Downsample without lowpass filtering to get default 1080p output

Isave{5,1} = Isave{4,10};
fprintf(['|%02d|               |>'],testCase);

%LowPass Filter
% for i = 1:4 I1080D{i} = G2LowPass(Isave{5,1}{i},settings.Downscale_Filter_Disable); end
I1080D = Isave{4,10};
Isave{5,2} = I1080D;fprintf('.');

%Crop and Assemble
DSoffsetx = 1;
DSoffsety = 1;

I1080Ds = cat(2,I1080D{1}([13:end-12]+DSoffsety,[17:end-16]+DSoffsetx,:),...
               I1080D{2}([13:end-12]+DSoffsety,[17:end-16]+DSoffsetx,:),...
               I1080D{3}([13:end-12]+DSoffsety,[17:end-16]+DSoffsetx,:),...
               I1080D{4}([13:end-12]+DSoffsety,[17:end-16]+DSoffsetx,:));

Isave{5,3} = I1080Ds;fprintf('.');

I1080Ds = G2DownScale(I1080Ds);

% % I1080D12bit = I1080Ds;
% Isave{5,4} = I1080Ds;fprintf('.');
% 
% % I1080Ds = G2Gamma(I1080Ds,settings.Gamma_1080);
% Isave{5,5} = I1080Ds;fprintf('.');
% 
% % I1080Ds = G2RGB2YCbCr(I1080Ds);
% Isave{5,6} = I1080Ds;fprintf('.');
% 
% % I1080Ds = G2PostPeaking(I1080Ds,settings.Peaking_Gain_4K,settings.Sharpening_Gain_4K);
% % I1080Ds = G2PostPeaking(I1080Ds,0,0);
% Isave{5,7} = I1080Ds;fprintf('.');
% 
% % I1080Ds = G2YCbCr2RGB(I1080Ds);
% Isave{5,8} = I1080Ds;fprintf('.');

fprintf('|1080 Downscaled Done.\n');


%% BioOptico
% Generate BioOptico Overlays from output of fiberscope filter and apply to
% final output

if(~or(settings.BioOptico_Spectral,settings.BioOptico_Structure))
%     I1080D_over = I1080Ds;
%     IDP4k_over = IDP4ks;
else
    fprintf(['|%02d|   |>'],testCase);
    %Output of FiberScopeFilter
    IBioOptico = Isave{1,11};
    Isave{6,1} = IBioOptico;fprintf('.');
    
    %Decimate By 2
    IBioOptico = IBioOptico(1:2:end,1:2:end,:);
    Isave{6,2} = IBioOptico;fprintf('.');
    
    %Initial Crop
    xOffset = double(16);
    yOffset = double(4);
    IBioOptico = IBioOptico((1:546)+yOffset,(1:992)+xOffset,:);
    Isave{6,3} = IBioOptico;fprintf('.');
    
    %11x11 Low Pass
    IBioOptico = G2BioOpticoLowPass(IBioOptico);
    Isave{6,4} = IBioOptico;fprintf('.');
    
    %BioOptico Modules
    %             1  2  3  4  5  6  7  8  9 10 11 12
    extrabits = [12  8  8  4  8  4  8  8  4  4  8  8];
    %Extrabits defines the precision to truncate to at the numbered locations.
%     [IBioStructure, IBioStructureAlpha, varianceCombined, meanCombined, Int, ...
%     varianceMid, meanMid] = G2BioOpticoStructure(IBioOptico,...
%         settings.LuminanceThreshold,settings.Vthreshold,extrabits);
%     
    [IBioStructure, IBioStructureAlpha, varianceCombined, meanCombined, Int, ...
    varianceMid, meanMid] = G2BioOpticoStructure(IBioOptico,...
        settings.LuminanceThreshold,settings.Vthreshold,extrabits);
    
    
    Isave{6,5} = IBioStructure;
    
    IBioStructure2(:,:,1) = uint16(IBioStructureAlpha).*uint16(Isave{6,5}*16) + Isave{6,3}(:,:,1).*(1-uint16(IBioStructureAlpha));
    IBioStructure2(:,:,2) = uint16(IBioStructureAlpha).*uint16(Isave{6,5}*16) + Isave{6,3}(:,:,2).*(1-uint16(IBioStructureAlpha));
    IBioStructure2(:,:,3) = uint16(IBioStructureAlpha).*uint16(Isave{6,5}*16) + Isave{6,3}(:,:,3).*(1-uint16(IBioStructureAlpha));
    
    [IBioSpectral, IThick] = G2BioOpticoSpectral(IBioOptico,Isave{6,3},[1 0 0 0 0 0 0],...
        [hex2dec(settings.B0) hex2dec(settings.B1) hex2dec(settings.B2) hex2dec(settings.B3)],...
        [ 64   192   320   384],3685,128);

    Isave{6,6} = IBioSpectral;
    IBioSpectralAlpha = ones(size(Isave{6,6},1),size(Isave{6,6},2));
    IBioSpectralAlpha(sum(Isave{6,6},3) == 0) = 0;
    IBioSpectralAlpha(1:10,:) = 0;
    IBioSpectralAlpha = cat(3,IBioSpectralAlpha,IBioSpectralAlpha,IBioSpectralAlpha);
    IBioSpectral2 = uint16(IBioSpectralAlpha).*uint16(Isave{6,6}*16) + Isave{6,3}.*(1-uint16(IBioSpectralAlpha));
    
    
    
    
    %2x Scaler
%     if(settings.StructureScalerBilinear)
        IBioStructure = G2Scaler(IBioStructure);
%     else
%         IBioStructure = G2ScalerNN(IBioStructure);
%     end
%     if(settings.SpectralScalerBilinear)
%         IBioSpectral = G2Scaler(IBioSpectral);
%     else    
        IBioSpectral = G2ScalerNN(IBioSpectral);
%     end
    Isave{6,7} = IBioStructure;
    Isave{6,8} = IBioSpectral;
    
    %Final Crop Before Memory
    xOffset = double(32);
    yOffset = double(0);
    IBioStructure = IBioStructure((1:1092)+yOffset,(1:1936)+xOffset,:);
    Isave{6,9} = IBioStructure;fprintf('.');
    IBioSpectral = IBioSpectral((1:1092)+yOffset,(1:1936)+xOffset,:);
    Isave{6,10} = IBioSpectral;fprintf('.');
    
    %Create 4k stripes
    IBioStructure4k{1} = IBioStructure((1:1092),(1:496)+0,:);
    IBioStructure4k{2} = IBioStructure((1:1092),(1:496)+480,:);
    IBioStructure4k{3} = IBioStructure((1:1092),(1:496)+960,:);
    IBioStructure4k{4} = IBioStructure((1:1092),(1:496)+1440,:);
    Isave{6,11} = IBioStructure4k;fprintf('.');
    IBioSpectral4k{1} = IBioSpectral((1:1092),(1:496)+0,:);
    IBioSpectral4k{2} = IBioSpectral((1:1092),(1:496)+480,:);
    IBioSpectral4k{3} = IBioSpectral((1:1092),(1:496)+960,:);
    IBioSpectral4k{4} = IBioSpectral((1:1092),(1:496)+1440,:);
    Isave{6,12} = IBioSpectral4k;fprintf('.');
    %2x Scale NN
    for i = 1:4
        IBioStructure4k{i} = G2ScalerNN(IBioStructure4k{i});
        IBioSpectral4k{i} = G2ScalerNN(IBioSpectral4k{i});
    end
    Isave{6,13} = IBioStructure4k;fprintf('.');
    Isave{6,14} = IBioSpectral4k;fprintf('.');
    %Crop
    DPoffsetx = 1;
    DPoffsety = 0;
    
    IBioStructure4ks = cat(2,...
        IBioStructure4k{1}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioStructure4k{2}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioStructure4k{3}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioStructure4k{4}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:));
     
    IBioSpectral4ks = cat(2,...
        IBioSpectral4k{1}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioSpectral4k{2}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioSpectral4k{3}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioSpectral4k{4}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:));
    Isave{6,15} = IBioStructure4ks;fprintf('.');
    Isave{6,16} = IBioSpectral4ks;fprintf('.');
    
    IBioStructureAlpha4ks = zeros(size(IBioStructure4ks),'uint8');
    IBioStructureAlpha4ks(IBioStructure4ks>0) = 1;
    IBioStructure4ks = cat(3,IBioStructure4ks,IBioStructure4ks,IBioStructure4ks);
    IBioStructureAlpha4ks = cat(3,IBioStructureAlpha4ks,IBioStructureAlpha4ks,IBioStructureAlpha4ks);
    
    IBioSpectralAlpha4ks = ones(size(IBioSpectral4ks,1),size(IBioSpectral4ks,2),'uint8');
    IBioSpectralAlpha4ks(sum(IBioSpectral4ks,3) == 0) = 0;
    IBioSpectralAlpha4ks([1:12 2148:end],:) = 0;
%     IBioSpectralAlpha4ks(:,3840) = 0;
    IBioSpectralAlpha4ks = cat(3,IBioSpectralAlpha4ks,IBioSpectralAlpha4ks,IBioSpectralAlpha4ks);
    
    if(settings.BioOptico_Structure)
        IDP4ks = IBioStructureAlpha4ks.*uint8(IBioStructure4ks) + IDP4ks.*(1-IBioStructureAlpha4ks);
    else
        IDP4ks = IBioSpectralAlpha4ks.*uint8(IBioSpectral4ks) + IDP4ks.*(1-IBioSpectralAlpha4ks);
    end
    
    %Downscale overlays
%     for i = 1:4
%         IBioStructureD{i} = G2LowPass(Isave{6,13}{i},settings.Downscale_Filter_Disable);
%         IBioSpectralD{i} = G2LowPass(Isave{6,14}{i},settings.Downscale_Filter_Disable);
%     end
    IBioStructureD = Isave{6,13};
    IBioSpectralD = Isave{6,14};
    
    DPoffsetx = 1;
    DPoffsety = 0;
    
    IBioStructureDs = cat(2,...
        IBioStructureD{1}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioStructureD{2}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioStructureD{3}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioStructureD{4}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:));
     
    IBioSpectralDs = cat(2,...
        IBioSpectralD{1}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioSpectralD{2}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioSpectralD{3}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
        IBioSpectralD{4}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:));
    
    IBioStructureDs = G2DownScale(IBioStructureDs);
    IBioStructureDsAlpha = zeros(size(IBioStructureDs),'uint8');
    IBioStructureDsAlpha(IBioStructureDs>0) = 1;
    IBioStructureDs = cat(3,IBioStructureDs,IBioStructureDs,IBioStructureDs);
    IBioStructureDsAlpha = cat(3,IBioStructureDsAlpha,IBioStructureDsAlpha,IBioStructureDsAlpha);
    
    IBioSpectralDs = G2DownScale(IBioSpectralDs);
    IBioSpectralDsAlpha = ones(size(IBioSpectralDs,1),size(IBioSpectralDs,2),'uint8');
    IBioSpectralDsAlpha(sum(IBioSpectralDs,3) == 0) = 0;
    IBioSpectralDsAlpha([1:6 1075:1080],:) = 0;
    IBioSpectralDsAlpha = cat(3,IBioSpectralDsAlpha,IBioSpectralDsAlpha,IBioSpectralDsAlpha);
    
    if(settings.BioOptico_Structure)
        I1080Ds = IBioStructureDsAlpha.*uint8(IBioStructureDs) + Isave{5,8}.*(1-IBioStructureDsAlpha);
    else
        I1080Ds = IBioSpectralDsAlpha.*uint8(IBioSpectralDs) + Isave{5,8}.*(1-IBioSpectralDsAlpha);
    end
    
    fprintf('|\n');
end

