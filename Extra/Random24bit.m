%Psuedo Random
        imgSize = [1080 1920];
        cSeed = '9350EA';
        bSeed = dec2bin(hex2dec(cSeed),24);
        
        
        for i = 1:length(bSeed)
            Seed(i) = logical(str2num(bSeed(i)));
        end
        
        I = zeros(imgSize(1),imgSize(2),3);
        
        Q = fliplr(Seed);
        
        for i = 1:imgSize(1)
            for j = 1:imgSize(2)
                temp = xor(xor(xor(Q(24),Q(23)),Q(22)),Q(17));
                r = sum(double(Q(17:24)).*(2.^(0:7)));
                g = sum(double(Q(9:16)).*(2.^(0:7)));
                b = sum(double(Q(1:8)).*(2.^(0:7)));
                I(i,j,:) = [r g b];
                Q = [temp Q(1:end-1)];
            end
        end