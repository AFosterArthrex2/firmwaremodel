function out = CMOSregWrite(Port,Agains,Cgains,varargin)
% Agains = [r g b];
%   r - Red Analog Gain (1 or 2)
%   g - Green Analog Gain (1 or 2)
%   b - Blue Analog Gain (1 or 2)
%
% Cgains = [r g b];
%   r - Red Analog Gain (1, 2 or 3)
%   g - Green Analog Gain (1, 2 or 3)
%   b - Blue Analog Gain (1, 2 or 3)


Alookup = ['80'; 'C0'];
Clookup = ['0'; '8'; 'C'];

if(isempty(varargin))
    %Red
    SerialWriteCCU(Port,2,7,'00000100')
    SerialWriteCCU(Port,2,6,['0020' Clookup(Cgains(1)) '0' Alookup(Agains(1),:)])
    SerialWriteCCU(Port,2,31,'80000000')
    
    %Green
    SerialWriteCCU(Port,2,7,'00000200')
    SerialWriteCCU(Port,2,6,['0020' Clookup(Cgains(2)) '0' Alookup(Agains(2),:)])
    SerialWriteCCU(Port,2,31,'80000000')
    
    %Blue
    SerialWriteCCU(Port,2,7,'00000300')
    SerialWriteCCU(Port,2,6,['0020' Clookup(Cgains(3)) '0' Alookup(Agains(3),:)])
    SerialWriteCCU(Port,2,31,'80000000')
    
    %Reset SPI setting
    SerialWriteCCU(Port,2,7,'00000000')
else
    %Red
    SerialWriteCCU(Port,2,7,'00000100',varargin{1})
    SerialWriteCCU(Port,2,6,['0020' Clookup(Cgains(1)) '0' Alookup(Agains(1),:)],varargin{1})
    SerialWriteCCU(Port,2,31,'80000000',varargin{1})
    
    %Green
    SerialWriteCCU(Port,2,7,'00000200',varargin{1})
    SerialWriteCCU(Port,2,6,['0020' Clookup(Cgains(2)) '0' Alookup(Agains(2),:)],varargin{1})
    SerialWriteCCU(Port,2,31,'80000000',varargin{1})
    
    %Blue
    SerialWriteCCU(Port,2,7,'00000300',varargin{1})
    SerialWriteCCU(Port,2,6,['0020' Clookup(Cgains(3)) '0' Alookup(Agains(3),:)],varargin{1})
    SerialWriteCCU(Port,2,31,'80000000',varargin{1})
    
    %Reset SPI setting
    SerialWriteCCU(Port,2,7,'00000000',varargin{1})
end
end