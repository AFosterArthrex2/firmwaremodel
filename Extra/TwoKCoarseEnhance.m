VidWrite1 = VideoWriter('C:\Users\AFoster\Pictures\Matlab Images\CoarseEnhance\2k_Standard Enhance.avi','Uncompressed AVI');
VidWrite2 = VideoWriter('C:\Users\AFoster\Pictures\Matlab Images\CoarseEnhance\2k_Coarse0.1 Enhance.avi','Uncompressed AVI');
VidWrite3 = VideoWriter('C:\Users\AFoster\Pictures\Matlab Images\CoarseEnhance\2k_Coarse0.25 Enhance.avi','Uncompressed AVI');

w = waitbar(0,'Loading');

directory = 'C:\Users\AFoster\Pictures\Matlab Images\CoarseEnhance\';

list1 = dir([directory '1080_0_*']);
list2 = dir([directory '1080_0.1_*']);
list3 = dir([directory '1080_0.25_*']);

open(VidWrite1);
open(VidWrite2);
open(VidWrite3);

for i = 1:300
    I = imread([directory list1(i).name]);
    writeVideo(VidWrite1,I);
    
    I = imread([directory list2(i).name]);
    writeVideo(VidWrite2,I);
    
    I = imread([directory list3(i).name]);
    writeVideo(VidWrite3,I);    
    
    waitbar(i/300,w,sprintf('Frame: %03d',i));
end
delete(w)
close(VidWrite1);
close(VidWrite2);
close(VidWrite3);