%choose starting points

indices = [1 30 65];

%Matrix of points
A = Offsets([row(indices(1)) col(indices(1)) vol(indices(1));...
             row(indices(2)) col(indices(2)) vol(indices(2));...
             row(indices(3)) col(indices(3)) vol(indices(3))]);
 
%Matrix of vectors
B = A - [A(2:3,:); A(1,:)];
 
Norm = cross(B(1,:),B(2,:));

a = Norm(1);
b = Norm(2);
c = Norm(3);

d1 = -(a*A(1,1) + b*A(1,2) + c*A(1,3));
d2 = -(a*A(2,1) + b*A(2,2) + c*A(2,3));
d3 = -(a*A(3,1) + b*A(3,2) + c*A(3,3));

% coeff(1)*x + coeff(2)*y + coeff(3)*z = 1
coeff = [a b c]/(-1*d1);