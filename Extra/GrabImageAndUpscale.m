Port = 'COM6';
if(~exist('S1','var'))
    S1 = serial(Port,'BaudRate',9600);
end
if(~strcmpi('open',S1.status) || ~isvalid(S1))
    fopen(S1);
end

I = G2Capture(3,vid,Port,S1);

I2 = G2Scaler(I);

I3 = G2GreenOffset(I2,1);

figure;imagesc(I3*16)