%Apply Gain Correction
function Iout = ApplyGainCorrection(Iin,ColGainCorrection,nbits)
Itest = double(Iin);

%Round ColGainCorrection
ColGainCorrectionR = floor(ColGainCorrection*2^nbits)/2^nbits;

Icorr = zeros(size(Itest));
for i = 1:3
    Icorr(:,:,i) = ones(1080,1)*ColGainCorrectionR(:,i)';
end
   
Iout = uint16(Itest.*Icorr);