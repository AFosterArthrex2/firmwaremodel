%FixedPattern
addpath('PolyfitnTools\','SSH&Misc\')
Port = 'COM6';
inputSpreadSheet = 'Settings&Results.xlsx';

testCase = 1;

% readSettingsFromExcel
% settings.TPG_Mode = 0;
% settings.AEC_Index = 400;
% WriteSettings

S1 = serial(Port,'BaudRate',9600);

fopen(S1)

set(vid,'FramesPerTrigger',256)
set(vid,'ROIPosition',[ 0 300 1920 599])
%12bit capture. Left Side

%Remeber Register 9
reg9 = SerialReadCCU(Port,1,9,S1);

%Disable Processing (Gamma and Post Peaking/Sharpening)
SerialBitWriteCCU(Port,1,9,'11',[25:-1:24],S1);

%Enable 12-bit mode left side
SerialWriteCCU(Port,1,46,'00000001',S1);

fclose(S1)
start(vid)
while(isrunning(vid))
    pause(0.1)
end

Iraw = getdata(vid);

% I12 = convert8to12(Iraw);

for i = 1:256
    I12(:,:,:,i) = convert8to12(Iraw(:,:,:,i));
end
figure;
clear Iraw

%Periodic
Channels = {'Red'; 'Green'; 'Blue'};
peakThresh = 2;

Ib = I12(1:256,400:655,:,:);
for i = 1:3
    tmp = squeeze(Ib(:,:,i,:));

[x, y, z] = meshgrid(1:256,1:256,1:256);
indepvar = [reshape(x,[],1,1) reshape(y,[],1,1) reshape(z,[],1,1)];

    depvar = double(reshape(tmp,[],1,1));
    Model = polyfitn(indepvar,depvar,2);
%     Model.Coefficients(end) = 0;
    Trend = polyvaln(Model,indepvar);
%     RedF = reshape(depvar-Trend+mean(mean(mean(Trend))),128,128,128);
    SignalF(:,:,:,i) = reshape(depvar-Trend,256,256,256);
    
Signal_Freq(:,:,:,i) = fft2(SignalF(:,:,:,i));

m = 256;    %Number of Samples
fs = 1;     %1 Samples per pixel 

n = 256;    %Output Samples
f = (0:255)*(fs/n);

RowS(:,i) = log(abs(fft(sum(sum(SignalF(:,:,:,i),2),3))));
[Rpeaks, Rlocs] = findpeaks(RowS(1:256,i),'SORTSTR','descend');
RowFreqs(:,:,i) = [Rpeaks(1:10) f(Rlocs(1:10))'];
ColS(:,i) = log(abs(fft(sum(sum(SignalF(:,:,:,i),1),3))));
[Cpeaks, Clocs] = findpeaks(ColS(1:256,i),'SORTSTR','descend');
ColFreqs(:,:,i) = [Cpeaks(1:10) f(Clocs(1:10))'];

ChannelMsg{i} = { [Channels{i} ' Channel Frequency Peaks:']};
ChannelMsg{i} = [ChannelMsg{i} ; '--|Vertical Frequencies:'];
for j = 1:10
    if(RowFreqs(j,1,i) - mean(Rpeaks(10:end)) >= peakThresh && RowFreqs(j,2,i) <= 129)
        ChannelMsg{i} = [ChannelMsg{i} ; ['----|Freq: ' num2str(RowFreqs(j,2,i),'%03.2f')...
            ' Amplitude: ' num2str(RowFreqs(j,1,i),'%04.2f')]];
    end
end
ChannelMsg{i} = [ChannelMsg{i} ; '--|Horizontal Frequencies:'];
for j = 1:10
    if(ColFreqs(j,1,i) - mean(Cpeaks(10:end)) >= peakThresh && ColFreqs(j,2,i) <= 129)
        ChannelMsg{i} = [ChannelMsg{i} ; ['----|Freq: ' num2str(ColFreqs(j,2,i),'%03.2f')...
            ' Amplitude: ' num2str(ColFreqs(j,1,i),'%04.2f')]];
    end
end
end

for i = 1:length(ChannelMsg)
    for j = 1:length(ChannelMsg{i})
        fprintf('%s\n',ChannelMsg{i}{j})
    end
end



for i = 1:3
subplot(3,2,2*(i-1)+1)
plot(f,RowS(:,i)),axis([0 1 5 17]),title('Row Spectrum')
subplot(3,2,2*i),plot(f,log(abs(fft(sum(sum(SignalF(:,:,:,i),1),3))))),axis([0 1 5 17]),title('Col Spectrum')

end
set(vid,'ROIPosition',[ 0 0 1920 1080])
