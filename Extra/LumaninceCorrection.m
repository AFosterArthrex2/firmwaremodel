%Lumanince Correction
% C = 0.1:0.1:1;
% M = 0.1:0.1:1;
% for i = 1:10
%     for j = 1:10
        I = double(I12bit_cap);
        
        Y = max(I,[],3);
        
        LogY = abs(log2(max(1,Y-150)));
        scale1 = max(max(LogY));
        BFLogY = bfilter2(LogY/scale1,15,[3 0.1]);
        
        BaseImage = (2.^(BFLogY)-1);
        
%         BaseImageC2
        
        C = 0.5;
        M = 0.5;
        CompBFLogY = zeros(size(BFLogY));
        CompBFLogY(BFLogY~=0) = C*(BFLogY(BFLogY~=0) - (M))+ (M);
%         CompBFLogY = C*(BFLogY - log2(M))+log2(M);
        BaseImageC = 2.^(CompBFLogY);
%         BaseImageC = 2.^(C*(log2(BaseImage) - log2(M))+log2(M));
        
        DetailImage = BaseImageC./(Y);
        
        Gain = 1;
        
        Iout = cat(3,DetailImage,DetailImage,DetailImage).*I*Gain;
        
        DetailImage2 = 2.^(CompBFLogY-LogY);
        
        Iout2 = cat(3,DetailImage2,DetailImage2,DetailImage2).*I*Gain;
        
        figure;imagesc([I Iout Iout2]),colorbar
        title(sprintf('Mean: %f. Compresion: %f.',M,C))
%     end
% end