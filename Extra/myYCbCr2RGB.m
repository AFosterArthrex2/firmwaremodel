function [Iout] = myYCbCr2RGB(Iin)

I = floor(double(Iin));

R =  2*I(:,:,1) + 0*I(:,:,2) + 2*I(:,:,3);
G =  2*I(:,:,1) - 1*I(:,:,2) - 1*I(:,:,3);
B =  2*I(:,:,1) + 2*I(:,:,2) + 0*I(:,:,3);

Iout = cat(3,R,G,B)/8;

%truncate and cap as a 12 bit integer
Iout = uint16(min(max(floor(Iout),0),2^12));

end