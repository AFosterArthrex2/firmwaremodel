%Optical Black Testing
% Writer1 = VideoWriter('UnCorrected.avi','Uncompressed AVI');
% Writer2 = VideoWriter('Corrected.avi','Uncompressed AVI');

% open(Writer1)
% open(Writer2)

addpath('SSH&Misc\','PolyfitnTools\')
Port = 'COM6';
if(~exist('S1','var'))
    S1 = serial(Port,'BaudRate',9600);
end
if(~strcmpi('open',S1.status) || ~isvalid(S1))
    fopen(S1);
end
set(vid,'FramesPerTrigger',600)

SerialBitWriteCCU(Port,1,9,'1',0,S1);
SerialBitWriteCCU(Port,1,10,'0',3,S1);
SerialWriteCCU(Port,1,11,'00000000',S1);
SerialWriteCCU(Port,1,45,'00000000',S1);
SerialWriteCCU(Port,1,46,'00000001',S1);

start(vid)

I = zeros(1080,960,3,600,'uint16');
test1 = zeros(8,960,3,600,'uint16');
for i = 1:600
    I(:,:,:,i) = convert8to12(getdata(vid,1));
    
    imwrite(I(:,:,:,i)-260,['unCorrected\' num2str(i) '.png'])
    test1(:,:,:,i) = I(1:8,:,:,i);
    if(i >= 16) %Look back 16 frames
        offsetR = ones(1080,1)*sum(sum(test1(:,:,1,i-15:i),4),1);
        offsetG = ones(1080,1)*sum(sum(test1(:,:,1,i-15:i),4),1);
        offsetB = ones(1080,1)*sum(sum(test1(:,:,1,i-15:i),4),1);
        
        offset = cat(3,offsetR,offsetG,offsetB);
        I2 = uint16((double(I(:,:,:,i))*16*8-offset)/8/16);
        imwrite(I2,['Corrected\' num2str(i) '.png'])
    else
        imwrite(I(:,:,:,i)-260,['Corrected\' num2str(i) '.png'])
    end
    if(mod(i,10) == 0)
        fprintf('%d\n',i)
    end
end

% test1 = I(1:8,:,:,:);


% close(Writer1)
% close(Writer2)