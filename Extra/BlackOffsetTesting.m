%Black Offset Testing

%Grab several frames that include optical black
%Crops
SerialBitWriteCCU(Port,1,45,dec2bin(0,7),[30:-1:24],S1);
SerialBitWriteCCU(Port,1,45,dec2bin(0,7),[22:-1:16],S1);
SerialBitWriteCCU(Port,1,45,dec2bin(0,7),[14:-1:8],S1);
SerialBitWriteCCU(Port,1,45,dec2bin(0,7),[6:-1:0],S1);

Port = 'COM4';
if(~exist('S1','var'))
    S1 = serial(Port,'BaudRate',9600);
end
if(~strcmpi('open',S1.status) || ~isvalid(S1))
    fopen(S1);
end
set(vid,'FramesPerTrigger',16)

SerialBitWriteCCU(Port,1,9,'1',[0],S1);

%Fix Exposure Index
SerialBitWriteCCU(Port,1,10,'00',[3 4],S1);

%Enable 12-bit mode middle
SerialWriteCCU(Port,1,46,'00000071',S1);

start(vid)
while(isrunning(vid))
    pause(0.1)
end

Iraw = getdata(vid);
I12 = convert8to12(Iraw);
clear Iraw
        
