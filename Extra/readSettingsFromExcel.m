%readSettingsFromExcel
%read test case spreadsheet (inputSpreadSheet)
%Alexander Foster 11/19/2013
function settings = readSettingsFromExcel(inputSpreadSheet,testCase)

[A B C] = xlsread(inputSpreadSheet,1);
SSlineNum = testCase + 2;
for i = 2:size(C,2)
    if(strcmpi(C{1,i},'GreenFlare_Enable'))
        abc = 1;
    end
    switch C{2,i}
        case 'boo'
            settings.(C{1,i}) = logical(C{SSlineNum,i});
        case 'int'
            settings.(C{1,i}) = double(C{SSlineNum,i});
        case 'hex'
            settings.(C{1,i}) = num2str(C{SSlineNum,i},'%04d');
        otherwise
            settings.(C{1,i}) = C{SSlineNum,i};
    end
end