function [Iout, Iover, Ialpha, varianceCombined, meanCombined, Int, varianceMid, meanMid] = procBioOpticoStructFPGAMATCH_3_04_13(I,Mthresh,Vthresh,P)
%Faster

%Inputs:
%I - image
%Vthresh - upper variance threshold
%Mthresh - lower mean threshold

%Outputs:
%Iover
%Ialpha
%SI
%Mean
%Var

fid = fopen('square_root_130301.txt');
C = textscan(fid,'%s\r\n');
fclose(fid);
D = hex2dec(C{1});

Ifilt =             zeros(size(I),'uint16');
Iover =             zeros([size(I,1) size(I,2)],'uint16');
meanH =             zeros([size(I,1) size(I,2)],'double');      %Mean (Horizontal)
varianceH =         zeros([size(I,1) size(I,2)],'double');  %Variance (Horizontal)
meanCombined =      zeros([size(I,1) size(I,2)],'double');      %Mean (Final)
varianceCombined =  zeros([size(I,1) size(I,2)],'double');  %Variance
sqrtvarianceClamped =   zeros([size(I,1) size(I,2)],'double');  %Variance

Iin = I;

%Filter input image with 11x11 averaging filter
Ipad = padarray(I,[20 20 0],'circular');
Ipad2 = zeros(size(Ipad));
Ipad3 = zeros(size(Ipad));
for k = 1:3
   for i = 1:size(Ipad,1)
        Ipad2(i,:,k) = floor(conv(double(Ipad(i,:,k)),ones(1,11)*5957/2^16,'same'));
    end
    for j = 1:size(Ipad,2)
        Ipad3(:,j,k) = floor(conv(double(Ipad2(:,j,k)),ones(11,1)*5957/2^16,'same'));
    end
end
Ifilt = Ipad3(21:end-20,21:end-20,:);
% %Outer 5 pixels are passed unfiltered
% Imask = ones(size(Ifilt));
% Imask(5:end-4,14:end-13,:) = 0;
% Ifilt(logical(Imask)) = Iin(logical(Imask));

%Truncate to 8bits
Ifilt8 = floor(double(Ifilt)/16);

%Intensity (76*R 151*G 29*B
Int = double(Ifilt8(:,:,1)*76 + Ifilt8(:,:,2)*151 + Ifilt8(:,:,3)*29)/64;
Int = floor(Int);

%%%%%%%%%%%%%%%%%HORIZONTAL PASS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% for i = 1:size(Int,1)            %Loop over every row
    meanH(:,1) = floor(sum(double(Int(:,1:3)),2)*3355443/16777216 *2^P(1))/2^P(1);
    meanH(:,2) = floor(sum(double(Int(:,1:4)),2)*3355443/16777216 *2^P(1))/2^P(1);
    meanH(:,end-1) = floor(sum(double(Int(:,end-3:end)),2)*3355443/16777216 *2^P(1))/2^P(1);
    meanH(:,end) = floor(sum(double(Int(:,end-2:end:4)),2)*3355443/16777216 *2^P(1))/2^P(1);
    for j = 3:size(Int,2)-2      %every column except first and last 2
        current = Int(:,j+[-2:2]);
        meanCur = floor(sum(double(current),2)*3355443/16777216 *2^P(1))/2^P(1);    % #1 10.12
        meanH(:,j) = meanCur;
        if(mod(j,floor(size(Int,2)/10)) == 0)
            fprintf('.')
        end
    end
    
    
    varianceH(:,1) = floor((floor((sum((double(Int(:,1:3)).^2),2))*3355443/16777216 *2^P(2))/2^P(2) - floor((double(meanH(:,1)).^2)*2^P(3))/2^P(3))*2^P(4))/2^P(4);
    varianceH(:,2) = floor((floor((sum((double(Int(:,1:4)).^2),2))*3355443/16777216 *2^P(2))/2^P(2) - floor((double(meanH(:,2)).^2)*2^P(3))/2^P(3))*2^P(4))/2^P(4);
    varianceH(:,end-1) = floor((floor((sum((double(Int(:,end-3:end)).^2),2))*3355443/16777216 *2^P(2))/2^P(2) - floor((double(meanH(:,end-1)).^2)*2^P(3))/2^P(3))*2^P(4))/2^P(4);
    varianceH(:,end) = floor((floor((sum((double(Int(:,end-2:end)).^2),2))*3355443/16777216 *2^P(2))/2^P(2) - floor((double(meanH(:,end)).^2)*2^P(3))/2^P(3))*2^P(4))/2^P(4);
    for j = 3:size(Int,2)-2      %every column except first and last 2
        current = Int(:,j+[-2:2]); 
        meanCur = double(meanH(:,j));
        sumofsquares = sum((double(current).^2),2);
        meanofsquares = floor((sumofsquares)*3355443/16777216 *2^P(2))/2^P(2);      % #2 20.8
        squareofmeans = floor((meanCur.^2)*2^P(3))/2^P(3);                          % #3 20.8 
        varCur = floor((meanofsquares - squareofmeans)*2^P(4))/2^P(4);              % #4 20.4
        varianceH(:,j) = varCur;
        if(mod(j,floor(size(Int,2)/10)) == 0)
            fprintf('.')
        end
    end
% end
varianceMid = varianceH;
meanMid = meanH;
%%%%%%%%%%%%%%%%%VERTICAL PASS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% for j = 1:size(Int,2)            %Loop over every column
    meanCombined(1,:) = floor(sum(floor(meanH(1:3,:)*2^P(5))/2^P(5),1)*3355443/16777216*2^P(7))/2^P(7);
    meanCombined(2,:) = floor(sum(floor(meanH(1:4,:)*2^P(5))/2^P(5),1)*3355443/16777216*2^P(7))/2^P(7);
    meanCombined(end-1,:) = floor(sum(floor(meanH(end-3:end,:)*2^P(5))/2^P(5),1)*3355443/16777216*2^P(7))/2^P(7);
    meanCombined(end,:) = floor(sum(floor(meanH(end-2:end,:)*2^P(5))/2^P(5),1)*3355443/16777216*2^P(7))/2^P(7);

    varianceCombined(1,:) = floor((sum(floor(varianceH(1:3,:)*2^P(6))/2^P(6)) + sum(floor((floor((double(floor(meanH(1:3,:)*2^P(5))/2^P(5))-ones(3,1)*floor(sum(floor(meanH(1:3,:)*2^P(5))/2^P(5),1)*3355443/16777216*2^P(7))/2^P(7))*2^P(8))/2^P(8).^2)*2^P(9))/2^P(9),1))*3355443/16777216*2^P(10))/2^P(10);
    varianceCombined(2,:) = floor((sum(floor(varianceH(1:4,:)*2^P(6))/2^P(6)) + sum(floor((floor((double(floor(meanH(1:4,:)*2^P(5))/2^P(5))-ones(4,1)*floor(sum(floor(meanH(1:4,:)*2^P(5))/2^P(5),1)*3355443/16777216*2^P(7))/2^P(7))*2^P(8))/2^P(8).^2)*2^P(9))/2^P(9),1))*3355443/16777216*2^P(10))/2^P(10);
    varianceCombined(end-1,:) = floor((sum(floor(varianceH(end-3:end,:)*2^P(6))/2^P(6)) + sum(floor((floor((double(floor(meanH(end-3:end,:)*2^P(5))/2^P(5))-ones(4,1)*floor(sum(floor(meanH(end-3:end,:)*2^P(5))/2^P(5),1)*3355443/16777216*2^P(7))/2^P(7))*2^P(8))/2^P(8).^2)*2^P(9))/2^P(9),1))*3355443/16777216*2^P(10))/2^P(10);
    varianceCombined(end,:) = floor((sum(floor(varianceH(end-2:end,:)*2^P(6))/2^P(6)) + sum(floor((floor((double(floor(meanH(end-2:end,:)*2^P(5))/2^P(5))-ones(3,1)*floor(sum(floor(meanH(end-2:end,:)*2^P(5))/2^P(5),1)*3355443/16777216*2^P(7))/2^P(7))*2^P(8))/2^P(8).^2)*2^P(9))/2^P(9),1))*3355443/16777216*2^P(10))/2^P(10);
    for i = 3:size(Int,1)-2      %every row except first and last 2
        currentM = floor(meanH(i+[-2:2],:)*2^P(5))/2^P(5);                          % #5 10.8
        currentV = floor(varianceH(i+[-2:2],:)*2^P(6))/2^P(6);                      % #6 20.4
        Mc = floor(sum(currentM,1)*3355443/16777216*2^P(7))/2^P(7);                 % #7 10.8
        sumOfVariances = sum(currentV);                                  
        meanMinusAvg = floor((double(currentM)-ones(5,1)*Mc)*2^P(8))/2^P(8);        % #8 10.8
        meanMinusAvgSqrd = floor((meanMinusAvg.^2)*2^P(9))/2^P(9);                  % #9 20.4
        S2c = floor((sumOfVariances + sum(meanMinusAvgSqrd,1))...
                    *3355443/16777216*2^P(10))/2^P(10);                             %#10 20.4
        meanCombined(i,:) = Mc;
        varianceCombined(i,:) = S2c;
        if(mod(i,floor(size(Int,1)/10)) == 0)
            fprintf('.')
        end
    end
% end

%First clamp to 8.4
varianceCombined2 = varianceCombined;
varianceCombined2(varianceCombined >=256) = hex2dec('FFF')/16;
varianceCombined2(varianceCombined2 < 0) = 0;

sqrtVariance = floor(D(varianceCombined2*16+1)/256*2^P(11))/2^P(11);
 
ThresholdImage = floor(double(Vthresh)*2^12*meanCombined/64*2^P(12))/2^P(12);

%Clamp Variance
for i = 1:size(sqrtVariance,1)
    for j = 1:size(sqrtVariance,2)
        if(sqrtVariance(i,j) >= ThresholdImage(i,j))
            sqrtvarianceClamped(i,j) = ThresholdImage(i,j);
        else
            sqrtvarianceClamped(i,j) = sqrtVariance(i,j);
        end
    end
end
sqrtvarianceClamped(sqrtvarianceClamped<0) = 0;

Iover(:,:,1) = floor((double(sqrtvarianceClamped)*255)./ThresholdImage);
Iover(:,:,2) = Iover(:,:,1);
Iover(:,:,3) = Iover(:,:,2);



Ialpha = zeros(size(meanCombined));
Ialpha(meanCombined >= Mthresh) = 1;
Imask = ones(size(Ialpha));
Imask(8:end-7,8:end-7,:) = 0;
Ialpha(logical(Imask)) = 0;


Iout(:,:,1) = 16*double(Iover(:,:,1)).*Ialpha + double(I(:,:,1)).*(1-Ialpha);
Iout(:,:,2) = 16*double(Iover(:,:,2)).*Ialpha + double(I(:,:,2)).*(1-Ialpha);
Iout(:,:,3) = 16*double(Iover(:,:,3)).*Ialpha + double(I(:,:,3)).*(1-Ialpha);

Iout = uint16(Iout);
