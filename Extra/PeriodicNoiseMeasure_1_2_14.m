%Periodic Noise Measurement
Channels = {'Red'; 'Green'; 'Blue'};
peakThresh = 1.5;

Ib = Iout(1:128,1:128,:,:);
for i = 1:3
    tmp = squeeze(Ib(:,:,i,:));
    
    [x, y, z] = meshgrid(1:128,1:128,1:128);
    indepvar = [reshape(x,[],1,1) reshape(y,[],1,1) reshape(z,[],1,1)];
    
    depvar = double(reshape(tmp,[],1,1));
    Model = polyfitn(indepvar,depvar,2);
    %     Model.Coefficients(end) = 0;
    Trend = polyvaln(Model,indepvar);
    %     RedF = reshape(depvar-Trend+mean(mean(mean(Trend))),128,128,128);
    SignalF2(:,:,:,i) = reshape(depvar-Trend,128,128,128);
    
%     Signal_Freq(:,:,:,i) = fft2(SignalF2(:,:,:,i));
    
    m = 128;    %Number of Samples
    fs = 1;     %1 Samples per pixel
    
    n = 128;    %Output Samples
    f = (0:127)*(fs/n);
    
    RowS(:,i) = log(abs(fft(sum(sum(SignalF2(:,:,:,i),2),3))));
    tmp = RowS(:,i)-polyval(polyfit(1:128,RowS(:,i)',2),1:128)';
    [Rpeaks, Rlocs] = findpeaks(tmp,'SORTSTR','descend');
    RowFreqs(:,:,i) = [Rpeaks(1:10) f(Rlocs(1:10))'];
    ColS(:,i) = log(abs(fft(sum(sum(SignalF2(:,:,:,i),1),3))));
    tmp = ColS(:,i)-polyval(polyfit(1:128,ColS(:,i)',2),1:128)';
    [Cpeaks, Clocs] = findpeaks(tmp,'SORTSTR','descend');
    ColFreqs(:,:,i) = [Cpeaks(1:10) f(Clocs(1:10))'];
    
    ChannelMsg{i} = { [Channels{i} ' Channel Frequency Peaks:']};
    ChannelMsg{i} = [ChannelMsg{i} ; '--|Vertical Frequencies:'];
    for j = 1:10
        if(RowFreqs(j,1,i) - mean(Rpeaks(10:end)) >= peakThresh && RowFreqs(j,2,i) <= 0.5 && RowFreqs(j,2,i) > 0.05 )
            ChannelMsg{i} = [ChannelMsg{i} ; ['----|Freq: ' num2str(RowFreqs(j,2,i),'%03.2f')...
                ' Amplitude: ' num2str(RowFreqs(j,1,i),'%04.2f')]];
        end
    end
    ChannelMsg{i} = [ChannelMsg{i} ; '--|Horizontal Frequencies:'];
    for j = 1:10
        if(ColFreqs(j,1,i) - mean(Cpeaks(10:end)) >= peakThresh && ColFreqs(j,2,i) <= 0.5 && ColFreqs(j,2,i) > 0.05)
            ChannelMsg{i} = [ChannelMsg{i} ; ['----|Freq: ' num2str(ColFreqs(j,2,i),'%03.2f')...
                ' Amplitude: ' num2str(ColFreqs(j,1,i),'%04.2f')]];
        end
    end
end

for i = 1:length(ChannelMsg)
    for j = 1:length(ChannelMsg{i})
        fprintf('%s\n',ChannelMsg{i}{j})
    end
end



for i = 1:3
    subplot(3,2,2*(i-1)+1)
    plot(f,RowS(:,i)),axis([0 1 5 17]),title('Row Spectrum')
    subplot(3,2,2*i),plot(f,log(abs(fft(sum(sum(SignalF2(:,:,:,i),1),3))))),axis([0 1 5 17]),title('Col Spectrum')
    
end
