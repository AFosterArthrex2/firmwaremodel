VidWrite1 = VideoWriter('C:\Users\AFoster\Pictures\Matlab Images\CoarseEnhance\2k_Compare.avi','Uncompressed AVI');
VidWrite2 = VideoWriter('C:\Users\AFoster\Pictures\Matlab Images\CoarseEnhance\2k_Compare.mp4','MPEG-4');
VidWrite3 = VideoWriter('C:\Users\AFoster\Pictures\Matlab Images\CoarseEnhance\4k_Compare.avi','Uncompressed AVI');
VidWrite4 = VideoWriter('C:\Users\AFoster\Pictures\Matlab Images\CoarseEnhance\4k_Compare.mp4','MPEG-4');

w = waitbar(0,'Loading');

directory = 'C:\Users\AFoster\Pictures\Matlab Images\CoarseEnhance\';

list1 = dir([directory '1080_0_*']);
list2 = dir([directory '1080_0.1_*']);

list3 = dir([directory '4k_0_*']);
list4 = dir([directory '4k_0.1_*']);

open(VidWrite1);
open(VidWrite2);
open(VidWrite3);
open(VidWrite4);

for i = 1:300
    I1 = imread([directory list1(i).name]);
    I2 = imread([directory list2(i).name]);
    writeVideo(VidWrite1,[I1 I2]);
    writeVideo(VidWrite2,[I1 I2]);
    
    I3 = imread([directory list3(i).name]);
    I4 = imread([directory list4(i).name]);
    writeVideo(VidWrite3,[I3 I4]);
    writeVideo(VidWrite4,[I3 I4]);
    
    waitbar(i/300,w,sprintf('Frame: %03d',i));
end
delete(w)
close(VidWrite1);
close(VidWrite2);
close(VidWrite3);
close(VidWrite4);
