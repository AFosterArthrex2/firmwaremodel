function out = max2nd(in)

N = [in(1,1) in(2,1) in(3,1) in(1,2) in(3,2) in(1,3) in(2,3) in(3,3)];

[B, I] = sort(N);

out = B(7);

end