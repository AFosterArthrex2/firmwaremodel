Id = G2Capture(2,vid);

%Check captured image

Signal = cat(3,Id(400:499,200:299,:),rgb2gray(Id(400:499,200:299,:)));
SignalF = zeros(size(Signal));

[x y] = meshgrid(1:100,1:100);
indepvar = [reshape(x,[],1) reshape(y,[],1)];
for k = 1:4
    depvar = double(reshape(Signal(:,:,k),[],1));
    Model = polyfitn(indepvar,depvar,2);
    Model.Coefficients(end) = 0;
    Trend = polyvaln(Model,indepvar);
    SignalF(:,:,k) = reshape(depvar-Trend,100,100);
end

Black = cat(3,Id(400:499,700:799,:),rgb2gray(Id(400:499,700:799,:)));
BlackF = zeros(size(Black));

for k = 1:4
    depvar = double(reshape(Black(:,:,k),[],1));
    Model = polyfitn(indepvar,depvar,2);
    Model.Coefficients(end) = 0;
    Trend = polyvaln(Model,indepvar);
    BlackF(:,:,k) = reshape(depvar-Trend,100,100);
    
end


Avg = squeeze(mean(mean(SignalF,1),2)-mean(mean(BlackF,1),2));

Noise = [std2(BlackF(:,:,1)) std2(BlackF(:,:,2)) std2(BlackF(:,:,3)) std2(BlackF(:,:,4))];

SNR = 20*log10(Avg'./Noise)
