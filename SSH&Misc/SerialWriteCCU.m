function SerialWriteCCU(Port,fpga,Reg,Value,varargin)

if(~isempty(varargin) && ~isempty(varargin{1}))
    S1 = varargin{1};
else
    S1 = serial(Port,'BaudRate',9600);
    fopen(S1);
end

code = [144; 160; 136];

Bank = floor(Reg/256);

Reg = mod(Reg,256);

send = [code(fpga)+Bank; Reg; hex2dec(Value(1:2)); hex2dec(Value(3:4)); hex2dec(Value(5:6)); hex2dec(Value(7:8))];

fwrite(S1,send);

if(~(~isempty(varargin) && ~isempty(varargin{1})))
    fclose(S1);
end