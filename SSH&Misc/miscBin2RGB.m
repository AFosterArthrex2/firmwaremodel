function [r g b] = miscBin2RGB(L)

r = 0;
g = 0;
b = 0;

for i = 1:length(L)/3
    
r = r + 2^(i-1)*double(L(2*length(L)/3+i));
g = g + 2^(i-1)*double(L(length(L)/3+i));
b = b + 2^(i-1)*double(L(i));    

end

end