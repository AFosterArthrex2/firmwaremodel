%Higher Level Serial Functions

function out = SerialControl(action,varargin)

if(~isempty(varargin))
    if(ischar(varargin{1}))
        S1 = serial(varargin{1},'BaudRate',9600);
    else
        S1 = varargin{1};
    end
end

action = lower(action);

switch action
    case 'bannersoff'
        for i = 192:239
            overlayRegs(i,:) = SerialReadCCU([],1,i+2*256,S1);
            SerialWriteCCU([],1,i+2*256,'00000000',S1);
        end
        out = overlayRegs;
    
    case 'patternsoff'
        patternRegs = SerialReadCCU([],1,128+4*256,S1);
        SerialWriteCCU([],1,128+4*256,'00000000',S1);
        
        out = patternRegs;
        
    otherwise
        out = 'Unknown Command';
end

