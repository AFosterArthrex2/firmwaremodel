function out = convert8to12(I)
%UHD4 12 bit conversion
%Takes an 8 bit capture and outputs the 12bit original with 1/2 the
%horizontal resolution
if(size(I,4) == 1)
    
    out = zeros(size(I,1),960,3);
    
    out = uint16(double(I(:,1:960,:))*2^4 + double(I(:,961:1920,:)));
    
else
    
    out = zeros(size(I,1),960,3,size(I,4));
        
    out = uint16(double(I(:,1:960,:,:))*2^4 + double(I(:,961:1920,:,:)));
    
end