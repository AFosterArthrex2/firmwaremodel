regs = 50:62;

for i = 1:length(regs)
    savedValues(i,:) = SerialReadCCU(Port,1,1*256+regs(i),S1);
end


[DecRed, DecGreen, DecBlue, LowPassRed, LowPassGreen, LowPassBlue,...
    HorizMean1, HorizMean2, HorizVar1, HorizVar2, VertMean1,...
    VertMean2, VertVar1, VertVar2, BioCodesRawSpec, BioCodesRawStruct,...
    ThicknessRaw] = G2Capture(21,vid,Port,S1);

HorizMean = uint32(double(HorizMean1) + double(HorizMean2)*2^12);
HorizVar = uint32(double(HorizVar1) + double(HorizVar2)*2^12);

VertMean = uint32(double(VertMean1) + double(VertMean2)*2^12);
VertVar = uint32(double(VertVar1) + double(VertVar2)*2^12);

ThicknessShift = padarray(ThicknessRaw(:,7:end),[0 6],'post');

Thickness = mod(double(ThicknessShift),2^10);

IntError = floor(double(ThicknessShift)/2^11);

SatError = floor(mod(double(ThicknessShift),2^11)/2^10);

for i = 1:length(regs)
    SerialWriteCCU(Port,1,1*256+regs(i),savedValues(i,:),S1)
end



