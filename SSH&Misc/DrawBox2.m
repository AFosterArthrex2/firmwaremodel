
% draws an overlay box
function [Iover] = DrawBox2(Iover,loc)
%loc = [x1 x2 y1 y2];
for i = 1:size(loc,1)

Iover(loc(i,3),loc(i,1):loc(i,2)) = 1;
Iover(loc(i,4),loc(i,1):loc(i,2)) = 1;
Iover(loc(i,3):loc(i,4),loc(i,1)) = 1;
Iover(loc(i,3):loc(i,4),loc(i,2)) = 1;

Iover(loc(i,3)-1,loc(i,1):loc(i,2)) = 1;
Iover(loc(i,4)-1,loc(i,1):loc(i,2)) = 1;
Iover(loc(i,3):loc(i,4),loc(i,1)-1) = 1;
Iover(loc(i,3):loc(i,4),loc(i,2)-1) = 1;

Iover(loc(i,3)+1,loc(i,1):loc(i,2)) = 1;
Iover(loc(i,4)+1,loc(i,1):loc(i,2)) = 1;
Iover(loc(i,3):loc(i,4),loc(i,1)+1) = 1;
Iover(loc(i,3):loc(i,4),loc(i,2)+1) = 1;
end

end