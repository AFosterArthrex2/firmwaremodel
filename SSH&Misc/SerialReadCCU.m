function out = SerialReadCCU(Port,fpga,Reg,varargin)

if(~isempty(varargin) && ~isempty(varargin{1}))
    S1 = varargin{1};
else
    S1 = serial(Port,'BaudRate',9600);
    fopen(S1);
end

code = [80; 96; 72];

Bank = floor(Reg/256);

Reg = mod(Reg,256);

send = [code(fpga) + Bank; Reg];

fwrite(S1,send);

[A,count,msg] = fread(S1,4);

if(~(~isempty(varargin) && ~isempty(varargin{1})))
    fclose(S1);
end

out = [dec2hex(A(1),2) dec2hex(A(2),2) dec2hex(A(3),2) dec2hex(A(4),2)];


