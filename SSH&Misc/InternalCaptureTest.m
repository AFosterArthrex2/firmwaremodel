IP = '192.168.1.2';
directory = 'C:\Users\AFoster\Pictures\Matlab Images\Internal Captures\';

[status, Capture_Message] = system('plink -pw Arthrex1 arthrex@192.168.1.2 capture');
pause(10)
[status, LS_Message] = system('plink -pw Arthrex1 arthrex@192.168.1.2 ls /var/log/*.rgb');
files = textscan(LS_Message,'%s');
clear imName
for i = 1:length(files{1})
    imName{i} = files{1}{i}(13:end-7);
end
[v new] = max(str2double(imName));

str = sprintf('pscp -pw Arthrex1 arthrex@%s:/var/log/im_%s_4k.rgb "%sim_%s_4k.rgb"',IP,imName{new},directory,imName{new});
[status Transfer4k_Message] = system(str);
if(status)
    abc=1;
end
str = sprintf('pscp -pw Arthrex1 arthrex@%s:/var/log/im_%s.rgb "%sim_%s.rgb"',IP,imName{new},directory,imName{new});
[status Transfer1080_Message] = system(str);
if(status)
    abc=1;
end

fid = fopen([directory 'im_' imName{new} '.rgb']);
data = fread(fid);fclose(fid);
I1080int = uint8(permute(reshape(data,3,1920,1080),[3 2 1]));

fid = fopen([directory 'im_' imName{new} '_4k.rgb']);
data = fread(fid);fclose(fid);
I4kint = uint8(permute(reshape(data,3,3840,2160),[3 2 1]));
