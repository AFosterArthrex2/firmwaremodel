function [varargout] = G2Capture(varargin)
%Mode - 1 basic capture
%     - 2 12 bit (centered)
%     - 3 12 bit (full 1080p)
%     - 4 4k from Quad 3g
%     - 5 Quad 3g 1 top left
%     - 6 Quad 3g 2 top right
%     - 7 Quad 3g 3 bottom left
%     - 8 Quad 3g 4 bottom right
%     - 9 4k from Displayport
%     - 10 Displayport Stripe 1
%     - 11 Displayport Stripe 2
%     - 12 Displayport Stripe 3
%     - 13 Displayport Stripe 4
%     - 21 BioOptico Capture

%Vid - Video Capture Object
%Port - Serial Address (COM#)






if(length(varargin) >= 1)
    Mode = varargin{1};
else
    Mode = 1;
end

if(length(varargin) >= 2)
    vid = varargin{2};
else
    vid = videoinput('winvideo');
end

SerialModes = ones(1,50);
SerialModes([1 19]) = 0;

if(SerialModes(Mode))
    if(length(varargin) >= 3)
        Port = varargin{3};
    else
        Port = 'COM5';
    end
    
    if(length(varargin) >= 4)
        S1 = varargin{4};
    else
        S1 = serial('COM5','BaudRate',9600);
        if(Mode ~= 1)
            fopen(S1);
        end
    end
end

switch Mode
    case 1 %Simple Capture no changing registers, no conversion
        %Disable 12-bit mode
%         SerialWriteCCU(Port,1,24,'00000000',S1);
        pause(0.25)
        Ic = getsnapshot(vid);
        varargout{1} = Ic;
        
        
    case 2 %12bit 960x1080 capture
        %Remeber Register 9
        reg9 = SerialReadCCU(Port,1,9,S1);

        %Disable Processing (Gamma and Post Peaking/Sharpening)
        SerialBitWriteCCU(Port,1,9,'11',[25:-1:24],S1);
        
        %Enable 12-bit mode
        SerialWriteCCU(Port,1,4*256+46,'00000071',S1);
        pause(0.2)
        Ic = getsnapshot(vid);
        varargout{1} = convert8to12(Ic);
           
        %Enable Processing
        SerialWriteCCU(Port,1,9,reg9,S1);
        
        %Disable 12-bit mode
        SerialWriteCCU(Port,1,4*256+46,'00000000',S1);
                       
    case 3 %12bit 1920x1080 capture  
        SerialBitWriteCCU(Port,1,4*256+8,'1100',[11:-1:8],S1);pause(0.25)
        %12bit 1920x1080 capture  
        %Remeber Register 9
        reg9 = SerialReadCCU(Port,1,9,S1);
        
        %4k Zoom Off
        SerialWriteCCU(Port,1,1*256+88,'00000000',S1);
        

        %Disable Processing (Gamma and Post Peaking/Sharpening)
        SerialBitWriteCCU(Port,1,9,'11',[25:-1:24],S1);
        
        %Enable 12-bit mode left side
        SerialWriteCCU(Port,1,4*256+46,'00000001',S1);
        pause(0.25)
        Ic = getsnapshot(vid);
        Ileft = convert8to12(Ic);
        
        %Enable 12-bit mode right side
        SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);
        pause(0.25)
        Ic = getsnapshot(vid);
        Iright = convert8to12(Ic);
        
        %Enable Processing
        SerialWriteCCU(Port,1,9,reg9,S1);
        
        %4k Zoom On
        SerialWriteCCU(Port,1,1*256+88,'00000009',S1);
        
        
        %Disable 12-bit mode
        SerialWriteCCU(Port,1,4*256+46,'02940000',S1);
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);
        varargout{1} = [Ileft Iright];
        
    case 4 %8bit 3840x2160 capture
        uiwait(msgbox('Don''t Use This Mode!'));
        %Upper Left
        SerialBitWriteCCU(Port,1,8,'0100',[11:-1:8],S1);pause(0.25)
        IupperLeft = getsnapshot(vid);
        
        %Upper Right
        SerialBitWriteCCU(Port,1,8,'0101',[11:-1:8],S1);pause(0.25)
        IupperRight = getsnapshot(vid);
        
        %Lower Left
        SerialBitWriteCCU(Port,1,8,'0110',[11:-1:8],S1);pause(0.25)
        IlowerLeft = getsnapshot(vid);
        
        %Lower Right
        SerialBitWriteCCU(Port,1,8,'0111',[11:-1:8],S1);pause(0.25)
        IlowerRight = getsnapshot(vid);
        
        %Reset Q3G mux
        SerialBitWriteCCU(Port,1,8,'0000',[11:-1:8],S1);

        varargout{1} = [IupperLeft IupperRight; IlowerLeft IlowerRight];
        
    case 5 %8bit Upper Left
        uiwait(msgbox('Don''t Use This Mode!'));
        SerialBitWriteCCU(Port,1,8,'0100',[11:-1:8],S1);
        varargout{1} = getsnapshot(vid);
        SerialBitWriteCCU(Port,1,8,'0000',[11:-1:8],S1);
                
    case 6 %8bit Upper Right
        uiwait(msgbox('Don''t Use This Mode!'));
        SerialBitWriteCCU(Port,1,8,'0101',[11:-1:8],S1);
        varargout{1} = getsnapshot(vid);
        SerialBitWriteCCU(Port,1,8,'0000',[11:-1:8],S1);
        
    case 7 %8bit Lower Left
        uiwait(msgbox('Don''t Use This Mode!'));
        SerialBitWriteCCU(Port,1,8,'0110',[11:-1:8],S1);
        varargout{1} = getsnapshot(vid);
        SerialBitWriteCCU(Port,1,8,'0000',[11:-1:8],S1);   
        
    case 8 %8bit Lower Right
        uiwait(msgbox('Don''t Use This Mode!'));
        SerialBitWriteCCU(Port,1,8,'0111',[11:-1:8],S1);
        varargout{1} = getsnapshot(vid);
        SerialBitWriteCCU(Port,1,8,'0000',[11:-1:8],S1);
        
    case 9 %4k from Displayport
        %Far Left
        SerialBitWriteCCU(Port,1,4*256+8,'1000',[11:-1:8],S1);pause(0.25)
        FarLeft = DPunscramble(getsnapshot(vid));
        
        %Mid Left
        SerialBitWriteCCU(Port,1,4*256+8,'1001',[11:-1:8],S1);pause(0.25)
        MidLeft = DPunscramble(getsnapshot(vid));
        
        %Mid Right
        SerialBitWriteCCU(Port,1,4*256+8,'1010',[11:-1:8],S1);pause(0.25)
        MidRight = DPunscramble(getsnapshot(vid));
        
        %Far Right
        SerialBitWriteCCU(Port,1,4*256+8,'1011',[11:-1:8],S1);pause(0.25)
        FarRight = DPunscramble(getsnapshot(vid));
        
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);
        %Combine
        varargout{1} = cat(2,FarLeft,MidLeft,MidRight,FarRight);
                
    case 10 %Displayport Stripe 1
        SerialBitWriteCCU(Port,1,4*256+8,'1000',[11:-1:8],S1);pause(0.25)
        varargout{1} = DPunscramble(getsnapshot(vid));
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);
        
    case 11 %Displayport Stripe 2
        SerialBitWriteCCU(Port,1,4*256+8,'1001',[11:-1:8],S1);pause(0.25)
        varargout{1} = DPunscramble(getsnapshot(vid));
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);
        
    case 12 %Displayport Stripe 3
        SerialBitWriteCCU(Port,1,4*256+8,'1010',[11:-1:8],S1);pause(0.25)
        varargout{1} = DPunscramble(getsnapshot(vid));
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);
        
    case 13 %Displayport Stripe 4
        SerialBitWriteCCU(Port,1,4*256+8,'1011',[11:-1:8],S1);pause(0.25)
        varargout{1} = DPunscramble(getsnapshot(vid));
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);
        
    case 14 %DisplayPort Downscaled to 1080p
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);pause(0.25)
        varargout{1} = getsnapshot(vid);
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);
        
    case 15 %DisplayPort Downscaled to 1080p 12 bit
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);pause(0.25)
        %12bit 1920x1080 capture  
        %Remeber Register 9
        reg9 = SerialReadCCU(Port,1,9,S1);

        %Disable Processing (Gamma and Post Peaking/Sharpening)
        SerialBitWriteCCU(Port,1,9,'11',[25:-1:24],S1);
        
        %Enable 12-bit mode left side
        SerialWriteCCU(Port,1,4*256+46,'00000001',S1);
        pause(0.25)
        Ic = getsnapshot(vid);
        Ileft = convert8to12(Ic);
        
        %Enable 12-bit mode right side
        SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);
        pause(0.25)
        Ic = getsnapshot(vid);
        Iright = convert8to12(Ic);
        
        %Enable Processing
        SerialWriteCCU(Port,1,9,reg9,S1);
        
        %Disable 12-bit mode
        SerialWriteCCU(Port,1,4*256+46,'02940000',S1);
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);
        varargout{1} = [Ileft Iright];
        
    case 16 %12bit 1920x1080 capture with Freeze Frame
        %Remeber Register 9
        reg9 = SerialReadCCU(Port,1,9,S1);
        
        %Freeze Video
        SerialBitWriteCCU(Port,1,4*256+161,'1',[18],S1);
        
        %View Buffer
        SerialBitWriteCCU(Port,1,4*256+8,'1',[15],S1);
        pause(0.25)
        %Disable Processing (Gamma and Post Peaking/Sharpening)
        SerialBitWriteCCU(Port,1,9,'11',[25:-1:24],S1);
    
        %Enable 12-bit mode left side
        SerialWriteCCU(Port,1,4*256+46,'00000001',S1);
        pause(0.25)
        Ic = getsnapshot(vid);
        Ileft = convert8to12(Ic);
        
        %Enable 12-bit mode right side
        SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);
        pause(0.25)
        Ic = getsnapshot(vid);
        Iright = convert8to12(Ic);
        
        %Enable Processing
        SerialWriteCCU(Port,1,9,reg9,S1);
        
        %Disable 12-bit mode
        SerialWriteCCU(Port,1,4*256+46,'02940000',S1);
        
        %unFreeze Video
        SerialBitWriteCCU(Port,1,4*256+161,'0',[18],S1);
        
        %View Live Video
        SerialBitWriteCCU(Port,1,4*256+8,'0',[15],S1);
        
        varargout{1} = [Ileft Iright];
    
    case 17 %1080 from frame buffer
        SerialBitWriteCCU(Port,1,4*256+8,'1100',[11:-1:8],S1);pause(0.25)
        varargout{1} = getsnapshot(vid);
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);
        
    case 18 %1080p 12 bit 
        SerialBitWriteCCU(Port,1,4*256+8,'1100',[11:-1:8],S1);pause(0.25)
        %12bit 1920x1080 capture  
        %Remeber Register 9
        reg9 = SerialReadCCU(Port,1,9,S1);

        %Disable Processing (Gamma and Post Peaking/Sharpening)
        SerialBitWriteCCU(Port,1,9,'11',[25:-1:24],S1);
        
        %Enable 12-bit mode left side
        SerialWriteCCU(Port,1,4*256+46,'00000001',S1);
        pause(0.25)
        Ic = getsnapshot(vid);
        Ileft = convert8to12(Ic);
        
        %Enable 12-bit mode right side
        SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);
        pause(0.25)
        Ic = getsnapshot(vid);
        Iright = convert8to12(Ic);
        
        %Enable Processing
        SerialWriteCCU(Port,1,9,reg9,S1);
        
        %Disable 12-bit mode
        SerialWriteCCU(Port,1,4*256+46,'02940000',S1);
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);
        varargout{1} = [Ileft Iright];
        
    case 19 %Display Port Capture Left no register changes
        varargout{1} = getsnapshot(vid);
        
    case 20 %12bit 1920x1080 capture with Freeze Frame and reg9=0xFFFFFFFF
        %Remeber Register 9
        reg9 = SerialReadCCU(Port,1,9,S1);
        
        %View Buffer
        SerialBitWriteCCU(Port,1,4*256+8,'1',[15],S1);
        pause(0.25)
        
        %Freeze Video
        SerialBitWriteCCU(Port,1,4*256+161,'1',[18],S1);
        
        %Disable Processing (Gamma and Post Peaking/Sharpening)
        SerialWriteCCU(Port,1,9,'FFFFFFFF',S1);
        
        %Enable 12-bit mode left side
        SerialWriteCCU(Port,1,4*256+46,'00000001',S1);
        pause(0.25)
        
        Ic = getsnapshot(vid);
        Ileft = convert8to12(Ic);
        
        %Enable 12-bit mode right side
        SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);
        pause(0.25)
        Ic = getsnapshot(vid);
        Iright = convert8to12(Ic);
        
        %Enable Processing
        SerialWriteCCU(Port,1,9,reg9,S1);
        
        %Disable 12-bit mode
        SerialWriteCCU(Port,1,4*256+46,'02940000',S1);
        
        %unFreeze Video
        SerialBitWriteCCU(Port,1,4*256+161,'0',[18],S1);
        
        %View Live Video
        SerialBitWriteCCU(Port,1,4*256+8,'0',[15],S1);
        
        varargout{1} = [Ileft Iright];
        
    case 21 %BioOptico Capture
        %Bypass 4k Processing
        SerialBitWriteCCU(Port,1,4*256+8,'1100',[11:-1:8],S1);pause(0.25)
        %Enable BioOptico Debug
        SerialBitWriteCCU(Port,1,4*256+50,'1',28,S1);
        %Disable Overlays
        SerialBitWriteCCU(Port,1,256*1+50,'0',[0],S1);
        
%         %Freeze Video
%         SerialBitWriteCCU(Port,1,4*256+161,'1',[18],S1);
%         
%         %View Buffer
%         SerialBitWriteCCU(Port,1,4*256+8,'1',[15],S1);
%         pause(0.25)
%         
        %Decimated Data Red
            SerialBitWriteCCU(Port,1,4*256+50,dec2bin(0,4),[27:-1:24],S1);

            %Enable 12-bit mode left side
            SerialWriteCCU(Port,1,4*256+46,'00000001',S1);pause(0.25)
            Ileft = convert8to12(getsnapshot(vid));

            %Enable 12-bit mode right side
            SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);pause(0.25)
            Iright = convert8to12(getsnapshot(vid));

            varargout{1} = [Ileft(1:2:end,1:2:end,1) Iright(1:2:end,1:2:end,1)];
        
        %Decimated Data Green
            SerialBitWriteCCU(Port,1,4*256+50,dec2bin(1,4),[27:-1:24],S1);

            %Enable 12-bit mode left side
            SerialWriteCCU(Port,1,4*256+46,'00000001',S1);pause(0.25)
            Ileft = convert8to12(getsnapshot(vid));

            %Enable 12-bit mode right side
            SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);pause(0.25)
            Iright = convert8to12(getsnapshot(vid));

            varargout{2} = [Ileft(1:2:end,1:2:end,1) Iright(1:2:end,1:2:end,1)];
        
        %Decimated Data Blue
            SerialBitWriteCCU(Port,1,4*256+50,dec2bin(2,4),[27:-1:24],S1);

            %Enable 12-bit mode left side
            SerialWriteCCU(Port,1,4*256+46,'00000001',S1);pause(0.25)
            Ileft = convert8to12(getsnapshot(vid));

            %Enable 12-bit mode right side
            SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);pause(0.25)
            Iright = convert8to12(getsnapshot(vid));

            varargout{3} = [Ileft(1:2:end,1:2:end,1) Iright(1:2:end,1:2:end,1)];
            
        %Decimated Data Blue
            SerialBitWriteCCU(Port,1,4*256+50,dec2bin(3,4),[27:-1:24],S1);

            %Enable 12-bit mode left side
            SerialWriteCCU(Port,1,4*256+46,'00000001',S1);pause(0.25)
            Ileft = convert8to12(getsnapshot(vid));

            %Enable 12-bit mode right side
            SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);pause(0.25)
            Iright = convert8to12(getsnapshot(vid));

            varargout{17} = [Ileft(1:2:end,1:2:end,1) Iright(1:2:end,1:2:end,1)];
            
        %Low Pass Data Red
            SerialBitWriteCCU(Port,1,4*256+50,dec2bin(4,4),[27:-1:24],S1);

            %Enable 12-bit mode left side
            SerialWriteCCU(Port,1,4*256+46,'00000001',S1);pause(0.25)
            Ileft = convert8to12(getsnapshot(vid));

            %Enable 12-bit mode right side
            SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);pause(0.25)
            Iright = convert8to12(getsnapshot(vid));

            varargout{4} = [Ileft(1:2:end,1:2:end,1) Iright(1:2:end,1:2:end,1)];
        
        %Low Pass Data Green
            SerialBitWriteCCU(Port,1,4*256+50,dec2bin(5,4),[27:-1:24],S1);

            %Enable 12-bit mode left side
            SerialWriteCCU(Port,1,4*256+46,'00000001',S1);pause(0.25)
            Ileft = convert8to12(getsnapshot(vid));

            %Enable 12-bit mode right side
            SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);pause(0.25)
            Iright = convert8to12(getsnapshot(vid));

            varargout{5} = [Ileft(1:2:end,1:2:end,1) Iright(1:2:end,1:2:end,1)];
        
        %Low Pass Data Blue
            SerialBitWriteCCU(Port,1,4*256+50,dec2bin(6,4),[27:-1:24],S1);

            %Enable 12-bit mode left side
            SerialWriteCCU(Port,1,4*256+46,'00000001',S1);pause(0.25)
            Ileft = convert8to12(getsnapshot(vid));

            %Enable 12-bit mode right side
            SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);pause(0.25)
            Iright = convert8to12(getsnapshot(vid));

            varargout{6} = [Ileft(1:2:end,1:2:end,1) Iright(1:2:end,1:2:end,1)];
            
        %Horizontal Mean (Low)
            SerialBitWriteCCU(Port,1,4*256+50,dec2bin(8,4),[27:-1:24],S1);

            %Enable 12-bit mode left side
            SerialWriteCCU(Port,1,4*256+46,'00000001',S1);pause(0.25)
            Ileft = convert8to12(getsnapshot(vid));

            %Enable 12-bit mode right side
            SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);pause(0.25)
            Iright = convert8to12(getsnapshot(vid));

            varargout{7} = [Ileft(1:2:end,1:2:end,1) Iright(1:2:end,1:2:end,1)];
        
        %Horizontal Mean (High)
            SerialBitWriteCCU(Port,1,4*256+50,dec2bin(9,4),[27:-1:24],S1);

            %Enable 12-bit mode left side
            SerialWriteCCU(Port,1,4*256+46,'00000001',S1);pause(0.25)
            Ileft = convert8to12(getsnapshot(vid));

            %Enable 12-bit mode right side
            SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);pause(0.25)
            Iright = convert8to12(getsnapshot(vid));

            varargout{8} = [Ileft(1:2:end,1:2:end,1) Iright(1:2:end,1:2:end,1)];
        
        %Horizontal Variance (Low)
            SerialBitWriteCCU(Port,1,4*256+50,dec2bin(10,4),[27:-1:24],S1);

            %Enable 12-bit mode left side
            SerialWriteCCU(Port,1,4*256+46,'00000001',S1);pause(0.25)
            Ileft = convert8to12(getsnapshot(vid));

            %Enable 12-bit mode right side
            SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);pause(0.25)
            Iright = convert8to12(getsnapshot(vid));

            varargout{9} = [Ileft(1:2:end,1:2:end,1) Iright(1:2:end,1:2:end,1)];
        
        %Horizontal Variance (High)
            SerialBitWriteCCU(Port,1,4*256+50,dec2bin(11,4),[27:-1:24],S1);

            %Enable 12-bit mode left side
            SerialWriteCCU(Port,1,4*256+46,'00000001',S1);pause(0.25)
            Ileft = convert8to12(getsnapshot(vid));

            %Enable 12-bit mode right side
            SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);pause(0.25)
            Iright = convert8to12(getsnapshot(vid));

            varargout{10} = [Ileft(1:2:end,1:2:end,1) Iright(1:2:end,1:2:end,1)];
        
        %Vertical Mean (Low)
            SerialBitWriteCCU(Port,1,4*256+50,dec2bin(12,4),[27:-1:24],S1);

            %Enable 12-bit mode left side
            SerialWriteCCU(Port,1,4*256+46,'00000001',S1);pause(0.25)
            Ileft = convert8to12(getsnapshot(vid));

            %Enable 12-bit mode right side
            SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);pause(0.25)
            Iright = convert8to12(getsnapshot(vid));

            varargout{11} = [Ileft(1:2:end,1:2:end,1) Iright(1:2:end,1:2:end,1)];
        
        %Vertical Mean (High)
            SerialBitWriteCCU(Port,1,4*256+50,dec2bin(13,4),[27:-1:24],S1);

            %Enable 12-bit mode left side
            SerialWriteCCU(Port,1,4*256+46,'00000001',S1);pause(0.25)
            Ileft = convert8to12(getsnapshot(vid));

            %Enable 12-bit mode right side
            SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);pause(0.25)
            Iright = convert8to12(getsnapshot(vid));

            varargout{12} = [Ileft(1:2:end,1:2:end,1) Iright(1:2:end,1:2:end,1)];
        
        %Vertical Variance (Low)
            SerialBitWriteCCU(Port,1,4*256+50,dec2bin(14,4),[27:-1:24],S1);

            %Enable 12-bit mode left side
            SerialWriteCCU(Port,1,4*256+46,'00000001',S1);pause(0.25)
            Ileft = convert8to12(getsnapshot(vid));

            %Enable 12-bit mode right side
            SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);pause(0.25)
            Iright = convert8to12(getsnapshot(vid));

            varargout{13} = [Ileft(1:2:end,1:2:end,1) Iright(1:2:end,1:2:end,1)];
        
        %Vertical Variance (High)
            SerialBitWriteCCU(Port,1,4*256+50,dec2bin(15,4),[27:-1:24],S1);

            %Enable 12-bit mode left side
            SerialWriteCCU(Port,1,4*256+46,'00000001',S1);pause(0.25)
            Ileft = convert8to12(getsnapshot(vid));

            %Enable 12-bit mode right side
            SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);pause(0.25)
            Iright = convert8to12(getsnapshot(vid));

            varargout{14} = [Ileft(1:2:end,1:2:end,1) Iright(1:2:end,1:2:end,1)];
                        
            %Disable 12-bit mode
            SerialWriteCCU(Port,1,4*256+46,'02940000',S1);
            
        %BioOptico Raw Codes
            %Spectral
            SerialBitWriteCCU(Port,1,4*256+50,'0100000',[30:-1:24],S1);
            
            pause(0.25)
            varargout{15} = (getsnapshot(vid));  
            
            %Structure
            SerialBitWriteCCU(Port,1,4*256+50,'1000000',[30:-1:24],S1);
            
            %Enable 12-bit mode left side
            pause(0.25)
            varargout{16} = (getsnapshot(vid));
            
            %Turn off Raw Codes
            SerialBitWriteCCU(Port,1,4*256+50,'0000000',[30:-1:24],S1);
            
            
        
        %Disable BioOptico Debug
        SerialBitWriteCCU(Port,1,4*256+50,'0',28,S1);
      
        %Restore 4k Processing
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);pause(0.25)

        %Disable 12-bit mode
        SerialWriteCCU(Port,1,4*256+46,'02940000',S1);
        
    case 22 %Full Sensor Capture (3MOS?)
        SerialBitWriteCCU(Port,1,4*256+8,'1100',[11:-1:8],S1);pause(0.25)
        %12bit 1920x1080 capture  
        %Remeber Register 9
        reg9 = SerialReadCCU(Port,1,9,S1);
        fiber = SerialReadCCU(Port,1,46,S1);
        greenflare = SerialReadCCU(Port,1,114,S1);
        
        %Remeber Crops
        crops = SerialReadCCU(Port,1,256*4+45,S1);

        %Disable Processing (Gamma and Post Peaking/Sharpening)
        SerialWriteCCU(Port,1,9,['FFFFFFFF'],S1);
        SerialWriteCCU(Port,1,46,['00000000'],S1);
        SerialWriteCCU(Port,1,114,['00000EA6'],S1);
        
%         %Freeze Video
%         SerialBitWriteCCU(Port,1,4*256+161,'1',[18],S1);
%         
%         %View Buffer
%         SerialBitWriteCCU(Port,1,4*256+8,'1',[15],S1);
%         pause(0.25)
        
        %Crop Top Left Corner
        SerialWriteCCU(Port,1,256*4+45,[dec2hex(0,2) dec2hex(0,2) dec2hex(0,2) dec2hex(0,2)],S1);
%         SerialBitWriteCCU(Port,1,256*4+45,dec2bin(0,7),[30:-1:24],S1);
%         SerialBitWriteCCU(Port,1,256*4+45,dec2bin(0,7),[22:-1:16],S1);
%         SerialBitWriteCCU(Port,1,256*4+45,dec2bin(0,7),[14:-1:8],S1);
%         SerialBitWriteCCU(Port,1,256*4+45,dec2bin(0,7),[6:-1:0],S1);
        
        %Enable 12-bit mode left side
        SerialWriteCCU(Port,1,4*256+46,'00000001',S1);
        pause(0.25)
        Ic = getsnapshot(vid);
        IA = convert8to12(Ic);
        
        %Enable 12-bit mode right side
        SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);
        pause(0.25)
        Ic = getsnapshot(vid);
        IB = convert8to12(Ic);
        
        %Crops Lower Left Corner
        SerialWriteCCU(Port,1,256*4+45,[dec2hex(0,2) dec2hex(16,2) dec2hex(0,2) dec2hex(12,2)],S1);
%         SerialBitWriteCCU(Port,1,256*4+45,dec2bin(0,7),[30:-1:24],S1);
%         SerialBitWriteCCU(Port,1,256*4+45,dec2bin(16,7),[22:-1:16],S1);
%         SerialBitWriteCCU(Port,1,256*4+45,dec2bin(0,7),[14:-1:8],S1);
%         SerialBitWriteCCU(Port,1,256*4+45,dec2bin(12,7),[6:-1:0],S1);
        
        %Enable 12-bit mode left side
        SerialWriteCCU(Port,1,4*256+46,'00000001',S1);
        pause(0.25)
        Ic = getsnapshot(vid);
        ID = convert8to12(Ic);
        
        %Enable 12-bit mode right side
        SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);
        pause(0.25)
        Ic = getsnapshot(vid);
        IE = convert8to12(Ic);
        
        %Crops Top Middle
        SerialWriteCCU(Port,1,256*4+45,[dec2hex(80,2) dec2hex(0,2) dec2hex(16,2) dec2hex(0,2)],S1);
%         SerialBitWriteCCU(Port,1,256*4+45,dec2bin(80,7),[30:-1:24],S1);
%         SerialBitWriteCCU(Port,1,256*4+45,dec2bin(0,7),[22:-1:16],S1);
%         SerialBitWriteCCU(Port,1,256*4+45,dec2bin(16,7),[14:-1:8],S1);
%         SerialBitWriteCCU(Port,1,256*4+45,dec2bin(0,7),[6:-1:0],S1);
        
        %Enable 12-bit mode right side
        SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);
        pause(0.25)
        Ic = getsnapshot(vid);
        IC = convert8to12(Ic);
        
        %Crops Lower Middle
        SerialWriteCCU(Port,1,256*4+45,[dec2hex(80,2) dec2hex(16,2) dec2hex(16,2) dec2hex(12,2)],S1);
%         SerialBitWriteCCU(Port,1,256*4+45,dec2bin(80,7),[30:-1:24],S1);
%         SerialBitWriteCCU(Port,1,256*4+45,dec2bin(16,7),[22:-1:16],S1);
%         SerialBitWriteCCU(Port,1,256*4+45,dec2bin(16,7),[14:-1:8],S1);
%         SerialBitWriteCCU(Port,1,256*4+45,dec2bin(12,7),[6:-1:0],S1);
        
        %Enable 12-bit mode right side
        SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);
        pause(0.25)
        Ic = getsnapshot(vid);
        IF = convert8to12(Ic);
        
%         %unFreeze Video
%         SerialBitWriteCCU(Port,1,4*256+161,'0',[18],S1);
%         
%         %View Live Video
%         SerialBitWriteCCU(Port,1,4*256+8,'0',[15],S1);
        
        %Enable Processing
        SerialWriteCCU(Port,1,9,reg9,S1);
        SerialWriteCCU(Port,1,46,fiber,S1);
        SerialWriteCCU(Port,1,114,greenflare,S1);
        
        %Reset Crops
        SerialWriteCCU(Port,1,256*4+45,crops,S1);
        
        %Disable 12-bit mode
        SerialWriteCCU(Port,1,4*256+46,'02940000',S1);
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);
        varargout{1} = [IA IB IC(:,865:end,:); ID(1053:end,:,:) IE(1053:end,:,:) IF(1053:end,865:end,:)];

    case 23 %Full Sensor Capture (3MOS?)
        SerialBitWriteCCU(Port,1,4*256+8,'1100',[11:-1:8],S1);pause(0.25)
        %12bit 1920x1080 capture  
        %Remeber Register 9
        reg9 = SerialReadCCU(Port,1,9,S1);
        fiber = SerialReadCCU(Port,1,46,S1);
        greenflare = SerialReadCCU(Port,1,114,S1);
        
        %Remeber Crops
        crops = SerialReadCCU(Port,1,256*4+45,S1);

        %Disable Processing (Gamma and Post Peaking/Sharpening)
        SerialWriteCCU(Port,1,9,['FFFFFFFF'],S1);
        SerialWriteCCU(Port,1,46,['00000000'],S1);
        SerialWriteCCU(Port,1,114,['00000EA6'],S1);
                
        %Crop Top Left Corner
        SerialWriteCCU(Port,1,256*4+45,[dec2hex(0,2) dec2hex(1,2) dec2hex(0,2) dec2hex(0,2)],S1);
        
        %Enable 12-bit mode left side
        SerialWriteCCU(Port,1,4*256+46,'00000001',S1);
        pause(0.25)
        Ic = getsnapshot(vid);
        IA = convert8to12(Ic);
        
        %Enable 12-bit mode right side
        SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);
        pause(0.25)
        Ic = getsnapshot(vid);
        IB = convert8to12(Ic);
        
        %Crops Lower Left Corner
        SerialWriteCCU(Port,1,256*4+45,[dec2hex(0,2) dec2hex(16,2) dec2hex(0,2) dec2hex(12,2)],S1);
        pause(0.25)
        %Enable 12-bit mode left side
        SerialWriteCCU(Port,1,4*256+46,'00000001',S1);
        pause(0.25)
        Ic = getsnapshot(vid);
        ID = convert8to12(Ic);
        
        %Enable 12-bit mode right side
        SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);
        pause(0.25)
        Ic = getsnapshot(vid);
        IE = convert8to12(Ic);
        
        %Crops Top Middle
        SerialWriteCCU(Port,1,256*4+45,[dec2hex(80,2) dec2hex(1,2) dec2hex(16,2) dec2hex(0,2)],S1);
        pause(0.25)
        %Enable 12-bit mode right side
        SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);
        pause(0.25)
        Ic = getsnapshot(vid);
        IC = convert8to12(Ic);
        
        %Crops Lower Middle
        SerialWriteCCU(Port,1,256*4+45,[dec2hex(80,2) dec2hex(16,2) dec2hex(16,2) dec2hex(12,2)],S1);
        pause(0.25)
        %Enable 12-bit mode right side
        SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);
        pause(0.25)
        Ic = getsnapshot(vid);
        IF = convert8to12(Ic);
        
        %Enable Processing
        SerialWriteCCU(Port,1,9,reg9,S1);
        SerialWriteCCU(Port,1,46,fiber,S1);
        SerialWriteCCU(Port,1,114,greenflare,S1);
        
        %Reset Crops
        SerialWriteCCU(Port,1,256*4+45,crops,S1);
        
        %Disable 12-bit mode
        SerialWriteCCU(Port,1,4*256+46,'02940000',S1);
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);
        
        %      1      2      3
        %  IA  |  IB  |  IC  |
        %------|------|------|-4
        %  ID  |  IE  |  IF  |
        %------|------|------|-5
        
        CutLoc = [1   960;...
                  1   960;...
                  865 960;...
                  1   978;...
                  951 960];
        
%         varargout{1} = [IA(1:978,:,:) IB(1:978,:,:) IC(1:978,865:end,:); ID(951:end,:,:) IE(951:end,:,:) IF(951:end,865:end,:)];

        %Not Sure Why, but this works for 1MOS
        varargout{1} = [IA(1:978,:,:) IB(1:978,:,:) IC(1:978,865:end,:); ID(952:end,:,:) IE(952:end,:,:) IF(952:end,865:end,:); ID(end,:,:) IE(end,:,:) IF(end,865:end,:)];
end

if(length(varargin) >= 4)
    %Don't Close S1
else
    if(Mode ~= 1 && exist('S1','var'))
        fclose(S1);
    end
end

function out = DPunscramble(in)
% 0 0 0 0 0 0 | 1 1 1 1 1 1
% 2 2 2 2 2 2 | 3 3 3 3 3 3 ...

% 0 0 0 0 0 0
% 1 1 1 1 1 1 ...

out = zeros(2160,960,3);

for i = 1:3
    out(:,:,i) = reshape(in(:,:,i)',960,2160)';
end

out = uint8(out);
