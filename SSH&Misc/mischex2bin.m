function [out] = mischex2bin(in)

out = dec2bin(hex2dec(in),32);

end