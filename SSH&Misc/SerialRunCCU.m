function [output, msg] = SerialRunCCU(S1,Action,varargin) 

if(length(varargin) > 0)
    pauseTime = varargin{1};
else
    pauseTime = 5;
end

output = 0;

msg = 'start';
if(strcmpi(S1.status,'closed'))
    fopen(S1);
else
    fclose(S1);
    pause(0.1)
    fopen(S1);
end

fwrite(S1,sprintf('\r\n'))
pause(0.1)
test0 = [];
if(S1.bytesAvailable)
    test0 = char(fread(S1,S1.bytesAvailable)');
end
maxAttempt = 5;
attempt = 0;

while(isempty(strfind(test0,'login:')))
    fwrite(S1,sprintf('exit\n'))
    pause(0.1)
    if(S1.bytesAvailable > 0)
        test0 = char(fread(S1,S1.bytesAvailable)');
    end
    attempt = attempt + 1;
    if(attempt >= maxAttempt)
        break;
    end
end
pause(0.1)
fwrite(S1,sprintf('arthrex\n'))
pause(0.1)
fwrite(S1,sprintf('Arthrex1\n'))
pause(0.1)

if(S1.bytesAvailable)
    junk = char(fread(S1,S1.bytesAvailable)');
end

fwrite(S1,sprintf('%s\n',Action))
pause(pauseTime)

if(S1.bytesAvailable)
    msg = char(fread(S1,S1.bytesAvailable)');
end
output = 1;
fwrite(S1,sprintf('exit\n'))
pause(0.1)
fwrite(S1,sprintf('exit\n'))

fclose(S1);