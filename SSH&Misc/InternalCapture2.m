IP = '192.168.1.2';
directory = 'C:\Users\AFoster\Pictures\Matlab Images\Internal Captures\';
pause(2)
[status, Capture_Message] = system('plink -pw Arthrex1 arthrex@192.168.1.2 capture');
if(regexp(Capture_Message,'FATAL ERROR'))
    pause(5)
    [status, Capture_Message] = system('plink -pw Arthrex1 arthrex@192.168.1.2 capture');
    if(regexp(Capture_Message,'FATAL ERROR'))
        pause(5)
        [status, Capture_Message] = system('plink -pw Arthrex1 arthrex@192.168.1.2 capture');
        if(regexp(Capture_Message,'FATAL ERROR'))
            pause(5)
            [status, Capture_Message] = system('plink -pw Arthrex1 arthrex@192.168.1.2 capture');
        end
    end
end
pause(30)
[status, LS_Message] = system('plink -pw Arthrex1 arthrex@192.168.1.2 ls /live/data/tmp/*.rgb');
if(regexp(LS_Message,'FATAL ERROR'))
    pause(5)
    [status, LS_Message] = system('plink -pw Arthrex1 arthrex@192.168.1.2 ls /live/data/tmp/*.rgb');
    if(regexp(LS_Message,'FATAL ERROR'))
        pause(5)
        [status, LS_Message] = system('plink -pw Arthrex1 arthrex@192.168.1.2 ls /live/data/tmp/*.rgb');
        if(regexp(LS_Message,'FATAL ERROR'))
            pause(5)
            [status, LS_Message] = system('plink -pw Arthrex1 arthrex@192.168.1.2 ls /live/data/tmp/*.rgb');
        end
    end
end

[status, LS_Message_Full] = system('plink -pw Arthrex1 arthrex@192.168.1.2 ls -l /live/data/tmp/*.rgb');
if(regexp(LS_Message_Full,'FATAL ERROR'))
    pause(5)
    [status, LS_Message_Full] = system('plink -pw Arthrex1 arthrex@192.168.1.2 ls -l /live/data/tmp/*.rgb');
    if(regexp(LS_Message_Full,'FATAL ERROR'))
        pause(5)
        [status, LS_Message_Full] = system('plink -pw Arthrex1 arthrex@192.168.1.2 ls -l /live/data/tmp/*.rgb');
        if(regexp(LS_Message_Full,'FATAL ERROR'))
            pause(5)
            [status, LS_Message_Full] = system('plink -pw Arthrex1 arthrex@192.168.1.2 ls -l /live/data/tmp/*.rgb');
        end
    end
end

files = textscan(LS_Message,'%s');
clear imName
for i = 1:length(files{1})
    imName{i} = files{1}{i}(19:end-10);
end
if(exist('Modified','var'))
    LastModified = Modified;
else
    LastModified{1} = '00:00';
end
Modified = regexp(LS_Message_Full,'\d\d:\d\d','match');

new = 1;

str = sprintf('pscp -pw Arthrex1 arthrex@%s:/live/data/tmp/im_%s_4k.rgb "%sim_%s_4k_%s.rgb"',IP,imName{new},directory,imName{new},Modified{3}(Modified{3} ~= ':'));
[status Transfer4k_Message] = system(str);

fid = fopen([directory 'im_' imName{new} '_4k_' Modified{3}(Modified{3} ~= ':') '.rgb']);
data = fread(fid);fclose(fid);
I4kint = uint8(permute(reshape(data,3,3840,2160),[3 2 1]));

str = sprintf('pscp -pw Arthrex1 arthrex@%s:/live/data/tmp/im_%s_1080p.rgb "%sim_%s_1080p_%s.rgb"',IP,imName{new},directory,imName{new},Modified{1}(Modified{1} ~= ':'));
[status Transfer1080_Message] = system(str);

fid = fopen([directory 'im_' imName{new} '_1080p_' Modified{1}(Modified{1} ~= ':') '.rgb']);
data = fread(fid);fclose(fid);
I1080int = uint8(permute(reshape(data,3,1920,1080),[3 2 1]));

str = sprintf('pscp -pw Arthrex1 arthrex@%s:/live/data/tmp/im_%s_1080p_pip.rgb "%sim_%s_1080p_pip_%s.rgb"',IP,imName{new},directory,imName{new}, Modified{2}(Modified{2} ~= ':'));
[status Transfer1080_pip_Message] = system(str);

fid = fopen([directory 'im_' imName{new} '_1080p_pip_' Modified{2}(Modified{2} ~= ':') '.rgb']);
data = fread(fid);fclose(fid);
I1080pip = uint8(permute(reshape(data,3,1920,1080),[3 2 1]));

str = sprintf('pscp -pw Arthrex1 arthrex@%s:/live/data/tmp/im_%s_thumb.rgb "%sim_%s_thumb_%s.rgb"',IP,imName{new},directory,imName{new},Modified{4}(Modified{4} ~= ':'));
[status Transferthumb_Message] = system(str);

fid = fopen([directory 'im_' imName{new} '_thumb_' Modified{4}(Modified{4} ~= ':') '.rgb']);
data = fread(fid);fclose(fid);
Ithumb = uint8(permute(reshape(data,3,480,270),[3 2 1]));

str = sprintf('pscp -pw Arthrex1 arthrex@%s:/live/data/tmp/im_%s_thumb_pip.rgb "%sim_%s_thumb_pip_%s.rgb"',IP,imName{new},directory,imName{new},Modified{5}(Modified{5} ~= ':'));
[status Transferthumb_pip_Message] = system(str);

fid = fopen([directory 'im_' imName{new} '_thumb_pip_' Modified{5}(Modified{5} ~= ':') '.rgb']);
data = fread(fid);fclose(fid);
Ithumbpip = uint8(permute(reshape(data,3,480,270),[3 2 1]));

figure(1); imagesc([I4kint [I1080int; I1080pip] imresize([Ithumb; Ithumbpip],4)])





