%Image Registration Function (Optimised)
%Given 2 images estimate the translation
%By Alexander Foster


%Input - 2 images of equal resolution
%Output - Estimated Translation (x and y)
function [Tx Ty] = imRegOpt(I1,I2)

%upsample by 2 for initial estimate
up = 2;

I1f = fftshift(fft2(I1));
I2f = fftshift(fft2(I2));
A = I1f.*conj(I2f); 
P = padarray(A,[floor(size(I1,1)*(up-1)/2) floor(size(I1,2)*(up-1)/2)]);

C = ifft2(ifftshift(P),'symmetric');

C = fftshift(C);

%initial estimate restricted to +/- 1.5 pixels in each direction
[r c v] = find(C == max(max(C(size(I1,1)-3:size(I1,1)+3,size(I1,2)-3:size(I1,2)+3))));

x0 = (c-size(I1,1)-1)/2;
y0 = (r-size(I1,2)-1)/2;

M = size(I1,1);
N = size(I1,2);
% u = ones(100,1)*(1:M);
% v = (1:N)'*ones(1,100);

%Function to be minimized
%1st Output returns the (negative) square of the cross correlation at (x,y)
%2nd Output returns the (negative) gradient vector (partials wrt [x y])

% func = @(x,y) deal(-sum(sum(I1f.*conj(I2f).*exp(1i*2*pi*(u.*x/M+v.*y/N)))).^2 ...  
%                   ,[ -2*imag(sum(sum(I1f.*conj(I2f).*exp(1i*2*pi*(u.*x/M+v.*y/N))))*sum(sum(2*pi*u/M.*conj(I1f).*I2f.*exp(-1i*2*pi*(u.*x/M+v.*y/N)))))...
%                      -2*imag(sum(sum(I1f.*conj(I2f).*exp(1i*2*pi*(u.*x/M+v.*y/N))))*sum(sum(2*pi*v/N.*conj(I1f).*I2f.*exp(-1i*2*pi*(u.*x/M+v.*y/N)))))]);   

func = @(x)minFunc(x,I1f,I2f,M,N);

% [out] = fminunc(func,[x0 y0],optimset('GradObj','on','Display','off'));

[out] = fmincon(func, [y0 x0],[],[],[],[],[-5 -5],[5 5],[],optimset('GradObj','on','Display','off','Algorithm','trust-region-reflective'));

Tx = -out(2); 
Ty = -out(1);
% 
% Tx = 1;
% Ty = 2;
end
