function Iout = StitchStripes(Iin)
% Assemble 4 stripes into full 4k output

%Crop from 992x2184 to 960x2160
DPoffsetx = 1;
DPoffsety = 1;

Iout = cat(2,Iin{1}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
             Iin{2}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
             Iin{3}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:),...
             Iin{4}([13:end-12]+DPoffsety,[17:end-16]+DPoffsetx,:));
           
