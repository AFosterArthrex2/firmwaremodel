%Test CaptureInternal() by setting a test pattern in the head and then
%taking an internal capture


SetupPath
Port = 'COM5';

if(~exist('S1','var') || ~isvalid(S1))
    S1 = serial(Port,'BaudRate',9600);
end

if(strcmp(S1.status,'closed'))
    fopen(S1);
else
    fclose(S1);
    pause(0.2)
    fopen(S1);
end

for i = 1:20
    
    %Write to Head TPG Register
    SerialWriteCCU(Port,2,10,'00000001',S1);
    
    [I,Modified,Errors] = CaptureInternal();
    I4kint = I{1};
    
    if(~exist('Ifirst','var'))
        Ifirst = I4kint;
        imwrite(I4kint,'Ifirst.png')
    end
    
    diff = double(I4kint)-double(Ifirst);
    
    result(i) = max(max(max(abs(diff))));
    fprintf('Cycle: %04d. Max difference = %04d.\n',i,result(i))

    imwrite(I4kint,sprintf('Image#%d.png',i))
    
    %Write to Head TPG Register
    SerialWriteCCU(Port,2,10,'00000000',S1);
    
    pause(1)
    
end