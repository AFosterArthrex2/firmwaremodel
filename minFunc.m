% func = @(x,y) deal(-sum(sum(F.*conj(G).*exp(1i*2*pi*(u.*x/M+v.*y/N)))).^2 ...  
%                   ,[ -2*imag(sum(sum(F.*conj(G).*exp(1i*2*pi*(u.*x/M+v.*y/N))))*sum(sum(2*pi*u/M.*conj(F).*G.*exp(-1i*2*pi*(u.*x/M+v.*y/N)))))...
%                      -2*imag(sum(sum(F.*conj(G).*exp(1i*2*pi*(u.*x/M+v.*y/N))))*sum(sum(2*pi*v/N.*conj(F).*G.*exp(-1i*2*pi*(u.*x/M+v.*y/N)))))]);   
%works as a function but fminunc doesn't seem to like it, probably deal()

function [val grad] = minFunc(x,F,G,M,N)

u = ones(size(F,1),1)*(1:N);
v = (1:M)'*ones(1,size(F,2));

val = -abs(sum(sum(F.*conj(G).*exp(1i*2*pi*(u.*x(1)/M+v.*x(2)/N)))))^2;

grad = [ -2*imag(sum(sum(F.*conj(G).*exp(1i*2*pi*(u.*x(1)/M+v.*x(2)/N))))*sum(sum(2*pi*u/M.*conj(F).*G.*exp(-1i*2*pi*(u.*x(1)/M+v.*x(2)/N)))))...
         -2*imag(sum(sum(F.*conj(G).*exp(1i*2*pi*(u.*x(1)/M+v.*x(2)/N))))*sum(sum(2*pi*v/N.*conj(F).*G.*exp(-1i*2*pi*(u.*x(1)/M+v.*x(2)/N)))))];   


end