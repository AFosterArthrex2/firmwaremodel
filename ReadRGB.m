%RGB2PNG
%Opens a .rgb image 

directory = 'G:\Matlab Images';

%             Image         Horz    Vert
imageTable = {'4k',         3840,   2160;...
    '1080p',      1920,   1080;...
    '1080p_pip',  1920,   1080;...
    'thumb',      0480,   0270;...
    'thumb_pip',  0480,   0270};

for i = 1:length(imageTable)
    
    filename = sprintf('%s\\im_0021_%s.rgb',directory,imageTable{i,1});
    
    fid = fopen(filename);
    data = fread(fid);fclose(fid);
    I{i} = uint8(permute(reshape(data,3,imageTable{i,2},imageTable{i,3}),[3 2 1]));

end