%% G2YCbCr2RGB1080     
% Converts yCbCr Color Space Image to RGB Color Space
%%
%  [Iout] = G2YCbCr2RGB1080(Iin,settings)
%   Inputs: 
%           Iin     - Input Image
%           settings
%               YCbCr2RGB1080_Enable
%               
%   Outputs: 
%           Iout    - Output Image
%

%   Gen2 Firmware Model - YCbCr2RGB
%   Author: Alexander Foster
%   Date:   10/12/15

function [Iout] = G2YCbCr2RGB1080(Iin,varargin)

%% Settings
reqsettings = {'YCbCr2RGB1080_Enable',    1};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

if(settings.YCbCr2RGB1080_Enable)
    %%%Iin(:,:,1) should be multiplied by 4 first
    I = floor(double(Iin))/16;
    
    R = 1*I(:,:,1) + 0*I(:,:,2) + 4*I(:,:,3);
    G = 1*I(:,:,1) - 2*I(:,:,2) - 2*I(:,:,3);
    B = 1*I(:,:,1) + 4*I(:,:,2) + 0*I(:,:,3);
    
    Iout(:,:,1) = min(max(fix(R),0),2^8-1);
    Iout(:,:,2) = min(max(fix(G),0),2^8-1);
    Iout(:,:,3) = min(max(fix(B),0),2^8-1);
    
    %truncate and cap as a 8 bit integer
    Iout = (max(floor(Iout),0));
else
    Iout = Iin;
end