function Iout = G2LowPass(Iin,disable)

if(~disable)
    K = [hex2dec('4318') hex2dec('79CE') hex2dec('4318')];
    
    Ip = padarray(double(Iin),[0 1 0],'replicate','both');
    
    %Horizontal
    K1 = floor(K(1)*Ip(:,1:end-2,:)/4096);
    K2 = floor(K(2)*Ip(:,2:end-1,:)/4096);
    K3 = floor(K(3)*Ip(:,3:end,:)/4096);
    
    Itmp = min(2^12,max(0,floor((K1+K2+K3)/16)));
    
    Itmp = padarray(Itmp,[1 0 0],'replicate','both');
    
    %Vertical
    K1 = floor(K(1)*Itmp(1:end-2,:,:)/4096);
    K2 = floor(K(2)*Itmp(2:end-1,:,:)/4096);
    K3 = floor(K(3)*Itmp(3:end,:,:)/4096);
    
    Iout = min(2^12,max(0,floor((K1+K2+K3)/16)));
    
    Iout = (Iout);
    
    %Seperated
    %Horizontal
%     Itmp = zeros(size(Iin));
%     for i = 1:3
%         for j = 1:size(Iin,1)
%             Itmp(j,:,i) = conv(double(Iin(j,:,i)),K1,'same');
%         end
%     end
%     Itmp = floor(Itmp);
%     %Vertical
%     Iout = zeros(size(Iin));
%     for i = 1:3
%         for j = 1:size(Iin,2)
%             Iout(:,j,i) = conv(double(Itmp(:,j,i)),K1,'same');
%         end
%     end
%     
%     Iout = uint16(floor(Iout));
    
else
    Iout = Iin;
end
