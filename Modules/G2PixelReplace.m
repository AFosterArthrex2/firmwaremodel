%% G2PixelReplace
% Replaces "Hot"/bright pixels in the image.
%%
%  [Iout] = G2PixelReplace(Iin,settings)
%   Inputs: 
%           Iin     - Input Image
%           settings
%               Pixel_Replace_Enable
%               HeadType
%
%   Outputs: 
%           Iout    - Output Image
%

%   Gen2 Firmware Model - Pixel Replacement Filter
%   Author: Alexander Foster
%   Date:   10/12/15

function [Iout] = G2PixelReplace(Iin,varargin)

%% Settings
reqsettings = {'Pixel_Replace_Disable',  1;...
               'Head_Type',              1;};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

%% Apply Pixel Replace Filter
if(settings.Pixel_Replace_Disable)
    Iout = Iin;
elseif(settings.Head_Type)
    
    Iinp = padarray(double(Iin),[1 1 0],0,'both');
    
    Iout = zeros(size(Iin));
    
    p = [7 7 7];

    counter = 0;
    
    myfunc = @(x) ColPixRep(x);
    
    Red = colfilt(Iinp(:,:,1),[3 3],'sliding',myfunc);
    Green = colfilt(Iinp(:,:,2),[3 3],'sliding',myfunc);
    Blue = colfilt(Iinp(:,:,3),[3 3],'sliding',myfunc);
    Iout = cat(3,Red(2:end-1,2:end-1,:),Green(2:end-1,2:end-1,:),Blue(2:end-1,2:end-1,:));

     %Pass 1 pixel border
    mask = true(size(Iin));
    mask(2:end-1,2:end-1,:) = 0;
    Iout(mask) = Iin(mask);
    
    %truncate and cap as a 12 bit integer
    Iout = uint16(min(floor(Iout),2^12-1));
    
else
    %1MOS, head_type = 0
    Iinp = padarray(double(Iin),[2 2 0],0,'both');
    
    Iout = zeros(size(Iin));
    
    p = [7 7 7];

    counter = 0;
    
    myfunc = @(x) ColPixRep_1MOS(x);
%     myfunc = @(x) NoReplace(x);
    
    I1 = Iinp(1:2:end,1:2:end,3);
    I2 = Iinp(1:2:end,2:2:end,3);
    I3 = Iinp(2:2:end,1:2:end,3);
    I4 = Iinp(2:2:end,2:2:end,3);
    
    I1f = colfilt(I1,[3 3],'sliding',myfunc);
    I2f = colfilt(I2,[3 3],'sliding',myfunc);
    I3f = colfilt(I3,[3 3],'sliding',myfunc);
    I4f = colfilt(I4,[3 3],'sliding',myfunc);

%     I1f = I1;
%     I2f = I2;
%     I3f = I3;
%     I4f = I4;
    
    Iout(1:2:end,1:2:end,3) = I1f(2:(end-1),2:(end-1));
    Iout(1:2:end,2:2:end,3) = I2f(2:(end-1),2:(end-1));
    Iout(2:2:end,1:2:end,3) = I3f(2:(end-1),2:(end-1));
    Iout(2:2:end,2:2:end,3) = I4f(2:(end-1),2:(end-1));
    
      %Pass 1 pixel border
    mask = true(size(Iin));
    mask(2:end-1,2:end-1,:) = 0;
    Iout(mask) = Iin(mask);
    
    %truncate and cap as a 12 bit integer
    Iout = uint16(min(floor(Iout),2^12-1));
end

end

%% Private Helper Functions
function out = ColPixRep(in)
    
    %Precision - number of bits to save
    P = [7 7 7];
    
    ins = in;
    c = in(5,:);
    ct = floor(c/2^2);
    
    in(5,:) = 0;
    
    [m i] = max(in,[],1);
    if(size(in,2) > 1)
        in(sub2ind(size(in),i, [1:size(in,2)])) = 0;
    else
        in(i) = 0;
    end
    [m2 i] = max(in,[],1);
    m2t = floor(m2/2^2);
    
    a = floor(sum(in)*1/64)+floor(sum(in)*1/8);
    
    at = floor(a/2^2);
    
    G = floor(103 + double(m2t)) - ct;
    gain = floor(G/128*2^P(1))/2^P(1) + floor(G/512*2^P(2))/2^P(2);
    
    gain(ct <= m2t) = 1;
    gain(ct >= 103 + m2t) = 0;
    
    gainT = max(0,min(1,floor(gain*2^P(3))/2^P(3)));
    out = max(0,min(floor((1-gainT).*double(a))+floor((gainT).*double(c)),2^12));
end
    

function out = ColPixRep_1MOS(in)
    
    %Precision - number of bits to save
    P = [7 7 7];
    
    ins = in;
    c = in(5,:);
    ct = floor(c/2^2);
    
%     in(5,:) = 0;
    
    [maxval, maxind] = max(in([1:4 6:9],:),[],1);
    [minval, minind] = min(in([1:4 6:9],:),[],1);
    
    %Calculate Average of 8 neighbors
    [avg] = floor(sum(in([1:4 6:9],:))*1/8);
    
    maxt = floor(maxval/2^2);
    mint = floor(minval/2^2);
    
    %  // "Hot pixel" when value is greater than knee (above max of neighbors)
    %  hotpixel1CCD_max<=({1'b0,pix3_3B[11:2]}>=({1'b0,knee}+{1'b0,max1CCD[11:2]})) ? 1'b1 : 1'b0;
    hot_pix_max = ct >= (103 + maxt);
    
    %  // "Cold pixel" when value is less than maximum of neighbors
    %  coldpixel1CCD_max<=(pix3_3B[11:2]<=max1CCD[11:2]) ? 1'b1 : 1'b0;
    cold_pix_max = ct <= maxt;
    
    %  // Pixel gain is the amount that pixel is less than knee
    %  pixgain1CCD_max<={1'b0,knee}+{1'b0,max1CCD[11:2]}-{1'b0,pix3_3B[11:2]};
    gain_pix_max = 103 + maxt - ct;
    gain_pix_max_scaled = floor(gain_pix_max/128*2^P(1))/2^P(1) + floor(gain_pix_max/512*2^P(2))/2^P(2);
    
    
    %  // "Hot pixel" when value plus knee is less than min of neighbors
    %  hotpixel1CCD_min<=(({1'b0,pix3_3B[11:2]}+{1'b0,knee})<={1'b0,min1CCD[11:2]}) ? rb_1cmos_pix_repl_dark_enable : 1'b0;
    hot_pix_min = ct + 103 <= mint;
    
    %  // "Cold pixel" when value is greater than minimum of neighbors
    %  coldpixel1CCD_min<=(pix3_3B[11:2]>=min1CCD[11:2]) ? 1'b1 : ~rb_1cmos_pix_repl_dark_enable;
    cold_pix_min = ct >= mint;
    
    %  // Pixel gain is the amount that pixel is greater than (minimum - knee)
    %  pixgain1CCD_min<={1'b0,pix3_3B[11:2]}+{1'b0,knee}-{1'b0,min1CCD[11:2]};
    gain_pix_min = 103 + ct - mint;
    gain_pix_min_scaled = floor(gain_pix_min/128*2^P(1))/2^P(1) + floor(gain_pix_min/512*2^P(2))/2^P(2);
    
    
    %  // Average gain is amount that pixel is greater than maximum of neighbors
    %  avggain1CCD_max<={1'b0,pix3_3B[11:2]}-{1'b0,max1CCD[11:2]};
    gain_avg_max = ct - maxt;
    gain_avg_max_scaled = floor(gain_avg_max/128*2^P(1))/2^P(1) + floor(gain_avg_max/512*2^P(2))/2^P(2);
    
    %  // Average gain is amount that pixel is less than minimum of neighbors
    %  avggain1CCD_min<={1'b0,min1CCD[11:2]}-{1'b0,pix3_3B[11:2]};
    gain_avg_min = mint - ct;
    gain_avg_min_scaled = floor(gain_avg_min/128*2^P(1))/2^P(1) + floor(gain_avg_min/512*2^P(2))/2^P(2);
    
    
    %Pixel Gain
    %Select appropriate scaled gain
    pixgain( cold_pix_min) = gain_pix_max_scaled( cold_pix_min);
    pixgain(~cold_pix_min) = gain_pix_min_scaled(~cold_pix_min);
    
    % // When pixel is "hot" then its gain is 0.0, when pixel is "cold" its
    % gain is 1.0, otherwise use pixel gain.
    pixgain( hot_pix_min |  hot_pix_max) = 0;
    pixgain(cold_pix_min & cold_pix_max) = 1;
    
    
    %Average Gain
    %Select appropriate scaled gain
    avggain( cold_pix_min) = gain_avg_max_scaled( cold_pix_min);
    avggain(~cold_pix_min) = gain_avg_min_scaled(~cold_pix_min);
    
    %  // When pixel is "hot" then average gain is 1.0, when pixel is "cold" then average gain is 0.0, 
    %  // otherwise use calculated average gain.
    avggain( hot_pix_min |  hot_pix_max) = 1;
    avggain(cold_pix_min & cold_pix_max) = 0;
    
    out = max(0,min(2^12-1,floor(pixgain.*c) + floor(avggain.*avg)));
    
    
    
    
    
    
%     gain_max = floor(Gmax/128*2^P(1))/2^P(1) + floor(Gmax/512*2^P(2))/2^P(2);
%     gain_max(ct <= maxt) = 1;
%     gain_max(ct >= 103 + maxt) = 0;
%     
%     gain_min = floor(Gmin/128*2^P(1))/2^P(1) + floor(Gmin/512*2^P(2))/2^P(2);
%     gain_min(ct >= mint) = 0;
%     gain_min(ct <= mint - 103) = 1;
%     
%     gainT_max = max(0,min(1,floor(gain_max*2^P(3))/2^P(3)));
%     gainT_min = max(0,min(1,floor(gain_min*2^P(3))/2^P(3)));
%     
%     if(c >= maxval)
%         out = max(0,min(floor((1-gainT_max).*double(avg))+floor((gainT_max).*double(c)),2^12));
%     elseif(c <= minval)
%         out = max(0,min(floor((1-gainT_min).*double(avg))+floor((gainT_min).*double(c)),2^12));
%     else
%         out = max(0,min(c,2^12));
%     end
%     out = 0;
end

function out = NoReplace(in)
    out = in(5,:);
end