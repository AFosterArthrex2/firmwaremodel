%% G2MedianFilter     
% Applies Median Filter to image (3x3)
%%
%  [Iout] = G2MedianFilter(Iin,settings)
%   Inputs: 
%           Iin     - Input Image
%           settings
%               MedianFilter_Enable
%               
%   Outputs: 
%           Iout    - Output Image
%

%   Gen2 Firmware Model - AEC Gain
%   Author: Alexander Foster
%   Date:   10/12/15

function [Iout] = G2MedianFilter_3MOS(Iin,varargin)

%% Settings
reqsettings = {'MedianFilter_Disable',      1;...
               'Head_Type',                 1};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

%% Apply Filter
if(settings.MedianFilter_Disable || ~settings.Head_Type)
    Iout = Iin;
else
    %3MOS Median Filter
    Iout(:,:,1) = medfilt2(Iin(:,:,1),'symmetric');
    Iout(:,:,2) = medfilt2(Iin(:,:,2),'symmetric');
    Iout(:,:,3) = medfilt2(Iin(:,:,3),'symmetric');
    
    %Pass Edges
    Iout(1:8,:,:) = Iin(1:8,:,:);
    Iout(end:end-7,:,:) = Iin(end:end-7,:,:);   
end