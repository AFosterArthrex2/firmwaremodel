function [Iout] = G2AlphaBlend(Ib,Ab,If,Af)
% G2AlphaBlend  Performs Alpha Blending Operation
%  [Iout] = G2AlphaBlend(Ib,Ab,If,Af)
%   Inputs: 
%           Ib      - Background Image
%           Ab      - Background Alpha
%           If      - Foreground Image
%           Af      - Foreground Alpha
%   Outputs: 
%           Iout    - Output Image
%

%   Gen2 Firmware Model - Alpha Blending
%   Author: Alexander Foster
%   Date:   10/12/15

Iout = zeros(size(Ib),'uint16');

Iout(:,:,:) = uint8(floor((Af.*double(If) + (1-Af).*Ab.*double(Ib))./(1-(1-Af).*(1-Ab))));

end