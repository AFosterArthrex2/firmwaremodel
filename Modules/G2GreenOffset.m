%% G2GreenOffset
% Shift Green Image by 1 pixel right and down.
%%
%  [Iout] = G2GreenOffset(Iin,settings)
%   Inputs: 
%           Iin     - Input Image
%           settings
%               Head_Type
%               Green_Offset_Disable
%
%   Outputs: 
%           Iout    - Output Image
%

%   Gen2 Firmware Model - Green Offset 
%   Author: Alexander Foster
%   Date:   10/19/15

function [Iout] = G2GreenOffset(Iin,varargin)

%% Settings
reqsettings = {'Head_Type',               1;...
               'Green_Offset_Disable',    1};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

%Shift Green Image by 1 pixel right and down.
if(settings.Green_Offset_Disable)
    Iout = Iin;
else
    if(settings.Head_Type)
        %3MOS Sensor is flipped
        Iout = zeros(size(Iin));
        
        Red   = [zeros(1,size(Iin,2)) ; zeros(size(Iin,1)-1,1) Iin(1:end-1,1:end-1,1)];
        Blue = [zeros(1,size(Iin,2)) ; zeros(size(Iin,1)-1,1) Iin(1:end-1,1:end-1,3)];
        
        %Copy Edge information
        Red(1,:) = Red(2,:);
        Red(:,1) = Red(:,2);
        Red(1,1) = Red(2,2);
        
        Blue(1,:) = Blue(2,:);
        Blue(:,1) = Blue(:,2);
        Blue(1,1) = Blue(2,2);
        
        Iout(:,:,1) = Red;
        Iout(:,:,2) = Iin(:,:,2);
        Iout(:,:,3) = Blue;
        
        Iout = max(min(floor(Iout),2^12-1),0);
    else
        
%         Green = [zeros(1,size(Iin,2)) ; zeros(size(Iin,1)-1,1) Iin(1:end-1,1:end-1,2)];
%         
%         Green(1,:) = Green(2,:);
%         Green(:,1) = Green(:,2);
%         Green(1,1) = Green(2,2);
%         
%         Iout(:,:,1) = Iin(:,:,1);
%         Iout(:,:,2) = Green;
%         Iout(:,:,3) = Iin(:,:,3);
        
        Iout = max(min(floor(Iin),2^12-1),0);
    end
end
