%% G2FiberScopeFilter
% Applies White Balance Gains to image
%%
%  [Iout] = G2FiberScopeFilter(Iin,settings)
%   Inputs: 
%           Iin     - Input Image
%           settings
%               Fiber_Scope_Enable  -   Enable
%               Fiber_Scope_Filter  -   First six of 11 coefficents
%
%   Outputs: 
%           Iout    - Output Image
%

%   Gen2 Firmware Model - AEC Gain
%   Author: Alexander Foster
%   Date:   10/12/15

function Iout = G2FiberScopeFilter(Iin,varargin)

%% Settings
reqsettings = {'FiberScope_Enable',    1;...
               'FiberScope1',         .1;...
               'FiberScope2',         .1;...
               'FiberScope3',         .1;...
               'FiberScope4',         .1;...
               'FiberScope5',         .1;...
               'FiberScope6',         0};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

%% Apply Filter
fiber = [settings.FiberScope1 settings.FiberScope2 settings.FiberScope3 ...
    settings.FiberScope4 settings.FiberScope5 settings.FiberScope6];
filter = double([fiber fliplr(fiber(1:5))])/2^12;

if(settings.FiberScope_Enable)
    %Filter input image with 11x11 averaging filter
    Ipad = padarray(Iin,[20 20 0],'circular');
    Ipad2 = zeros(size(Ipad));
    Ipad3 = zeros(size(Ipad));
    for k = 1:3
        for i = 1:size(Ipad,1)
            Ipad2(i,:,k) = floor(conv(double(Ipad(i,:,k)),filter,'same'));
        end
        for j = 1:size(Ipad,2)
            Ipad3(:,j,k) = floor(conv(double(Ipad2(:,j,k)),filter,'same'));
        end
    end
    Iout = max(min(Ipad3(21:end-20,21:end-20,:),2^12-1),0);
else
    Iout = Iin;
end
