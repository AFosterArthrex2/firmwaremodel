%% G2Scaler
% Scales the input image by 2 using bilinear method
%%
%  [Iout] = G2Scaler(Iin)
%   Inputs: 
%           Iin     - Input Image
%
%   Outputs: 
%           Iout    - Output Image
%

%   Gen2 Firmware Model - Scaler
%   Author: Alexander Foster
%   Date:   10/19/15

function [Iout] = G2Scaler(Iin,varargin)
%Truncated Bilinear 2x Upscaling

[vert, horz, ~] = size(Iin);

I = double(Iin);

Iout = zeros(2*size(I,1),2*size(I,2),size(I,3));

%Odd Row Odd Column
%Copy corresponding value from Iin
Iout(1:2:2*vert,1:2:2*horz,:) = I(1:vert,1:horz,:);

%Even Row Odd Column
%Average 2 Neighbors (Above and Below)
Iout(2:2:2*vert,1:2:2*horz,:) = (double(I(1:vert,1:horz,:))...
    +padarray(double(I(2:vert,1:horz,:)),[1 0],'replicate','post'))/2;

%Odd Row Even Column
%Average 2 Neighbors (Left and Right)
Iout(1:2:2*vert,2:2:2*horz,:) = (double(I(1:vert,1:horz,:))+...
    padarray(double(I(1:vert,2:horz,:)),[0 1],'replicate','post'))/2;

%Odd Row Odd Column
%Average 4 Neighbors (Left, Right, Above and Below)
Iout(2:2:2*vert,2:2:2*horz,:) = (double(I(1:vert,1:horz,:))+...
    padarray(double(I(2:vert,1:horz,:)),[1 0],'replicate','post')+...
    padarray(double(I(1:vert,2:horz,:)),[0 1],'replicate','post')+...
    padarray(double(I(2:vert,2:horz,:)),[1 1],'replicate','post'))/4;

Iout = (min(max(floor(Iout),0),2^12-1));
end