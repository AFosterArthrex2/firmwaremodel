function [Iout] = G2PixelReplace(Iin,PixelReplace)

if(~PixelReplace)
    Iout = Iin;
else
    
    Iinp = padarray(double(Iin),[1 1 0],0,'both');
    
    Iout = zeros(size(Iin));
    
    p = [7 7 7];

    counter = 0;
    for i = 2:size(Iinp,1)-1
        for j = 2:size(Iinp,2)-1
            for k = 1:3
                
                %Create Window
                win = [Iinp(i-1,j-1,k) Iinp(i-1,j,k) Iinp(i-1,j+1,k) ...
                    Iinp(i,j-1,k)   Iinp(i,j+1,k) Iinp(i+1,j-1,k) ...
                    Iinp(i+1,j,k)   Iinp(i+1,j+1,k)];
                %Sort by pixel intensity
                swin = sort(win,'descend');
                
                swin10 = floor(swin/2^2);
                
                a = floor(sum(swin(2:end))*(1/8))+floor(sum(swin(2:end))*(1/64));
                
                %round off to integer format
                a = floor(a/2^0)*2^0;
                a10 = floor(a/2^2);
                %If Current Pixel is greater than 2nd greatest
                if(floor((Iinp(i,j,k)/2^2)) <= swin10(2))
                    gain = 1;
                elseif( floor((Iinp(i,j,k)/2^2)) >= 103 + swin10(2))
                    gain = 0;
                else
                    G = floor(103 + double(swin10(2)) - floor((double(Iinp(i,j,k))/2^2)));
                    gain = floor(G/128*2^p(1))/2^p(1)+floor(G/512*2^p(2))/2^p(2);
                end
                gainT = max(0,min(1,floor(gain*2^p(3))/2^p(3)));
                Iout(i-1,j-1,k) = floor((1-gainT)*double(a))+floor((gainT)*double(Iinp(i,j,k)));
            end
        end
    end
    
    %truncate and cap as a 12 bit integer
    Iout = uint16(min(floor(Iout),2^12-1));
end
