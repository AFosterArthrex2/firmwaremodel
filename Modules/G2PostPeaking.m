%Post Peaking Function for G2 Firmware Model
%Alex Foster
%11-04-13
function [Iout] = G2PostPeaking(Iin,R_PGain,R_SGain)
%Applies Sharpening and Peaking Filters to input image Iin
%Input image is assumed to be in YCbCr format with 12 bits of precision per
%channel

I = double(Iin);

PGain = double(R_PGain)/2^8;
SGain = double(R_SGain)/2^8;

%Peaking Kernel
KP = [-1 -1 -1 -1 -1;...
     -1 -2 -2 -2 -1;...
     -1 -2 32 -2 -1;...
     -1 -2 -2 -2 -1;...
     -1 -1 -1 -1 -1];

%Sharpening Kernel
KS = [-1 -1 -1 -1 -1;...
     -1  2  2  2 -1;...
     -1  2  0  2 -1;...
     -1  2  2  2 -1;...
     -1 -1 -1 -1 -1];

%Truncate off last 2 bits (Iin has 12 bits of precision) 
It = floor(I/4);
 
KernelP = conv2(It(:,:,1),KP,'valid');
KernelS = conv2(It(:,:,1),KS,'valid');
%truncate Kernel to 8 bits
% Kernel = floor(Kernel/16)*16;

%Multiply by gain
EdgeP = (KernelP*PGain*2^8);
EdgeS = (KernelS*SGain*2^8);

EdgeP = padarray(EdgeP,[2 2],0);
EdgeS = padarray(EdgeS,[2 2],0);

Iout = zeros(size(Iin,1),size(Iin,2),3,'double');

%Add Edge Information 12bit format
Iout(:,:,1) = (I(:,:,1)*256+(EdgeP)+EdgeS)/64;

%Clamps
Iout(:,:,1) = min(Iout(:,:,1),2^12-1);
Iout(:,:,1) = max(Iout(:,:,1),0);

Iout(:,:,1) = floor(Iout(:,:,1));
Iout(:,:,2) = Iin(:,:,2);
Iout(:,:,3) = Iin(:,:,3);

Iout = int16(Iout);
end