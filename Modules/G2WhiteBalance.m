%% G2WhiteBalance
% Applies White Balance Gains to image
%%
%  [Iout] = G2WhiteBalance(Iin,settings)
%   Inputs: 
%           Iin     - Input Image
%           settings
%               HeadType
%               WB_Disable
%               WB_Gain_Red_3MOS
%               WB_Gain_Green_3MOS
%               WB_Gain_Blue_3MOS
%               WB_Gain_Red_1MOS
%               WB_Gain_Green_1MOS
%               WB_Gain_Blue_1MOS
%
%   Outputs: 
%           Iout    - Output Image
%

%   Gen2 Firmware Model - White Balance
%   Author: Alexander Foster
%   Date:   10/12/15

function [Iout] = G2WhiteBalance(Iin,varargin)

%% Settings
reqsettings = {'WB_Gain_Red_3MOS',    1.5;...
               'WB_Gain_Green_3MOS',  1;...
               'WB_Gain_Blue_3MOS_100',   3;...
               'WB_Gain_Blue_3MOS_75',   3;...
               'WB_Gain_Blue_3MOS_50',   3;...
               'WB_Gain_Red_1MOS',    1.5;...
               'WB_Gain_Green_1MOS',  1;...
               'WB_Gain_Blue_1MOS_100',   3;...
               'WB_Gain_Blue_1MOS_75',   3;...
               'WB_Gain_Blue_1MOS_50',   3;...
               'WB_Gain_Red_BB',    1.5;...
               'WB_Gain_Green_BB',  1;...
               'WB_Gain_Blue_BB_100',   3;...
               'WB_Gain_Blue_BB_75',   3;...
               'WB_Gain_Blue_BB_50',   3;...
               'WB_Gain_Red_4k1MOS',    1.5;...
               'WB_Gain_Green_4k1MOS',  1;...
               'WB_Gain_Blue_4k1MOS_100',   3;...
               'WB_Gain_Blue_4k1MOS_75',   3;...
               'WB_Gain_Blue_4k1MOS_50',   3};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

if(settings.WB_Disable)
    Iout = Iin;
    return;
end

%% Apply White Balance Gains
% gains in 3.10 format

if(settings.Head_Type == 1)
    %3MOS
    red   =   double(settings.WB_Gain_Red_3MOS);
    green = double(settings.WB_Gain_Green_3MOS);
    blue  =  double(settings.WB_Gain_Blue_3MOS_100);
elseif(settings.Head_Type == 0)
    %1MOS
    red   =   double(settings.WB_Gain_Red_1MOS);
    green = double(settings.WB_Gain_Green_1MOS);
    blue  =  double(settings.WB_Gain_Blue_1MOS_100);
elseif(settings.Head_Type == 2)
    %BroadBand
    red   =   double(settings.WB_Gain_Red_BB);
    green = double(settings.WB_Gain_Green_BB);
    blue  =  double(settings.WB_Gain_Blue_BB_100);
elseif(settings.Head_Type == 3)
    %4k1MOS
    red   =   double(settings.WB_Gain_Red_4k1MOS);
    green = double(settings.WB_Gain_Green_4k1MOS);
    blue  =  double(settings.WB_Gain_Blue_4k1MOS_100);
end
I = double(Iin);

Iout(:,:,1) = round(I(:,:,1)*round(red*1024))/1024;
% Iout(:,:,1) = (uint64(I(:,:,1))*uint64(round(red*1024)))/1024;
Iout(:,:,2) = round(I(:,:,2)*round(green*1024))/1024;
Iout(:,:,3) = round(I(:,:,3)*round(blue*1024))/1024;

%% Truncate and cap as a 12 bit integer
Iout = max(min(floor(Iout),2^12-1),0);

end