%% G2BioOpticoSpectral 
% BioOptico Spectral Overlay 
%%
%  [Iout] = G2BioOpticoSpectral(Iin,Idec,settings)
%   Inputs: 
%           Iin     - Input Image
%           Idec    - Decimated Image  (1/2)
%           settings
%               BioSpectral_C           - BioOptico Parameter
%               BioSpectral_B           - BioOptico Parameter
%               BioSpectral_Dlim        - Thickness Limits
%               BioSpectral_Imin        - Minimum Intensity
%
%   Outputs: 
%           Iout    - Output Image
%           Iover   - Thickness Overlay
%

%   Gen2 Firmware Model - BioOptico Sectral Overlay
%   Author: Alexander Foster
%   Date:   10/12/15

function [Iout, D] = G2BioOpticoSpectral(Iin,Idec,varargin)
% Uses spectral response to estimate the thickness of cartilage

%% Settings
reqsettings = {'BioSpectral_B0',    5017;...
               'BioSpectral_B1',    9369;...
               'BioSpectral_B2',    4864;...
               'BioSpectral_B3',    0;...
               'BioSpectral_Dlim', [ 64   192   320   434];...
               'BioSpectral_C',    [1 0 0 0 0 0 0];...
               'BioSpectral_Imin', 3685};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

%% Initialize
I = double(Iin);
B = [settings.BioSpectral_B0 settings.BioSpectral_B1 settings.BioSpectral_B2 settings.BioSpectral_B3];
C = settings.BioSpectral_C;
Dlim = settings.BioSpectral_Dlim;
Imin = settings.BioSpectral_Imin;

%Output Image
Iout = zeros(size(I));

%Intensity Image
Int = sum(double(I),3);

%% Thickness Calculation
% Calculate Thickness D and clamp to 0<=D<=Dlim(4)
D = B(1) - floor((B(2)*double(I(:,:,1)) + B(3)*double(I(:,:,3)))./Int) + floor((1/4096)*B(4)*double(Int));
D(D>Dlim(4)) = Dlim(4);
D(D<1) = 0;

%% Create Overlay
% Select overlay color based on thickness, d.
for i = 1:size(I,1)
    for j = 1:size(I,2)

        if(C(1) == 0)                       %Check On/Off Control Bit

        elseif(Int(i,j) < Imin)             %Intensity Error
            if(C(3))
                Iout(i,j,:) = [0 4095 0];   %If C[2] set display green
            end
        elseif(sum(Idec(i,j,:)>=4095) && C(7) == 0)  %Saturation
            if(C(6))
                Iout(i,j,:) = [2048 2048 2048];   %If C[5] set display gray
            end
        elseif(D(i,j) == 0)
            if(C(4))
                Iout(i,j,:) = [0 0 4095];   %If C[3] set display Blue.
            end
        elseif(D(i,j) < Dlim(1))
            Iout(i,j,:) = [255 0 0];       %Red if less than D[0]
        elseif(D(i,j) < Dlim(2))
            Iout(i,j,:) = [255 135 135];%Orange if less than D[1]
        elseif(D(i,j) < Dlim(3))
            Iout(i,j,:) = [255 184 184];    %Yellow if less than D[2]
        elseif(C(5))
            Iout(i,j,:) = [4095 0 4095];    %Magenta if C[4] = 1
        end
    end
    if(mod(i,floor(size(I,1)/10)) == 0)
        fprintf('.')
    end
end