%% G2GreenEdgeEnhance
% Applies Green Edge Enhancement
%%
%  [Iout] = G2GreenEdgeEnhance(Iin,settings)
%   Inputs: 
%           Iin     - Input Image
%           settings
%               Green_Edge_Disable
%               Green_Edge_Gain
%               Green_Edge_Thresh_Enable
%               Green_Edge_Threshold
%
%   Outputs: 
%           Iout    - Output Image
%

%   Gen2 Firmware Model - Green Edge Enhance
%   Author: Alexander Foster
%   Date:   10/12/15

function [Iout] = G2GreenEdgeEnhance(Iin,varargin)

%% Settings
reqsettings = {'Green_Edge_Disable',    1;...
               'Green_Edge_Gain',       1;...
               'Green_Edge_Thresh_Enable',     1;...
               'Green_Edge_Threshold',        24;...
               'Head_Type',                     1};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

%Generates Edge information from the Red and Green Channels.
%Edge information is added back to a smoothed version of each channel.

K7 = [1 2 3 3 3 2 1;...
      2 3 5 6 5 3 2;...
      3 5 7 8 7 5 3;...
      3 6 8 9 8 6 3;...
      3 5 7 8 7 5 3;...
      2 3 5 6 5 3 2;...
      1 2 3 3 3 2 1];
  
K5 = [1  4  7  4  1 ;...
      4  20 33 20 4 ;...
      7  33 55 33 7 ;...
      4  20 33 20 4 ;...
      1  4  7  4  1];

%Disabled for 1MOS Heads (Head_Type = 0)
if(~settings.Green_Edge_Disable && (settings.Head_Type ~= 0))
    
    I = double(Iin);
    CIndex = 2^4*double(settings.Green_Edge_Threshold);
    %.8
    Gain = double(settings.Green_Edge_Gain);
    
    Ia(:,:,1) = floor(conv2(I(:,:,1),K7,'same')*83468/2^18);
    Ia(:,:,2) = floor(conv2(I(:,:,2),K7,'same')*83468/2^18);
    Ia(:,:,3) = floor(conv2(I(:,:,3),K7,'same')*83468/2^18);
    
    Ire = I(:,:,1)*64-floor(conv2(I(:,:,1),K5,'same')*50686/2^18);
    Ige = I(:,:,2)*64-floor(conv2(I(:,:,2),K5,'same')*50686/2^18);
    
    Ie = floor((Ire+Ige)/2);
    
    if(settings.Green_Edge_Thresh_Enable)
        
        Is = Ie*Gain;

        Is(Is>CIndex*64) = CIndex*64;
        Is(Is<-CIndex*64) = -CIndex*64;
        If(:,:,1) = floor((Ia(:,:,1)+Is)/64);
        If(:,:,2) = floor((Ia(:,:,2)+Is)/64);
        If(:,:,3) = floor((Ia(:,:,3)+Is)/64);
    else
        If(:,:,1) = floor((Ia(:,:,1)+Ie*Gain)/64);
        If(:,:,2) = floor((Ia(:,:,2)+Ie*Gain)/64);
        If(:,:,3) = floor((Ia(:,:,3)+Ie*Gain)/64);
    end
    %Cap to 12bits
    Iout = (min(2^12-1,max(0,If)));
    
    %3 pixel border
    Iout(1:3,:,:) = Iin(1:3,:,:);
    Iout(end-2:end,:,:) = Iin(end-2:end,:,:);
    Iout(:,1:3,:) = Iin(:,1:3,:);
    Iout(:,end-2:end,:) = Iin(:,end-2:end,:);
    
else
    Iout = Iin;
end