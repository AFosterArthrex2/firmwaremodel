%% G211x11Filter
% Performs a low-pass filter, 11x11 Window
%%
%  [Iout] = G211x11Filter(Iin,filter)
%   Inputs: 
%           Iin         - Input Image
%           settings
%               Filter_11x11_Enable  -   Enable
%               Filter_11x11_Filter  -   First six of 11 coefficents
%
%   Outputs: 
%           Iout        - Output Image
%

%   Gen2 Firmware Model - Low Pass Filter
%   Author: Alexander Foster
%   Date:   10/12/15

function Iout = G211x11Filter(Iin,varargin)

%% Settings
reqsettings = {'Filter_11x11_Enable',    1;...
               'Filter_11x11_Filter',    [.1 .1 .1 .1 .1 0]};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

filter = [settings.Filter_11x11_Filter fliplr(settings.Filter_11x11_Filter(1:5))];

if(settings.Filter_11x11_Enable)
    %Filter input image with 11x11 averaging filter
    Ipad = padarray(Iin,[20 20 0],'circular');
    Ipad2 = zeros(size(Ipad));
    Ipad3 = zeros(size(Ipad));
    for k = 1:3
        for i = 1:size(Ipad,1)
            Ipad2(i,:,k) = floor(conv(double(Ipad(i,:,k)),filter,'same'));
        end
        for j = 1:size(Ipad,2)
            Ipad3(:,j,k) = floor(conv(double(Ipad2(:,j,k)),filter,'same'));
        end
    end
    Iout = (Ipad3(21:end-20,21:end-20,:));
else
    Iout = Iin;
end