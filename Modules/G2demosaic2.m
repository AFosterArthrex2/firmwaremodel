function [Iout] = G2demosaic2(Iin,enable)
%Demosaic Filter for G2 1MOS Head

if(enable)
    Iin = double(Iin);
    [x, y] = meshgrid(1:size(Iin,2),1:size(Iin,1));
    
    %Assumed pixel grid:   (1,1) (2,1)  -  G    B  -  One    Two
    %                      (1,2) (2,2)  -  R    G  -  Three  Four
    
    Three = (mod(x,2) == 1) & (mod(y,2) == 1);
    Four =  (mod(x,2) == 0) & (mod(y,2) == 1);
    One =   (mod(x,2) == 1) & (mod(y,2) == 0);
    Two =   (mod(x,2) == 0) & (mod(y,2) == 0);
    
    % G @ R and G @ B
    G1 = One | Four;
    
    % R @ G, B col R row
    G2r = One;
    
    % B @ G, R col B row
    G2b = Four;
    
    % R @ G, B row R col
    G3r = Four;
    
    % B @ G, R row B col
    G3b = One;
    
    % R @ B
    G4r = Three;
    
    % B @ R
    G4b = Two;
    
    % R @ R
    G5 = Two;
    
    % G @ G
    G6 = ~G1;
    
    % B @ B
    G7 = Three;  
    
    Kg1 = [0 0 -2 0 0; 0 0 4 0 0; -2 4 8 4 -2; 0 0 4 0 0; 0 0 -2 0 0];
    
    Kg2 = [0 0 1 0 0; 0 -2 0 -2 0; -2 8 10 8 -2; 0 -2 0 -2 0; 0 0 1 0 0];
    
    Kg3 = Kg2';
    
    Kg4 = [0 0 -3 0 0; 0 4 0 4 0; -3 0 12 0 -3; 0 4 0 4 0; 0 0 -3 0 0];
    
    %Run all four kernals
    Out1 = conv2(Iin,Kg1,'same');
    Out2 = conv2(Iin,Kg2,'same');
    Out3 = conv2(Iin,Kg3,'same');
    Out4 = conv2(Iin,Kg4,'same');
    
    %Build Channels by masking output of four kernals and summing.
    Red = Out2.*G2r + Out3.*G3r + Out4.*G4r + 16*Iin.*G5; 
    Green = Out1.*G1 + 16*Iin.*G6;
    Blue = Out2.*G2b + Out3.*G3b + Out4.*G4b + 16*Iin.*G7;
    
    Iout = max(min(cat(3,Red,Green,Blue)/16,2^12),0);
else
    Iout = Iin;
end
