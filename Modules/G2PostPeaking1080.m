%% G2PostPeaking1080     
% Post Peaking Function for G2 Firmware Model
%%
%  [Iout] = G2PostPeaking1080(Iin,settings)
%   Inputs: 
%           Iin     - Input Image
%           settings
%               PostPeaking_PGain
%               PostPeaking_SGain
%               
%   Outputs: 
%           Iout    - Output Image
%

%   Gen2 Firmware Model - PostPeaking
%   Author: Alexander Foster
%   Date:   10/12/15

function [Iout] = G2PostPeaking1080(Iin,varargin)
%Applies Sharpening and Peaking Filters to input image Iin
%Input image is assumed to be in YCbCr format with 12 bits of precision per
%channel

%% Settings
reqsettings = {'PostPeaking_PGain',     1;...
               'PostPeaking_SGain',     1};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

I = double(Iin);

PGain = double(settings.PostPeaking_PGain)/2^8;
SGain = double(settings.PostPeaking_SGain)/2^8;

%Peaking Kernel
KP = [-1 -1 -1 -1 -1;...
     -1 -2 -2 -2 -1;...
     -1 -2 32 -2 -1;...
     -1 -2 -2 -2 -1;...
     -1 -1 -1 -1 -1];

%Sharpening Kernel
KS = [-1 -1 -1 -1 -1;...
     -1  2  2  2 -1;...
     -1  2  0  2 -1;...
     -1  2  2  2 -1;...
     -1 -1 -1 -1 -1];

%Truncate off last 2 bits (Iin has 12 bits of precision) 
It = floor(I/4);
 
KernelP = conv2(It(:,:,1),KP,'valid');
KernelS = conv2(It(:,:,1),KS,'valid');
%truncate Kernel to 8 bits
% Kernel = floor(Kernel/16)*16;

%Multiply by gain
EdgeP = (KernelP*PGain*2^8);
EdgeS = (KernelS*SGain*2^8);

EdgeP = padarray(EdgeP,[2 2],0);
EdgeS = padarray(EdgeS,[2 2],0);

Iout = zeros(size(Iin,1),size(Iin,2),3,'double');

%Add Edge Information 12bit format
Iout(:,:,1) = (I(:,:,1)*256+(EdgeP)+EdgeS)/64;

%Clamps
Iout(:,:,1) = min(Iout(:,:,1),2^12-1);
Iout(:,:,1) = max(Iout(:,:,1),0);

Iout(:,:,1) = floor(Iout(:,:,1));
Iout(:,:,2) = Iin(:,:,2);
Iout(:,:,3) = Iin(:,:,3);

% %Pass 2 pix border
% Iout(1:2,:,:) = Iin(1:2,:,:);
% Iout(end-1:end,:,:) = Iin(end-1:end,:,:);
% Iout(:,1:2,:) = Iin(:,1:2,:);
% Iout(:,end-1:end,:) = Iin(:,end-1:end,:);

Iout = (Iout);
end