%% G2RGB2YCbCr1080     
% Converts RGB Color Space Image to yCbCr Color Space
%%
%  [Iout] = G2RGB2YCbCr1080(Iin,settings)
%   Inputs: 
%           Iin     - Input Image
%           settings
%               RGB2YCbCr1080_Enable
%               
%   Outputs: 
%           Iout    - Output Image
%

%   Gen2 Firmware Model - RGB to YCrCb Conversion
%   Author: Alexander Foster
%   Date:   10/12/15

function Iout = G2RGB2YCbCr1080(Iin,varargin)

%% Settings
reqsettings = {'RGB2YCbCr1080_Enable',    1};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

if(settings.RGB2YCbCr1080_Enable)
    I = double(Iin);
    
    Y = I(:,:,1) + 2*I(:,:,2) + I(:,:,3);
    Cb = -1*I(:,:,1) - 2*I(:,:,2) + 3*I(:,:,3);
    Cr = 3*I(:,:,1) - 2*I(:,:,2) - 1*I(:,:,3);
    
    Iout = cat(3,Y,Cb,Cr);
else
    Iout = Iin;
end