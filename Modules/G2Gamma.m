%% G2Gamma
% Applies gamma correction 
%%
%  [Iout] = G2Gamma(Iin,settings)
%   Inputs: 
%           Iin     - Input Image (12 bits)
%           settings
%               Gamma_Disable
%               Gamma_Index
%
%   Outputs: 
%           Iout    - Output Image (8 bits)
%

%   Gen2 Firmware Model - Gamma
%   Author: Alexander Foster
%   Date:   10/19/15

function [Iout] = G2Gamma(Iin,varargin)

%% Settings
reqsettings = {'Gamma_Disable',  1;...
               'Gamma_Index',    1};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

%% Apply Gamma
if(~settings.Gamma_Disable)
    % Imports Data from 10to10Gamma.xlsx
    
    [a] = importdata('10to10Gamma.csv');
    % convert to decimal and reshape into a 8x1024 array
    
%     GammaTable = reshape(hex2dec(Q{1}),1024,8)';
    GammaTable = a';
    
    %Truncate to 10 bits
    I = floor(double(Iin)/4);
%     I = double(Iin);

    reorder = [ 4 4  1 4];

    Iout = max(min(floor(reshape(GammaTable(reorder(settings.Gamma_Index+1),min(1024,I+1)),size(I,1),size(I,2),size(I,3))/4),2^8),0);
else
    Iout = (floor(double(Iin)/16));
end