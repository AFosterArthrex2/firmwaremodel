%% G2RGB2YCbCr     
% Converts RGB Color Space Image to yCbCr Color Space
%%
%  [Iout] = G2RGB2YCbCr(Iin,settings)
%   Inputs: 
%           Iin     - Input Image
%           settings
%               RGB2YCbCr_Enable
%               
%   Outputs: 
%           Iout    - Output Image
%

%   Gen2 Firmware Model - RGB to YCrCb Conversion
%   Author: Alexander Foster
%   Date:   10/12/15

function Iout = G2RGB2YCbCr(Iin,varargin)

%% Settings
reqsettings = {'RGB2YCbCr_Enable',    1};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

if(settings.RGB2YCbCr_Enable)
    I = double(Iin);
    
    Y = I(:,:,1) + 2*I(:,:,2) + I(:,:,3);
    Cb = -1*I(:,:,1) - 2*I(:,:,2) + 3*I(:,:,3);
    Cr = 3*I(:,:,1) - 2*I(:,:,2) - 1*I(:,:,3);
    
    Iout(:,:,1) = min(max(fix(Y),0),2^14-1);
    Iout(:,:,2) = min(max(fix(Cb),-2^14+1),2^14-1);
    Iout(:,:,3) = min(max(fix(Cr),-2^14+1),2^14-1);
    
end