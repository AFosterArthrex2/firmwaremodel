%% G2Gamma1080
% Applies gamma correction
%%
%  [Iout] = G2Gamma1080(Iin,settings)
%   Inputs: 
%           Iin     - Input Image (12 bits)
%           settings
%               Gamma_Disable
%               Gamma_Index
%
%   Outputs: 
%           Iout    - Output Image (8 bits)
%

%   Gen2 Firmware Model - Gamma 1080
%   Author: Alexander Foster
%   Date:   10/19/15

function [Iout] = G2Gamma1080(Iin,varargin)

%% Settings
reqsettings = {'Gamma_Disable',  1;...
               'Gamma_Index',    1};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

%% Apply Gamma

Iout = (floor(double(Iin)/16));
