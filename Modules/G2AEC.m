%% G2AEC
% Applies AEC gain to image
%%
%  [Iout] = G2AEC(Iin,settings)
%   Inputs: 
%           Iin     - Input Image
%           settings
%               AEC_Enable
%               AEC_Gain
%
%   Outputs: 
%           Iout    - Output Image
%

%   Gen2 Firmware Model - AEC Gain
%   Author: Alexander Foster
%   Date:   10/12/15

function [Iout] = G2AEC(Iin,varargin)

%% Settings
reqsettings = {'AEC_Disable',   1;...
               'AEC_Gain',      2};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

%% Apply AEC Gain
if(settings.AEC_Disable)
    Iout = Iin;
    return
end

gain = double(settings.AEC_Gain);

Iout = double(Iin)*gain;

%truncate and cap as a 12 bit integer
Iout = max(min(floor(Iout),2^12-1),0);
