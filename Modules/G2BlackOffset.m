%% G2BlackOffset
% Applies Black Offset to image
%%
%  [Iout] = G2BlackOffset(Iin,settings)
%   Inputs: 
%           Iin     - Input Image
%           settings
%               BlackOffest_Enable
%               BlackOffset_RedLevel
%               BlackOffset_GreenLevel
%               BlackOffset_BlueLevel
%
%   Outputs: 
%           Iout    - Output Image
%

%   Gen2 Firmware Model - Black Offset
%   Author: Alexander Foster
%   Date:   10/12/15

function [Iout] = G2BlackOffset(Iin,varargin)

%% Settings
reqsettings = {'BlackOffset_Disable',    0;...
               'BlackOffset_RedLevel',  100;...
               'BlackOffset_GreenLevel',100;...
               'BlackOffset_BlueLevel', 100};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

%% Apply Black Level
if(~settings.BlackOffset_Disable)
    Iout(:,:,1) = double(Iin(:,:,1))-double(settings.BlackOffset_RedLevel);
    Iout(:,:,2) = double(Iin(:,:,2))-double(settings.BlackOffset_GreenLevel);
    Iout(:,:,3) = double(Iin(:,:,3))-double(settings.BlackOffset_BlueLevel);
    
    %Not active on columns 1-54
    Iout(:,1:54,:) = Iin(:,1:54,:);
    
    %truncate and cap as a 12 bit integer
    Iout = max(min(floor(Iout),2^12-1),0);
else
    Iout = Iin;
end

