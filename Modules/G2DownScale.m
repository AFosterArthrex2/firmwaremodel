function Iout = G2DownScale(Iin)

for i = 1:size(Iin,3)
    Iout(:,:,i) = (Iin(1:2:end,1:2:end,i));
end
% for i = 1:size(Iin,3)
%     Iout(:,:,i) = uint8(Iin(2:2:end,2:2:end,i));
% end