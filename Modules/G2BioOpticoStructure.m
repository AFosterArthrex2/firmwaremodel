%% G2BioOpticoSpectral 
% BioOptico Spectral Overlay 
%%
%  [Iout] = G2BioOpticoSpectral(Iin,Idec,settings)
%   Inputs: 
%           Iin     - Input Image
%           settings
%               BioStructure_Mthresh    - Lower mean threshold
%               BioStructure_Vthresh    - Upper variance threshold
%               BioStructure_Precision  - Percision Vector
%                     Precision, specifies the precision in bits to truncate to. 12 element
%                           vector, each element refers to a location.
%                      1 - Horizontal, Mean of Intensity Signal before getting squared.
%                      2 - Horizontal, Mean of Squared Intensity signal.
%                      3 - Horizontal, Mean Intensity Squared signal.
%                      4 - Horizontal, Variance (Horizontal) signal.
%                      5 -   Vertical, Horiz. Means Input
%                      6 -   Vertical, Horiz. Variances Input
%                      7 -   Vertical, Mc - mean of Horiz. Means
%                      8 -   Vertical, Horiz. Means minus Average (of Horiz. Means)
%                      9 -   Vertical, Horiz. Means minus Average Squared
%                     10 -   Vertical, Variance before threshold
%                     11 -   Vertical, Square Root of Variance
%                     12 -   Vertical, Threshold Image (Mc * Threshold)
%
%   Outputs: 
%           Iout    - Output Image
%           Iover   - Thickness Overlay
%

%   Gen2 Firmware Model - BioOptico Sectral Overlay
%   Author: Alexander Foster
%   Date:   10/12/15

function [Iover, Ialpha, varianceCombined, meanCombined, Int, ...
    varianceMid, meanMid] = G2BioOpticoStructure(Iin,P,varargin)

%% Settings
reqsettings = {'BioStructure_Mthresh',          64;...
               'BioStructure_Vthresh',          960};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

fid = fopen('square_root_130301.txt');
C = textscan(fid,'%s\r\n');
fclose(fid);
D = hex2dec(C{1});

I = floor(double(Iin)/16);

Iover =             zeros([size(I,1) size(I,2)],'uint16');
meanH =             zeros([size(I,1) size(I,2)],'double');      %Mean (Horizontal)
varianceH =         zeros([size(I,1) size(I,2)],'double');  %Variance (Horizontal)
meanCombined =      zeros([size(I,1) size(I,2)],'double');      %Mean (Final)
varianceCombined =  zeros([size(I,1) size(I,2)],'double');  %Variance
sqrtvarianceClamped =   zeros([size(I,1) size(I,2)],'double');  %Variance

%Intensity = 76*R 151*G 29*B;
Int = (double(I(:,:,1))*76 + double(I(:,:,2))*151 + double(I(:,:,3))*29)/64;
Int = floor(Int);

%%%%%%%%%%%%%%%%%HORIZONTAL PASS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%First and last two columns are handled as special cases (zero padded)
    meanH(:,1) = floor(sum(double(Int(:,1:3)),2)*3355443/16777216 *2^P(1))/2^P(1);
    meanH(:,2) = floor(sum(double(Int(:,1:4)),2)*3355443/16777216 *2^P(1))/2^P(1);
    meanH(:,end-1) = floor(sum(double(Int(:,end-3:end)),2)*3355443/16777216 *2^P(1))/2^P(1);
    meanH(:,end) = floor(sum(double(Int(:,end-2:end:4)),2)*3355443/16777216 *2^P(1))/2^P(1);
    
    for j = 3:size(Int,2)-2      %every column except first and last 2
        current = Int(:,j+[-2:2]);
        meanCur = floor(sum(double(current),2)*3355443/16777216 *2^P(1))/2^P(1);    % #1 10.12
        meanH(:,j) = meanCur;
        if(mod(j,floor(size(Int,2)/10)) == 0)
            fprintf('.')
        end
    end
    
    %First and last two columns are handled as special cases (zero padded)
    varianceH(:,1) = floor((floor((sum((double(Int(:,1:3)).^2),2))*3355443/16777216 *2^P(2))/2^P(2) - floor((double(meanH(:,1)).^2)*2^P(3))/2^P(3))*2^P(4))/2^P(4);
    varianceH(:,2) = floor((floor((sum((double(Int(:,1:4)).^2),2))*3355443/16777216 *2^P(2))/2^P(2) - floor((double(meanH(:,2)).^2)*2^P(3))/2^P(3))*2^P(4))/2^P(4);
    varianceH(:,end-1) = floor((floor((sum((double(Int(:,end-3:end)).^2),2))*3355443/16777216 *2^P(2))/2^P(2) - floor((double(meanH(:,end-1)).^2)*2^P(3))/2^P(3))*2^P(4))/2^P(4);
    varianceH(:,end) = floor((floor((sum((double(Int(:,end-2:end)).^2),2))*3355443/16777216 *2^P(2))/2^P(2) - floor((double(meanH(:,end)).^2)*2^P(3))/2^P(3))*2^P(4))/2^P(4);
    
    for j = 3:size(Int,2)-2      %every column except first and last 2
        current = Int(:,j+[-2:2]); 
        meanCur = double(meanH(:,j));
        sumofsquares = sum((double(current).^2),2);
        meanofsquares = floor((sumofsquares)*3355443/16777216 *2^P(2))/2^P(2);      % #2 20.8
        squareofmeans = floor((meanCur.^2)*2^P(3))/2^P(3);                          % #3 20.8 
        varCur = floor((meanofsquares - squareofmeans)*2^P(4))/2^P(4);              % #4 20.4
        varianceH(:,j) = varCur;
        if(mod(j,floor(size(Int,2)/10)) == 0)
            fprintf('.')
        end
    end
    
varianceMid = varianceH;
meanMid = meanH;
%%%%%%%%%%%%%%%%%VERTICAL PASS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %First and last two rows are handled as special cases (zero padded)
    meanCombined(1,:) = floor(sum(floor(meanH(1:3,:)*2^P(5))/2^P(5),1)*3355443/16777216*2^P(7))/2^P(7);
    meanCombined(2,:) = floor(sum(floor(meanH(1:4,:)*2^P(5))/2^P(5),1)*3355443/16777216*2^P(7))/2^P(7);
    meanCombined(end-1,:) = floor(sum(floor(meanH(end-3:end,:)*2^P(5))/2^P(5),1)*3355443/16777216*2^P(7))/2^P(7);
    meanCombined(end,:) = floor(sum(floor(meanH(end-2:end,:)*2^P(5))/2^P(5),1)*3355443/16777216*2^P(7))/2^P(7);
    
    %First and last two rows are handled as special cases (zero padded)
    varianceCombined(1,:) = floor((sum(floor(varianceH(1:3,:)*2^P(6))/2^P(6)) + sum(floor((floor((double(floor(meanH(1:3,:)*2^P(5))/2^P(5))-ones(3,1)*floor(sum(floor(meanH(1:3,:)*2^P(5))/2^P(5),1)*3355443/16777216*2^P(7))/2^P(7))*2^P(8))/2^P(8).^2)*2^P(9))/2^P(9),1))*3355443/16777216*2^P(10))/2^P(10);
    varianceCombined(2,:) = floor((sum(floor(varianceH(1:4,:)*2^P(6))/2^P(6)) + sum(floor((floor((double(floor(meanH(1:4,:)*2^P(5))/2^P(5))-ones(4,1)*floor(sum(floor(meanH(1:4,:)*2^P(5))/2^P(5),1)*3355443/16777216*2^P(7))/2^P(7))*2^P(8))/2^P(8).^2)*2^P(9))/2^P(9),1))*3355443/16777216*2^P(10))/2^P(10);
    varianceCombined(end-1,:) = floor((sum(floor(varianceH(end-3:end,:)*2^P(6))/2^P(6)) + sum(floor((floor((double(floor(meanH(end-3:end,:)*2^P(5))/2^P(5))-ones(4,1)*floor(sum(floor(meanH(end-3:end,:)*2^P(5))/2^P(5),1)*3355443/16777216*2^P(7))/2^P(7))*2^P(8))/2^P(8).^2)*2^P(9))/2^P(9),1))*3355443/16777216*2^P(10))/2^P(10);
    varianceCombined(end,:) = floor((sum(floor(varianceH(end-2:end,:)*2^P(6))/2^P(6)) + sum(floor((floor((double(floor(meanH(end-2:end,:)*2^P(5))/2^P(5))-ones(3,1)*floor(sum(floor(meanH(end-2:end,:)*2^P(5))/2^P(5),1)*3355443/16777216*2^P(7))/2^P(7))*2^P(8))/2^P(8).^2)*2^P(9))/2^P(9),1))*3355443/16777216*2^P(10))/2^P(10);
    for i = 3:size(Int,1)-2      %every row except first and last 2
        currentM = floor(meanH(i+[-2:2],:)*2^P(5))/2^P(5);                          % #5 10.8
        currentV = floor(varianceH(i+[-2:2],:)*2^P(6))/2^P(6);                      % #6 20.4
        Mc = floor(sum(currentM,1)*3355443/16777216*2^P(7))/2^P(7);                 % #7 10.8
        sumOfVariances = sum(currentV);                                  
        meanMinusAvg = floor((double(currentM)-ones(5,1)*Mc)*2^P(8))/2^P(8);        % #8 10.8
        meanMinusAvgSqrd = floor((meanMinusAvg.^2)*2^P(9))/2^P(9);                  % #9 20.4
        S2c = floor((sumOfVariances + sum(meanMinusAvgSqrd,1))...
                    *3355443/16777216*2^P(10))/2^P(10);                             %#10 20.4
        meanCombined(i,:) = Mc;
        varianceCombined(i,:) = S2c;
        if(mod(i,floor(size(Int,1)/10)) == 0)
            fprintf('.')
        end
    end
% end

%First clamp to 8.4
varianceCombined2 = varianceCombined;
varianceCombined2(varianceCombined >=256) = hex2dec('FFF')/16;
varianceCombined2(varianceCombined2 < 0) = 0;

sqrtVariance = floor(D(varianceCombined2*16+1)/256*2^P(11))/2^P(11);
ThresholdImage = floor(double(settings.BioStructure_Vthresh)/2^12*meanCombined*2^P(12))/2^P(12);
% ThresholdImage = min(16,floor(Vthresh/2^12*meanCombined*2^P(12))/2^P(12));

%Clamp Variance
for i = 1:size(sqrtVariance,1)
    for j = 1:size(sqrtVariance,2)
        if(sqrtVariance(i,j) >= min(16,ThresholdImage(i,j)))
%         if(sqrtVariance(i,j) >= ThresholdImage(i,j))
            sqrtvarianceClamped(i,j) = ThresholdImage(i,j);
        else
            sqrtvarianceClamped(i,j) = sqrtVariance(i,j);
        end
    end
end
sqrtvarianceClamped(sqrtvarianceClamped<0) = 0;

Iover = floor((double(sqrtvarianceClamped)*255)./ThresholdImage);

Ialpha = zeros(size(meanCombined));
Ialpha(meanCombined >= settings.BioStructure_Mthresh/4) = 1;
Imask = ones(size(Ialpha));
Imask(12:end-11,12:end-11,:) = 0;
Ialpha(logical(Imask)) = 0;

%Reserve 0 value for live video
Iover(Iover == 0) = 1;

Iover(Ialpha == 0) = 0;

meanMid = meanMid*256;
varianceMid = varianceMid*16;
meanCombined = meanCombined*256;
varianceCombined = varianceCombined*16;

