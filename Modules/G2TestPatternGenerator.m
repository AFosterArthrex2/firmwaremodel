%% G2TestPatternGenerator     
%Test Pattern Generator for G2 Firmware Model
%%
%  [Iout] = G2TestPatternGenerator(settings)
%   Inputs: 
%           settings
%               TPG_Mode                - Test Pattern (Motherboard)
%               TPG_Head                - Test Pattern (Head)
%               Seed_SIM                - Test Pattern Seed
%               TPG_8bitMode_Enable     - 8 bit Mode for Ramps
%               
%   Outputs: 
%           Iout    - Output Image
%

%   Gen2 Firmware Model - AEC Gain
%   Author: Alexander Foster
%   Date:   10/12/15

function [Iout] = G2TestPatternGenerator(imgSize,varargin)

%% Settings
reqsettings = {'TPG_Mode',              1;...
               'TPG_Head',              0;...
               'Seed_SIM',              '9D36CAF15';...
               'TPG_8bitMode_Enable',   1};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

Iout = zeros(imgSize(1),imgSize(2),3,'double');
seeds = cell(imgSize(1),imgSize(2));
if(settings.TPG_Mode == 4)
    settings.TPG_Mode = 3;
end
if(settings.TPG_Head == 4)
    settings.TPG_Head = 3;
end

switch settings.TPG_Mode
    case 1
        %Horizontal Ramp Generator
        ramp = 0:2^12-1;
        if(settings.TPG_8bitMode_Enable)
            ramp = mod(ramp,256)*16;
        end
        
%         Iout(:,:,1) = min(4078,ones(imgSize(1),1)*ramp(1:imgSize(2)));
        Iout(:,:,1) = ones(imgSize(1),1)*ramp(1:imgSize(2));
        Iout(:,:,2) = Iout(:,:,1);
        Iout(:,:,3) = Iout(:,:,1);
        
    case 2
        %Vertical Ramp Generator
        ramp = 0:2^12-1;        
        if(settings.TPG_8bitMode_Enable)
            ramp = mod(ramp,256)*16;
        end
        
        for i = 1:imgSize(1)
            Iout(i,:,1) = ramp(i:(i+imgSize(2)-1));
        end
        
        Iout(:,:,2) = Iout(:,:,1);
        Iout(:,:,3) = Iout(:,:,1);
        
    case 3
        %Psuedo Random
        cSeed = settings.Seed_SIM;
        bSeed = dec2bin(hex2dec(cSeed),36);
        if(~exist(['PrandomImages\' num2str(imgSize(1)) num2str(imgSize(2)) '\'],'dir'))
            mkdir(['PrandomImages\' num2str(imgSize(1)) num2str(imgSize(2)) '\']);
        end
        if(exist(['PrandomImages\' num2str(imgSize(1)) num2str(imgSize(2)) '\' cSeed '.png'],'file'))
            Iout = imread(['PrandomImages\' num2str(imgSize(1)) num2str(imgSize(2)) '\' cSeed '.png']);
        else
            
            for i = 1:length(bSeed)
                Seed(i) = logical(str2num(bSeed(i)));
            end
            
            I = zeros(imgSize(1),imgSize(2),3);
            
            Q = fliplr(Seed);
            
            for i = 1:imgSize(1)
                for j = 1:imgSize(2)
                    seeds(i,j) = {char(double(squeeze(Q))+48)};
                    temp = xor(Q(36),Q(25));
                    r = sum(double(Q(25:36)).*(2.^(0:11)));
                    g = sum(double(Q(13:24)).*(2.^(0:11)));
                    b = sum(double(Q(1:12)).*(2.^(0:11)));
                    I(i,j,:) = [r g b];
                    Q = [temp Q(1:end-1)];
                end
            end
            
            Iout = (I);
            imwrite(uint16(Iout),['PrandomImages\' num2str(imgSize(1)) num2str(imgSize(2)) '\' cSeed '.png']);
        end
    case 0
        switch settings.TPG_Head
            case 1
                %Horizontal Ramp Generator
                ramp = 0:2^12-1; 
                
                Iout(:,:,1) = ones(imgSize(1),1)*ramp(1:imgSize(2));
                Iout(:,:,2) = Iout(:,:,1);
                Iout(:,:,3) = Iout(:,:,1);
                
            case 2
                %Vertical Ramp Generator
                ramp = 0:2^12-1;
                
                for i = 1:imgSize(1)
                    Iout(i,:,1) = ramp(i:(i+imgSize(2)-1));
                end
                
                Iout(:,:,2) = Iout(:,:,1);
                Iout(:,:,3) = Iout(:,:,1);
                
            case 3
                %Psuedo Random
                cSeed = 'A7D9350EA';
                bSeed = dec2bin(hex2dec(cSeed),36);
                if(~exist(['PrandomImages\' num2str(imgSize(1)) num2str(imgSize(2)) '\'],'dir'))
                    mkdir(['PrandomImages\' num2str(imgSize(1)) num2str(imgSize(2)) '\']);
                end
                if(exist(['PrandomImages\' num2str(imgSize(1)) num2str(imgSize(2)) '\' cSeed '.png'],'file'))
                    Iout = imread(['PrandomImages\' num2str(imgSize(1)) num2str(imgSize(2)) '\' cSeed '.png']);
                else
                    
                    for i = 1:length(bSeed)
                        Seed(i) = logical(str2num(bSeed(i)));
                    end
                    
                    I = zeros(imgSize(1),imgSize(2),3);
                    
                    Q = fliplr(Seed);
                    
                    for i = 1:imgSize(1)
                        for j = 1:imgSize(2)
                            seeds(i,j) = {char(double(squeeze(Q))+48)};
                            temp = xor(Q(36),Q(25));
                            r = sum(double(Q(25:36)).*(2.^(0:11)));
                            g = sum(double(Q(13:24)).*(2.^(0:11)));
                            b = sum(double(Q(1:12)).*(2.^(0:11)));
                            I(i,j,:) = [r g b];
                            Q = [temp Q(1:end-1)];
                        end
                    end
                    
                    Iout = (I);
                    imwrite(uint16(Iout),['PrandomImages\' num2str(imgSize(1)) num2str(imgSize(2)) '\' cSeed '.png']);
                end
            otherwise
        end
    otherwise
        
end

