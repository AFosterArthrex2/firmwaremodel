%% G2BioOpticoLowPass
% Performs a low-pass filter, 11x11 Window
%%
function Iout = G2BioOpticoLowPass(Iin,varargin)
%  [Iout] = G2BioOpticoLowPass(Iin)
%   Inputs: 
%           Iin         - Input Image
%   Outputs: 
%           Iout        - Output Image
%

%   Gen2 Firmware Model - BioOptico Low Pass Filter
%   Author: Alexander Foster
%   Date:   10/12/15

settings = struct('Filter_11x11_Enable',1,'Filter_11x11_Filter',ones(1,6)*5957/2^16);

Iout = G211x11Filter(Iin,settings);