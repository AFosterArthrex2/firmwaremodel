%% G2VerticalBinning
% Averages nearby rows
%%
%  [Iout] = G2VerticalBinning(Iin,settings)
%   Inputs: 
%           Iin     - Input Image
%           settings
%               Vertical_Binning_Disable
%
%   Outputs: 
%           Iout    - Output Image
%

%   Gen2 Firmware Model - Vertical Binning
%   Author: Alexander Foster
%   Date:   10/19/15

function Iout = G2VerticalBinning(Iin,varargin)

%% Settings
reqsettings = {'Vertical_Binning_Disable',    1};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

if(~settings.Vertical_Binning_Disable && settings.Head_Type)
    Iout = zeros(size(Iin));

    Iout(1,:,:) = Iin(1,:,:);
    
    for i = 1:3
        for j = 2:size(Iin,1)
            Iout(j,:,i) = floor(mean(Iin(j-1:j,:,i),1));
        end
    end
    
    Iout = (Iout);
else
    Iout = Iin;
end