%% G2ScalerNN
% Scales the input image by 2 using nearest neighbor method
%%
%  [Iout] = G2ScalerNN(Iin)
%   Inputs: 
%           Iin     - Input Image
%
%   Outputs: 
%           Iout    - Output Image
%

%   Gen2 Firmware Model - Scaler NN
%   Author: Alexander Foster
%   Date:   10/19/15

function [Iout] = G2ScalerNN(Iin,varargin)
%Nearest Neighbor 2x Upscaling

[vert, horz, ~] = size(Iin);

I = double(Iin);

Iout = zeros(2*size(I,1),2*size(I,2),size(I,3));

%Odd Row Odd Column
Iout(1:2:2*vert,1:2:2*horz,:) = I(1:vert,1:horz,:);

%Even Row Odd Column
Iout(2:2:2*vert,1:2:2*horz,:) = I(1:vert,1:horz,:);

%Odd Row Even Column
Iout(1:2:2*vert,2:2:2*horz,:) = I(1:vert,1:horz,:);

%Odd Row Odd Column
Iout(2:2:2*vert,2:2:2*horz,:) = I(1:vert,1:horz,:);

Iout = (min(max(floor(Iout),0),2^12-1));
end