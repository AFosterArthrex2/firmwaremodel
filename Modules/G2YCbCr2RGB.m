%% G2YCbCr2RGB1080     
% Converts yCbCr Color Space Image to RGB Color Space
%%
%  [Iout] = G2YCbCr2RGB1080(Iin,settings)
%   Inputs: 
%           Iin     - Input Image
%           settings
%               YCbCr2RGB_Enable
%               
%   Outputs: 
%           Iout    - Output Image
%

%   Gen2 Firmware Model - YCbCr2RGB
%   Author: Alexander Foster
%   Date:   10/12/15

function [Iout] = G2YCbCr2RGB(Iin,varargin)

%% Settings
reqsettings = {'YCbCr2RGB1080_Enable',    1};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

if(settings.YCbCr2RGB1080_Enable)
I = (double(Iin));

R = 1*I(:,:,1) + 0*I(:,:,2) + 4*I(:,:,3);
G = 1*I(:,:,1) - 2*I(:,:,2) - 2*I(:,:,3);
B = 1*I(:,:,1) + 4*I(:,:,2) + 0*I(:,:,3);

Iout(:,:,1) = min(max(floor(R/16),0),2^12-1);
Iout(:,:,2) = min(max(floor(G/16),0),2^12-1);
Iout(:,:,3) = min(max(floor(B/16),0),2^12-1);

%truncate and cap as a 12 bit integer
Iout = (Iout);
else
    Iout = Iin;
end