%% G2BilateralSlope   
% Performs Bilateral Filtering on slopes, 3x3 Window
%%
%  [Iout] = G2Bilateral(Iin,settings)
%   Inputs: 
%           Iin         - Input Image
%           settings
%               Bilateral_Slope_Disable        - Select Off/Horz/Vert/On
%               Bilateral_Slope_Thresh    - Gradient Threshold
%               Bilateral_Slope_Bit_Select     - Register 44 bits [27:26] 
%                                         0 = [11:4], 1 = [10:3], 2 = [9:2]
%
%   Outputs: 
%           Iout        - Output Image
%

%   Gen2 Firmware Model - Bilateral Slope Filter
%   Author: Alexander Foster
%   Date:   10/12/15


function [Iout] = G2BilateralSlope(Iin,varargin)

%% Settings
reqsettings = {'Bilateral_Slope_Disable',         1;...
               'Bilateral_Slope_Thresh',    20;...
               'Bilateral_Slope_Bit_Select',      1};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end


if(settings.Bilateral_Slope_Disable == 1)
    C = 0;
else
    C = 3;
end

%% Initialize
thresh = double(settings.Bilateral_Slope_Thresh);
BitSelect = settings.Bilateral_Slope_Bit_Select;

Iin = double(Iin);

Iout = zeros(size(Iin));

LUT = [128 127 125 121 116 110 102 95 86 78 69 61 53 45 38 32 26 22 17 14 11 8 6 5 4 3 2 1 1 1 0 0];
LUT = [LUT zeros(1,5000)];

for j = 1:3         %Three Color Channels
    
    I = zeros(size(Iin(:,:,j)));
    
    %Copy outer rows
    I(1:3,:) = Iin(1:3,:,j);
    I(end-2:end,:) = Iin(end-2:end,:,j);
    I(:,1:3) = Iin(:,1:3,j);
    I(:,end-2:end) = Iin(:,end-2:end,j);

%% Filter
% Perform Bilateral Filtering
    switch C
        case 0      %No Filter. Pass image
                I = Iin(:,:,j);
        case 3      %Both
            Itmp = Iin(:,:,j);
            for i = 4:size(Iin,2)-3
                %Window minus Center
                diffH = abs(Itmp(:,i+[-1,1]) - Itmp(:,i)*ones(1,2));
                diffH2 = abs(diffH(:,1)-diffH(:,2))/(2^(5-double(BitSelect)));
                %Check for gradiant
                enable = ((max(0,Itmp(:,i-1)-thresh) >= Itmp(:,i))...
                      &(min(4095,Itmp(:,i+1)+thresh) <= Itmp(:,i))...
                        |((max(0,Itmp(:,i+1)-thresh) >= Itmp(:,i))...
                      &(min(4095,Itmp(:,i-1)+thresh) <= Itmp(:,i))));

                %Apply Look up Table
                weightsH = (enable.*LUT((floor(diffH2))+1)')*ones(1,3);
                weightsH(:,2) = 128;
                %Multiply Weights by Window. Divide by sum of Weights.
                I(:,i) = floor(sum(weightsH.*Itmp(:,i+[-1:1]),2)./sum(weightsH,2));
            end
            
            Itmp2 = permute(I(:,:),[2 1]);
            for i = 4:size(Iin,1)-3
                %Window minus Center
                diffH = abs(Itmp2(:,i+[-1,1]) - Itmp2(:,i)*ones(1,2));
                diffH2 = abs(diffH(:,1)-diffH(:,2))/(2^(5-double(BitSelect)));
                %Check for gradiant
                enable = ((max(0,Itmp2(:,i-1)-thresh) >= Itmp2(:,i))...
                      &(min(4095,Itmp2(:,i+1)+thresh) <= Itmp2(:,i))...
                        |((max(0,Itmp2(:,i+1)-thresh) >= Itmp2(:,i))...
                      &(min(4095,Itmp2(:,i-1)+thresh) <= Itmp2(:,i))));
                %Apply Look up Table
                weightsH = (enable.*LUT((floor(diffH2))+1)')*ones(1,3);
                weightsH(:,2) = 128;
                %Multiply Weights by Window. Divide by sum of Weights.
                I(i,:) = permute(floor(sum(weightsH.*Itmp2(:,i+[-1:1]),2)./sum(weightsH,2)),[2 1]);
            end
        otherwise
    end
%% Output
    %truncate and cap as a 12 bit integer
    Iout(:,:,j) = max(min(floor(I),2^12-1),0);
end