%% G2Bilateral   
% Performs Bilateral Filtering, 3x3 Window
%%
%  [Iout] = G2Bilateral(Iin,settings)
%   Inputs: 
%           Iin         - Input Image
%           settings
%               Bilateral_Enable        - Select Off/Horz/Vert/On
%               Bilateral_BitSelect     - Register 44 bits [27:26] 
%                                         0 = [11:4], 1 = [10:3], 2 = [9:2]
%
%   Outputs: 
%           Iout        - Output Image
%

%   Gen2 Firmware Model - Bilateral Filter
%   Author: Alexander Foster
%   Date:   10/12/15

function [Iout] = G2Bilateral(Iin,varargin)

%% Settings
reqsettings = {'Bilateral_Filter_Disable',   1;...
               'Bilateral_Bit_Select',      2};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

if(settings.Bilateral_Filter_Disable)
    Iout = Iin;
    return;
end

%% Initialize
Iin = double(Iin);

Iout = zeros(size(Iin),'double');

LUT = [128 127 125 121 116 110 102 95 86 78 69 61 53 45 38 32 26 22 17 14 11 8 6 5 4 3 2 1 1 1 0 0];
LUT = [LUT zeros(1,5000)];

for j = 1:3         %Three Color Channels
    
    I = zeros(size(Iin(:,:,j)));
    
    %Copy outer rows
    I(1:3,:) = Iin(1:3,:,j);
    I(end-2:end,:) = Iin(end-2:end,:,j);
    I(:,1:3) = Iin(:,1:3,j);
    I(:,end-2:end) = Iin(:,end-2:end,j);
    
    Itmp = zeros(size(I));
    
    %% Filter
    % Perform Bilateral Filtering
    
    for i = 4:size(Iin,2)-3
        %Window minus Center
        diffH = floor(abs(Iin(:,i+[-1:1],j) - Iin(:,i,j)*ones(1,3))/(2^(4-double(settings.Bilateral_Bit_Select))));
        %Apply Look up Table
        weightsH = LUT(diffH + 1);
        %Multiply Weights by Window. Divide by sum of Weights.
        Itmp(:,i) = floor(sum(weightsH.*Iin(:,i+[-1:1],j),2)./sum(weightsH,2));
    end
    
    for i = 4:size(Iin,1)-3
        %Window minus Center
        diffV = floor(abs(Itmp(i+[-1:1],:) - ones(3,1)*Itmp(i,:))/(2^(4-double(settings.Bilateral_Bit_Select))));
        %Apply Look up Table
        weightsV = LUT(diffV+1);
        %Multiply Weights by Window. Divide by sum of Weights.
        I(i,:) = floor(sum(Itmp(i+[-1:1],:).*weightsV)./sum(weightsV));
    end
    
    %% Output
    %truncate and cap as a 12 bit integer
    Iout(:,:,j) = max(min(floor(I),2^12-1),0);
end