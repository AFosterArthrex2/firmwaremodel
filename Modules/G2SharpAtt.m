%% G2SharpAtt     
% Sharpening Function for G2 Firmware Model
%%
%  [Iout] = G2SharpAtt(Iin,settings)
%   Inputs: 
%           Iin     - Input Image
%           settings
%               Sharpening_Enable
%               Sharpening_Gain
%               Sharpening_Attenuation
%               YSAT_K
%               
%   Outputs: 
%           Iout    - Output Image
%

%   Gen2 Firmware Model - YCbCr2RGB
%   Author: Alexander Foster
%   Date:   10/12/15

%Sharpening Function for G2 Firmware Model
%10 bit with Dark Attenuation
%Alex Foster
%11-04-13
function [Iout] = G2SharpAtt(Iin,varargin)
%Applies Sharpening Filter to input image Iin
%Input image is assumed to be in YCbCr format with 12 bits of precision per
%channel

%% Settings
reqsettings = {'Sharpening_Disable',            1;...
               'Sharpening_Gain',               1;...
               'Sharpening_Dark_Att_Disable',   1;...
               'YSAT_K',                        1};

if(~isempty(varargin))
    settings = varargin{1};
else
    settings = struct();
end

for i = 1:size(reqsettings,1)
    if(~isfield(settings,reqsettings{i,1}))
        settings.(reqsettings{i,1}) = reqsettings{i,2};
    end
end

I = double(Iin);

if(settings.Sharpening_Disable)
    Iout = Iin;
    Iout(:,:,1) = Iin(:,:,1)*4;
    return;
end


SGain = double(settings.Sharpening_Gain)/2^0;

%Sharpening Kernel
KS = [-1 -1 -1 -1 -1;...
     -1  2  2  2 -1;...
     -1  2  0  2 -1;...
     -1  2  2  2 -1;...
     -1 -1 -1 -1 -1];

%Truncate off last 2 bits (Iin has 12 bits of precision) 
It = floor(I/4);
 
% KernelP = conv2(It(:,:,1),KP,'valid');
KernelS = conv2(It(:,:,1),KS,'valid');

%Multiply by gain
EdgeS = (KernelS);

% EdgeP = padarray(EdgeP,[2 2],0);
EdgeS = padarray(EdgeS,[2 2],0);

%Calculate Attenuated Edge Information.
KernelA = padarray(floor(conv2(floor(It(:,:,1)/4),ones(3),'valid')),[1 1],0);
AttKernelA = ones(size(KernelA));
if(~settings.Sharpening_Dark_Att_Disable)
    AttKernelA(floor(KernelA/32) < 5)  = 0;
    AttKernelA(floor(KernelA/32) >= 5)  = 0.25;
    AttKernelA(floor(KernelA/32) >= 7)  = 0.5;
    AttKernelA(floor(KernelA/32) >= 9)  = 0.75;
    AttKernelA(floor(KernelA/32) >= 11) = 1;
end

Iout = zeros(size(Iin,1),size(Iin,2),3,'double');

AttGain = AttKernelA*SGain*2^5;

%Caculate YSAT filter
YSAT = round(1023*(floor(abs(I(:,:,2)/16) + abs(I(:,:,3)/16))/1023).^2);

%Add Edge Information 12bit format
Iout(:,:,1) = (I(:,:,1)*4+floor(EdgeS.*AttGain)-YSAT*settings.YSAT_K*64);

%Clamps
Iout(:,:,1) = min(Iout(:,:,1),2^16-1);
Iout(:,:,1) = max(Iout(:,:,1),0);

Iout(:,:,1) = (Iout(:,:,1));
Iout(:,:,2) = Iin(:,:,2);
Iout(:,:,3) = Iin(:,:,3);

Iout = (Iout);
end