function [outputs] = RegTest(Iin,S,R)

IMaster = double(Iin)/2^12;

% Calculates the estimated translations on a grid

% Size of sample box (in Pixels)
% S = 50;
% Distance between samples (in Pixels)
% R = 50;

Cy = 1:R:size(Iin,1)-S;
Cx = 1:R:size(Iin,2)-S;
Im = cell(size(Cy,2),size(Cx,2));
w = waitbar(0,'Alignment Test In Progress');
%Split into smaller sections
for i = 1:size(Cy,2)
    for j = 1:size(Cx,2)
        Im{i,j} = IMaster(Cy(i):Cy(i)+S-1,Cx(j):Cx(j)+S-1,:);
    end
end

%Main computation Loop
for i = 1:size(Cy,2)
    for j = 1:size(Cx,2)
        
        I = Im{i,j};
        q(i,j) = sum(sum(sum(rangefilt(I))));

        I1 = I(:,:,1);
        I2 = I(:,:,2);
        I3 = I(:,:,3);
        try
        %Red To Green
        [y12 x12] = imRegOpt(I1,I2);
        %Green to Blue
        [y23 x23] = imRegOpt(I2,I3);
        %Blue to Red
        [y31 x31] = imRegOpt(I3,I1);
        catch err
            msgbox('Error encountered in Alignment Test. Check focus and chart position')
            close(w);
            outputs = 0;
            return
        end
        %Main output variable
        %RtG: [ x , y ] estimated translation
        %GtB: [ x , y ] estimated translation
        %BtR: [ x , y ] estimated translation
        %RGB: [ x , y ] error: RtG + GtB + BtR = 0 under ideal conditons
        E(:,:,i,j) = [x12 y12; x23 y23; x31 y31; x12+x23+x31 y12+y23+y31];
                
        %Find the magnitude of the error vector
        RG(i,j) = sqrt((x12-0.5)^2+(y12-0.5)^2);
        GB(i,j) = sqrt((x23+0.5)^2+(y23+0.5)^2);
        BR(i,j) = sqrt(x31^2+y31^2);
        waitbar(((i-1)*size(Cx,2)+j)/(size(Cy,2)*size(Cx,2)),w);
    end
end
delete(w)
RG = medfilt2(RG,[5 5]);
GB = medfilt2(GB,[5 5]);
BR = medfilt2(BR,[5 5]);

F(1,1,:,:) = medfilt2(squeeze(E(1,1,:,:)),[5 5]);
F(1,2,:,:) = medfilt2(squeeze(E(1,2,:,:)),[5 5]);
F(2,1,:,:) = medfilt2(squeeze(E(2,1,:,:)),[5 5]);
F(2,2,:,:) = medfilt2(squeeze(E(2,2,:,:)),[5 5]);
F(3,1,:,:) = medfilt2(squeeze(E(3,1,:,:)),[5 5]);
F(3,2,:,:) = medfilt2(squeeze(E(3,2,:,:)),[5 5]);
outputs = {F; RG; GB; BR; E; q};

%Copy over edges
for s = 2:4
    for t = 1:1:3
        for u = 1:1:2
            outputs{s}(1,1) = outputs{s}(2,2);
            outputs{s}(1,2) = outputs{s}(2,2);
            outputs{s}(2,1) = outputs{s}(2,2);
            
            outputs{s}(1,end) = outputs{s}(2,end-1);
            outputs{s}(1,end-1) = outputs{s}(2,end-1);
            outputs{s}(2,end) = outputs{s}(2,end-1);
            
            outputs{s}(end,1) = outputs{s}(end-1,2);
            outputs{s}(end,2) = outputs{s}(end-1,2);
            outputs{s}(end-1,1) = outputs{s}(end-1,2);
            
            outputs{s}(end,end) = outputs{s}(end-1,end-1);
            outputs{s}(end,end-1) = outputs{s}(end-1,end-1);
            outputs{s}(end-1,end) = outputs{s}(end-1,end-1);
        end
    end
end

for t = 1:1:3
    for u = 1:1:2
        outputs{1}(t,u,1,1) = outputs{1}(t,u,2,2);
        outputs{1}(t,u,1,2) = outputs{1}(t,u,2,2);
        outputs{1}(t,u,2,1) = outputs{1}(t,u,2,2);
        
        outputs{1}(t,u,1,end) = outputs{1}(t,u,2,end-1);
        outputs{1}(t,u,1,end-1) = outputs{1}(t,u,2,end-1);
        outputs{1}(t,u,2,end) = outputs{1}(t,u,2,end-1);
        
        outputs{1}(t,u,end,1) = outputs{1}(t,u,end-1,2);
        outputs{1}(t,u,end,2) = outputs{1}(t,u,end-1,2);
        outputs{1}(t,u,end-1,1) = outputs{1}(t,u,end-1,2);

        outputs{1}(t,u,end,end) = outputs{1}(t,u,end-1,end-1);
        outputs{1}(t,u,end,end-1) = outputs{1}(t,u,end-1,end-1);
        outputs{1}(t,u,end-1,end) = outputs{1}(t,u,end-1,end-1);
    end
end
abc = 1;
% 
% % %Plot the Error Magnitudes
% figure;imagesc(RG)
% figure;imagesc(GB)
% figure;imagesc(BR)
% % 
% % %Plot the Displacement Vectors
% figure;quiver(Cx,Cy,squeeze(E(1,1,:,:)),squeeze(E(1,2,:,:)),'r')
% figure;quiver(Cx,Cy,squeeze(-E(2,1,:,:)),squeeze(-E(2,2,:,:)),'b')
% % 
% figure;quiver(Cx,Cy,squeeze(E(3,1,:,:)),squeeze(E(3,2,:,:)),'g')
% % 
% [Mgridx Mgridy] = meshgrid(Cx,Cy);
% % 
% figure;scatter(reshape(Mgridx+squeeze(E(1,1,:,:))+0.5,1,[]),reshape(Mgridy+squeeze(E(1,2,:,:))+0.5,1,[]))
% % hold on
% scatter(reshape(Mgridx,1,[]),reshape(Mgridy,1,[]))