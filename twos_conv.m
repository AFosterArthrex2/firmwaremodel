function out = twos_conv(in,bits)

if(isnumeric(in))
    places = ceil(bits/4);
    if(in >= 0)
        out = dec2hex(in,places);
    else
        out = dec2hex(2^bits+in,places);
    end
else
    data_out = hex2dec(in);
    
    if data_out >= 2^(bits - 1)
        data_out = data_out - 2^bits ;
    end
    
    out = data_out;
end