start = Isave{4,4}{1};

settings.Green_Edge_Gain = 0;

stop = G2GreenEdgeEnhance(start,settings);

before_red = fftshift(fft2(start(:,:,1)));
before_green = fftshift(fft2(start(:,:,2)));
before_blue = fftshift(fft2(start(:,:,3)));

after_red = fftshift(fft2(stop(:,:,1)));
after_green = fftshift(fft2(stop(:,:,2)));
after_blue = fftshift(fft2(stop(:,:,3)));

figure;imagesc(abs(after_red)-abs(before_red))